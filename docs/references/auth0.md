---
description: User guide: Opstrace with a custom Auth0 integration
---

# User guide: Opstrace with a custom Auth0 integration

This user guide explains what to do on the Auth0 configuration side of things before you can fill the two Opstrace instance config parameters `custom_auth0_client_id` and `custom_auth0_domain` with meaningful values.

## Sign up to Auth0

<!-- markdownlint-disable MD044 -->
If you have not yet done so, sign up for a new user account at [auth0.com/signup](https://auth0.com/signup).
A free-tier user account will do just fine.

Here is a screenshot of the choices I made during account creation, for this user guide:

<!-- markdownlint-disable MD033 -->
<img src="https://user-images.githubusercontent.com/265630/140336168-9af367cd-d868-4e43-8dc0-5dc8e082cfc6.png" width="400" />

## Create new tenant with expressive name

For writing this guide I chose to create a new Auth0 tenant with the name `jp-opstrace-demo`.
The resulting Auth0 tenant domain is `jp-opstrace-demo.eu.auth0.com`.
Take note of the domain -- that is the value to use for the `custom_auth0_domain` parameter when creating your Opstrace instance.

Screenshot:

<!-- markdownlint-disable MD033 -->
<img src="https://user-images.githubusercontent.com/265630/140336203-6ec1d02b-bd60-4992-af2b-a8ea6b451e94.png" width="500" />

## Create Auth0 application

Create a new Auth0 application in the newly created Auth0 tenant. The type of the application must be `Single Page Web Application`.

Choose an expressive name. I chose `jp-opstrace-demo-web-ui`. This application will be specific to your Opstrace instance.

<!-- markdownlint-disable MD033 -->
<img src="https://user-images.githubusercontent.com/265630/140336273-8d29fa7e-b18e-4498-a265-edf3a9d7e83a.png" width="400" />


## Configure Auth0 application

Go to the `Settings` tab of the newly created application.

### Add a description text

I recommend to fill the description field with meaningful information so that later on when you visit the Auth0 dashboard again you can easily remember what you configured this Auth0 application for.

Screenshot:

<!-- markdownlint-disable MD033 -->
<img src="https://user-images.githubusercontent.com/265630/140337041-5d10133a-2892-4f14-993c-3db427f812b1.png" width="500" />

### Take note of the client ID

Take note of the client ID associated with this Auth0 application. You will need it for creating the Opstrace instance. You can find the client ID at the top of the `Settings` tab in the `Basic Information` section (see screenshot above). In case of this demo walk-through the client ID is `ymRXFt75TfPDko2JBZRaGanDA8Zm2Anz`. Note that this is non-sensitive data.

### Set allowed callback URL

For completing this step you have to know the DNS name under which your Opstrace instance will be reachable.
Here, it is `jp-opstrace-demo.opstrace.io`.
In your case it is a different one, represented by `<dns-name>` below.

Fill the following four form fields with URLs specific to the DNS name of your Opstrace instance:

* Allowed Callback URLs: `https://<dns-name>/login`
* Allowed Logout URLs: `https://<dns-name>/login` (that is not an oversight)
* Allowed Web Origins: `https://<dns-name>`
* Allowed Origins (CORS): `https://<dns-name>`

Note that if you are using the Opstrace `custom_dns_name` parameter as documented [here](https://opstrace.com/docs/references/configuration#custom_dns_name) then `<dns-name>` above needs to of course correspond to that DNS name of choice.

### Configure token properties

* Set `ID Token Expiration` to `300` (it's only needed for the rare login event).
* Disable `Refresh Token Rotation`.

### Configure Grant Types

At the bottom of the `Settings` tab, toggle the `Advanced Settings` section.
Switch to the `Grant Types` tab in there.
Enable only these two grants:

* `Implicit`
* `Authorization Code`

That is, disable `Refresh Token`.

Screenshot:

<!-- markdownlint-disable MD033 -->
<img src="https://user-images.githubusercontent.com/265630/140338473-722d3181-b009-45e6-bd86-f7fad171cb87.png" width="500" />

### Save Changes

Press the `Save Changes` button.

### Configure Connections

In the `Connections` tab, you can configure the different methods via which users are supposed to be able to log in to your Opstrace instance. In my case, I only enabled the single sign-on via Google:

<!-- markdownlint-disable MD033 -->
<img src="https://user-images.githubusercontent.com/265630/140775523-54d6fba2-abde-4851-8de9-5eac2bf200d6.png" width="500" />

Note that in the left sidebar under `Authentication -> Social` you can activate more identity providers, such as GitHub. Showing the relevant steps is out of scope of this user guide.

## Configure Auth0 API

Under `Applications -> APIs`, create a new API with name and identifier set to `https://user-cluster.opstrace.io/api`.

Screenshot:

<!-- markdownlint-disable MD033 -->
<img src="https://user-images.githubusercontent.com/265630/140776680-57b3f38b-2d4b-4e7c-9d22-d8a3ff1860ca.png" width="400" />

This is required for making this integration work.
Note that `https://user-cluster.opstrace.io/api` does not represent an actual API endpoint. The API entry you create should have exactly this value and should not be customized, regardless of your DNS configuration.

## Opstrace instance setup

When creating your Opstrace instance, set the two parameters `custom_auth0_client_id` and `custom_auth0_domain` accordingly. For example, I used:

```yaml
custom_auth0_client_id: ymRXFt75Tf...
custom_auth0_domain: jp-opstrace-demo.eu.auth0.com
```

## Log in to the web UI of your Opstrace instance

Access `https://<dns-name>` in your browser. It displays your custom Auth0 login screen. In this example it looked like this:

<!-- markdownlint-disable MD033 -->
<img src="https://user-images.githubusercontent.com/265630/140773385-c58fff3c-5b96-4ff0-9c1b-fa3a72efce34.png" width="500" />

