# Generating and Sending Traces using OpenTelemetry

This [docker compose](../../../test/tracing/docker-compose.yaml) can be used to generate traces and send them to an Opstrace namespace.

## Prerequisites

You must have an Opstrace instance running and have a namespace provisioned for Observability. See the [QuickStart](../../quickstart.md)
for more details.

This document assumes that Opstrace is running locally on your machine. If you're using a remote instance,
replace `http://localhost` with the URL to your Opstrace instance.


Navigate to your namespace in Opstrace and create an API Key.

![api_key](../../assets/create-token.png)

Copy the created Key:

![key](../../assets/key.png)

## Getting started

First install a small tool to help us workaround an issue of the OTLP exporter trying to send data to the
loopback interface.

```bash
go install github.com/axetroy/forward-cli/cmd/forward@latest
```

Start the reverse proxy with any port of your choosing.

```bash
forward --port=4000 http://localhost:80
```

You should get output like the following:

```bash
forward --port=4000 http://localhost:80
2022/05/19 12:13:06 Proxy 'http://192.168.1.47:4000' to 'http://localhost:80'
```

Copy the address of the forwarder ([http://192.168.1.47:4000](http://192.168.1.47:4000) in the example above).


Take the key and the namespace which the key was created in and let's start the docker services that will
generate and send traces to this namespace:

```bash
OPSTRACE_ADDRESS=http://192.168.1.47:4000 \
NAMESPACE=13490748 \
NAMESPACE_TOKEN=eyJrIjoiTTdqekNOV0JIOHpvMjRsRFlXb1MxcVVaNHNCZFVucmMiLCJuIjoiZGVtbyIsImlkIjoxMzQ5MDc0OH0= \
docker-compose up
```

Open [http://localhost:8080](http://localhost:8080) and click some buttons. This will create traces and they will be sent
to your Opstrace namespace.

Navigate to the datasources tab in the UI for your namespace and create a new Jaeger datasource (this is yet to be automated).

![datasources-tab](../../assets/datasources-tab.png)

Click Add datasource and then select Jaeger.

In the URL field of the datasource configuration, add `jaeger-{namespace}.{namespace}.svc.cluster.local:16686`
where namespace is the value of the NAMESPACE environment variable set above (the namespace ID for which this maps
to in GitLab). Click Save and Test.

![jaeger-datasource](../../assets/jaeger-datasource.png)

Navigate to the explore view in your namespace and select Jaeger as the datasource. You should see
traces which you can select and view.

![traces](../../assets/traces.png)

