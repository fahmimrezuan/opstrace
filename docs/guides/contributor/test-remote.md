# The `test-remote` suite of tests

Our so-called `test-remote` suite is located in `tests/test-remote`.
It is the main test suite for checking the validity of an Opstrace instance's health and functionality.
We run this test suite regularly as part of CI for each code commit.

Architecturally, this test suite is designed to be executed locally on your machine for testing a remote Opstrace instance through the Internet.

Most of the individual tests in this suite inspect instance properties through the data API endpoints, i.e., from the user's (and their tooling's) point of view.
Other tests in this suite may inspect implementation details through private interfaces.

**WARNING: that this test suite is invasive. That is, never run it against a production instance.**


## How to run `test-remote` against a remote instance

The recommended way to run this test suite is via `make test-remote` executed at the root directory of the Opstrace repository.

Before invoking `make test-remote`, the environment in the current shell needs to be configured, as documented below.

First, set the name and cloud provider of the to-be-tested instance:

```bash
export OPSTRACE_CLUSTER_NAME=<opstrace-instance-name>
export OPSTRACE_CLOUD_PROVIDER=<aws or gcp>
```

Next up, make your local `kubectl` configuration point to the running instance:

```bash
make kconfig
```

Now invoke the test runner with

```text
make test-remote
```

Follow the log output.

Further notes for development and debugging:

* Mocha test \(suite\) teardown is, as of now, not always reliably executed.
A SIGINT-aborted test runner might leave resources behind, such as running child processes.
* Containerized applications and all kubectl processes leave behind their stdout/err data in the host's /tmp directory.
If things go wrong, look in the /tmp directory.
* About test isolation: cleanup does not need to be perfect yet, but please write tests so that they can be run arbitrarily often against the same cluster and still succeed, without relying on a previous test run's side effects to be present. That is, whenever a test inserts data, make it so so that the corresponding metrics or logs injected have unique/random properties (in a log stream label name, for instance).
