# Managing Tenants Guide

This guide will show you how to:

* Add a new tenant to a running instance
* Delete a tenant

## Introduction

Opstrace supports multiple, secured tenants to logically isolate concerns on otherwise shared infrastructure.
If you are not familiar with what a "tenant" is, see our [tenant key concepts](../../references/concepts.md#tenants) documentation.

Tenants are presented in the Admin section of our UI:

![tenant overview page](../../assets/tenants-guide-overview-1.png)

Let's walk through an example of adding a new tenant named `newtenant` to a running Opstrace instance named `showdown`.

<!-- TODO link to the integrations guide when it exists
If you’re coming from the [quick start](../../quickstart.md), and haven’t yet sent data to one of your tenants, stay tuned for our forthcoming integrations guide to make that process easy.
-->

## Add a New Tenant with the UI

Visit `https://showdown.opstrace.io/cluster/tenants` and press the `Add Tenant` button.
Type the name of the new tenant (here: `newtenant`).

![add tenant gif](../../assets/tenants-guide-add.gif)

**Warning**: we do not yet do strict tenant name validation.
To make sure things work, please keep the name lower case `[a-z]` for now.

The Opstrace controller running in the Opstrace instance will start a number of new components and initiate a DNS reconfiguration.

Effectively, we're now waiting for the DNS name `cortex.newtenant.showdown.opstrace.io` to become available.

We can probe that from our point of view with `curl`:

```bash
curl https://cortex.newtenant.showdown.opstrace.io/api/v1/labels
```

It should take about 5 minutes for DNS name resolution errors to disappear.
Next up, expect an HTTP response with status code `401`, showing the error message
`Authorization header missing` in the response body.


## Tenant API Token with the UI

Because Opstrace is secure by default we will first generate an API Keyn for our new tenant that will be used to authenticate clients that wish to send data to Opstrace.

To generate a new token, navigate to the tenant Grafana UI at `newtenant.showdown.opstrace.io/grafana/org/apikeys`. Then click `Add API key`, enter a descriptive name, set the role to `Editor`, and enter a duration until the token should expire (e.g. `365d`). After clicking `Add` you will be shown the token only once. Copy the token somewhere for testing in the next step.

Note that you can come back to this configuration page at any time to create new tokens. Therefore you can make them as granular as you like, making it easy to revoke individual tokens that have been lost or that are otherwise no longer in use.

## Test Tenant API Authentication

Let's add the token (`TOKEN_HERE`) to an example HTTP call against the Cortex API for the new tenant:

```bash
$ curl -vH "Authorization: Bearer TOKEN_HERE" \
    https://cortex.newtenant.showdown.opstrace.io/api/v1/labels
...
< HTTP/2 200
...
{"status":"success","data":[]}
```

Getting a `200` response (and not a `401` response) means: the authentication token provided in the request was accepted.
You may receive a `401` response if the token was missing, invalid, expired, or had the wrong permission level.
The so-called _authenticator_ in the Opstrace instance extracted the public key ID from the token's header section, found a corresponding public key in its trust store (think: "set of public keys that I am configured to trust"), and then performed a cryptographic verification using that public key.
It also confirmed that the tenant name encoded in the token matches the tenant associated with the API endpoint.

You could now go ahead and take this authentication token and configure serious API clients with it, such as a Prometheus instance to be able to `remote_write` to `https://cortex.newtenant.showdown.opstrace.io/api/v1/push`.

## Send Data to a new Tenant with an Integration

Opstrace provides integrations that simplify collection of data from various data sources.
This includes automating the use of the API tokens in the assocated configuration.
For example, collecting metrics from a Kubernetes cluster.

## Delete a Tenant

Deleting a tenant is simple, although we do not currently purge the data automatically.  We don't deploy the purgers for Cortex yet, so we, therefore, don't purge the data that exists in Cortex for a tenant.
(In other words, while we delete the tenants, there are still remnants of the data in storage, which means a new tenant could be created with the same name, and it would have access to the existing data left behind.)
We will ship this in a future release.

![deleting a tenant gif](../../assets/tenants-guide-delete.gif)
