import { call, race, delay } from "redux-saga/effects";
import createSagaMiddleware from "redux-saga";
import { createStore, applyMiddleware } from "redux";

import { smErrorLastResort } from "./util";
import * as ucc from "./ucc";
import * as cli from "./index";
import { SECOND } from "@opstrace/utils";
import {
  setClusterConfig,
  LatestClusterConfigType,
  LatestAWSInfraConfigType,
  LatestGCPInfraConfigType
} from "@opstrace/config";
import {
  ClusterCreateConfigInterface,
  setCreateConfig,
  waitUntilHTTPEndpointsAreReachable
} from "@opstrace/installer";
import * as schemas from "./schemas";

/**
 * Simple function that checks if Cortex is up and running in all tenants.
 */
async function checkClusterStatus() {
  // Note(JP): this following codeblock is here copied from the `create` module
  // -- with the sole purpose to get the list of tenants from the cluster
  // config file. I think we should not require the cluster config file as an
  // input parameter for this, but maybe a list of tenants instead, if desired:
  //
  //   opstrace status aws jpdev --tenant system --tenant default..
  //
  // Definitely technical debt leaking as an interface, needs to be
  // consolidated.
  // Update(JP, June 2021): now this also needs 'custom DNS' info from the
  // cluster config file
  const [userClusterConfig, infraConfigAWS, infraConfigGCP]: [
    schemas.LatestClusterConfigFileSchemaType,
    LatestAWSInfraConfigType | undefined,
    LatestGCPInfraConfigType | undefined
  ] = await ucc.uccGetAndValidate(
    cli.CLIARGS.clusterConfigFilePath,
    cli.CLIARGS.cloudProvider
  );

  // this is here just to compile, right?
  const ccfg: LatestClusterConfigType = {
    ...userClusterConfig,
    ...{
      aws: infraConfigAWS,
      gcp: infraConfigGCP,
      cloud_provider: cli.CLIARGS.cloudProvider,
      cluster_name: cli.CLIARGS.instanceName
    }
  };
  setClusterConfig(ccfg);

  const createConfig: ClusterCreateConfigInterface = {
    holdController: cli.CLIARGS.holdController,
    kubeconfigFilePath: ""
  };
  setCreateConfig(createConfig);

  await waitUntilHTTPEndpointsAreReachable(ccfg);
}

// Race to check cluster status and fail if it takes longer than 60 seconds.
function* rootTaskStatusCore() {
  const { timeout } = yield race({
    status: call(checkClusterStatus),
    timeout: delay(300 * SECOND)
  });

  if (timeout) {
    throw new Error("timeout checking the status of the cluster");
  }
}

// Wrapper to make redux-saga happy.
function* rootTaskStatus() {
  yield call(rootTaskStatusCore);
}

// Empty reducer to make redux-saga happy.
const reducer = (state = 0) => {
  return state;
};

export async function status(): Promise<void> {
  const sm = createSagaMiddleware({ onError: smErrorLastResort });
  createStore(reducer, applyMiddleware(sm));
  await sm.run(rootTaskStatus).toPromise();
}
