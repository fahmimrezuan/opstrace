import { ClusterConfigFileSchemaV2 } from "./schemasv2";

describe("ClusterConfigFileSchemaV2", () => {
  const generateValidClusterConfigFile = (overwrite = {}) => ({
    node_count: 3,
    controller_image: "some-controller-image",
    tenants: ["validtenant"],
    env_label: "some-env-label",
    cert_issuer: "letsencrypt-staging",
    metric_retention_days: 7,
    trace_retention_days: 5,
    data_api_authorized_ip_ranges: ["0.0.0.0/0"],
    custom_dns_name: "foo",
    custom_auth0_domain: "bar",
    custom_auth0_client_id: "foobar",
    gitlab: {
      instance_url: "https://gitlab.com",
      group_allowed_access: "*",
      group_allowed_system_access: "6543",
      oauth_client_id: "foo",
      oauth_client_secret: "bar"
    },
    aws: {},
    gcp: {},
    ...overwrite
  });
  it("should accept valid schema", async () => {
    const configFile = generateValidClusterConfigFile();
    expect(await ClusterConfigFileSchemaV2.isValid(configFile)).toBe(true);
  });

  describe("tenants", () => {
    it("length may not be <1", async () => {
      const configFile = generateValidClusterConfigFile({ tenants: [] });
      expect(await ClusterConfigFileSchemaV2.isValid(configFile)).toBe(false);
    });

    it("name may not contain capital letters", async () => {
      const configFile = generateValidClusterConfigFile({
        tenants: ["Capital"]
      });
      expect(await ClusterConfigFileSchemaV2.isValid(configFile)).toBe(false);
    });

    it("name may include numbers", async () => {
      const configFile = generateValidClusterConfigFile({
        tenants: ["luckynumber7"]
      });
      expect(await ClusterConfigFileSchemaV2.isValid(configFile)).toBe(true);
    });

    it("name may start with number", async () => {
      const configFile = generateValidClusterConfigFile({ tenants: ["7yeah"] });
      expect(await ClusterConfigFileSchemaV2.isValid(configFile)).toBe(true);
    });

    it("name may contain only numbers", async () => {
      const configFile = generateValidClusterConfigFile({
        tenants: ["123456"]
      });
      expect(await ClusterConfigFileSchemaV2.isValid(configFile)).toBe(true);
    });

    it("name may be very short", async () => {
      const configFile = generateValidClusterConfigFile({ tenants: ["a"] });
      expect(await ClusterConfigFileSchemaV2.isValid(configFile)).toBe(true);
    });

    it("name may not contain special characters", async () => {
      const configFile = generateValidClusterConfigFile({
        tenants: ["abc^def"]
      });
      expect(await ClusterConfigFileSchemaV2.isValid(configFile)).toBe(false);
    });

    it("name may not contain hyphen", async () => {
      const configFile = generateValidClusterConfigFile({
        tenants: ["my-tenant"]
      });
      expect(await ClusterConfigFileSchemaV2.isValid(configFile)).toBe(false);
    });
  });
});
