import { log, die, BUILD_INFO } from "@opstrace/utils";
import { setAWSRegion } from "@opstrace/aws";
import { GCPAuthOptions } from "@opstrace/gcp";

import {
  LatestClusterConfigType,
  LatestAWSInfraConfigType,
  LatestGCPInfraConfigType,
  setClusterConfig,
  REGION_EKS_AMI_MAPPING
} from "@opstrace/config";
import {
  createCluster,
  setGcpProjectID,
  setCreateConfig,
  ClusterCreateConfigInterface
} from "@opstrace/installer";

import * as cli from "./index";
import * as ucc from "./ucc";
import * as util from "./util";
import * as schemas from "./schemas";

import {
  OPSTRACE_DOCKERHUB_TOKEN,
  OPSTRACE_DOCKERHUB_USERNAME,
  logDockerHubCredentialsMessage
} from "@opstrace/controller-config";

export async function create(): Promise<void> {
  const [userClusterConfig, infraConfigAWS, infraConfigGCP]: [
    schemas.LatestClusterConfigFileSchemaType,
    LatestAWSInfraConfigType | undefined,
    LatestGCPInfraConfigType | undefined
  ] = await ucc.uccGetAndValidate(
    cli.CLIARGS.clusterConfigFilePath,
    cli.CLIARGS.cloudProvider
  );
  // If the cluster is sufficiently large, the user can hit dockerhub rate limits
  // for image pulling https://www.docker.com/increase-rate-limits
  // To be safe, we'll exit out of cluster creation early if they don't have
  // env vars set containing dockerhub credentials.
  // Assert dockerhub username and token are set as environment variables.
  // These env vars (if they exist) will be used during the deployControllerResources phase of install/upgrade
  const username = process.env[OPSTRACE_DOCKERHUB_USERNAME];
  const token = process.env[OPSTRACE_DOCKERHUB_TOKEN];

  if (userClusterConfig.node_count > 5) {
    if (!(username && token)) {
      die(
        "OPSTRACE_DOCKERHUB_USERNAME, OPSTRACE_DOCKERHUB_TOKEN environment variables must be set to avoid image pull rate-limits from dockerhub (https://www.docker.com/increase-rate-limits)." +
          "OPSTRACE_DOCKERHUB_USERNAME must be your dockerhub username, and OPSTRACE_DOCKERHUB_TOKEN must be a valid dockerhub access token." +
          "You can create an access token here: https://hub.docker.com/settings/security"
      );
    }
  }

  if (cli.CLIARGS.cloudProvider == "gcp") {
    const gcpopts: GCPAuthOptions =
      util.gcpValidateCredFileAndGetDetailOrError();
    log.info("GCP project ID: %s", gcpopts.projectId);
    log.info(
      "GCP service account email notation: %s",
      gcpopts.credentials.client_email
    );
    setGcpProjectID(gcpopts.projectId);
  }

  // renderedClusterConfig: internal, complete
  const renderedClusterConfig: LatestClusterConfigType = {
    ...userClusterConfig,
    ...{
      aws: infraConfigAWS,
      gcp: infraConfigGCP,
      cloud_provider: cli.CLIARGS.cloudProvider,
      cluster_name: cli.CLIARGS.instanceName
    }
  };

  log.info(
    "rendered cluster config:\n%s",
    JSON.stringify(renderedClusterConfig, null, 2)
  );

  // Sanity check. It's a bug (not user error) when this fails. Make
  // corresponding checks beforehand.
  schemas.LatestRenderedClusterConfigSchema.validateSync(
    renderedClusterConfig,
    {
      strict: true
    }
  );

  // make config for current context globally known in installer
  // dirty cfg-as-singleton-immutable approach
  setClusterConfig(renderedClusterConfig);

  const createConfig: ClusterCreateConfigInterface = {
    holdController: cli.CLIARGS.holdController,
    kubeconfigFilePath: cli.CLIARGS.kubeconfigFilePath
  };
  setCreateConfig(createConfig);

  // Note(JP): `setAWSRegion()` must be called precisely once per `... create
  // ...` invocation. Doing that here is a pragmatic way to achieve that.
  if (infraConfigAWS !== undefined) {
    // Set region for AWS client library abstractions.
    if (!(infraConfigAWS.region in REGION_EKS_AMI_MAPPING)) {
      die(
        `The AWS region you have configured is not (yet) supported: ${infraConfigAWS.region}`
      );
    }
    setAWSRegion(infraConfigAWS.region);
  }
  // Log the DockerHub credentials just before prompt so user can verify (if they're present)
  logDockerHubCredentialsMessage("create");

  await promptForResourceCreation(renderedClusterConfig);

  await createCluster(util.smErrorLastResort);
}

async function promptForResourceCreation(ccfg: LatestClusterConfigType) {
  if (ccfg.cloud_provider === "aws") {
    const url = `https://go.opstrace.com/cli-aws-mutating-api-calls/${BUILD_INFO.VERSION_STRING}`;
    log.info(
      "Before we continue, please review the set of state-mutating " +
        "AWS API calls emitted by this CLI during cluster creation: %s",
      url
    );
    await util.promptForProceed();
  }
}
