import { eventChannel } from "redux-saga";
import { put, take, cancelled } from "redux-saga/effects";
import * as k8s from "@opstrace/kubernetes";
import { KubeConfig } from "@kubernetes/client-node";
import { log, debugLogErrorDetail } from "@opstrace/utils";

//eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export function* runInformers(kubeConfig: KubeConfig) {
  log.info("starting informers");

  const clusterChannel = eventChannel(channel => {
    const unsubscribes = [
      k8s.ConfigMap.startInformer(kubeConfig, channel),
      k8s.DaemonSet.startInformer(kubeConfig, channel),
      k8s.Deployment.startInformer(kubeConfig, channel),
      k8s.Secret.startInformer(kubeConfig, channel),
      k8s.StatefulSet.startInformer(kubeConfig, channel),
      k8s.V1CertificateResource.startInformer(kubeConfig, channel)
    ];

    // return the unsubscribe function for eventChannel. This will be called
    // when the channel is closed.
    return () => {
      log.info("shutting down k8s informers");
      unsubscribes.forEach(fn => fn());
    };
  });

  while (true) {
    let event;
    try {
      //@ts-ignore: TS7075 generator lacks return type (TS 4.3)
      event = yield take(clusterChannel);
    } catch (err: any) {
      log.warning("error during `event = yield take(clusterChannel)`: %s", err);
      debugLogErrorDetail(err);
    }
    if (event !== undefined) {
      try {
        yield put(event);
      } catch (err: any) {
        log.warning("error during `yield put(event)`: %s", err);
        debugLogErrorDetail(err);
      }
    }
    //@ts-ignore: TS7075 generator lacks return type (TS 4.3)
    if (yield cancelled()) {
      log.info(
        "informer loop got signal: cancelled -- close cluster channels, end loop"
      );
      clusterChannel.close();
      break;
    }
  }
}
