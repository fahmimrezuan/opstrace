import { all, call, delay } from "redux-saga/effects";
import { protos as gkeProtos } from "@google-cloud/container";

import {
  ensureBucketExists,
  ensureNetworkExists,
  ensureSubNetworkExists,
  ensureGatewayExists,
  ensureGKEExists,
  generateKubeconfigStringForGkeCluster,
  ensureCloudSQLExists,
  sql_v1beta4,
  ensureServiceAccountExists,
  setCertManagerServiceAccount,
  setExternalDNSServiceAccount,
  setCortexServiceAccount
} from "@opstrace/gcp";
import { getGKEClusterConfig, getCloudSQLConfig } from "@opstrace/config";
import { generateSecretValue } from "@opstrace/controller-config";
import { GCPAuthOptions } from "@opstrace/gcp";
import { die, getBucketName, log, SECOND } from "@opstrace/utils";
import {
  getClusterConfig,
  //RenderedClusterConfigSchemaType
  LatestClusterConfigType
} from "@opstrace/config";
import { gcpProjectID } from "./index";
import { EnsureInfraExistsResponse } from "./types";

export function* ensureGCPInfraExists(
  gcpAuthOptions: GCPAuthOptions
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
): Generator<any, EnsureInfraExistsResponse, any> {
  const ccfg: LatestClusterConfigType = getClusterConfig();

  if (ccfg.gcp === undefined) {
    throw Error("`gcp` property expected");
  }

  if (!ccfg.custom_dns_name) {
    die("custom_dns_name is not set");
  }

  if (!ccfg.custom_auth0_client_id || !ccfg.custom_auth0_domain) {
    die("custom_dns_name and custom_auth0_domain are not set");
  }

  if (!ccfg.gitlab.oauth_client_id || !ccfg.gitlab.oauth_client_secret) {
    die("gitlab.oauth_client_id and gitlab.oauth_client_secret are not set");
  }

  if (!ccfg.gitlab.instance_url) {
    die("gitlab.instance_url is not set");
  }

  if (!ccfg.gitlab.group_allowed_access) {
    die("gitlab.group_allowed_access is not set");
  }

  if (!ccfg.gitlab.group_allowed_system_access) {
    die("gitlab.group_allowed_system_access is not set");
  }

  const gkeConf = getGKEClusterConfig(ccfg, gcpAuthOptions);

  yield call(ensureBucketExists, {
    bucketName: getBucketName({
      clusterName: ccfg.cluster_name,
      suffix: "cortex"
    }),
    retentionDays: ccfg.metric_retention_days,
    region: ccfg.gcp.region
  });

  yield call(ensureBucketExists, {
    bucketName: getBucketName({
      clusterName: ccfg.cluster_name,
      suffix: "cortex-config"
    }),
    retentionDays: null, // no TTL this bucket: configs should not expire
    region: ccfg.gcp.region
  });

  log.info(`Ensuring VPC exists`);
  yield call(ensureNetworkExists, ccfg.cluster_name);

  // Delay to work around VPC not being ready. TODO - investigate
  // how to detect if VPC is ready.
  yield delay(20 * SECOND);

  log.info(`Ensuring Subnet exists`);
  yield call(ensureSubNetworkExists, {
    opstraceClusterName: ccfg.cluster_name,
    gcpRegion: ccfg.gcp.region,
    gcpProjectID: gcpProjectID,
    ipCidrRange: "192.168.0.0/19"
  });

  log.info(`Ensuring CloudNat and Router exists`);
  yield call(ensureGatewayExists, {
    name: ccfg.cluster_name,
    region: ccfg.gcp.region,
    gcpProjectID
  });

  // We randomly generate a password for use when communicating with CloudSQL.
  // The instance has subnet rules to only allow access from the cluster, but this provides additional
  // protection against compromised pods in the cluster being able to access Postgres without access to
  // the Secret.
  // However, this comes at the cost of potentially losing this credential in the process of installing
  // Opstrace, or later on accidentally deleting it. If this occurs, the user should manually go to the
  // DB configuration in the cloud console and configure credentials, then update the cluster to use them.
  const postgresPassword = generateSecretValue();
  const postgresDB = "opstrace";

  const cloudSQLConfig = getCloudSQLConfig(
    ccfg,
    gcpAuthOptions,
    postgresPassword
  );

  // Note(jp): the `gkeConf` object contains a property called `name` which
  // is the GKE cluster name. In addition we provide a `name` property
  // directly, which I have now renamed to `GKEClusterName` because I think
  // it's semantically the same. When that is confirmed I think we should
  // not pass this piece of information in twice, but only once.
  log.info(`Ensuring GKE exists`);
  const [gkecluster, cloudSQLInstance]: [
    gkeProtos.google.container.v1.ICluster,
    sql_v1beta4.Schema$DatabaseInstance
  ] = yield all([
    call(ensureGKEExists, {
      GKEClusterName: ccfg.cluster_name,
      region: ccfg.gcp.region,
      gcpProjectID,
      zone: ccfg.gcp.zone_suffix,
      cluster: gkeConf
    }),
    call(ensureCloudSQLExists, {
      opstraceClusterName: ccfg.cluster_name,
      instance: cloudSQLConfig,
      // The network here is our VPC that we launch the cluster into
      network: `projects/${gcpProjectID}/global/networks/${ccfg.cluster_name}`,
      addressName: `google-managed-services-${ccfg.cluster_name}`,
      region: ccfg.gcp.region,
      // This range must not collide with the subnet range, which is also attached to the same VPC.
      // Select a range that still provides room for making the subnet above larger if we need to.
      ipCidrRange: "192.168.64.0" // this technically isn't a range because we don't have the /number, but we don't need it
      // because it's added as a separate parameter when calling the cloud api (for some reason google requires it to be split..)
    })
  ]);

  const privateAddress = cloudSQLInstance.ipAddresses?.find(
    address => address.type === "PRIVATE"
  );
  if (!privateAddress || !privateAddress.ipAddress) {
    throw Error("did not return a privateIp address for CloudSQL instance");
  }

  const gkeKubeconfigString: string = generateKubeconfigStringForGkeCluster(
    gcpProjectID,
    gkecluster
  );

  // Create a Google service account to be used by cert-manager.
  log.info(`Ensuring cert-manager service account exists`);
  const certManagerSA = yield call(ensureServiceAccountExists, {
    // service account name ("account ID") in GCP must be at least 6 and at
    // most 30 characters. `ccfg.cluster_name` is at most 23 characters at the
    // time of writing. 23 plus 7 (len('-crtmgr')) is 30. Good.
    // Note: this is a name-based convention, the uninstaller relies on this
    // convention.
    name: `${ccfg.cluster_name}-crtmgr`,
    projectId: gcpProjectID,
    role: "roles/dns.admin",
    kubernetesServiceAccount: "ingress/cert-manager"
  });

  setCertManagerServiceAccount(certManagerSA);

  // Create a Google service account to be used by external-dns.
  log.info(`Ensuring external-dns service account exists`);
  const externalDNSSA = yield call(ensureServiceAccountExists, {
    // max length for svc acc name: 30, cluster name is max 23.
    name: `${ccfg.cluster_name}-extdns`,
    projectId: gcpProjectID,
    role: "roles/dns.admin",
    kubernetesServiceAccount: "ingress/external-dns"
  });
  setExternalDNSServiceAccount(externalDNSSA);

  // Create a Google service account to be used by cortex.
  log.info(`Ensuring cortex service account exists`);
  const cortexSA = yield call(ensureServiceAccountExists, {
    name: `${ccfg.cluster_name}-cortex`,
    projectId: gcpProjectID,
    role: "roles/storage.admin",
    kubernetesServiceAccount: "cortex/cortex"
  });
  setCortexServiceAccount(cortexSA);

  return {
    kubeconfigString: gkeKubeconfigString,
    // The default user created when standing up a CloudSQL instance is "postgres".
    postgreSQLEndpoint: `postgres://postgres:${postgresPassword}@${privateAddress.ipAddress}:5432/`,
    opstraceDBName: postgresDB
  };
}
