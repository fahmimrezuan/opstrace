export class ClusterDestroyTimeoutError extends Error {
  constructor(...args: string[] | undefined[]) {
    super(...args);
    Error.captureStackTrace(this, ClusterDestroyTimeoutError);
  }
}
