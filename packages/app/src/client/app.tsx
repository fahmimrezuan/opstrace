import React from "react";

import { makeStyles } from "@material-ui/core/styles";
import { Switch, Route, Redirect } from "react-router";

import { WithSession } from "client/components/withSession";

import ErrorBoundary from "client/components/Error/Boundary";
import { Box } from "client/components/Box";

import Theme from "client/themes";
import Services from "client/services";
import { StoreProvider } from "state/provider";
import SideBar, {
  minimizedSidebarWidth,
  sidebarWidth
} from "client/views/common/SideBar";

import HelpDialog from "client/views/help";
import NotFound from "client/views/404/404";

const App = () => (
  <StoreProvider>
    <Theme.ThemeSwitcher>
      <ErrorBoundary>
        <WithSession>
          <Services>
            <Switch>
              <Redirect exact key="/" from="/" to="/tenant/system" />
              <Route key="*" path="*">
                <AuthProtectedApplication />
              </Route>
            </Switch>
          </Services>
        </WithSession>
      </ErrorBoundary>
    </Theme.ThemeSwitcher>
  </StoreProvider>
);

export default App;

const useStyles = makeStyles(theme => ({
  content: {
    flexGrow: 1,
    marginLeft: sidebarWidth,
    [theme.breakpoints.down("md")]: {
      marginLeft: minimizedSidebarWidth
    },
    minHeight: `calc(100vh)`,
    position: "relative"
  }
}));

const AuthProtectedApplication = () => {
  const classes = useStyles();

  return (
    <Box>
      <SideBar tenantNavItems={[]} clusterNavItems={[]} />
      <Box minWidth="100%">
        <Box className={classes.content} p={3}>
          <Switch>
            <Route key="*" path="*" component={NotFound} />
          </Switch>
        </Box>

        <HelpDialog />
      </Box>
    </Box>
  );
};
