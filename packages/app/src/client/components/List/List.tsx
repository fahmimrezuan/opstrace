import React from "react";
import {
  VariableSizeList,
  areEqual,
  ListChildComponentProps
} from "react-window";
import AutoSizer, { Size } from "react-virtualized-auto-sizer";
import { Scrollable } from "../Scrollable";

export type VirtualListRenderItemProps<Item> = {
  data: Item;
  index: number;
};
export type ItemRenderer<Item> = (
  props: VirtualListRenderItemProps<Item>
) => JSX.Element;
export type VirtualListProps<Item> = {
  height?: number;
  width?: number;
  items: Item[];
  itemSize: (index: number) => number;
  renderItem: ItemRenderer<Item>;
};

const ScrollableWithRef = React.forwardRef<HTMLDivElement>((props, ref) => (
  <Scrollable forwardedRef={ref} {...props} />
));

const Render = <Item,>(
  renderItem: (props: VirtualListRenderItemProps<Item>) => JSX.Element
) =>
  React.memo((props: ListChildComponentProps) => {
    return (
      <div style={props.style} key={`vl-${props.index}`}>
        {renderItem({ index: props.index, data: props.data[props.index] })}
      </div>
    );
  }, areEqual);

function VirtualList<Item>({
  items,
  itemSize,
  renderItem,
  ...props
}: VirtualListProps<Item>) {
  const listRef = React.createRef();
  const outerRef = React.createRef();
  const renderer = Render(renderItem);

  return (
    <AutoSizer
      defaultHeight={props.height}
      defaultWidth={props.width}
      disableHeight={Boolean(props.height)}
      disableWidth={Boolean(props.width)}
    >
      {({ height, width }: Size) => {
        return (
          <VariableSizeList
            height={height || props.height || 500}
            width={width}
            itemCount={items.length}
            itemData={items}
            itemSize={itemSize}
            outerRef={outerRef}
            innerRef={listRef}
            outerElementType={ScrollableWithRef}
          >
            {renderer}
          </VariableSizeList>
        );
      }}
    </AutoSizer>
  );
}

const MemoList = React.memo(VirtualList) as typeof VirtualList;

export default MemoList;
