import { ITheme } from "client/themes";
import styled from "styled-components";

import { Box } from "../Box";

function spacing(theme: ITheme) {
  if (theme && typeof theme.spacing === "function") {
    return theme.spacing(1);
  }
  return 0;
}

const Container = styled(Box)`
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  background: ${props => props.theme?.palette?.background?.default};
  display: flex;
  justifycontent: center;
  alignitems: center;
  padding: ${props => spacing(props.theme)}px;
`;

export default Container;
