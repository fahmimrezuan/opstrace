import React, { ReactNode } from "react";

import { Box } from "client/components/Box";

import Typography from "client/components/Typography/Typography";

export const Key = (props: { children: ReactNode }) => (
  <Box pt={2} pb={2}>
    <Typography variant="h6" color="textSecondary">
      {props.children}
    </Typography>
  </Box>
);

export const Value = (props: { children: ReactNode }) => (
  <Box p={3} pt={2} pb={2}>
    <Typography variant="h6">{props.children}</Typography>
  </Box>
);

const attribute = { Key, Value };

export default attribute;
