import React, { useState, useEffect } from "react";
import copyToClipboard from "copy-to-clipboard";

import AssignmentIcon from "@material-ui/icons/Assignment";
import AssignmentTurnedInIcon from "@material-ui/icons/AssignmentTurnedIn";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  copyToClipboard: {
    cursor: "pointer"
  }
}));

export const CopyToClipboardIcon = ({ text }: { text: string }) => {
  const [copied, setCopied] = useState(false);
  const classes = useStyles();

  const clickHandler = () => {
    copyToClipboard(text);
    setCopied(true);
  };

  useEffect(() => {
    if (!copied) return;

    const timer = setTimeout(() => {
      setCopied(false);
    }, 2000);

    return () => clearTimeout(timer);
  }, [copied, setCopied]);

  return copied ? (
    <AssignmentTurnedInIcon fontSize="small" />
  ) : (
    <AssignmentIcon
      fontSize="small"
      onClick={clickHandler}
      className={classes.copyToClipboard}
    />
  );
};
