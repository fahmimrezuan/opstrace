export { default as DialogTitle } from "./DialogTitle";
export * from "./DialogTitle";

export { default as DialogContent } from "./DialogContent";
export * from "./DialogContent";

export { default as DialogActions } from "./DialogActions";
export * from "./DialogActions";

export { default as DialogContentText } from "./DialogContentText";
export * from "./DialogContentText";

export { default as Dialog } from "./Dialog";
export * from "./Dialog";
