import React from "react";

import { Box } from "../Box";
import { Button } from "../Button";
import { Typography } from "../Typography";
import { ErrorView } from "../Error";
import { CardActions } from "../Card";
import { useHistory } from "react-router-dom";

export type NotFoundProps = {
  title?: string;
  subheader?: string;
  content?: React.ReactNode;
};

const NotFound = ({ title, subheader, content }: NotFoundProps) => {
  const history = useHistory();
  return (
    <ErrorView
      title={title ? title : "Under Construction"}
      subheader={subheader ? subheader : "New UI Coming Soon"}
      emoji="🚧"
      actions={
        <CardActions>
          <Box display="flex" justifyContent="center" width="100%">
            <Box mr={1}>
              <Button
                variant="contained"
                state="primary"
                onClick={() => history.goBack()}
              >
                Go Back
              </Button>
            </Box>
            <Box>
              <Button
                variant="outlined"
                state="info"
                onClick={() => history.push("/")}
              >
                Go Home
              </Button>
            </Box>
          </Box>
        </CardActions>
      }
    >
      {content ? (
        content
      ) : (
        <Typography data-test="logged-in">
          You have successfully logged in, but there is nothing to see here.
          Tenant UIs like Grafana, Prometheus, and Jaeger should still work for
          now.
        </Typography>
      )}
    </ErrorView>
  );
};

export default NotFound;
