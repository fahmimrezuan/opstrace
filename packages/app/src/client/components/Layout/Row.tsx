import React from "react";
import Column from "./Column";
import * as constants from "./constants";
import styled from "styled-components";

const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  flex-grow: 1;
  flex-wrap: wrap;
`;

export type RowProps = {
  minHeight?: number;
  children: React.ReactElement | React.ReactElement[];
};

const Row = (props: RowProps) => {
  let children = React.Children.map(props.children, (c, idx) => {
    if (c.type !== Column) {
      // wrap in an Column component
      return (
        <Column key={idx} minHeight={props.minHeight}>
          {c}
        </Column>
      );
    }
    return React.cloneElement(c, {
      minHeight: props.minHeight,
      key: idx
    });
  });

  return (
    <Wrapper
      style={{
        minHeight: props.minHeight || constants.MIN_ITEM_HEIGHT,
        width: "100%"
      }}
      className="LayoutRow"
    >
      {children}
    </Wrapper>
  );
};

Row.displayName = "LayoutRow";

export default Row;
