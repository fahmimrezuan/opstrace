import React from "react";
import { AsyncComponent } from "../Loadable";
import type { YamlEditorProps } from "./lib/types";
import EditorSkeleton from "./lib/components/EditorSkeleton";

export { default as EditorSkeleton } from "./lib/components/EditorSkeleton";

export const YamlEditor = AsyncComponent<YamlEditorProps>(
  /* #__LOADABLE__ */ () =>
    import(/* webpackChunkName: "yaml-editor" */ "./editors/YamlEditor"),
  false,
  <EditorSkeleton />
);
