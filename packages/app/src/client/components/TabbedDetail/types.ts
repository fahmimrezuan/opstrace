import React from "react";

export type Tab = {
  key: string;
  label: string;
  content: React.FunctionComponent;
};

export type Tabs = Tab[];
