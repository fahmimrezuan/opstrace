import React from "react";

import { makeStyles, Theme } from "@material-ui/core/styles";
import MuiTabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import { Route, Switch, useLocation } from "react-router";
import { Link } from "react-router-dom";

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    flexGrow: 1,
    borderBottom: `1px solid ${theme.palette.divider}`
  }
}));

export type TabItem = {
  title: string;
  path: string;
  to?: string;
  component: React.ComponentType;
};

export type TabsProps = {
  tabs: TabItem[];
};

export function Tabs({ tabs }: TabsProps) {
  const classes = useStyles();
  const location = useLocation();

  return (
    <>
      <MuiTabs
        value={location.pathname}
        indicatorColor="primary"
        variant="scrollable"
        scrollButtons="auto"
        classes={classes}
      >
        {tabs.map(tab => (
          <Tab
            key={tab.title}
            label={tab.title}
            value={tab.to || tab.path}
            component={Link}
            to={tab.to || tab.path}
          />
        ))}
      </MuiTabs>
      <Switch>
        {tabs.map(tab => (
          <Route key={tab.path} path={tab.path} component={tab.component} />
        ))}
      </Switch>
    </>
  );
}
