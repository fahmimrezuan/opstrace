import React from "react";
import styled from "styled-components";

import MuiCardContent, {
  CardContentProps
} from "@material-ui/core/CardContent";

const BaseCardContent = (props: CardContentProps) => (
  <MuiCardContent {...props} />
);

const CardContent = styled(BaseCardContent)`
  padding-left: 0px;
  padding-right: 0px;

  &.MuiCardContent-root:last-child {
    padding-bottom: 0px;
  }
`;

export default CardContent;
