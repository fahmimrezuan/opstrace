import React, { ReactNode, useEffect, useState } from "react";
import { render as rtlRender, RenderResult } from "@testing-library/react";
import { History, createMemoryHistory } from "history";
import { Router } from "react-router-dom";
import UE from "@testing-library/user-event";
import { Provider } from "react-redux";
import { createMainStore } from "state/store";
import "@testing-library/jest-dom";
import ThemeProvider from "client/themes/Provider";
import light from "client/themes/light";
import Services from "client/services";
import { useCommandService } from "client/services/Command";

export type RenderOptions = {
  // optionally pass in a history object to control routes in the test
  history?: History;
};

type WrapperProps = {
  children?: React.ReactNode;
};

export function render(
  ui: React.ReactNode,
  renderOptions?: RenderOptions
): RenderResult {
  function Wrapper({ children }: WrapperProps) {
    return (
      <React.StrictMode>
        <Router
          history={
            (renderOptions && renderOptions.history) || createMemoryHistory()
          }
        >
          {children}
        </Router>
      </React.StrictMode>
    );
  }

  return rtlRender(<div>{ui}</div>, { wrapper: Wrapper, ...renderOptions });
}

export const renderWithEnv = (
  children: React.ReactNode,
  { store = createMainStore(), history = createMemoryHistory() } = {}
) => {
  return rtlRender(
    <Provider store={store}>
      <ThemeProvider theme={light}>
        <Services>
          <Router history={history}>{children}</Router>
        </Services>
      </ThemeProvider>
    </Provider>
  );
};

export function sleep(ms: number) {
  return new Promise(resolve => {
    setTimeout(resolve, ms);
  });
}

export const CommandServiceTrigger = ({
  children,
  commandId
}: {
  children: ReactNode;
  commandId: string;
}) => {
  const cmdService = useCommandService();
  const [commandServiceReady, setCommandServiceReady] = useState(false);
  useEffect(() => {
    // CommandService is not ready on first render, as commands havent been registered yet.
    // This will retriger the command service on second render cycle.
    setCommandServiceReady(true);
  }, []);
  useEffect(() => {
    cmdService.executeCommand(commandId);
  }, [commandServiceReady, cmdService, commandId]);
  return <>{children}</>;
};

// re-export everything
export * from "@testing-library/react";
export const userEvent = UE;
