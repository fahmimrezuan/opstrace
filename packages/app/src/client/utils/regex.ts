// note: webkit doesn't support "look behind" syntax which means can't use the preferred regex for this
// const tenantNameValidatorWithLookBehind = /^(?!-)[a-z0-9-]{1,63}(?<!-)$/;

// 2021.07.06 - [terrcin] I've removed "-" as a valid char as we're having problems with it in Grafana
// pod names. This means that only alphanumeric chars are allowed.
// Related issue: https://github.com/opstrace/opstrace/issues/957
export const tenantNameValidator = /^[a-z0-9]{2,63}$/;
