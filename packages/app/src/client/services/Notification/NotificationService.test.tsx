import React from "react";
import {
  useSimpleNotification,
  NotificationService
} from "client/services/Notification";
import userEvent from "@testing-library/user-event";
import { renderWithEnv, screen, act, within } from "client/utils/testutils";

test("useNotificationService", async () => {
  const buttonName = "click me!";
  const title = "my title";
  const information = "my information";
  const state = "error";
  const TestComponent = () => {
    const { registerNotification } = useSimpleNotification();
    return (
      <button
        onClick={() =>
          registerNotification({
            title,
            information,
            state
          })
        }
      >
        {buttonName}
      </button>
    );
  };

  renderComponent(<TestComponent />);
  act(() => {
    userEvent.click(screen.getByRole("button", { name: buttonName }));
  });

  const alert = within((await screen.findAllByRole("alert"))[0]);
  expect(await alert.findByText(title)).toBeInTheDocument();
  expect(await alert.findByText(information)).toBeInTheDocument();
});

const renderComponent = (children: React.ReactNode) => {
  return renderWithEnv(<NotificationService>{children}</NotificationService>);
};
