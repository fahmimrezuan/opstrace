import {
  getPlatformMetaKey,
  replaceModKeyWithPlatformMetaKey,
  getModifierSymbol,
  getKeysFromKeybinding
} from "../util";

const navigatorGetter = jest.spyOn(global, "navigator", "get");

afterAll(() => {
  navigatorGetter.mockRestore();
});

describe("handle platformMetaKey", () => {
  test("return correct value when platform is iPod", () => {
    navigatorGetter.mockReturnValue({
      ...global.navigator,
      platform: "iPod"
    });
    expect(getPlatformMetaKey()).toEqual("cmd");
  });

  test("return correct value when platform is unknown", () => {
    const navigatorGetter = jest.spyOn(global, "navigator", "get");
    navigatorGetter.mockReturnValue({
      ...global.navigator,
      platform: "unknown"
    });
    expect(getPlatformMetaKey()).toEqual("ctrl");
  });
});

describe("handle replaceModKeyWithPlatformMetaKey", () => {
  test("return correct value when platform is iPhone", () => {
    navigatorGetter.mockReturnValue({
      ...global.navigator,
      platform: "iPhone"
    });
    expect(replaceModKeyWithPlatformMetaKey("mod+shift+z")).toEqual(
      "cmd+shift+z"
    );
  });

  test("return correct value when platform is unknown", () => {
    const navigatorGetter = jest.spyOn(global, "navigator", "get");
    navigatorGetter.mockReturnValue({
      ...global.navigator,
      platform: "test"
    });
    expect(replaceModKeyWithPlatformMetaKey("shift+mod+q")).toEqual(
      "shift+ctrl+q"
    );
  });
});

test("get correct modifier symbols", () => {
  expect(getModifierSymbol("shift")).toEqual("⇧");
  expect(getModifierSymbol("control")).toEqual("⌃");
  expect(getModifierSymbol("enter")).toEqual("enter");
});

describe("handle getKeysFromKeybinding", () => {
  test("return correct value when platform is iPad", () => {
    navigatorGetter.mockReturnValue({
      ...global.navigator,
      platform: "iPad"
    });
    expect(getKeysFromKeybinding("mod+shift+z")).toEqual(["⌘", "⇧", "z"]);
  });

  test("return correct value when platform is unknown", () => {
    const navigatorGetter = jest.spyOn(global, "navigator", "get");
    navigatorGetter.mockReturnValue({
      ...global.navigator,
      platform: "test"
    });
    expect(getKeysFromKeybinding("shift+mod+q")).toEqual(["⇧", "⌃", "q"]);
  });
});
