import React, { useCallback } from "react";
import { PickerOption, usePickerService } from "../Picker";
import { useCommandService } from "./CommandService";
import { Command } from "./types";
import { getKeysFromKeybinding } from "client/services/Command/util";
import Box from "client/components/Box/Box";

export const cmdID = "open-command-picker";

type CommandPickerProps = {
  commands: Command[];
};

function commandToPickerOption(cmd: Command): PickerOption {
  return {
    id: cmd.id,
    text: cmd.description
  };
}

function filterCommands(cmds: Command[]): PickerOption[] {
  return cmds
    .filter(
      cmd => cmd.id !== cmdID && cmd.category !== "Hidden" && !cmd.disabled
    )
    .map(commandToPickerOption);
}

function renderKeybindings(data?: Command) {
  if (!data?.keybindings?.length) {
    return null;
  }

  const keybindings = data.keybindings.map(keys => getKeysFromKeybinding(keys));

  return (
    <Box display="flex">
      {keybindings.map((keys, idx) => (
        <Box display="flex" ml={1} key={`keybinding-${idx}`}>
          {keys.map((keyCode, partIdx) => (
            <Box p={0.3} ml={0.3} key={`keybinding-key-${partIdx}`}>
              {keyCode}
            </Box>
          ))}
        </Box>
      ))}
    </Box>
  );
}

function CommandPicker({ commands }: CommandPickerProps) {
  const cmdService = useCommandService({
    id: cmdID,
    description: "Show and Run Commands",
    handler: e => {
      e.keyboardEvent?.preventDefault();
      activatePickerWithText("");
    },
    keybindings: ["mod+k"]
  });

  const getCommand = useCallback(
    id => commands.find(command => command.id === id),
    [commands]
  );

  const { activatePickerWithText } = usePickerService(
    {
      activationPrefix: "",
      options: filterCommands(commands),
      onSelected: option => {
        cmdService.executeCommand(option.id);
      },
      secondaryAction: option => renderKeybindings(getCommand(option.id))
    },
    [commands]
  );

  return null;
}

export default React.memo(CommandPicker);
