export type CommandCategory = "Help" | "Module" | "View" | "Hidden";

export type CommandEvent = {
  keyboardEvent?: KeyboardEvent;
  keys?: string;
  preventNext: () => void;
};

export type Command = {
  id: string;
  description: string;
  disabled?: boolean;
  handler: (event: CommandEvent, args?: any[]) => any;
  keybindings?: string[];
  category?: CommandCategory;
};

export type CommandServiceApi = {
  register: (command: Command) => void;
  unregister: (command: Command) => void;
  executeCommand: (id: string, args?: any[]) => any;
};

export type KeyBindingState = { [key: string]: Command[] };

export type CommandServiceState = {
  commands: Command[];
  keyBindings: KeyBindingState;
};
