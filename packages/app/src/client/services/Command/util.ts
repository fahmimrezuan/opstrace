const modifiers: { [key: string]: string } = {
  // shiftKey
  shift: "⇧",
  // altKey
  alt: "⌥",
  option: "⌥",
  // ctrlKey
  ctrl: "⌃",
  control: "⌃",
  // metaKey
  cmd: "⌘",
  command: "⌘"
};

export const getPlatformMetaKey = () =>
  /Mac|iPod|iPhone|iPad/.test(
    typeof navigator !== "undefined" ? navigator.platform : ""
  )
    ? "cmd"
    : "ctrl";

export function replaceModKeyWithPlatformMetaKey(key: string) {
  return key.replace(/mod/g, getPlatformMetaKey());
}

export function getModifierSymbol(modifier: string) {
  return modifier in modifiers ? modifiers[modifier] : modifier;
}

export function getKeysFromKeybinding(keybinding: string) {
  const keysArray = replaceModKeyWithPlatformMetaKey(keybinding).split("+");
  return keysArray.map(key => getModifierSymbol(key));
}
