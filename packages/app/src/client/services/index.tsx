import React from "react";
import { CommandService } from "./Command";
import { PickerService } from "./Picker";
import { NotificationService } from "./Notification";
import { DisplayService } from "./Display";

const Services = ({ children }: { children: React.ReactNode }) => {
  return (
    <PickerService>
      <CommandService>
        <NotificationService>
          <DisplayService>{children}</DisplayService>
        </NotificationService>
      </CommandService>
    </PickerService>
  );
};

export default React.memo(Services);
