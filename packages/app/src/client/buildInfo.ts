/**
 * Used for cache busting when fetching files from the server.
 */
const buildTime: string = process.env.BUILD_TIME;

export default buildTime;
