import light from "./light";
import dark from "./dark";
import Provider, { ThemeSwitcher } from "./Provider";
export * from "./types";

const theme = {
  light,
  dark,
  Provider,
  ThemeSwitcher
};

export default theme;
