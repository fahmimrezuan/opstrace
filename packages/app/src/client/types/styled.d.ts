// import original module declarations
import "styled-components";
import { Theme } from "@material-ui/core/styles";

// and extend them!
declare module "styled-components" {
  // ensure styled-components theme conforms to the same interface
  // as our MUI theme so we can pass the same theme interface to both MUI
  // and styled-components

  // eslint-disable-next-line @typescript-eslint/no-empty-interface
  export interface DefaultTheme extends Theme {}
}
