import http from "http";
import { Server } from "socket.io";

export default function createWebsocketServer(server: http.Server) {
  new Server(server, {
    serveClient: false,
    path: "/_/socket"
  });
}
