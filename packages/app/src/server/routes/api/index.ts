import express from "express";

import { GeneralServerError } from "server/errors";

import createAuthHandler from "./authentication";
import { pubUiCfgHandler, buildInfoHandler } from "./uicfg";

function createAPIRoutes(): express.Router {
  const api = express.Router();
  api.use("/auth", createAuthHandler());
  api.use("/public-ui-config", pubUiCfgHandler);
  api.use("/buildinfo", buildInfoHandler);

  api.all("*", function (req, res, next) {
    next(new GeneralServerError(404, "api route not found"));
  });

  return api;
}

export default createAPIRoutes;
