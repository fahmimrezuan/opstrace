export const parseEnv = <T>(
  envName: string,
  parser: (val: string) => T,
  defaultValue: T
) => {
  const value = process.env[envName];
  if (value !== undefined) {
    return parser(value);
  }
  return defaultValue;
};

export const parseRequiredEnv = <T>(
  envName: string,
  parser: (val: string) => T,
  errorMessage: string = `must provide env vars: ${envName}`
) => {
  const parsedValue = parseEnv(envName, parser, null);
  if (parsedValue === null) {
    throw new Error(errorMessage);
  }
  return parsedValue;
};
