import express, { Request } from "express";
import { IncomingMessage, ServerResponse, ClientRequest } from "http";
import { ServerError } from "./errors";

export const getHeader = (
  req: Request,
  header: string
): [string, Error | null] => {
  let headerValue = req.get(header);
  if (Array.isArray(headerValue)) {
    headerValue = headerValue[0];
  }
  if (!headerValue) {
    return ["", new Error(`missing header: ${header}`)];
  }
  return [headerValue, null];
};

export const getQueryParam = (
  req: Request,
  param: string
): [string, Error | null] => {
  let value = req.query[param];

  if (Array.isArray(value)) {
    value = value[0];
  }
  if (!value) {
    return ["", new Error(`missing query parameter: ${param}`)];
  }
  return [value.toString(), null];
};

export const handleError = (err: ServerError, res: express.Response): void => {
  res
    .status(err.statusCode || 500)
    .json({ ...err, message: err.message, stack: err.stack });
};

export function onProxyReq(
  proxyReq: ClientRequest,
  req: IncomingMessage,
  res: ServerResponse
) {
  // @ts-ignore
  if (!req.body || !Object.keys(req.body).length) {
    return;
  }

  const contentType = proxyReq.getHeader("Content-Type");
  const writeBody = (bodyData: string) => {
    proxyReq.setHeader("Content-Length", Buffer.byteLength(bodyData));
    proxyReq.write(bodyData);
  };

  if (contentType === "application/json") {
    // @ts-ignore
    writeBody(JSON.stringify(req.body));
  }

  if (contentType === "application/x-www-form-urlencoded") {
    // @ts-ignore
    writeBody(querystring.stringify(req.body));
  }
}
