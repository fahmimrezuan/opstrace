import { combineReducers } from "redux";
import { reducer as formReducer } from "./form/reducer";
import { notificationServiceReducer } from "client/services/Notification/reducer";

export const mainReducers = {
  form: formReducer,
  notifications: notificationServiceReducer
};

export const mainReducer = combineReducers(mainReducers);
export type State = ReturnType<typeof mainReducer>;
