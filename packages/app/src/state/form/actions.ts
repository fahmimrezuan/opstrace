import { createAction } from "typesafe-actions";

export const registerForm = createAction("REGISTER_FORM")<{
  id: string;
  status?: string;
  data?: object;
}>();
export const unregisterForm = createAction("UNREGISTOR_FORM")<string>();

export const updateFormStatus = createAction("UPDATE_FORM_STATUS")<{
  id: string;
  status: string;
}>();

export const updateForm = createAction("UPDATE_FORM")<{
  id: string;
  status?: string;
  data: object;
  replaceData?: boolean;
}>();
