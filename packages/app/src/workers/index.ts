// Import only what is needed from monaco-editor
import "monaco-editor/esm/vs/editor/editor.api";
import "monaco-editor/esm/vs/editor/edcore.main";
import "monaco-editor/esm/vs/basic-languages/typescript/typescript.contribution";
import "monaco-editor/esm/vs/basic-languages/yaml/yaml.contribution";
import "monaco-yaml/lib/esm/monaco.contribution";
/* eslint-disable import/no-webpack-loader-syntax */
/*
  ts-ignore these imports that use worker-loader:
  Cannot find module 'worker-loader!monaco-yaml/lib/esm/yaml.worker' or its corresponding type declarations
*/
//@ts-ignore
import EditorWorker from "worker-loader!monaco-editor/esm/vs/editor/editor.worker.js";
//@ts-ignore
import YamlWorker from "worker-loader!monaco-yaml/lib/esm/yaml.worker";

import { languages } from "monaco-editor/esm/vs/editor/editor.api";

// @ts-ignore
export const { yaml } = languages || {};

//@ts-ignore
/* eslint-disable-line no-restricted-globals */ self.MonacoEnvironment = {
  getWorker: function (_: any, label: string) {
    if (label === "yaml") {
      return new YamlWorker();
    }
    return new EditorWorker();
  }
};
