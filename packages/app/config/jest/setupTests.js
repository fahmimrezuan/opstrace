const { configure } = require("@testing-library/react");
const nock = require("nock");
const axios = require("axios");

require("regenerator-runtime/runtime");
// jest-dom adds custom jest matchers for asserting on DOM nodes.
// allows you to do things like:
// expect(element).toHaveTextContent(/react/i)
// learn more: https://github.com/testing-library/jest-dom
require("@testing-library/jest-dom/extend-expect");

// Mock our workers file because Jest will have a hard time resolving our workers
jest.mock("workers", () => {
  return {
    getOpScriptWorker: () => {}
  };
});

const testTimeout = 15 * 1000; // 30s

jest.setTimeout(testTimeout);
configure({
  asyncUtilTimeout: 10000
});
/* limiting net requests to localhost, see
 * https://github.com/nock/nock#enabledisable-real-http-requests
 */
nock.disableNetConnect();
nock.enableNetConnect(
  host => host.includes("127.0.0.1") || host.includes("localhost")
);

// If you are using jsdom, axios will default to using the XHR adapter which
// can't be intercepted by nock. So, configure axios to use the node adapter.
//
// References:
// https://github.com/nock/nock/issues/699#issuecomment-272708264
// https://github.com/axios/axios/issues/305
axios.defaults.adapter = require("axios/lib/adapters/http");

// Has to be mocked as react-virtualized-auto-sizer does not work with the test renderer. 
// See more here:
// https://github.com/bvaughn/react-virtualized/issues/493#issuecomment-631093196
jest.mock('react-virtualized-auto-sizer', () => ({ children }) => children({ height: 600, width: 600 }), );