/*eslint-disable */
import { createHandlerBoundToURL, precacheAndRoute } from "workbox-precaching";
import { NavigationRoute, registerRoute } from "workbox-routing";
import { setCacheNameDetails, clientsClaim, skipWaiting } from "workbox-core";

// cache our /app-shell for client to fall back on after first render
self.__precacheManifest = [{ url: "/app-shell", revision: null }].concat(
  self.__WB_MANIFEST || []
);

setCacheNameDetails({
  prefix: "opstrace-app",
  suffix: "v1",
  precache: "install-time",
  runtime: "run-time"
});

// https://developers.google.com/web/tools/workbox/modules/workbox-core#skip_waiting_and_clients_claim
clientsClaim();
skipWaiting();

// precache and route asserts built by webpack
precacheAndRoute(self.__precacheManifest);

// return app shell for all navigation requests
// This assumes /app-shell has been precached.
const handler = createHandlerBoundToURL("/app-shell");
const navigationRoute = new NavigationRoute(handler, {
  // make sure we don't cache routes to our apis
  denylist: [new RegExp("^/_/"), new RegExp("^/modules/.*/latest/"), new RegExp("^/api/")]
});
registerRoute(navigationRoute);

/*eslint-enable */
