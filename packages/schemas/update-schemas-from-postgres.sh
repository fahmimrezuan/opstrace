#!/bin/bash

set -euo pipefail
IFS=$'\n\t'

# Retrieves and generates Sequelize schemas from a running Opstrace PostgreSQL server.
# This is meant to be a migration path for switching away from Hasura's schema management.
# This sync utility should be deleted once we are no longer using Hasura's schema migrations.

# expected format: "postgres://USERNAME:PASSWORD@HOST"
SECRET_ENDPOINT="$(kubectl get secret -n kube-system -o json postgres-secret | jq -r .data.endpoint | base64 -d)"

POSTGRES_HOST=$(echo "$SECRET_ENDPOINT" | sed 's/.*\/\/.*:.*@\(.*\)/\1/g')
POSTGRES_USER=$(echo "$SECRET_ENDPOINT" | sed 's/.*\/\/\(.*\):.*@.*/\1/g')
POSTGRES_PASS=$(echo "$SECRET_ENDPOINT" | sed 's/.*\/\/.*:\(.*\)@.*/\1/g')

# Postgres is only accessible to pods in the cluster.
# Start a pod for port-forwarding into Postgres (or fail silently if it already exists).
# The pod can be manually deleted when syncing is complete.
kubectl run \
  -n kube-system \
  --env REMOTE_HOST="$POSTGRES_HOST" \
  --env REMOTE_PORT=5432 \
  --env LOCAL_PORT=5432 \
  --image marcnuri/port-forward:latest@sha256:fd7042c165b1afd4bfab7f7ce474344ecf6364233d839b2b756affa148325952 \
  forward-postgres || true

kubectl port-forward -n kube-system forward-postgres 5432 &
PORT_FORWARD_PID=$!
echo "Port forward in background: PID $PORT_FORWARD_PID"
stop_port_forward() {
    echo "Stopping port forward: $PORT_FORWARD_PID"
    kill $PORT_FORWARD_PID
}
trap stop_port_forward EXIT

echo "Waiting 5s for port-forward to be reachable..."
sleep 5

# Export all tables under the "public" schema
# Tweaks:
# - sequelize-auto-options.json configures underscores on field names rather than camelCase
# - don't add explicit 'created_at'/'updated_at' fields to schemas, they are already handled implicitly
yarn sequelize-auto \
    --host 127.0.0.1 \
    --user "$POSTGRES_USER" \
    --pass "$POSTGRES_PASS" \
    --dialect postgres \
    --output ./src/models/ \
    --lang ts \
    --additional ./sequelize-auto-options.json \
    --database "opstrace" \
    --schema "public" \
    --skipFields "created_at" \
    --skipFields "updated_at"

yarn prettier -w ./src/

echo "Sync complete, note that some manual fixes will be needed to the model files. See git diff."
echo " => Remove redundant created_at/updated_at fields from types"
echo " => Fix 'jsonb_build_object' call"
echo " => Readd validator on tenant name"
