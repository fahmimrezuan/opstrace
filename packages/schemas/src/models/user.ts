import * as Sequelize from "sequelize";
import { DataTypes, Model, Optional } from "sequelize";
import type { user_preference, user_preferenceId } from "./user_preference";

export interface userAttributes {
  email: string;
  username: string;
  avatar?: string;
  session_last_updated?: Date;
  role: string;
  active: boolean;
  id: string;
}

export type userPk = "id";
export type userId = user[userPk];
// EDIT: omit implicit created_at
export type userOptionalAttributes =
  | "avatar"
  | "session_last_updated"
  | "role"
  | "active"
  | "id";
export type userCreationAttributes = Optional<
  userAttributes,
  userOptionalAttributes
>;

export class user
  extends Model<userAttributes, userCreationAttributes>
  implements userAttributes
{
  email!: string;
  username!: string;
  avatar?: string;
  session_last_updated?: Date;
  role!: string;
  active!: boolean;
  id!: string;

  // user hasOne user_preference via user_id
  user_preference!: user_preference;
  getUser_preference!: Sequelize.HasOneGetAssociationMixin<user_preference>;
  setUser_preference!: Sequelize.HasOneSetAssociationMixin<
    user_preference,
    user_preferenceId
  >;
  createUser_preference!: Sequelize.HasOneCreateAssociationMixin<user_preference>;

  static initModel(sequelize: Sequelize.Sequelize): typeof user {
    return user.init(
      {
        email: {
          type: DataTypes.TEXT,
          allowNull: false,
          unique: "user_email_key"
        },
        username: {
          type: DataTypes.TEXT,
          allowNull: false
        },
        avatar: {
          type: DataTypes.TEXT,
          allowNull: true
        },
        session_last_updated: {
          type: DataTypes.DATE,
          allowNull: true
        },
        role: {
          type: DataTypes.TEXT,
          allowNull: false,
          defaultValue: "user_admin"
        },
        active: {
          type: DataTypes.BOOLEAN,
          allowNull: false,
          defaultValue: true
        },
        id: {
          type: DataTypes.UUID,
          allowNull: false,
          defaultValue: DataTypes.UUIDV4,
          primaryKey: true,
          unique: "user_id_key"
        }
      },
      {
        sequelize,
        tableName: "user",
        schema: "public",
        timestamps: true,
        underscored: true,
        createdAt: "created_at",
        updatedAt: "updated_at",
        indexes: [
          {
            name: "user_email_key",
            unique: true,
            fields: [{ name: "email" }]
          },
          {
            name: "user_id_key",
            unique: true,
            fields: [{ name: "id" }]
          },
          {
            name: "user_pkey",
            unique: true,
            fields: [{ name: "id" }]
          }
        ]
      }
    );
  }
}
