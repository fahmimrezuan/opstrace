import * as Sequelize from "sequelize";
import { DataTypes, Model, Optional } from "sequelize";
import type { user, userId } from "./user";

export interface user_preferenceAttributes {
  dark_mode: boolean;
  id: string;
  user_id?: string;
}

export type user_preferencePk = "id";
export type user_preferenceId = user_preference[user_preferencePk];
export type user_preferenceOptionalAttributes = "dark_mode" | "id" | "user_id";
export type user_preferenceCreationAttributes = Optional<
  user_preferenceAttributes,
  user_preferenceOptionalAttributes
>;

export class user_preference
  extends Model<user_preferenceAttributes, user_preferenceCreationAttributes>
  implements user_preferenceAttributes
{
  dark_mode!: boolean;
  id!: string;
  user_id?: string;

  // user_preference belongsTo user via user_id
  user!: user;
  getUser!: Sequelize.BelongsToGetAssociationMixin<user>;
  setUser!: Sequelize.BelongsToSetAssociationMixin<user, userId>;
  createUser!: Sequelize.BelongsToCreateAssociationMixin<user>;

  static initModel(sequelize: Sequelize.Sequelize): typeof user_preference {
    return user_preference.init(
      {
        dark_mode: {
          type: DataTypes.BOOLEAN,
          allowNull: false,
          defaultValue: true
        },
        id: {
          type: DataTypes.UUID,
          allowNull: false,
          defaultValue: DataTypes.UUIDV4,
          primaryKey: true,
          unique: "user_preference_id_key"
        },
        user_id: {
          type: DataTypes.UUID,
          allowNull: true,
          references: {
            model: "user",
            key: "id"
          },
          unique: "user_preference_user_id_key"
        }
      },
      {
        sequelize,
        tableName: "user_preference",
        schema: "public",
        timestamps: false,
        underscored: true,
        createdAt: "created_at",
        updatedAt: "updated_at",
        indexes: [
          {
            name: "user_preference_id_key",
            unique: true,
            fields: [{ name: "id" }]
          },
          {
            name: "user_preference_pkey",
            unique: true,
            fields: [{ name: "id" }]
          },
          {
            name: "user_preference_user_id_key",
            unique: true,
            fields: [{ name: "user_id" }]
          }
        ]
      }
    );
  }
}
