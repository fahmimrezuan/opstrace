import { log } from "@opstrace/utils";
import * as pg from "pg";
import { Sequelize } from "sequelize";
import {
  initModels,
  integration,
  tenant,
  user,
  user_preference
} from "./models";

let _schemas: {
  integration: typeof integration;
  tenant: typeof tenant;
  user: typeof user;
  user_preference: typeof user_preference;
} | null;

// Initializes schemas/client for future calls to schemas(). Requires POSTGRES_ENDPOINT envvar.
// This must be called exactly once when starting a service that will use these schemas.
export function initSchemas() {
  if (_schemas != null) {
    throw Error("initSchemas() was already called");
  }

  const envval = process.env.POSTGRES_ENDPOINT;
  if (!envval) {
    throw Error(
      "Missing POSTGRES_ENDPOINT envvar, should be like 'postgres://user:pass@host'"
    );
  }

  const client = new Sequelize(envval, {
    // Manually import/invoke pg library to verify that it's linked into the application
    // This mainly helps with fixing webpack builds
    dialectModule: pg,
    // Reuse Opstrace log config when logging Postgres queries
    // For now we log SQL queries by default. To stop that, change this to "log.debug":
    logging: (...params) => log.info(params)
  });
  _schemas = initModels(client);
}

// Creates any missing tables in Postgres if they don't yet exist.
// This should only be called by the Controller to set up tables as the instance is deployed.
export async function createTables() {
  if (_schemas == null) {
    throw Error("initSchemas() must be called before createTables()");
  }

  // Tables must be created in the correct order to avoid startup errors like
  // "relation public.user does not exist" on the first run.

  // tenant has no external references
  await _schemas.tenant.sync();

  // user has no external references
  await _schemas.user.sync();

  // integration depends on tenant
  await _schemas.integration.sync();

  // user_preference depends on user
  await _schemas.user_preference.sync();
}

// Lazy load to avoid errors around POSTGRES_ENDPOINT until DB access is actually used.
// For example let's avoid errors when someone is just doing "app --help".
export function schemas() {
  if (_schemas == null) {
    throw Error("Call to schemas() without preceding call to initSchemas()");
  }
  return _schemas;
}
