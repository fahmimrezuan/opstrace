import { combineReducers } from "redux";

import { tenantsReducer } from "./tenants";

export const reducer = combineReducers({
  list: tenantsReducer
});
