import * as yup from "yup";

export const tenantSchema = yup.object({
  // Provided by the user
  name: yup
    .string()
    .required("Must provide a name")
    .matches(
      /^[A-Za-z0-9-]+$/,
      "must only contain alphanumeric characters and -"
    ),

  // Provided by the user
  type: yup
    .mixed<"USER" | "SYSTEM">()
    .oneOf(["SYSTEM", "USER"])
    .default("USER"),

  // Generated when the tenant is written to GraphQL, and then synced back to here.
  id: yup.string().optional()
});

export type Tenant = yup.InferType<typeof tenantSchema>;
export type Tenants = Tenant[];
