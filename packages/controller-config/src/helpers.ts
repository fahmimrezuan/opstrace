/**
 * Generates a random 75 char long string of alphanumeric characters
 */
// commented out due to https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/1689#note_981849905
// export function generateSecretValue(): string {
//   return (
//     Math.random().toString(36).substring(2, 15) +
//     Math.random().toString(36).substring(2, 15) +
//     Math.random().toString(36).substring(2, 15) +
//     Math.random().toString(36).substring(2, 15) +
//     Math.random().toString(36).substring(2, 15) +
//     Math.random().toString(36).substring(2, 15) +
//     Math.random().toString(36).substring(2, 15)
//   );
// }
