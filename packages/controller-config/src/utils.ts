import { ConfigMap, KubeConfiguration } from "@opstrace/kubernetes";
import {
  LatestControllerConfigType,
  LatestControllerConfigSchema
} from "./schema";

export const CONFIGMAP_NAME = "opstrace-controller-config";
// TODO(nickbp): Move this to kube-system? Not sure why it's in default.
export const CONFIGMAP_NAMESPACE = "default";
export const STORAGE_KEY = "config.json";

export const isConfigStorage = (configMap: ConfigMap): boolean =>
  configMap.name === CONFIGMAP_NAME &&
  configMap.namespace === CONFIGMAP_NAMESPACE;

export const deserialize = (
  configMap: ConfigMap
): LatestControllerConfigType => {
  return LatestControllerConfigSchema.cast(
    JSON.parse(configMap.spec.data?.[STORAGE_KEY] ?? "")
  );
};

export const configmap = (kubeConfig: KubeConfiguration): ConfigMap => {
  return new ConfigMap(
    {
      apiVersion: "v1",
      kind: "ConfigMap",
      metadata: {
        name: CONFIGMAP_NAME,
        namespace: CONFIGMAP_NAMESPACE
      },
      data: {
        [STORAGE_KEY]: "{}"
      }
    },
    kubeConfig
  );
};

export const serializeControllerConfig = (
  ccfg: LatestControllerConfigType,
  kubeConfig: KubeConfiguration
): ConfigMap => {
  const cm = new ConfigMap(
    {
      apiVersion: "v1",
      kind: "ConfigMap",
      metadata: {
        name: CONFIGMAP_NAME,
        namespace: CONFIGMAP_NAMESPACE
      },
      data: {
        [STORAGE_KEY]: JSON.stringify(ccfg)
      }
    },
    kubeConfig
  );
  cm.setManagementOption({ protect: true }); // Protect so the reconciliation loop doesn't destroy it again.

  return cm;
};
