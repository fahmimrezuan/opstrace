import { log } from "@opstrace/utils";

import { ControllerConfigSchemaV1, ControllerConfigTypeV1 } from "./schemav1";
import {
  ControllerConfigSchemaV1alpha,
  ControllerConfigTypeV1alpha
} from "./schemav1alpha";
import { ControllerConfigSchemaV2, ControllerConfigTypeV2 } from "./schemav2";
import { ControllerConfigSchemaV3, ControllerConfigTypeV3 } from "./schemav3";

export { NodeSelectorTermsType } from "./schemav3";
export type LatestControllerConfigType = ControllerConfigTypeV3;
export const LatestControllerConfigSchema = ControllerConfigSchemaV3;

function V2toV3(cfg: ControllerConfigTypeV2): ControllerConfigTypeV3 {
  const { metricRetentionDays } = cfg;

  return {
    ...cfg,
    // Default to matching the metric retention
    traceRetentionDays: metricRetentionDays,
    // insert some placeholders for the following gitlab values. They will be overriden
    // because they're required parameters in the cluster-config.yaml
    gitlab: {
      instance_url: "GITLAB_INSTANCE_URL",
      oauth_client_id: "GITLAB_OAUTH_CLIENT_ID",
      oauth_client_secret: "GITLAB_OAUTH_CLIENT_SECRET",
      group_allowed_access: "*",
      group_allowed_system_access: "6543"
    },
    cliMetadata: {
      allCLIVersions: []
    }
  };
}

// upgrade function
function V1toV2(cfg: ControllerConfigTypeV1): ControllerConfigTypeV2 {
  const { metricRetention, ...restConfig } = cfg;

  return {
    ...restConfig,
    metricRetentionDays: metricRetention,
    // insert some placeholders for the following gitlab values. They will be overriden
    // because they're required parameters in the cluster-config.yaml
    gitlab: {
      instance_url: "GITLAB_INSTANCE_URL",
      oauth_client_id: "GITLAB_OAUTH_CLIENT_ID",
      oauth_client_secret: "GITLAB_OAUTH_CLIENT_SECRET",
      group_allowed_access: "*",
      group_allowed_system_access: "6543"
    },
    // just a placeholder to keep things working
    tenant_api_authenticator_pubkey_set_json: ""
  };
}

function V1alphatoV2(cfg: ControllerConfigTypeV1alpha): ControllerConfigTypeV2 {
  const { metricRetention, ...restConfig } = cfg;

  return {
    ...restConfig,
    metricRetentionDays: metricRetention,
    // insert some placeholders for the following gitlab values. They will be overriden
    // because they're required parameters in the cluster-config.yaml
    gitlab: {
      instance_url: "GITLAB_INSTANCE_URL",
      oauth_client_id: "GITLAB_OAUTH_CLIENT_ID",
      oauth_client_secret: "GITLAB_OAUTH_CLIENT_SECRET",
      group_allowed_access: "*",
      group_allowed_system_access: "6543"
    }
  };
}

export function upgradeControllerConfigMapToLatest(
  json: object
): LatestControllerConfigType {
  if (LatestControllerConfigSchema.isValidSync(json, { strict: true })) {
    // validate again, this time "only" to interpolate with defaults, see
    // https://github.com/jquense/yup/pull/961
    log.debug("got latest controller config version");
    return LatestControllerConfigSchema.validateSync(json);
  }

  let cfgV2: ControllerConfigTypeV2 | undefined;
  if (ControllerConfigSchemaV1.isValidSync(json, { strict: true })) {
    log.debug("got v1 controller config, upgrading...");
    cfgV2 = V1toV2(ControllerConfigSchemaV1.validateSync(json));
  } else if (
    ControllerConfigSchemaV1alpha.isValidSync(json, { strict: true })
  ) {
    log.debug("got v1alpha controller config, upgrading...");
    cfgV2 = V1alphatoV2(ControllerConfigSchemaV1alpha.validateSync(json));
  } else if (ControllerConfigSchemaV2.isValidSync(json, { strict: true })) {
    log.debug("got v2 controller config, upgrading...");
    cfgV2 = ControllerConfigSchemaV2.validateSync(json);
  }

  if (cfgV2 !== undefined) {
    log.debug("upgrading from v2 to v3");
    return V2toV3(cfgV2);
  }

  log.debug(
    "no valid controller config schema found, attempting sync with latest"
  );

  // Possible user error. Parse again and it'll throw a meaningful error
  // message.
  return LatestControllerConfigSchema.validateSync(json, { strict: true });
}
