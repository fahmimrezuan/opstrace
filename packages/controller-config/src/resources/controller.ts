import {
  ResourceCollection,
  Deployment,
  ClusterRoleBinding,
  ServiceAccount,
  Secret
} from "@opstrace/kubernetes";
import { KubeConfig } from "@kubernetes/client-node";
import { getImagePullSecrets, dockerHubCredsSecret } from "./dockerhub";

export const CONTROLLER_NAME = "opstrace-controller";
export const CONTROLLER_NAMESPACE = "kube-system";
export const POSTGRES_SECRET_NAME = "postgres-secret";

export const isControllerDeployment = (d: Deployment): boolean =>
  d.name === CONTROLLER_NAME && d.namespace === CONTROLLER_NAMESPACE;

export const isPostgresSecret = (s: Secret): boolean =>
  s.name === POSTGRES_SECRET_NAME && s.namespace === CONTROLLER_NAMESPACE;

export function ControllerResources({
  controllerImage,
  opstraceClusterName,
  postgresEndpoint,
  kubeConfig
}: {
  controllerImage: string;
  opstraceClusterName: string;
  postgresEndpoint: string;
  kubeConfig: KubeConfig;
}): ResourceCollection {
  const collection = new ResourceCollection();
  const namespace = CONTROLLER_NAMESPACE;
  const name = CONTROLLER_NAME;

  const controllerCmdlineArgs = [`${opstraceClusterName}`];

  // Create dockerhub credentials secret if we have the credentials set
  // as environment variables
  collection.add(dockerHubCredsSecret(kubeConfig));

  // Create the Postgres secret in the kube-system namespace.
  // The controller will copy these to the application namespace when it starts.
  const postgresEndpointSecret = new Secret(
    {
      apiVersion: "v1",
      data: {
        endpoint: Buffer.from(postgresEndpoint).toString("base64")
      },
      kind: "Secret",
      metadata: {
        name: POSTGRES_SECRET_NAME,
        namespace: namespace
      }
    },
    kubeConfig
  );

  // We don't want this value to change once it exists, but it can still be edited manually if needed.
  postgresEndpointSecret.setImmutable();
  collection.add(postgresEndpointSecret);

  collection.add(
    new Deployment(
      {
        apiVersion: "apps/v1",
        kind: "Deployment",
        metadata: {
          name,
          namespace,
          labels: {
            "k8s-app": "opstrace-controller"
          }
        },
        spec: {
          replicas: 1,
          strategy: {
            type: "Recreate"
          },
          selector: {
            matchLabels: {
              name
            }
          },
          template: {
            metadata: {
              labels: {
                name
              }
            },
            spec: {
              imagePullSecrets: getImagePullSecrets(),
              serviceAccountName: "opstrace-controller",
              containers: [
                {
                  name: "opstrace-controller",
                  image: `${controllerImage}`,
                  imagePullPolicy: "IfNotPresent",
                  command: ["node", "./cmd.js"],
                  args: controllerCmdlineArgs,
                  resources: {
                    limits: {
                      cpu: "1",
                      memory: "1Gi"
                    },
                    requests: {
                      cpu: "0.5",
                      memory: "500Mi"
                    }
                  },
                  ports: [
                    {
                      name: "metrics",
                      containerPort: 8900
                    },
                    {
                      name: "readiness",
                      containerPort: 9000
                    }
                  ],
                  env: [
                    {
                      name: "POSTGRES_ENDPOINT",
                      valueFrom: {
                        secretKeyRef: {
                          name: POSTGRES_SECRET_NAME,
                          key: "endpoint"
                        }
                      }
                    },
                    {
                      name: "ALERTMANAGER_ENDPOINT",
                      value:
                        "http://alertmanager.cortex.svc.cluster.local/api/v1/alerts"
                    },
                    {
                      name: "RULER_ENDPOINT",
                      value:
                        "http://ruler.cortex.svc.cluster.local/api/v1/rules"
                    },
                    {
                      name: "CLICKHOUSE_ENDPOINT",
                      value: "http://cluster.clickhouse.svc.cluster.local:8123/"
                    }
                  ],
                  readinessProbe: {
                    httpGet: {
                      path: "/ready",
                      // eslint-disable-next-line @typescript-eslint/no-explicit-any
                      port: "readiness" as any,
                      scheme: "HTTP"
                    }
                  }
                }
              ]
            }
          }
        }
      },
      kubeConfig
    )
  );

  collection.add(
    new ServiceAccount(
      {
        apiVersion: "v1",
        kind: "ServiceAccount",
        metadata: {
          name: "opstrace-controller",
          namespace
        }
      },
      kubeConfig
    )
  );

  collection.add(
    new ClusterRoleBinding(
      {
        apiVersion: "rbac.authorization.k8s.io/v1",
        kind: "ClusterRoleBinding",
        metadata: {
          name: "opstrace-controller-clusteradmin-binding"
        },
        roleRef: {
          apiGroup: "rbac.authorization.k8s.io",
          kind: "ClusterRole",
          name: "cluster-admin"
        },
        subjects: [
          {
            kind: "ServiceAccount",
            name: "opstrace-controller",
            namespace
          }
        ]
      },
      kubeConfig
    )
  );

  return collection;
}
