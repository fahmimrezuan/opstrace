import * as yup from "yup";
import { GCPConfig } from "@opstrace/gcp";
import { AWSConfig } from "@opstrace/aws";

export const ControllerConfigSchemaV2 = yup
  .object({
    name: yup.string(),
    target: yup
      .mixed<"gcp" | "aws">()
      .oneOf(["gcp", "aws"])
      .required("must specify a target (gcp | aws)"),
    region: yup.string().required("must specify region"),
    metricRetentionDays: yup
      .number()
      .required("must specify metric retention in number of days"),
    dnsName: yup.string().required(),

    // Added later to V2, but is an optional parameter, therefore not strictly
    // justifying a new schema version. Content and name need to be iterated on.
    custom_dns_name: yup.string().notRequired(),
    custom_auth0_client_id: yup.string().notRequired(),
    custom_auth0_domain: yup.string().notRequired(),
    // Adding to V2 since we don't need to worry about breaking changes to V2.
    gitlab: yup
      .object({
        instance_url: yup.string().required(),
        group_allowed_access: yup.string().required(),
        group_allowed_system_access: yup.string().required(),
        oauth_client_id: yup.string().required(),
        oauth_client_secret: yup.string().required()
      })
      .required(),

    terminate: yup.bool().default(false),
    // https://stackoverflow.com/a/63944333/145400 `data_api_authn_pubkey_pem`
    // is optional, is a legacy controller config option, a noop right now
    // (future: set fallback key for authenticator).
    data_api_authn_pubkey_pem: yup.string().optional(),
    tenant_api_authenticator_pubkey_set_json: yup
      .string()
      .typeError()
      .strict(true),
    disable_data_api_authentication: yup.bool().required(),
    uiSourceIpFirewallRules: yup.array(yup.string()).ensure(),
    apiSourceIpFirewallRules: yup.array(yup.string()).ensure(),
    postgreSQLEndpoint: yup.string().notRequired(),
    opstraceDBName: yup.string().notRequired(),
    envLabel: yup.string(),
    // Note: remove one of cert_issuer and `tlsCertificateIssuer`.
    cert_issuer: yup
      .string()
      .oneOf(["letsencrypt-prod", "letsencrypt-staging"])
      .required(),
    tlsCertificateIssuer: yup
      .mixed<"letsencrypt-staging" | "letsencrypt-prod">()
      .oneOf(["letsencrypt-staging", "letsencrypt-prod"])
      .required(),
    infrastructureName: yup.string().required(),
    aws: yup.mixed<AWSConfig | undefined>(),
    gcp: yup.mixed<GCPConfig | undefined>(),
    controllerTerminated: yup.bool().default(false)
  })
  .noUnknown()
  .defined();

export type ControllerConfigTypeV2 = yup.InferType<
  typeof ControllerConfigSchemaV2
>;
