import { createReducer, ActionType } from "typesafe-actions";
import { configMapActions } from "@opstrace/kubernetes";
import { LatestControllerConfigType } from "../schema";
import { isConfigStorage, deserialize } from "../utils";

type Actions = ActionType<typeof configMapActions>;

type ConfigState = {
  config: LatestControllerConfigType | undefined;
  loading: boolean;
  backendExists: boolean | null;
  error: Error | null;
};

const configInitialState: ConfigState = {
  config: undefined,
  loading: false,
  backendExists: null, // Indicates that we don't know yet
  error: null
};

export const configReducer = createReducer<ConfigState, Actions>(
  configInitialState
)
  .handleAction(
    configMapActions.fetch.request,
    (state): ConfigState => ({
      ...state,
      loading: true
    })
  )
  .handleAction(
    configMapActions.fetch.success,
    (state, action): ConfigState => {
      const configMap = action.payload.resources.find(isConfigStorage);
      let config: LatestControllerConfigType | undefined = undefined;
      if (configMap) {
        config = deserialize(configMap);
      }

      return {
        ...state,
        backendExists: configMap ? true : false,
        config,
        error: null,
        loading: false
      };
    }
  )
  .handleAction(
    configMapActions.fetch.failure,
    (state, action): ConfigState => ({
      ...state,
      ...action.payload,
      loading: false
    })
  )
  .handleAction(
    [configMapActions.onUpdated, configMapActions.onAdded],
    (state, action): ConfigState => {
      const configMap = isConfigStorage(action.payload)
        ? action.payload
        : false;

      if (!configMap) {
        return state;
      }
      const config = deserialize(configMap);

      return {
        ...state,
        backendExists: true,
        config,
        error: null,
        loading: false
      };
    }
  );
