import {
  Deployment,
  Ingress,
  ResourceCollection,
  Service,
  V1ServicemonitorResource,
  withPodAntiAffinityRequired
} from "@opstrace/kubernetes";
import { State } from "../../reducer";
import { getApiDomain, getNodeCount, getTenantNamespace } from "../../helpers";
import { nodecountToReplicacount } from "./index";
import { Tenant } from "@opstrace/tenants";
import { KubeConfig } from "@kubernetes/client-node";
import { DockerImages, getImagePullSecrets } from "@opstrace/controller-config";

export function CortexAPIResources(
  state: State,
  kubeConfig: KubeConfig,
  namespace: string
): ResourceCollection {
  const collection = new ResourceCollection();

  const api = "cortex";
  const name = `${api}-api`;
  const cortexQuerierUrl = "http://query-frontend.cortex.svc.cluster.local";
  const cortexDistributorUrl = "http://distributor.cortex.svc.cluster.local";

  collection.add(
    new Deployment(
      {
        apiVersion: "apps/v1",
        kind: "Deployment",
        metadata: {
          name: name,
          namespace,
          labels: {
            app: name
          }
        },
        spec: {
          replicas: nodecountToReplicacount(getNodeCount(state)),
          selector: {
            matchLabels: {
              app: name
            }
          },
          template: {
            metadata: {
              labels: {
                app: name
              }
            },
            spec: {
              affinity: withPodAntiAffinityRequired({
                app: name
              }),
              imagePullSecrets: getImagePullSecrets(),
              containers: [
                {
                  name,
                  image: DockerImages.cortexApiProxy,
                  imagePullPolicy: "IfNotPresent",
                  args: [
                    "-listen=:8080",
                    // Upstream endpoints for the opstrace cortex proxy
                    `-cortex-querier-url=${cortexQuerierUrl}`,
                    `-cortex-distributor-url=${cortexDistributorUrl}`
                  ],
                  ports: [
                    {
                      name: "http",
                      protocol: "TCP",
                      containerPort: 8080
                    }
                  ],
                  readinessProbe: {
                    httpGet: {
                      path: "/metrics",
                      // eslint-disable-next-line @typescript-eslint/no-explicit-any
                      port: 8080 as any,
                      scheme: "HTTP"
                    },
                    timeoutSeconds: 1,
                    periodSeconds: 10,
                    successThreshold: 1,
                    failureThreshold: 3
                  },
                  livenessProbe: {
                    httpGet: {
                      path: "/metrics",
                      // eslint-disable-next-line @typescript-eslint/no-explicit-any
                      port: 8080 as any,
                      scheme: "HTTP"
                    },
                    timeoutSeconds: 1,
                    periodSeconds: 10,
                    successThreshold: 1,
                    failureThreshold: 3
                  },
                  resources: {}
                }
              ]
            }
          }
        }
      },
      kubeConfig
    )
  );

  collection.add(
    new Service(
      {
        apiVersion: "v1",
        kind: "Service",
        metadata: {
          name,
          labels: {
            app: name,
            job: `${namespace}.${name}`
          },
          namespace
        },
        spec: {
          ports: [
            {
              name: "http",
              port: 8080,
              protocol: "TCP",
              // eslint-disable-next-line @typescript-eslint/no-explicit-any
              targetPort: 8080 as any
            }
          ],
          selector: {
            app: name
          }
        }
      },
      kubeConfig
    )
  );

  collection.add(
    new V1ServicemonitorResource(
      {
        apiVersion: "monitoring.coreos.com/v1",
        kind: "ServiceMonitor",
        metadata: {
          labels: {
            app: name,
            tenant: "system"
          },
          name,
          namespace
        },
        spec: {
          endpoints: [
            {
              interval: "30s",
              path: "/metrics",
              port: "http"
            }
          ],
          jobLabel: "job",
          namespaceSelector: {
            matchNames: [namespace]
          },
          selector: {
            matchLabels: {
              app: name
            }
          }
        }
      },
      kubeConfig
    )
  );

  return collection;
}

export function CortexTenantIngressResources(
  state: State,
  tenant: Tenant,
  kubeConfig: KubeConfig,
  targetNamespace: string
): ResourceCollection {
  const collection = new ResourceCollection();

  const namespace = getTenantNamespace(tenant);
  const api = "cortex";
  const name = `${api}-api`;

  const ingressAnnotations: Record<string, string> = {
    "kubernetes.io/ingress.class": "api",
    "external-dns.alpha.kubernetes.io/ttl": "30",
    "nginx.ingress.kubernetes.io/client-body-buffer-size": "10m",

    // On successful auth, assign the correct X-Scope-OrgID header in the backend request.
    // This is used by cortex-api to identify the tenant.
    // WARNING: Don't forget the trailing semicolon or else routes will silently fail.
    "nginx.ingress.kubernetes.io/configuration-snippet": `more_set_input_headers "X-Scope-OrgID: ${tenant.name}";`,

    // Forward "Authorization: Bearer xxxxx" API token to Gatekeeper for validation
    "nginx.ingress.kubernetes.io/auth-url":
      "http://gatekeeper.application.svc.cluster.local:3001/apiauth/check",
    // In the gatekeeper auth request, specify the tenant in an X-Scope-OrgID header.
    "nginx.ingress.kubernetes.io/auth-snippet": `proxy_set_header X-Scope-OrgID "${tenant.name}";`,
    // Cache the auth response to avoid querying the auth endpoint too frequently.
    "nginx.ingress.kubernetes.io/auth-cache-key":
      "$remote_user$http_authorization",
    // 401: If something fails auth, it's unlikely to start soon, since new tokens are randomly generated.
    // 20x: If a token is revoked or expires, allow them to start failing after at most 2 minutes
    // 50x: If Grafana is returning 500 errors, allow them to clear up quickly when Grafana comes back,
    //      but allow some caching to avoid Grafana getting hammered with retries when down.
    "nginx.ingress.kubernetes.io/auth-cache-duration":
      "401 5m, 200 202 2m, 500 503 30s"
  };

  const apiHost = getApiDomain(api, tenant, state);
  collection.add(
    new Ingress(
      {
        apiVersion: "networking.k8s.io/v1beta1",
        kind: "Ingress",
        metadata: {
          name: api,
          namespace,
          annotations: ingressAnnotations
        },
        spec: {
          tls: [
            {
              hosts: [apiHost],
              secretName: "https-cert"
            }
          ],
          rules: [
            {
              host: apiHost,
              http: {
                paths: [
                  {
                    backend: {
                      serviceName: name,
                      // eslint-disable-next-line @typescript-eslint/no-explicit-any
                      servicePort: 8080 as any
                    },
                    pathType: "ImplementationSpecific",
                    path: "/"
                  }
                ]
              }
            }
          ]
        }
      },
      kubeConfig
    )
  );

  // Points to the multitenant Cortex API service.
  // This allows the tenant Ingress to route to the shared API pod.
  // Requests must include an "X-Scope-OrgID" header to identify the tenant, this is handled at the Ingress.
  collection.add(
    new Service(
      {
        apiVersion: "v1",
        kind: "Service",
        metadata: {
          name,
          namespace
        },
        spec: {
          type: "ExternalName",
          externalName: `${name}.${targetNamespace}.svc.cluster.local`
        }
      },
      kubeConfig
    )
  );

  return collection;
}
