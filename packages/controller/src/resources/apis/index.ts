import { KubeConfig } from "@kubernetes/client-node";

import { ResourceCollection } from "@opstrace/kubernetes";
import { getTenantNamespace } from "../../helpers";
import { State } from "../../reducer";

// End-user APIs that are authenticated using API Tokens
import { ConfigAPIResources, ConfigTenantIngressResources } from "./config";
import { CortexAPIResources, CortexTenantIngressResources } from "./cortex";
import { DDAPIResources, DDTenantIngressResources } from "./dd";
import { TracingAPIResources, TracingTenantIngressResources } from "./tracing";

/* Translate node count into replica count*/
export function nodecountToReplicacount(nodecount: number): number {
  const NC_RC_MAP: Record<string, number> = {
    "1": 1,
    "2": 2,
    "3": 2,
    "4": 3
  };

  const ncstring = nodecount.toFixed(0);
  if (ncstring in NC_RC_MAP) {
    return NC_RC_MAP[ncstring];
  }

  return Math.floor(nodecount / 2);
}

export function APIResources(
  state: State,
  kubeConfig: KubeConfig,
  namespace: string
): ResourceCollection {
  const collection = new ResourceCollection();

  // Multitenant pods
  collection.add(ConfigAPIResources(kubeConfig, namespace));
  collection.add(CortexAPIResources(state, kubeConfig, namespace));
  collection.add(DDAPIResources(state, kubeConfig, namespace));

  // Per-tenant resources
  state.tenants.list.tenants.forEach(tenant => {
    // Tenants get an ingress+service that points to the multitenant pods
    collection.add(
      ConfigTenantIngressResources(state, tenant, kubeConfig, namespace)
    );
    collection.add(
      CortexTenantIngressResources(state, tenant, kubeConfig, namespace)
    );
    collection.add(
      DDTenantIngressResources(state, tenant, kubeConfig, namespace)
    );

    // TODO: Figure out how to run otel-collector output to tenant jaegers in a multitenant way..
    //       For now we run it per-tenant and have a fixed output to the matching tenant.
    collection.add(TracingAPIResources(kubeConfig, getTenantNamespace(tenant)));
    collection.add(TracingTenantIngressResources(state, tenant, kubeConfig));
  });

  return collection;
}
