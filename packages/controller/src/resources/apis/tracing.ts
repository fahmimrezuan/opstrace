import yaml from "js-yaml";
import {
  ResourceCollection,
  ConfigMap,
  Deployment,
  Ingress,
  Service,
  V1ServicemonitorResource,
  withPodAntiAffinityRequired
} from "@opstrace/kubernetes";
import { State } from "../../reducer";
import { Tenant } from "@opstrace/tenants";
import { KubeConfig } from "@kubernetes/client-node";
import { getApiDomain, getTenantNamespace } from "../../helpers";
import { DockerImages, getImagePullSecrets } from "@opstrace/controller-config";

export function TracingAPIResources(
  kubeConfig: KubeConfig,
  namespace: string
): ResourceCollection {
  const collection = new ResourceCollection();

  const api = "tracing";
  const name = `${api}-api`;

  const config = {
    receivers: {
      otlp: {
        protocols: {
          grpc: {
            endpoint: ":4317"
          }
        }
      }
    },
    processors: {
      // The memory limiter processor is used to prevent out of memory situations on the collector.
      // Keep in mind that if the soft limit is crossed, the collector will return errors to all receive operations until enough memory is freed.
      memory_limiter: {
        // Time between measurements of memory usage. The recommended value is 1 second.
        // default 0s
        check_interval: "1s",
        // Maximum amount of memory, in MiB, targeted to be allocated by the process heap.
        // default 0
        limit_mib: 512,
        // Maximum spike expected between the measurements of memory usage. The value must be less than limit_mib.
        // default 20% of limit_mib
        spike_limit_mib: 128
      },
      tail_sampling: {
        // Wait time since the first span of a trace before making a sampling decision
        decision_wait: "10s",
        // Number of traces kept in memory
        num_traces: 100,
        // Expected number of new traces (helps in allocating data structures)
        expected_new_traces_per_sec: 10,
        policies: [
          {
            name: "rate_100spans",
            type: "rate_limiting",
            rate_limiting: {
              spans_per_second: 100
            }
          }
        ]
      },
      // Keep batches for up to 5s, or 1000 entries, whichever comes first.
      batch: {
        send_batch_size: 1000,
        timeout: "5s"
      }
    },
    exporters: {
      jaeger: {
        // TODO: could instead use jaeger-collector-headless?
        endpoint: `jaeger-collector.${namespace}.svc.cluster.local:14250`,
        tls: {
          insecure: true
        }
      },
      // TODO(nickbp): remove after testing that tracing works E2E
      logging: {
        logLevel: "debug"
      }
    },
    extensions: {
      // It is highly recommended to configure ballastextension as well as the memory_limiter processor on every collector.
      // The ballast should be configured to be 1/3 to 1/2 of the memory allocated to the collector.
      memory_ballast: {
        size_mib: 128
      }
    },
    service: {
      extensions: ["memory_ballast"],
      pipelines: {
        traces: {
          receivers: ["otlp"],
          processors: ["memory_limiter", "tail_sampling", "batch"],
          exporters: ["jaeger", "logging"]
        }
      }
    }
  };

  collection.add(
    new ConfigMap(
      {
        apiVersion: "v1",
        kind: "ConfigMap",
        metadata: {
          name,
          namespace,
          labels: {
            "k8s-app": name
          }
        },
        data: {
          "config.yaml": yaml.safeDump(config)
        }
      },
      kubeConfig
    )
  );

  collection.add(
    new Deployment(
      {
        apiVersion: "apps/v1",
        kind: "Deployment",
        metadata: {
          name: name,
          namespace,
          labels: {
            "k8s-app": name
          }
        },
        spec: {
          // Single replica per tenant due to use of tail_sampling processor:
          // "Today, this processor only works with a single instance of the collector."
          // In practice the tenants are rate limited anyway so this isn't expected to be a problem.
          replicas: 1,
          selector: {
            matchLabels: {
              "k8s-app": name
            }
          },
          // Since we only have one replica, ensure that we keep it running during rollouts
          strategy: {
            type: "RollingUpdate",
            rollingUpdate: {
              // eslint-disable-next-line @typescript-eslint/no-explicit-any
              maxSurge: 1 as any,
              // eslint-disable-next-line @typescript-eslint/no-explicit-any
              maxUnavailable: 0 as any
            }
          },
          template: {
            metadata: {
              labels: {
                "k8s-app": name
              }
            },
            spec: {
              affinity: withPodAntiAffinityRequired({
                "k8s-app": name
              }),
              imagePullSecrets: getImagePullSecrets(),
              containers: [
                {
                  name,
                  image: DockerImages.tracingApi,
                  imagePullPolicy: "IfNotPresent",
                  args: ["--config=/etc/collector/config.yaml"],
                  ports: [
                    {
                      name: "otlp-grpc",
                      protocol: "TCP",
                      containerPort: 4317
                    },
                    {
                      name: "metrics",
                      protocol: "TCP",
                      containerPort: 8888
                    }
                  ],
                  readinessProbe: {
                    httpGet: {
                      path: "/metrics",
                      // eslint-disable-next-line @typescript-eslint/no-explicit-any
                      port: "metrics" as any,
                      scheme: "HTTP"
                    },
                    timeoutSeconds: 1,
                    periodSeconds: 10,
                    successThreshold: 1,
                    failureThreshold: 3
                  },
                  volumeMounts: [
                    {
                      mountPath: "/etc/collector",
                      name: "config"
                    }
                  ]
                }
              ],
              volumes: [
                {
                  configMap: {
                    name
                  },
                  name: "config"
                }
              ]
            }
          }
        }
      },
      kubeConfig
    )
  );

  collection.add(
    new Service(
      {
        apiVersion: "v1",
        kind: "Service",
        metadata: {
          name,
          labels: {
            "k8s-app": name,
            job: `${namespace}.${name}`
          },
          namespace
        },
        spec: {
          ports: [
            {
              name: "otlp-grpc",
              port: 4317,
              protocol: "TCP",
              // eslint-disable-next-line @typescript-eslint/no-explicit-any
              targetPort: "otlp-grpc" as any
            },
            {
              name: "metrics",
              port: 8888,
              protocol: "TCP",
              // eslint-disable-next-line @typescript-eslint/no-explicit-any
              targetPort: "metrics" as any
            }
          ],
          selector: {
            "k8s-app": name
          }
        }
      },
      kubeConfig
    )
  );

  collection.add(
    new V1ServicemonitorResource(
      {
        apiVersion: "monitoring.coreos.com/v1",
        kind: "ServiceMonitor",
        metadata: {
          labels: {
            "k8s-app": name,
            tenant: "system"
          },
          name,
          namespace
        },
        spec: {
          endpoints: [
            {
              interval: "30s",
              path: "/metrics",
              port: "metrics"
            }
          ],
          jobLabel: "job",
          namespaceSelector: {
            matchNames: [namespace]
          },
          selector: {
            matchLabels: {
              "k8s-app": name
            }
          }
        }
      },
      kubeConfig
    )
  );

  return collection;
}

export function TracingTenantIngressResources(
  state: State,
  tenant: Tenant,
  kubeConfig: KubeConfig
): ResourceCollection {
  const collection = new ResourceCollection();

  const namespace = getTenantNamespace(tenant);
  const api = "tracing";

  const ingressAnnotations: Record<string, string> = {
    // Use the 'tracing' ingress controller which listens at port 4317
    // See also nginxIngress.ts.
    "kubernetes.io/ingress.class": "tracing",
    "external-dns.alpha.kubernetes.io/ttl": "30",
    "nginx.ingress.kubernetes.io/client-body-buffer-size": "10m",

    // Tracing input is OTLP-over-gRPC
    // see also https://kubernetes.github.io/ingress-nginx/examples/grpc/
    "nginx.ingress.kubernetes.io/backend-protocol": "GRPC",

    // Forward "Authorization: Bearer xxxxx" API token to Gatekeeper for validation
    "nginx.ingress.kubernetes.io/auth-url":
      "http://gatekeeper.application.svc.cluster.local:3001/apiauth/check",
    // In the gatekeeper auth request, specify the tenant in an X-Scope-OrgID header.
    "nginx.ingress.kubernetes.io/auth-snippet": `proxy_set_header X-Scope-OrgID "${tenant.name}";`,
    // Cache the auth response to avoid querying the auth endpoint too frequently.
    "nginx.ingress.kubernetes.io/auth-cache-key":
      "$remote_user$http_authorization",
    // 401: If something fails auth, it's unlikely to start soon, since new tokens are randomly generated.
    // 20x: If a token is revoked or expires, allow them to start failing after at most 2 minutes
    // 50x: If Grafana is returning 500 errors, allow them to clear up quickly when Grafana comes back,
    //      but allow some caching to avoid Grafana getting hammered with retries when down.
    "nginx.ingress.kubernetes.io/auth-cache-duration":
      "401 5m, 200 202 2m, 500 503 30s"
  };

  const apiHost = getApiDomain(api, tenant, state);
  collection.add(
    new Ingress(
      {
        apiVersion: "networking.k8s.io/v1beta1",
        kind: "Ingress",
        metadata: {
          name: api,
          namespace,
          annotations: ingressAnnotations
        },
        spec: {
          tls: [
            {
              hosts: [apiHost],
              secretName: "https-cert"
            }
          ],
          rules: [
            {
              host: apiHost,
              http: {
                paths: [
                  {
                    backend: {
                      serviceName: `${api}-api`,
                      // eslint-disable-next-line @typescript-eslint/no-explicit-any
                      servicePort: "otlp-grpc" as any
                    },
                    pathType: "ImplementationSpecific",
                    path: "/"
                  }
                ]
              }
            }
          ]
        }
      },
      kubeConfig
    )
  );

  return collection;
}
