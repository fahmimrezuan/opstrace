import {
  Deployment,
  Ingress,
  ResourceCollection,
  Service,
  V1ServicemonitorResource,
  withPodAntiAffinityRequired
} from "@opstrace/kubernetes";
import { State } from "../../reducer";
import { getApiDomain, getNodeCount, getTenantNamespace } from "../../helpers";
import { nodecountToReplicacount } from "./index";
import { Tenant } from "@opstrace/tenants";
import { KubeConfig } from "@kubernetes/client-node";
import { DockerImages, getImagePullSecrets } from "@opstrace/controller-config";

export function DDAPIResources(
  state: State,
  kubeConfig: KubeConfig,
  namespace: string
): ResourceCollection {
  const collection = new ResourceCollection();

  const api = "dd";
  const name = `${api}-api`;
  const remoteWriteURL =
    "http://distributor.cortex.svc.cluster.local/api/v1/push";

  collection.add(
    new Deployment(
      {
        apiVersion: "apps/v1",
        kind: "Deployment",
        metadata: {
          name: name,
          namespace,
          labels: {
            app: name
          }
        },
        spec: {
          replicas: nodecountToReplicacount(getNodeCount(state)),
          selector: {
            matchLabels: {
              app: name
            }
          },
          template: {
            metadata: {
              labels: {
                app: name
              }
            },
            spec: {
              imagePullSecrets: getImagePullSecrets(),
              affinity: withPodAntiAffinityRequired({
                app: name
              }),
              containers: [
                {
                  name,
                  image: DockerImages.ddApi,
                  imagePullPolicy: "IfNotPresent",
                  args: [
                    "-listen=:8080",
                    `-prom-remote-write-url=${remoteWriteURL}`
                  ],
                  ports: [
                    {
                      name: "http",
                      protocol: "TCP",
                      containerPort: 8080
                    }
                  ],
                  readinessProbe: {
                    httpGet: {
                      path: "/metrics",
                      // eslint-disable-next-line @typescript-eslint/no-explicit-any
                      port: 8080 as any,
                      scheme: "HTTP"
                    },
                    timeoutSeconds: 1,
                    periodSeconds: 10,
                    successThreshold: 1,
                    failureThreshold: 3
                  },
                  livenessProbe: {
                    httpGet: {
                      path: "/metrics",
                      // eslint-disable-next-line @typescript-eslint/no-explicit-any
                      port: 8080 as any,
                      scheme: "HTTP"
                    },
                    timeoutSeconds: 1,
                    periodSeconds: 10,
                    successThreshold: 1,
                    failureThreshold: 3
                  },
                  resources: {}
                }
              ]
            }
          }
        }
      },
      kubeConfig
    )
  );

  collection.add(
    new Service(
      {
        apiVersion: "v1",
        kind: "Service",
        metadata: {
          name: name,
          labels: {
            app: name,
            job: `${namespace}.${name}`
          },
          namespace
        },
        spec: {
          ports: [
            {
              name: "http",
              port: 8080,
              protocol: "TCP",
              // eslint-disable-next-line @typescript-eslint/no-explicit-any
              targetPort: 8080 as any
            }
          ],
          selector: {
            app: name
          }
        }
      },
      kubeConfig
    )
  );

  collection.add(
    new V1ServicemonitorResource(
      {
        apiVersion: "monitoring.coreos.com/v1",
        kind: "ServiceMonitor",
        metadata: {
          labels: {
            app: name,
            tenant: "system"
          },
          name: name,
          namespace
        },
        spec: {
          endpoints: [
            {
              interval: "30s",
              path: "/metrics",
              port: "http"
            }
          ],
          jobLabel: "job",
          namespaceSelector: {
            matchNames: [namespace]
          },
          selector: {
            matchLabels: {
              app: name
            }
          }
        }
      },
      kubeConfig
    )
  );

  return collection;
}

export function DDTenantIngressResources(
  state: State,
  tenant: Tenant,
  kubeConfig: KubeConfig,
  targetNamespace: string
): ResourceCollection {
  const collection = new ResourceCollection();

  // Do not deploy DD API for system tenant.
  if (tenant.type === "SYSTEM") {
    return collection;
  }

  const namespace = getTenantNamespace(tenant);
  const api = "dd";
  const name = `${api}-api`;

  const ingressAnnotations: Record<string, string> = {
    "kubernetes.io/ingress.class": "api",
    "external-dns.alpha.kubernetes.io/ttl": "30",
    "nginx.ingress.kubernetes.io/client-body-buffer-size": "10m",

    // 1. When forwarding the auth request, get the token from the Datadog '?api_key=...' query string
    //    and pass it to the auth service as an 'Authorization: Bearer ...' header.
    //    This allows us to support the datadog agent which passes api tokens via '?api_key=...'
    //    See also: https://stackoverflow.com/questions/54180469/nginx-decode-url-query-parameter-and-forward-it-as-request-header
    // 2. On successful auth, assign the correct X-Scope-OrgID header in the backend request.
    //    This is used by ddapi to identify the tenant.
    //    WARNING: Don't forget the trailing semicolon or else routes will silently fail.
    "nginx.ingress.kubernetes.io/configuration-snippet": `
more_set_input_headers "X-Scope-OrgID: ${tenant.name}";

# extract "?api_key=..." query string value for "Authorization: Bearer ..." header to auth service
set $api_key_content $arg_api_key;
if ($args ~* (.*)(^|&)api_key=[^&]*(\\2|$)&?(.*)) {
  # remove api_key from query to be forwarded
  set $args $1$3$4;
}
# ideally we'd have a rewrite_by_lua_block for unescaping the api_key URI string,
# but the resulting config fails due to duplicate rewrite_by_lua_block`,

    // Forward "Authorization: Bearer xxxxx" API token to Gatekeeper for validation
    "nginx.ingress.kubernetes.io/auth-url":
      "http://gatekeeper.application.svc.cluster.local:3001/apiauth/check",
    // In the gatekeeper auth request, specify the tenant in an X-Scope-OrgID header.
    // ddapi-specific: Ensure the extracted api_key value is sent as an Authorization header.
    "nginx.ingress.kubernetes.io/auth-snippet": `
proxy_set_header Authorization "Bearer $api_key_content";
proxy_set_header X-Scope-OrgID "${tenant.name}";`,
    // Cache the auth response to avoid querying the auth endpoint too frequently.
    // Note that we must explicitly key on the api_key value.
    // Keying on $http_authorization doesn't work because the original request doesn't specify that.
    "nginx.ingress.kubernetes.io/auth-cache-key":
      "$remote_user$api_key_content",
    // 401: If something fails auth, it's unlikely to start soon, since new tokens are randomly generated.
    // 20x: If a token is revoked or expires, allow them to start failing after at most 2 minutes
    // 50x: If Grafana is returning 500 errors, allow them to clear up quickly when Grafana comes back,
    //      but allow some caching to avoid Grafana getting hammered with retries when down.
    "nginx.ingress.kubernetes.io/auth-cache-duration":
      "401 5m, 200 202 2m, 500 503 30s"
  };

  const apiHost = getApiDomain(api, tenant, state);
  collection.add(
    new Ingress(
      {
        apiVersion: "networking.k8s.io/v1beta1",
        kind: "Ingress",
        metadata: {
          name: api,
          namespace,
          annotations: ingressAnnotations
        },
        spec: {
          tls: [
            {
              hosts: [apiHost],
              secretName: "https-cert"
            }
          ],
          rules: [
            {
              host: apiHost,
              http: {
                paths: [
                  {
                    backend: {
                      serviceName: name,
                      // eslint-disable-next-line @typescript-eslint/no-explicit-any
                      servicePort: 8080 as any
                    },
                    pathType: "ImplementationSpecific",
                    path: "/"
                  }
                ]
              }
            }
          ]
        }
      },
      kubeConfig
    )
  );

  // Points to the multitenant DD API service.
  // This allows the tenant Ingress to route to the shared API pod.
  // Requests must include an "X-Scope-OrgID" header to identify the tenant, this is handled at the Ingress.
  collection.add(
    new Service(
      {
        apiVersion: "v1",
        kind: "Service",
        metadata: {
          name,
          namespace
        },
        spec: {
          type: "ExternalName",
          externalName: `${name}.${targetNamespace}.svc.cluster.local`
        }
      },
      kubeConfig
    )
  );

  return collection;
}
