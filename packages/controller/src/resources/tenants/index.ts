import { KubeConfig } from "@kubernetes/client-node";
import { ResourceCollection, Namespace } from "@opstrace/kubernetes";
import { getTenantNamespace } from "../../helpers";
import { State } from "../../reducer";

export function TenantResources(
  state: State,
  kubeConfig: KubeConfig
): ResourceCollection {
  const collection = new ResourceCollection();

  state.tenants.list.tenants.forEach(tenant => {
    const tenantNamespace = getTenantNamespace(tenant);

    collection.add(
      new Namespace(
        {
          apiVersion: "v1",
          kind: "Namespace",
          metadata: {
            name: tenantNamespace,
            labels: {
              tenant: tenant.name,
              "cert-manager.io/disable-validation": "true"
            }
          }
        },
        kubeConfig
      )
    );
  });

  return collection;
}
