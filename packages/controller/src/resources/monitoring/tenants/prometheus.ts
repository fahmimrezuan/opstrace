import {
  ResourceCollection,
  Service,
  V1PrometheusResource,
  V1ServicemonitorResource,
  ServiceAccount,
  Role,
  RoleBinding,
  withPodAntiAffinityRequired
} from "@opstrace/kubernetes";
import {
  getTenantNamespace,
  getPrometheusName,
  getDomain,
  getControllerConfig,
  getNodeAffinity
} from "../../../helpers";
import { State } from "../../../reducer";
import { Tenant } from "@opstrace/tenants";
import { KubeConfig } from "@kubernetes/client-node";
import { DockerImages } from "@opstrace/controller-config";

export function PrometheusResources(
  state: State,
  kubeConfig: KubeConfig,
  tenant: Tenant
): ResourceCollection {
  const collection = new ResourceCollection();
  const namespace = getTenantNamespace(tenant);
  const name = getPrometheusName(tenant);

  const { node_selector_terms } = getControllerConfig(state);

  const config = {
    // TODO: set number of replicas per shard with
    // https://github.com/opstrace/opstrace/pull/215
    replicas: 1,
    diskSize: "10Gi",
    resources: {}
  };

  let ruleNamespaceSelector = {};
  let serviceMonitorNamespaceSelector = {};

  // Remote reads are only used by the system tenant
  // Technically this will not work on non-system tenants, but we do not use it.
  let remoteReads: {
    url: string;
    headers?: Record<string, string>;
    bearerTokenFile?: string;
  }[] = [];

  if (tenant.type === "SYSTEM") {
    // Read metrics via the cortex-api pod with the system tenant configured.
    // This could also just go directly to Cortex...
    remoteReads = [
      {
        url: `http://cortex-api.application.svc.cluster.local:8080/api/v1/read`,
        headers: { "X-Scope-OrgID": tenant.name }
      }
    ];
  } else {
    ruleNamespaceSelector = serviceMonitorNamespaceSelector = {
      matchLabels: {
        tenant: tenant.name
      }
    };
  }

  collection.add(
    new Service(
      {
        apiVersion: "v1",
        kind: "Service",
        metadata: {
          labels: {
            prometheus: name,
            app: "prometheus"
          },
          name: "prometheus",
          namespace
        },
        spec: {
          ports: [
            {
              name: "web",
              port: 9090,
              // eslint-disable-next-line @typescript-eslint/no-explicit-any
              targetPort: "web" as any
            }
          ],
          selector: {
            app: "prometheus",
            prometheus: name
          },
          sessionAffinity: "ClientIP"
        }
      },
      kubeConfig
    )
  );

  collection.add(
    new V1ServicemonitorResource(
      {
        apiVersion: "monitoring.coreos.com/v1",
        kind: "ServiceMonitor",
        metadata: {
          labels: {
            app: "prometheus",
            prometheus: name,
            tenant: "system"
          },
          name: "prometheus",
          namespace
        },
        spec: {
          jobLabel: "prometheus",
          endpoints: [
            {
              interval: "30s",
              port: "web",
              path: "/prometheus/metrics"
            }
          ],
          selector: {
            matchLabels: {
              app: "prometheus"
            }
          }
        }
      },
      kubeConfig
    )
  );

  const resource = new V1PrometheusResource(
    {
      apiVersion: "monitoring.coreos.com/v1",
      kind: "Prometheus",
      metadata: {
        labels: {
          prometheus: name
        },
        name,
        namespace
      },
      spec: {
        externalUrl: `https://system.${getDomain(state)}/prometheus`,
        routePrefix: "/prometheus",
        affinity: withPodAntiAffinityRequired({
          prometheus: name
        }),
        storage: {
          volumeClaimTemplate: {
            spec: {
              storageClassName: "pd-ssd",
              accessModes: ["ReadWriteOnce"],
              resources: {
                requests: {
                  storage: config.diskSize
                }
              }
            }
          }
        },
        alerting: {
          alertmanagers: [
            {
              name: "alertmanager", // This is the alertmanager svc
              pathPrefix: "/alertmanager",
              namespace,
              port: "web"
            }
          ]
        },
        // Send metrics to the cortex-api pod with the correct tenant configured.
        remoteWrite: [
          {
            url: "http://cortex-api.application.svc.cluster.local:8080/api/v1/push",
            headers: { "X-Scope-OrgID": tenant.name }
          }
        ],
        remoteRead: remoteReads,
        image: DockerImages.prometheus,
        nodeSelector: {
          "kubernetes.io/os": "linux"
        },
        podMonitorSelector: {},
        probeNamespaceSelector: {},
        probeSelector: {},
        replicas: config.replicas,
        resources: config.resources,
        ruleNamespaceSelector,
        ruleSelector: {
          matchLabels: {
            prometheus: name,
            role: "alert-rules"
          }
        },
        securityContext: {
          fsGroup: 2000,
          runAsNonRoot: true,
          runAsUser: 1000
        },
        serviceAccountName: name,
        serviceMonitorNamespaceSelector,
        serviceMonitorSelector: {
          matchLabels: {
            tenant: tenant.name
          }
        },
        version: "v2.25.1"
      }
    },
    kubeConfig
  );

  if (node_selector_terms !== undefined) {
    const desiredAffinity = getNodeAffinity(
      node_selector_terms,
      "statefulset",
      namespace,
      name
    );

    if (desiredAffinity !== undefined) {
      resource.spec.spec.affinity = desiredAffinity;
    }
  }

  collection.add(resource);

  collection.add(
    new ServiceAccount(
      {
        apiVersion: "v1",
        kind: "ServiceAccount",
        metadata: {
          name,
          namespace
        }
      },
      kubeConfig
    )
  );

  collection.add(
    new Role(
      {
        apiVersion: "rbac.authorization.k8s.io/v1",
        kind: "Role",
        metadata: {
          name,
          namespace
        },
        rules: [
          {
            apiGroups: [""],
            resources: ["services", "endpoints", "pods"],
            verbs: ["get", "list", "watch"]
          }
        ]
      },
      kubeConfig
    )
  );
  collection.add(
    new RoleBinding(
      {
        apiVersion: "rbac.authorization.k8s.io/v1",
        kind: "RoleBinding",
        metadata: {
          name,
          namespace
        },
        roleRef: {
          apiGroup: "rbac.authorization.k8s.io",
          kind: "Role",
          name: name
        },
        subjects: [
          {
            kind: "ServiceAccount",
            name,
            namespace
          }
        ]
      },
      kubeConfig
    )
  );

  return collection;
}
