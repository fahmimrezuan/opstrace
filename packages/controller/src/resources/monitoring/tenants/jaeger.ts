import * as yaml from "js-yaml";
import {
  ConfigMap,
  ResourceCollection,
  Secret,
  Service,
  V1JaegerResource,
  V1ServicemonitorResource
} from "@opstrace/kubernetes";

import { State } from "../../../reducer";
import { Tenant } from "@opstrace/tenants";
import { generateSecretValue, getTenantNamespace } from "../../../helpers";
import { getTenantClickHouseName } from "../../../tasks/clickhouseTenants";
import { CLICKHOUSE_CLUSTER_SERVICE_NAME } from "../../clickhouse";
import { KubeConfig } from "@kubernetes/client-node";
import { DockerImages, getImagePullSecrets } from "@opstrace/controller-config";

const clickhousePasswordSecretName = "clickhouse-password";

export const clickhouseTracingDBName = "opstrace_tracing";

export function GetClickHousePassword(
  state: State,
  tenant: Tenant
): string | undefined {
  const secret = state.kubernetes.cluster.Secrets.resources.find(
    secret =>
      secret.name === clickhousePasswordSecretName &&
      secret.namespace === getTenantNamespace(tenant)
  );
  const b64val = secret?.data?.password ?? "";
  if (b64val === "") {
    // Secret not found or missing expected value
    return undefined;
  }
  return Buffer.from(b64val, "base64").toString("utf8");
}

export function JaegerResources(
  state: State,
  kubeConfig: KubeConfig,
  tenant: Tenant
): ResourceCollection {
  const collection = new ResourceCollection();
  const namespace = getTenantNamespace(tenant);
  const clickhouseTenant = getTenantClickHouseName(tenant);

  let clickhousePassword = GetClickHousePassword(state, tenant);
  if (clickhousePassword === undefined) {
    // Password not found, generate a new one.
    // This will be stored as a Secret and then appear in future GetClickhousePassword lookups.
    clickhousePassword = generateSecretValue();
  }

  const clickhousePasswordSecret = new Secret(
    {
      apiVersion: "v1",
      data: {
        // convert to b64 for secret
        password: Buffer.from(clickhousePassword).toString("base64")
      },
      kind: "Secret",
      metadata: {
        name: clickhousePasswordSecretName,
        namespace
      }
    },
    kubeConfig
  );
  // Set this as immutable to avoid changing later.
  // In any case, we don't support automatic updates/syncs of ClickHouse tenant passwords.
  clickhousePasswordSecret.setImmutable();
  collection.add(clickhousePasswordSecret);

  const jaegerClickhouseConfig = {
    address: `tcp://${CLICKHOUSE_CLUSTER_SERVICE_NAME}.clickhouse.svc.cluster.local:9000`,

    // maximum number of pending spans in memory before discarding data.
    // default is 10_000_000, but that was observed to use multiple gigs of memory if
    // clickhouse was too bogged down, to the point of evicting the pod on a 16GB host node.
    // meanwhile 100K results in ~1.2GB of memory usage when the queue is full/stuck.
    max_span_count: 100_000,

    // wait until 10k spans have been received, or after 5s, whichever comes first
    batch_write_size: 10_000,
    batch_flush_interval: "5s",

    // json or protobuf: guessing that protobuf is a bit faster/smaller
    encoding: "protobuf",

    username: clickhouseTenant,
    password: clickhousePassword,

    // shared tracing DB across tenants
    database: clickhouseTracingDBName,

    // include a tenant name for shared/multitenant tables
    // the value just needs to be a valid string that's distinct to the tenant
    tenant: tenant.name,

    metrics_endpoint: "0.0.0.0:9090",

    // use replication-enabled schemas in clickhouse
    replication: true,

    // days of data retention, or 0 to disable. configured at table setup via init sql scripts
    // reference: https://clickhouse.com/docs/en/engines/table-engines/mergetree-family/mergetree/#table_engine-mergetree-ttl
    ttl: state.config.config?.traceRetentionDays
  };

  // Store the jaeger-clickhouse configuration as a Secret because it contains tenant credentials
  // Don't set this as immutable to allow config changes later. Only the password secret is immutable.
  collection.add(
    new Secret(
      {
        apiVersion: "v1",
        data: {
          // convert to b64 for secret
          "config.yaml": Buffer.from(
            yaml.safeDump(jaegerClickhouseConfig)
          ).toString("base64")
        },
        kind: "Secret",
        metadata: {
          name: "jaeger-clickhouse",
          namespace
        }
      },
      kubeConfig
    )
  );

  // For now we just have a flat rate limit across the tenant at which point sampling starts occurring.
  // This effectively keeps a single tenant from eating all of the tracing storage.
  // See also: https://www.jaegertracing.io/docs/1.30/sampling/#file-sampling
  // Note that Adaptive Sampling is not currently supported for ClickHouse storage.
  const jaegerSamplingConfig = {
    default_strategy: {
      type: "ratelimiting",
      // Traces per second. See also Jaeger client metrics:
      //   "sum(rate(jaeger_tracer_traces_total[1m])) by (job,state,sampled)"
      // TODO(nickbp): Expose as a per-tenant configuration option?
      param: 1
    }
  };

  collection.add(
    new ConfigMap(
      {
        apiVersion: "v1",
        data: {
          "config.json": JSON.stringify(jaegerSamplingConfig)
        },
        kind: "ConfigMap",
        metadata: {
          name: "jaeger-sampling",
          namespace
        }
      },
      kubeConfig
    )
  );

  collection.add(
    new V1JaegerResource(
      {
        apiVersion: "jaegertracing.io/v1",
        kind: "Jaeger",
        metadata: {
          name: "jaeger",
          namespace
        },
        spec: {
          allInOne: {
            image: DockerImages.jaegerAllInOne,
            imagePullSecrets: getImagePullSecrets(),
            // reference: https://www.jaegertracing.io/docs/1.27/cli/#jaeger-all-in-one-grpc-plugin
            options: {
              //"log-level": "debug",
              // Ensure the Jaeger UI works at <tenant>.cluster/jaeger
              "query.base-path": "/jaeger",
              // Configure our own configmap for the collector sampling config
              "sampling.strategies-file": "/sampling-config/config.json"
            }
          },
          ingress: {
            // We manage the Service/Ingress creation ourselves.
            enabled: false
          },
          storage: {
            grpcPlugin: {
              image: DockerImages.jaegerClickhouse,
              imagePullSecrets: getImagePullSecrets()
            },
            options: {
              "grpc-storage-plugin": {
                //"log-level": "debug",
                binary: "/plugin/jaeger-clickhouse",
                "configuration-file": "/plugin-config/config.yaml"
              }
            },
            type: "grpc-plugin"
          },
          // strategy can be allInOne (default), production (separate pods), or streaming (separate pods + kafka)
          // for now we just use allInOne for each (per-tenant) instance
          // reference: https://www.jaegertracing.io/docs/1.27/operator/#allinone-default-strategy
          strategy: "allInOne",
          ui: {
            options: {
              // Just an example, see docs: https://www.jaegertracing.io/docs/1.27/frontend-ui/
              menu: [
                {
                  items: [
                    {
                      label: "Metrics Dashboard",
                      // We are at <tenant>.<cluster>/jaeger, so link to <tenant>.<cluster>/grafana
                      url: "/grafana"
                    },
                    {
                      label: "GitLab",
                      url: "https://gitlab.com"
                    }
                  ],
                  label: "GitLab"
                }
              ]
            }
          },
          // other options reference, e.g. labels or resources:
          //   https://www.jaegertracing.io/docs/1.27/operator/#finer-grained-configuration
          volumeMounts: [
            {
              name: "plugin-config",
              mountPath: "/plugin-config"
            },
            {
              name: "sampling-config",
              mountPath: "/sampling-config"
            }
          ],
          volumes: [
            {
              secret: {
                secretName: "jaeger-clickhouse"
              },
              name: "plugin-config"
            },
            {
              configMap: {
                name: "jaeger-sampling"
              },
              name: "sampling-config"
            }
          ]
        }
      },
      kubeConfig
    )
  );

  // We have configured ingress:false, so we need to create the UI endpoint service
  collection.add(
    new Service(
      {
        apiVersion: "v1",
        kind: "Service",
        metadata: {
          name: "jaeger",
          namespace,
          labels: {
            app: "jaeger"
          }
        },
        spec: {
          ports: [
            {
              name: "query",
              port: 16686,
              // eslint-disable-next-line @typescript-eslint/no-explicit-any
              targetPort: "query" as any
            },
            {
              name: "admin-http",
              port: 14269,
              // eslint-disable-next-line @typescript-eslint/no-explicit-any
              targetPort: "admin-http" as any
            },
            // see metrics_endpoint in jaeger-clickhouse config
            {
              name: "metrics-plugin",
              port: 9090,
              // eslint-disable-next-line @typescript-eslint/no-explicit-any
              targetPort: 9090 as any
            }
          ],
          selector: {
            app: "jaeger"
          }
        }
      },
      kubeConfig
    )
  );

  collection.add(
    new V1ServicemonitorResource(
      {
        apiVersion: "monitoring.coreos.com/v1",
        kind: "ServiceMonitor",
        metadata: {
          name: "jaeger",
          namespace,
          labels: {
            app: "jaeger",
            tenant: "system"
          }
        },
        spec: {
          endpoints: [
            {
              interval: "60s",
              port: "admin-http"
            },
            {
              interval: "60s",
              port: "metrics-plugin"
            }
          ],
          jobLabel: "app",
          namespaceSelector: {
            matchNames: [namespace]
          },
          selector: {
            matchLabels: {
              app: "jaeger"
            }
          }
        }
      },
      kubeConfig
    )
  );

  return collection;
}
