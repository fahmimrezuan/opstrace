import * as glob from "glob";
import * as path from "path";

export function GetGrafanaDashboards(): {
  name: string;
  content: string;
}[] {
  const dashboards: { name: string; content: string }[] = [];

  glob.sync(`${__dirname}/dashboards/**/*.json`).forEach(function (file) {
    // eslint-disable-next-line @typescript-eslint/no-var-requires
    const content = require(path.resolve(file));
    const filename = file.split("/").pop() || "";

    dashboards.push({ name: filename.replace(".json", ""), content });
  });

  return dashboards;
}
