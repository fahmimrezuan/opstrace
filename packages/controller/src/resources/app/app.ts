import { strict as assert } from "assert";

import {
  ClusterRoleBinding,
  Deployment,
  Namespace,
  ResourceCollection,
  Service,
  ServiceAccount,
  withPodAntiAffinityRequired
} from "@opstrace/kubernetes";
import { KubeConfig } from "@kubernetes/client-node";
import { State } from "../../reducer";
import { getControllerConfig } from "../../helpers";
import {
  DockerImages,
  getImagePullSecrets,
  POSTGRES_SECRET_NAME
} from "@opstrace/controller-config";

export function OpstraceApplicationResources(
  state: State,
  kubeConfig: KubeConfig,
  namespace: string,
  domain: string
): ResourceCollection {
  const collection = new ResourceCollection();

  const { custom_auth0_client_id, custom_auth0_domain } =
    getControllerConfig(state);

  assert(custom_auth0_domain, "custom_auth0_domain is not set");
  assert(custom_auth0_client_id, "custom_auth0_client_id is not set");
  const auth0_client_id = custom_auth0_client_id;
  const auth0_domain = custom_auth0_domain;

  collection.add(
    new Namespace(
      {
        apiVersion: "v1",
        kind: "Namespace",
        metadata: {
          name: namespace,
          labels: {
            "cert-manager.io/disable-validation": "true"
          }
        }
      },
      kubeConfig
    )
  );

  collection.add(
    new Service(
      {
        apiVersion: "v1",
        kind: "Service",
        metadata: {
          name: "opstrace-application",
          labels: {
            app: "opstrace-application"
          },
          namespace
        },
        spec: {
          ports: [
            {
              name: "http",
              port: 3001,
              // eslint-disable-next-line @typescript-eslint/no-explicit-any
              targetPort: "http" as any
            }
          ],
          selector: {
            app: "opstrace-application"
          }
        }
      },
      kubeConfig
    )
  );

  // While the new UI is being developed, display a stub page.
  collection.add(
    new Deployment(
      {
        apiVersion: "apps/v1",
        kind: "Deployment",
        metadata: {
          name: "opstrace-application",
          namespace,
          labels: {
            app: "opstrace-application"
          }
        },
        spec: {
          replicas: 1,
          strategy: {
            type: "RollingUpdate",
            rollingUpdate: {
              // eslint-disable-next-line @typescript-eslint/no-explicit-any
              maxSurge: "25%" as any,
              // eslint-disable-next-line @typescript-eslint/no-explicit-any
              maxUnavailable: "25%" as any
            }
          },
          selector: {
            matchLabels: {
              app: "opstrace-application"
            }
          },
          template: {
            metadata: {
              labels: {
                app: "opstrace-application"
              }
            },
            spec: {
              imagePullSecrets: getImagePullSecrets(),
              serviceAccountName: "opstrace-application",
              terminationGracePeriodSeconds: 80, // we give the app 60 seconds to drain existing connections
              affinity: withPodAntiAffinityRequired({
                app: "opstrace-application"
              }),
              containers: [
                {
                  name: "opstrace-application",
                  // Stub remnants of the app UI for handling initial auth.
                  image: DockerImages.app,
                  imagePullPolicy: "IfNotPresent",
                  command: ["node", "dist/server.js"],
                  env: [
                    // Postgres host/credentials for creating an initial user entry on first login
                    {
                      name: "POSTGRES_ENDPOINT",
                      valueFrom: {
                        secretKeyRef: {
                          name: POSTGRES_SECRET_NAME,
                          key: "endpoint"
                        }
                      }
                    },

                    // For storing session info
                    {
                      name: "REDIS_HOST",
                      value: `redis-master.${namespace}.svc.cluster.local`
                    },
                    {
                      name: "REDIS_PASSWORD",
                      valueFrom: {
                        secretKeyRef: {
                          name: "redis-password",
                          key: "REDIS_MASTER_PASSWORD"
                        }
                      }
                    },

                    // For serving the auth endpoints
                    {
                      name: "AUTH0_CLIENT_ID",
                      value: auth0_client_id
                    },
                    {
                      name: "AUTH0_DOMAIN",
                      value: auth0_domain
                    },
                    { name: "DOMAIN", value: `https://${domain}` },
                    { name: "UI_DOMAIN", value: `https://${domain}` },
                    {
                      name: "COOKIE_SECRET",
                      valueFrom: {
                        secretKeyRef: {
                          // Created by gatekeeper.ts
                          name: "session-cookie-secret",
                          key: "COOKIE_SECRET"
                        }
                      }
                    }
                  ],
                  ports: [
                    // pod also listens on port 9000 but that's only for health checks
                    {
                      containerPort: 3001,
                      name: "http"
                    }
                  ],
                  resources: {},
                  readinessProbe: {
                    httpGet: {
                      path: "/ready",
                      // eslint-disable-next-line @typescript-eslint/no-explicit-any
                      port: 9000 as any,
                      scheme: "HTTP"
                    },
                    failureThreshold: 1,
                    initialDelaySeconds: 5,
                    periodSeconds: 5,
                    successThreshold: 1,
                    timeoutSeconds: 5
                  },
                  livenessProbe: {
                    httpGet: {
                      path: "/live",
                      // eslint-disable-next-line @typescript-eslint/no-explicit-any
                      port: 9000 as any,
                      scheme: "HTTP"
                    },
                    failureThreshold: 3,
                    initialDelaySeconds: 5,
                    periodSeconds: 30,
                    successThreshold: 1,
                    timeoutSeconds: 5
                  },
                  startupProbe: {
                    httpGet: {
                      path: "/live",
                      // eslint-disable-next-line @typescript-eslint/no-explicit-any
                      port: 9000 as any
                    },
                    failureThreshold: 3,
                    initialDelaySeconds: 10,
                    periodSeconds: 30,
                    successThreshold: 1,
                    timeoutSeconds: 5
                  }
                }
              ]
            }
          }
        }
      },
      kubeConfig
    )
  );

  collection.add(
    new ServiceAccount(
      {
        apiVersion: "v1",
        kind: "ServiceAccount",
        metadata: {
          name: "opstrace-application",
          namespace
        }
      },
      kubeConfig
    )
  );

  collection.add(
    new ClusterRoleBinding(
      {
        apiVersion: "rbac.authorization.k8s.io/v1",
        kind: "ClusterRoleBinding",
        metadata: {
          name: "opstrace-application-clusteradmin-binding"
        },
        roleRef: {
          apiGroup: "rbac.authorization.k8s.io",
          kind: "ClusterRole",
          name: "cluster-admin"
        },
        subjects: [
          {
            kind: "ServiceAccount",
            name: "opstrace-application",
            namespace
          }
        ]
      },
      kubeConfig
    )
  );

  return collection;
}
