import { createReducer, createAsyncAction, ActionType } from "typesafe-actions";
import { schemas } from "@opstrace/schemas";
import { log } from "@opstrace/utils";
import { ResourceCache } from "../util";

export interface Integration {
  // UUID
  id: string;
  // Arbitrary string provided by the user
  name: string;
  // Derived from the name, more user-friendly than the id
  key: string;
  kind: string;
  tenant_id: string;
  // This data is defined on a per-integration basis, see *IntegrationData types.
  // Some integration kinds do not involve the controller at all, so we allow this to be any.
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  data: any;
}

export const actions = {
  fetch: createAsyncAction(
    "FETCH_INTEGRATIONS_REQUEST",
    "FETCH_INTEGRATIONS_SUCCESS",
    "FETCH_INTEGRATIONS_FAILURE"
  )<Record<string, unknown>, { resources: Integration[] }, { error: Error }>()
};
export type IntegrationActions = ActionType<typeof actions>;
export type IntegrationState = ResourceCache<Integration[]>;

const initialState: IntegrationState = {
  loaded: false,
  error: null,
  resources: []
};

export const reducer = createReducer<IntegrationState, IntegrationActions>(
  initialState
)
  .handleAction(
    actions.fetch.request,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    (state, _): IntegrationState => ({
      ...state,
      loaded: false
    })
  )
  .handleAction(
    actions.fetch.success,
    (state, action): IntegrationState => ({
      ...state,
      ...action.payload,
      error: null,
      loaded: true
    })
  )
  .handleAction(
    actions.fetch.failure,
    (state, action): IntegrationState => ({
      ...state,
      ...action.payload,
      loaded: false
    })
  );

export function startInformer(channel: (input: unknown) => void): () => void {
  const { integration } = schemas();
  let cancelled = false;
  //@ts-ignore: TS7023 'poll' implicitly has return type 'any'
  const poll = async () => {
    if (cancelled) {
      return;
    }
    try {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      const integrations: any[] = await integration.findAll();
      if (integrations.length) {
        channel(
          actions.fetch.success({
            resources: integrations
          })
        );
      }
      // refresh in 3s
      return setTimeout(poll, 3000);
    } catch (error: any) {
      channel(actions.fetch.failure({ error }));
      log.warning("polling integrations failed (retrying in 15s): %s", error);
      // seems like a good idea to wait a bit longer in the event of failure
      return setTimeout(poll, 15000);
    }
  };
  poll();
  // Return a function to stop the polling loop
  return () => {
    cancelled = true;
  };
}
