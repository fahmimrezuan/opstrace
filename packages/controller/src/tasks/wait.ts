import { select, delay, CallEffect, SelectEffect } from "redux-saga/effects";
import { State } from "../reducer";
import { SECOND, log } from "@opstrace/utils";
import { CombinedState } from "redux";

export function* blockUntilCacheHydrated(): Generator<
  CallEffect | SelectEffect,
  void,
  CombinedState<State>
> {
  while (true) {
    const { kubernetes }: State = yield select();
    const {
      Nodes,
      Namespaces,
      ApiServices,
      ClusterRoles,
      ClusterRoleBindings,
      ConfigMaps,
      CustomResourceDefinitions,
      DaemonSets,
      Deployments,
      Roles,
      RoleBindings,
      Secrets,
      Services,
      ServiceAccounts,
      StatefulSets,
      StorageClasses,
      PersistentVolumeClaims,
      Ingresses
    } = kubernetes.cluster;

    if (
      Nodes.loaded &&
      Namespaces.loaded &&
      ApiServices.loaded &&
      ClusterRoles.loaded &&
      ClusterRoleBindings.loaded &&
      ConfigMaps.loaded &&
      CustomResourceDefinitions.loaded &&
      DaemonSets.loaded &&
      Deployments.loaded &&
      Roles.loaded &&
      RoleBindings.loaded &&
      Secrets.loaded &&
      Services.loaded &&
      ServiceAccounts.loaded &&
      StatefulSets.loaded &&
      StorageClasses.loaded &&
      PersistentVolumeClaims.loaded &&
      Ingresses.loaded
    ) {
      log.info(`Kubernetes cache is hydrated`);
      break;
    }
    log.info(`Waiting for the Kubernetes cache to hydrate...`);
    yield delay(1 * SECOND);
  }
}
