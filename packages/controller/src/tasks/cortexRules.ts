import { call, delay, select, CallEffect } from "redux-saga/effects";
import * as yaml from "js-yaml";
import axios from "axios";

import { log, SECOND } from "@opstrace/utils";
import { State } from "../reducer";
import { getDomain } from "../helpers";
import getRules from "../system/rules";
import getAlerts from "../system/alerts";

// Make this optional, so that running the controller locally during dev
// doesn't require a connection to the service.
const rulerEndpoint = process.env.RULER_ENDPOINT ?? "";

export function* cortexSystemRulesReconciler(): Generator<
  CallEffect,
  unknown,
  unknown
> {
  return yield call(function* () {
    if (rulerEndpoint === "") {
      log.info(
        "disabled system alert rules reconciliation: ruler endpoint not configured"
      );
      return;
    }

    const state: State = yield select();
    const domain = getDomain(state);

    // TODO this could reference the SHA of the controller build
    const runbookUrl =
      "https://github.com/opstrace/opstrace/blob/main/docs/alerts";
    const grafanaArgs = "?orgId=1&refresh=10s&from=now-30m&to=now";
    const grafanaUrl =
      `https://system.${domain}/grafana/d/bF4hjRpZk/opstrace-system` +
      grafanaArgs;

    const rules = getRules();
    const alerts = getAlerts(runbookUrl, grafanaUrl);

    while (true) {
      try {
        // Apply rules to system tenant
        yield Promise.all(
          [...rules, ...alerts].map(group =>
            axios({
              url: `${rulerEndpoint}/system`,
              method: "POST",
              headers: {
                "Content-Type": "application/yaml",
                "X-Scope-OrgID": "system"
              },
              timeout: 30 * SECOND,
              data: yaml.safeDump(group)
            })
          )
        );
      } catch (err: any) {
        log.error("failed applying system rules/alerts: %s", err);
      }
      // loop through again in 1 min
      yield delay(60 * SECOND);
    }
  });
}
