import { KubeConfig } from "@kubernetes/client-node";
import {
  DaemonSet,
  Deployment,
  ResourceCollection,
  StatefulSet
} from "@opstrace/kubernetes";
import { setNodeAffinityHelper } from "./affinity";

// mock only the logger functions, from
// https://jestjs.io/docs/mock-functions#mocking-partials
// required so we can use deepMerge.
jest.mock("@opstrace/utils", () => {
  const originalModule = jest.requireActual("@opstrace/utils");
  return {
    ...originalModule,
    log: {
      info: jest.fn,
      debug: jest.fn,
      warning: jest.fn
    }
  };
});

jest.mock("@kubernetes/client-node");

describe("node affinity helper", () => {
  const node_selector_terms = {
    default: {
      key: "nodepool",
      values: ["primary_nodes"]
    },
    rules: [
      {
        kind: "deployment",
        namespace: "test",
        name: "test",
        match_expression: {
          key: "nodepool",
          values: ["test_deployment_target_nodes"]
        }
      },
      {
        kind: "any",
        namespace: "test",
        name: "",
        match_expression: {
          key: "nodepool",
          values: ["test_nodes"]
        }
      }
    ]
  };

  test("should set default affinity terms on daemonset", () => {
    const state = new ResourceCollection();
    state.add(
      new DaemonSet(
        {
          apiVersion: "apps/v1",
          kind: "DaemonSet",
          metadata: {
            name: "test",
            namespace: "test-foobar"
          },
          spec: {
            selector: {},
            template: {
              spec: {
                containers: [
                  {
                    image: "fake:image",
                    imagePullPolicy: "IfNotPresent",
                    name: "test"
                  }
                ]
              }
            }
          }
        },
        new KubeConfig()
      )
    );

    setNodeAffinityHelper(node_selector_terms, state);

    const result = state.get()[0].get();
    expect(result.spec.template.spec.affinity).toStrictEqual({
      nodeAffinity: {
        requiredDuringSchedulingIgnoredDuringExecution: {
          nodeSelectorTerms: [
            {
              matchExpressions: [
                { key: "nodepool", operator: "In", values: ["primary_nodes"] }
              ]
            }
          ]
        }
      }
    });
  });

  test("should set target deployment affinity terms", () => {
    const state = new ResourceCollection();
    state.add(
      new Deployment(
        {
          apiVersion: "apps/v1",
          kind: "Deployment",
          metadata: {
            name: "test",
            namespace: "test"
          },
          spec: {
            replicas: 1,
            selector: {},
            template: {
              spec: {
                containers: [
                  {
                    image: "fake:image",
                    imagePullPolicy: "IfNotPresent",
                    name: "test"
                  }
                ]
              }
            }
          }
        },
        new KubeConfig()
      )
    );

    setNodeAffinityHelper(node_selector_terms, state);

    const result = state.get()[0].get();
    expect(result.spec.template.spec.affinity).toStrictEqual({
      nodeAffinity: {
        requiredDuringSchedulingIgnoredDuringExecution: {
          nodeSelectorTerms: [
            {
              matchExpressions: [
                {
                  key: "nodepool",
                  operator: "In",
                  values: ["test_deployment_target_nodes"]
                }
              ]
            }
          ]
        }
      }
    });
  });

  test("should set namespace affinity terms on statefulset", () => {
    const state = new ResourceCollection();
    state.add(
      new StatefulSet(
        {
          apiVersion: "apps/v1",
          kind: "StatefulSet",
          metadata: {
            name: "test",
            namespace: "test"
          },
          spec: {
            replicas: 1,
            selector: {},
            serviceName: "test",
            template: {
              spec: {
                containers: [
                  {
                    image: "fake:image",
                    imagePullPolicy: "IfNotPresent",
                    name: "test"
                  }
                ]
              }
            }
          }
        },
        new KubeConfig()
      )
    );

    setNodeAffinityHelper(node_selector_terms, state);

    const result = state.get()[0].get();
    expect(result.spec.template.spec.affinity).toStrictEqual({
      nodeAffinity: {
        requiredDuringSchedulingIgnoredDuringExecution: {
          nodeSelectorTerms: [
            {
              matchExpressions: [
                { key: "nodepool", operator: "In", values: ["test_nodes"] }
              ]
            }
          ]
        }
      }
    });
  });

  test("should skip setting affinity terms", () => {
    const state = new ResourceCollection();
    state.add(
      new DaemonSet(
        {
          apiVersion: "apps/v1",
          kind: "DaemonSet",
          metadata: {
            name: "test",
            namespace: "test-foobar"
          },
          spec: {
            selector: {},
            template: {
              spec: {
                containers: [
                  {
                    image: "fake:image",
                    imagePullPolicy: "IfNotPresent",
                    name: "test"
                  }
                ]
              }
            }
          }
        },
        new KubeConfig()
      )
    );

    setNodeAffinityHelper(
      { default: { key: "", values: [] }, rules: [] },
      state
    );

    const result = state.get()[0].get();
    expect(result.spec.template.spec.affinity).toBeUndefined();
  });

  test("should set namespace affinity terms on statefulset", () => {
    const state = new ResourceCollection();
    state.add(
      new StatefulSet(
        {
          apiVersion: "apps/v1",
          kind: "StatefulSet",
          metadata: {
            name: "test",
            namespace: "test"
          },
          spec: {
            replicas: 1,
            selector: {},
            serviceName: "test",
            template: {
              spec: {
                containers: [
                  {
                    image: "fake:image",
                    imagePullPolicy: "IfNotPresent",
                    name: "test"
                  }
                ],
                affinity: {
                  podAntiAffinity: {
                    preferredDuringSchedulingIgnoredDuringExecution: [
                      {
                        podAffinityTerm: {
                          labelSelector: {
                            matchLabels: { test: "test" }
                          },
                          topologyKey: "kubernetes.io/hostname"
                        },
                        weight: 100
                      }
                    ]
                  }
                }
              }
            }
          }
        },
        new KubeConfig()
      )
    );

    setNodeAffinityHelper(node_selector_terms, state);

    const result = state.get()[0].get();
    expect(result.spec.template.spec.affinity).toStrictEqual({
      podAntiAffinity: {
        preferredDuringSchedulingIgnoredDuringExecution: [
          {
            podAffinityTerm: {
              labelSelector: {
                matchLabels: { test: "test" }
              },
              topologyKey: "kubernetes.io/hostname"
            },
            weight: 100
          }
        ]
      },
      nodeAffinity: {
        requiredDuringSchedulingIgnoredDuringExecution: {
          nodeSelectorTerms: [
            {
              matchExpressions: [
                { key: "nodepool", operator: "In", values: ["test_nodes"] }
              ]
            }
          ]
        }
      }
    });
  });
});
