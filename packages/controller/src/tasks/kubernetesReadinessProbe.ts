import { createLightship } from "lightship";
import { log } from "@opstrace/utils";

// Setup Kubernetes a very simple readiness probe using
// https://github.com/gajus/lightship. When the controller is ready it should
// call setToReady.
const lightship = createLightship({
  signals: ["SIGINT", "SIGTERM"],
  port: 9000
});

lightship.registerShutdownHandler(async () => {
  log.debug("shutting down gracefully");
});

export function setToReady() {
  // Lightship default state is "SERVER_IS_NOT_READY". Therefore, you must signal
  // that the server is now ready to accept connections.
  log.debug("controller is ready");
  lightship.signalReady();
}
