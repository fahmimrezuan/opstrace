import { call, delay, select, CallEffect } from "redux-saga/effects";

import { log, SECOND } from "@opstrace/utils";
import { Tenant } from "@opstrace/tenants";
import { State } from "../reducer";

import { dbClient } from "../clickhouseClient";
import {
  clickhouseTracingDBName,
  GetClickHousePassword
} from "../resources/monitoring/tenants/jaeger";

// The ClickHouse user we create for DB access by per-tenant resources
// NOTE: This could be removed but it's not hurting anything to keep for now
const tenantUserPrefix = "tenant_";

// on cluster syntax uses {cluster} macro to expand to current cluster
const onCluster = "ON CLUSTER '{cluster}'";

export function getTenantClickHouseName(tenant: Tenant): string {
  return `${tenantUserPrefix}${tenant.name.replace("-", "_")}`;
}

export function* clickhouseTenantsReconciler(): Generator<
  CallEffect,
  unknown,
  unknown
> {
  return yield call(function* () {
    if (dbClient === null) {
      log.warning(
        "skipping ClickHouse tenant database/user reconciliation: CLICKHOUSE_ENDPOINT not configured"
      );
      return;
    }
    const dbClient2 = dbClient;
    let initializedGrants = false;

    // For now we only use ClickHouse for tracing data.
    // Any other hardcoded DBs can be added here.
    const desiredClickhouseDBs = [clickhouseTracingDBName];

    while (true) {
      // loop through again in 5s
      yield delay(5 * SECOND);

      const state: State = yield select();
      if (state.tenants.list.tenants.length === 0) {
        // tenants not loaded? something's not right. wait and try again after delay
        // In particular we don't want to drop all DBs if the tenants list is missing/empty
        log.warning("skipping ClickHouse tenant sync: empty tenants list");
        continue;
      }

      if (
        !state.clickhouse.Databases.loaded ||
        !state.clickhouse.Users.loaded
      ) {
        log.debug(
          "skipping ClickHouse tenant sync: databases or users not loaded"
        );
        continue;
      }

      // This list is explicitly kept separate from clickhouseTenants.
      // We only use it when creating the user, and do not use it to decide if a user should be deleted...
      const clickhouseTenantPasswords: Record<string, string | undefined> = {};
      // Convert e.g. "system" => "tenant_system", "user-tenant" => "tenant_user_tenant"
      // Clickhouse does not like dashes, so we map them to underscores which are supported
      const desiredClickhouseUsers = state.tenants.list.tenants.map(tenant => {
        const name = getTenantClickHouseName(tenant);
        // Get the password, if any, for creating the user.
        // If this is undefined then we will wait until it appears to create the user.
        clickhouseTenantPasswords[name] = GetClickHousePassword(state, tenant);
        return name;
      });

      const currentDBs = state.clickhouse.Databases.resources;
      const currentUsers = state.clickhouse.Users.resources;

      // Once per controller process, try to initialize grants for all DB tenant users.
      // This is to allow automatically "repairing" permissions of existing tenant users,
      // just in case the controller had just restarted at a very unlucky time when creating them.
      // This is ultimately just a best-effort attempt at repairing grants if they are missing,
      // and should be a no-op most of the time. This repair can be removed if we start checking
      // SHOW GRANTS, as mentioned below.
      let permissionsToGrant: string[] = [];
      if (!initializedGrants) {
        permissionsToGrant = desiredClickhouseUsers.flatMap(user => [
          queryGrantDB(user, clickhouseTracingDBName),
          queryGrantRemote(user)
        ]);
        initializedGrants = true;
      }

      const dbsToAdd = desiredClickhouseDBs
        // DB doesn't exist already
        .filter(dbName => !currentDBs.includes(dbName))
        .map(queryCreateDatabase);
      const usersToAdd = desiredClickhouseUsers
        // User doesn't exist already
        .filter(clickhouseTenant => !currentUsers.includes(clickhouseTenant))
        // User has a generated password available
        // If not, then we just create the DB per above and wait to create the user
        .filter(
          clickhouseTenant => clickhouseTenant in clickhouseTenantPasswords
        )
        .flatMap(userToAdd =>
          // There is a theoretical bug here where the controller could exit between the
          // CREATE USER and GRANT queries, resulting in a degraded user missing permissions.
          // To fix this properly we could poll permissions using SHOW GRANTS, but parsing that would be
          // complicated. For now we somewhat punt on this, reapplying grants after a restart (above)
          // and ensuring that queries are run sequentially (below).
          // Note that the CREATE USER + GRANT unfortunately cannot be sent as a single semicolon-separated
          // query/transaction: the server returns a 'Multi-statements are not allowed' error.
          [
            // We checked clickhouseTenantPasswords above
            queryCreateUser(
              userToAdd,
              clickhouseTenantPasswords[userToAdd] ?? "",
              // pick an arbitrary database as the "default". this avoids warnings on user login
              "default"
            ),
            queryGrantDB(userToAdd, clickhouseTracingDBName),
            queryGrantRemote(userToAdd)
          ]
        );

      // Search for users and DBs that start with 'tenant_',
      // but that aren't present in the list of expected tenants.
      // In particular we want to avoid deleting any system users/dbs.
      const usersToRemove = currentUsers
        .filter(
          curuser =>
            // User looks like a tenant (starts with "tenant_")
            curuser.startsWith(tenantUserPrefix) &&
            // User is not among the list of valid tenants. We do NOT base this on whether a
            // generated password is available in clickhouseTenantPasswords.
            !desiredClickhouseUsers.includes(curuser)
        )
        .map(queryDropUser);

      // NOTE: We don't currently delete tracing data for deleted tenants,
      // instead the data will automatically "fall off" when its TTL expires.
      // If we wanted to proactively delete the data, we could use a query like this:
      // "DELETE FROM <each relevant table> WHERE tenant = <name>"
      // But the tricky part would be detecting when a delete is needed - e.g.
      // by fetching a list of per-tenant clickhouse partitions that should be deleted.

      // The ordering here probably doesn't matter,
      // but it feels like DBs should always be created before their associated users.
      const queries = dbsToAdd
        .concat(usersToAdd)
        // Perform any extra GRANTs after potentially adding the users involved
        .concat(permissionsToGrant)
        .concat(usersToRemove);

      // This log may contain internal DB user passwords on user creation
      log.debug("ClickHouse tenant sync queries to execute: %s", queries);

      // Run the queries sequentially. In theory it should be okay to run them in parallel,
      // but that introduces a potential for weird races/corner cases on ClickHouse's end.
      // Note that we MUST currently run queries in sequential order anyway, to ensure that
      // GRANT requests occur after their associated CREATE USER requests. See above comments.
      for (const query of queries) {
        try {
          yield dbClient2.query(query).toPromise();
        } catch (err: any) {
          log.error(
            "executing ClickHouse query '%s' failed (retrying in 5s): %s",
            query,
            err
          );
        }
      }
    }
  });
}

function queryCreateUser(
  userToCreate: string,
  userPassword: string,
  dbDefault: string
): string {
  return `CREATE USER IF NOT EXISTS ${userToCreate} ${onCluster} IDENTIFIED WITH plaintext_password BY '${userPassword}' HOST ANY DEFAULT DATABASE ${dbDefault}`;
}

function queryGrantDB(userToGrant: string, dbToGrant: string): string {
  return `GRANT ${onCluster} SELECT, INSERT, ALTER, CREATE, DROP, TRUNCATE, OPTIMIZE, SHOW ON ${dbToGrant}.* TO ${userToGrant}`;
}

function queryGrantRemote(userToGrant: string): string {
  // REMOTE source privilege required for Distributed tables
  // This can't be specified on the database level
  return `GRANT ${onCluster} REMOTE ON *.* TO ${userToGrant}`;
}

function queryDropUser(userToDrop: string): string {
  return `DROP USER IF EXISTS ${userToDrop} ${onCluster}`;
}

function queryCreateDatabase(dbToCreate: string): string {
  return `CREATE DATABASE IF NOT EXISTS ${dbToCreate} ${onCluster}`;
}
