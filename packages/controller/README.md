# Opstrace controller

This can be thought of as Opstrace's Kubernetes Operator.

Deploys things into the K8s cluster and keeps them running.
Automatically adds/removes things to align with the list of configured tenants.

## Running the controller locally

You can run the controller as a local system process which communicates with the cluster via port forwards.

This makes it easy to quickly test changes to the cluster.

Here are the steps:

### Prerequisites

1. Disable existing controller in the cluster. Either create the Opstrace instance with the `--hold-controller` argument, or turn off the controller deployment manually:
    ```bash
    $ kubectl edit -n kube-system opstrace-controller
    => set 'replicas: 0'
    ```
2. Deploy a pod that will route access to Postgres. We use AWS/GCP Hosted Postgres to manage some cluster state. The Postgres instance is configured to only be accessible from inside the cluster. We therefore need to set up a proxy so that we can reach Postgres via a port-forward:
    ```bash
    $ kubectl run \
      -n kube-system \
      --env REMOTE_HOST=$(kubectl get secret -n kube-system -o json postgres-secret | jq -r .data.endpoint | base64 -d | sed 's/.*@//g') \
      --env REMOTE_PORT=5432 \
      --env LOCAL_PORT=5432 \
      --port 8080 \
      --image marcnuri/port-forward:latest@sha256:fd7042c165b1afd4bfab7f7ce474344ecf6364233d839b2b756affa148325952 \
      forward-postgres
    ```
3. Set up port forwards so that ClickHouse and Postgres (via `forward-postgres`) are reachable via localhost:
    ```bash
    $ kubectl port-forward -n kube-system forward-postgres 5432
    $ kubectl port-forward -n clickhouse statefulset/cluster-0-0 8123
    ```
4. (Optional) Set up additional port forwards for Cortex alertmanager/ruler:
    ```bash
    $ kubectl port-forward -n cortex deployment/alertmanager 8091:80
    $ kubectl port-forward -n cortex deployment/ruler 8092:80
    ```

### Start controller

From the `packages/controller` directory, compile and run the controller:

```bash
/packages/controller$ yarn build \
  && OPSTRACE_BUILDINFO_PATH=./package.json \
  POSTGRES_ENDPOINT="$(kubectl get secret -n kube-system -o json postgres-secret | jq -r .data.endpoint | base64 -d | sed 's/@.*//g')@127.0.0.1" \
  CLICKHOUSE_ENDPOINT=http://127.0.0.1:8123/ \
  node ./build/cmd.js --external nick-test
```

You can optionally point the controller to alertmanager/ruler port-forwards with these additional envvars:

```bash
  ALERTMANAGER_ENDPOINT=http://127.0.0.1:8091/api/v1/alerts \
  RULER_ENDPOINT=http://127.0.0.1:8092/api/v1/rules \
```

### Clean up

Undoing the above steps in reverse:

1. Ctrl+C the controller itself
2. Ctrl+C the port-forwards
3. `kubectl delete pod -n kube-system forward-postgres`
4. `kubectl edit deployment -n kube-system opstrace-controller` and set `replicas: 1`
