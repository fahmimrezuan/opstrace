# Typescript Packages

Legacy Opstrace business logic implemented in Typescript lives here. Much of this is currently being reimplemented, but is left here for now as a reference.

If your code is generic (like interacting with GCP or Kubernetes) then consider if it should be a library under [lib](../lib)
