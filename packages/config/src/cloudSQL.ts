import { GCPAuthOptions, sql_v1beta4 } from "@opstrace/gcp";
import { LatestClusterConfigType } from "./clusterconfig";

export function getCloudSQLConfig(
  ccfg: LatestClusterConfigType,
  gcpAuthOptions: GCPAuthOptions,
  postgresPassword: string
): sql_v1beta4.Schema$DatabaseInstance {
  if (ccfg.gcp === undefined) {
    throw Error("`gcp` property expected");
  }
  const userLabels = {
    opstrace_cluster_name: ccfg.cluster_name
  };
  return {
    connectionName: ccfg.cluster_name,
    databaseVersion: "POSTGRES_11",
    instanceType: "CLOUD_SQL_INSTANCE",
    name: ccfg.cluster_name,
    project: gcpAuthOptions.credentials.project_id,
    region: ccfg.gcp.region,
    rootPassword: postgresPassword,
    serviceAccountEmailAddress: gcpAuthOptions.credentials.client_email,
    settings: {
      userLabels,
      // https://cloud.google.com/sql/docs/postgres/create-instance#machine-types
      tier: "db-custom-2-3840",
      ipConfiguration: {
        // Don't assign a public IP
        ipv4Enabled: false,
        // Our resource link for our GKE VPC
        privateNetwork: ccfg.cluster_name,
        requireSsl: false
      }
    }
  };
}
