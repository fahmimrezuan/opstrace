export interface AWSInfraConfigTypeV2 {
  region: string;
  zone_suffix: string;
  instance_type: string;
  eks_admin_roles: string[];
}

export interface GCPInfraConfigTypeV2 {
  machine_type: string;
  region: string;
  zone_suffix: string;
}

// Using the yup-inferred types across the code base, deep down in other packages
// is creating a lot of pain. Use yup for user-given doc validation, but then
// save to 'natively defined Typescript types/interfaces' asap.
export interface ClusterConfigTypeV2 {
  cluster_name: string;
  cloud_provider: "aws" | "gcp";
  cert_issuer: "letsencrypt-prod" | "letsencrypt-staging";
  controller_image: string;
  tenants: string[];
  env_label: string;
  metric_retention_days: number; // bigint to force this to integer?
  trace_retention_days: number; // bigint to force this to integer?
  data_api_authorized_ip_ranges: string[];
  node_count: number; // bigint to force this to integer?
  aws: AWSInfraConfigTypeV2 | undefined;
  gcp: GCPInfraConfigTypeV2 | undefined;
  custom_dns_name: string;
  custom_auth0_client_id: string;
  custom_auth0_domain: string;
  gitlab: {
    instance_url: string;
    group_allowed_access: string;
    group_allowed_system_access: string;
    oauth_client_id: string;
    oauth_client_secret: string;
  };
}
