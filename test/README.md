# Typescript-based E2E tests

This directory contains tests for exercising Opstrace in CI, using a live EKS or GKE K8s cluster.

* `browser` has tests for the old Opstrace UI, using Playwright to exercise browser behavior
* `test-remote` has tests for APIs and backend components, such as E2E tracing storage/retrieval, and Cortex-based alerting
