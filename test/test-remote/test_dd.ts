import events from "events";
import fs from "fs";
import { strict as assert } from "assert";

import Docker from "dockerode";
import { ZonedDateTime } from "@js-joda/core";
import got from "got";

import {
  createTempfile,
  enrichHeadersWithAuthToken,
  globalTestSuiteSetupOnce,
  httpTimeoutSettings,
  log,
  logHTTPResponse,
  mtime,
  mtimeDeadlineInSeconds,
  readFirstNBytes,
  rndstring,
  sleep,
  TENANT_DEFAULT_API_TOKEN,
  TENANT_DEFAULT_CORTEX_API_BASE_URL,
  TENANT_DEFAULT_DD_API_BASE_URL
} from "./testutils";

import { waitForCortexMetricResult } from "./testutils/metrics";

export async function startDDagentContainer() {
  log.info("start containerized DD agent");
  const docker = new Docker();

  // Use magic DD agent environment variables to point the DD agent to
  // the tested cluster's DD API implementation. Set the 'default' tenant's
  // API authentication token as DD API key. The environnment variables
  // supported by the DD agent are documented here:
  // https://docs.datadoghq.com/agent/guide/environment-variables/
  const ddEnv = [
    `DD_API_KEY=${TENANT_DEFAULT_API_TOKEN}`,
    `DD_DD_URL=${TENANT_DEFAULT_DD_API_BASE_URL}`
  ];

  log.info("DD agent container env: %s", ddEnv);

  // Prepare file for capturing DD agent stdout/err.
  const outerrfilePath = createTempfile("dd-agent-container-", ".outerr");
  const outstream = fs.createWriteStream(outerrfilePath);
  await events.once(outstream, "open");

  const cont = await docker.createContainer({
    Image: "gcr.io/datadoghq/agent:7",
    AttachStdin: false,
    AttachStdout: false,
    AttachStderr: false,
    Tty: false,
    Env: ddEnv,
    HostConfig: {
      Mounts: [
        {
          Type: "bind",
          Source: `${__dirname}/containers/letsencrypt-stg-root-x1.pem`,
          // /etc/ssl/ca-bundle.pem is a place where the Golang-based HTTP
          // client in the DD agent discovers and uses the CA file  when doing
          // HTTP requests. It does however not overwrite the system store
          // baked into the container image, because that path is not used by
          // the distro in the DD agent container image. Kudos to
          // https://stackoverflow.com/a/40051432/145400 for showing the paths
          // that Golang walks through.
          Target: "/etc/ssl/ca-bundle.pem",
          ReadOnly: true
        },
        {
          Type: "bind",
          Source: "/var/run/docker.sock",
          Target: "/var/run/docker.sock",
          ReadOnly: true
        },
        {
          Type: "bind",
          Source: "/proc/",
          Target: "/host/proc/",
          ReadOnly: true
        },
        {
          Type: "bind",
          Source: "/sys/fs/cgroup/",
          Target: "/host/sys/fs/cgroup",
          ReadOnly: true
        }
      ]
    }
  });

  log.info("attach file-backed stream");
  const stream = await cont.attach({
    stream: true,
    stdout: true,
    stderr: true
  });
  stream.pipe(outstream);

  log.info("container start()");
  await cont.start();

  // Expect a special log messaged emitted by the DD agent to confirm that time
  // series fragments were successfully POSTed (2xx-acked) to the Opstrace
  // cluster's DD API implementation.
  // The first confirmation may either be for /series or for /check_run, see
  // https://github.com/opstrace/opstrace/issues/405
  const logNeedle = `Successfully posted payload to "${TENANT_DEFAULT_DD_API_BASE_URL}/api/v1`;

  // It's known that this may take a while after DD agent startup.
  // Note: also see https://github.com/opstrace/opstrace/issues/384
  const maxWaitSeconds = 120;
  const deadline = mtimeDeadlineInSeconds(maxWaitSeconds);
  log.info(
    "Waiting for needle to appear in container log, deadline in %s s. Needle: %s",
    maxWaitSeconds,
    logNeedle
  );

  async function terminateContainer() {
    log.info("terminate container");

    try {
      await cont.kill({ signal: "SIGTERM" });
    } catch (err: any) {
      log.warning("could not kill container: %s", err.message);
    }

    log.info("wait for container to stop");
    try {
      await cont.wait();
    } catch (err: any) {
      log.warning("error waiting for container: %s", err.message);
    }

    log.info("force-remove container");
    try {
      await cont.remove({ force: true });
    } catch (err: any) {
      log.warning("could not remove container: %s", err.message);
    }
    log.info(
      "log output emitted by container (stdout/err from %s):\n%s",
      outerrfilePath,
      fs.readFileSync(outerrfilePath, {
        encoding: "utf-8"
      })
    );
  }

  while (true) {
    if (mtime() > deadline) {
      log.info("deadline hit");
      await terminateContainer();
      throw new Error(
        "DD agent container setup failed: deadline hit waiting for log needle"
      );
    }
    const fileHeadBytes = await readFirstNBytes(outerrfilePath, 10 ** 5);
    if (fileHeadBytes.includes(Buffer.from(logNeedle, "utf-8"))) {
      log.info("log needle found in container log, proceed");
      break;
    }
    await sleep(0.2);
  }

  return terminateContainer;
}

suite("DD API test suite", function () {
  suiteSetup(async function () {
    log.info("suite setup");
    await globalTestSuiteSetupOnce();
  });

  suiteTeardown(async function () {
    // Note: this does not seem to be run upon Node shutdown, e.g. triggered
    // with SIGINT. Make cleanup better.
    log.info("suite teardown");
  });

  test("dd_api_insert_single_ts_fragment", async function () {
    const rndstr = rndstring(5);
    const metricname = `opstrace.dd.test-remote-${rndstr}`;
    const metricnameSanitized = `opstrace_dd_test_remote_${rndstr}`;

    const now = ZonedDateTime.now();
    const tsnow = now.toEpochSecond();

    const payload = {
      series: [
        {
          metric: metricname,
          // Note: these samples are descending in time, which is
          // the order that the DD agent sends fragments with. This order
          // is currently strictly required by the receiving end.
          points: [
            [tsnow, 2],
            [tsnow - 120, 1],
            [tsnow - 240, 0]
          ],
          tags: ["version:7.24.1", "testtag:testvalue"],
          host: "somehost",
          type: "rate",
          interval: 5
        }
      ]
    };

    log.info("POST body doc:\n%s", JSON.stringify(payload, null, 2));
    const payloadBytes = Buffer.from(JSON.stringify(payload), "utf-8");

    // Send the token via 'api_key' URL param, not headers.
    // This is how a stock datadog agent sends its API key.
    const response = await got.post(
      `${TENANT_DEFAULT_DD_API_BASE_URL}/api/v1/series?api_key=${TENANT_DEFAULT_API_TOKEN}`,
      {
        body: payloadBytes,
        throwHttpErrors: false,
        // We do not include an auth header to reflect datadog agent behavior
        headers: {
          "Content-Type": "application/json"
        },
        timeout: httpTimeoutSettings,
        https: { rejectUnauthorized: false }
      }
    );
    if (response.statusCode >= 300) {
      // Only log if the request fails - request url has token!
      logHTTPResponse(response);
    }

    // now query cortex
    const searchStart = now.minusMinutes(45);
    const searchEnd = now.plusMinutes(10);

    const queryParams = {
      query: `${metricnameSanitized}{job="ddagent"}`,
      start: searchStart.toEpochSecond().toString(),
      end: searchEnd.toEpochSecond().toString(),
      step: "60s"
    };

    const resultArray = await waitForCortexMetricResult(
      TENANT_DEFAULT_CORTEX_API_BASE_URL,
      TENANT_DEFAULT_API_TOKEN,
      queryParams,
      "query_range"
    );

    log.info("resultArray: %s", JSON.stringify(resultArray, null, 2));

    // Check that all three values in the original submit request are
    // covered by the query response.
    const valuesSeen = resultArray[0].values.map(
      (sample: Array<[number, string]>) => sample[1]
    );
    log.info("values seen: %s", valuesSeen);
    for (const v of ["0", "1", "2"]) {
      log.info("check for presence of value %s", v);
      assert(valuesSeen.includes(v));
    }

    // pragmatic criterion for starters: expect a number of values. with the
    // 1-second step size there should be tens or hundreds of values/samples.
    assert.strictEqual(resultArray[0]["values"].length > 5, true);
  });

  test("dd_api_auth_sanity", async function () {
    // Request with missing token should fail
    let response = await got.post(
      `${TENANT_DEFAULT_DD_API_BASE_URL}/api/v1/series`,
      {
        throwHttpErrors: false,
        timeout: httpTimeoutSettings,
        https: { rejectUnauthorized: false }
      }
    );
    assert(response.statusCode == 401);

    // Request with token instead provided via Authorization header should fail
    response = await got.post(
      `${TENANT_DEFAULT_DD_API_BASE_URL}/api/v1/series`,
      {
        throwHttpErrors: false,
        headers: enrichHeadersWithAuthToken(TENANT_DEFAULT_API_TOKEN, {}),
        timeout: httpTimeoutSettings,
        https: { rejectUnauthorized: false }
      }
    );
    assert(response.statusCode == 401);

    // Request with empty token should fail
    response = await got.post(
      `${TENANT_DEFAULT_DD_API_BASE_URL}/api/v1/series?api_key=`,
      {
        throwHttpErrors: false,
        timeout: httpTimeoutSettings,
        https: { rejectUnauthorized: false }
      }
    );
    assert(response.statusCode == 401);

    // Request with bad token should fail
    response = await got.post(
      `${TENANT_DEFAULT_DD_API_BASE_URL}/api/v1/series?api_key=bad${TENANT_DEFAULT_API_TOKEN}`,
      {
        throwHttpErrors: false,
        timeout: httpTimeoutSettings,
        https: { rejectUnauthorized: false }
      }
    );
    assert(response.statusCode == 401);

    // Request with correct token should succeed
    response = await got.post(
      `${TENANT_DEFAULT_DD_API_BASE_URL}/api/v1/series?api_key=${TENANT_DEFAULT_API_TOKEN}`,
      {
        headers: {
          "Content-Type": "application/json"
        },
        body: "{}",
        throwHttpErrors: false,
        timeout: httpTimeoutSettings,
        https: { rejectUnauthorized: false }
      }
    );
    logHTTPResponse(response);
    assert(response.statusCode == 202);
    assert(response.headers["content-type"] == "application/json");
    assert(response.body.includes("ok"));

    // Request with manually specified internal tenant header should have no effect
    response = await got.post(
      `${TENANT_DEFAULT_DD_API_BASE_URL}/api/v1/series?api_key=${TENANT_DEFAULT_API_TOKEN}`,
      {
        headers: {
          "Content-Type": "application/json",
          "X-Scope-OrgID": "badvalue"
        },
        body: "{}",
        throwHttpErrors: false,
        timeout: httpTimeoutSettings,
        https: { rejectUnauthorized: false }
      }
    );
    logHTTPResponse(response);
    assert(response.statusCode == 202);
    assert(response.headers["content-type"] == "application/json");
    assert(response.body.includes("ok"));
  });

  test("dd_api_run_agent_container_query_sysuptime", async function () {
    const now = ZonedDateTime.now();

    // The DD agent container is currently configured to send metrics to the DD
    // API endpoint for the 'default' tenant.
    const terminateContainer = await startDDagentContainer();

    // Wait for some more samples to be pushed. Terminate container before
    // starting the query phase, so that the termination happens more or less
    // reliably (regardless of errors during query phase).
    await sleep(15);
    await terminateContainer();
    const searchStart = now.minusMinutes(45);
    const searchEnd = now.plusMinutes(10);

    // Note that this current setup does not insert a unique metric stream,
    // i.e. if the test passes it does only guarantee that the insertion
    // succeeded when the cluster is fresh (when this test was not run before,
    // against the same cluster). TODO: think about how to set a unique label
    // here.
    const queryParams = {
      // This implicitly tests for two labels to be set by the translation
      // layer. Change with care!
      query: `system_uptime{job="ddagent", type="gauge"}`,
      start: searchStart.toEpochSecond().toString(),
      end: searchEnd.toEpochSecond().toString(),
      step: "60s"
    };

    const resultArray = await waitForCortexMetricResult(
      TENANT_DEFAULT_CORTEX_API_BASE_URL,
      TENANT_DEFAULT_API_TOKEN,
      queryParams,
      "query_range"
    );

    log.info("resultArray: %s", JSON.stringify(resultArray, null, 2));

    assert(resultArray[0].values.length > 1);
    // confirm that there is just one stream (set of labels)
    assert(resultArray.length == 1);

    // Expected structure:
    // "metric": {
    //   "__name__": "system_uptime",
    //   "instance": "x1carb6",
    //   "job": "ddagent",
    //   "source_type_name": "System",
    //   "type": "gauge"
    // },
    assert(resultArray[0].metric.source_type_name === "System");

    log.info("values seen: %s", JSON.stringify(resultArray[0].values, null, 2));
  });
});
