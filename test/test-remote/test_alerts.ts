// Deploys the 'e2ealerting' tool for firing alerts and checking that they are received.
// See also: https://github.com/grafana/cortex-tools/blob/main/docs/e2ealerting.md
//
// The chain works like this:
// - The test deploys an e2ealerting pod
// - e2ealerting pod serves a metric: e2ealerting_webhook_receiver_end_to_end_duration_seconds
// - the metric is scraped by the tenant prometheus
// - the tenant prometheus forwards it to cortex against the tenant (X-Scope-OrgId)
// - cortex ruler has been configured with an alert rule to always fire against the metric
// - cortex alertmanager has been configured to report firing rules back to e2ealerting via a webhook endpoint
// - e2ealerting receives the alert, and then increments e2ealerting_webhook_receiver_evaluations_total
//
// So, what we need to do is:
// - Deploy e2ealerting and configure scraping of its metrics endpoint so that metrics get into cortex
// - Configure cortex ruler to fire against the e2ealerting metric
// - Configure cortex alertmanager for the tenant to send alerts back to e2ealerting
//
// This will validate:
// - That scraped metrics from a tenant pod are getting into cortex automatically
//   If this fails, then maybe the tenant prometheus isnt routing metrics into cortex?
// - That we can configure cortex ruler/alertmanager for the tenant via Opstrace config-api endpoints
//   If this fails, then maybe there is a regression in config-api, the cortex APIs, or the K8s ingress config?
// - That the cortex tenant ruler/alertmanager work and send alerts as configured
//   If this fails, then maybe there is a config issue or regression in cortex ruler/alertmanager?

import { strict as assert, fail } from "assert";

import got, { HTTPError } from "got";

import { KubeConfig } from "@kubernetes/client-node";

import {
  Deployment,
  K8sResource,
  Service,
  V1ServicemonitorResource
} from "@opstrace/kubernetes";

import {
  enrichHeadersWithAuthToken,
  globalTestSuiteSetupOnce,
  httpTimeoutSettings,
  log,
  logHTTPResponse,
  mtime,
  mtimeDeadlineInSeconds,
  rndstring,
  sleep,
  CORTEX_API_TLS_VERIFY,
  OPSTRACE_INSTANCE_DNS_NAME,
  TENANT_DEFAULT_API_TOKEN,
  TENANT_DEFAULT_CORTEX_API_BASE_URL,
  TENANT_SYSTEM_API_TOKEN,
  TENANT_SYSTEM_CORTEX_API_BASE_URL
} from "./testutils";

import { deleteAll, deployAll, waitForAllReady } from "./testutils/deployment";

import {
  waitForCortexMetricResult,
  waitForPrometheusTarget
} from "./testutils/metrics";

function getE2EAlertingResources(
  kubeConfig: KubeConfig,
  tenant: string,
  job: string
): Array<K8sResource> {
  const name = "e2ealerting";
  const namespace = `${tenant}-tenant`;
  const matchLabels = {
    app: name,
    tenant
  };
  // With a randomized 'job' label that will appear in metrics
  const labels = {
    app: name,
    tenant,
    job
  };

  const resources: Array<K8sResource> = [];

  resources.push(
    new Deployment(
      {
        apiVersion: "apps/v1",
        kind: "Deployment",
        metadata: {
          name,
          namespace,
          labels
        },
        spec: {
          replicas: 1,
          selector: {
            matchLabels
          },
          template: {
            metadata: {
              labels
            },
            spec: {
              containers: [
                {
                  name: "e2ealerting",
                  image: "grafana/e2ealerting:master-db38b142",
                  args: ["-server.http-listen-port=8080", "-log.level=debug"],
                  ports: [{ name: "http", containerPort: 8080 }],
                  livenessProbe: {
                    httpGet: {
                      path: "/metrics",
                      // eslint-disable-next-line @typescript-eslint/no-explicit-any
                      port: "http" as any,
                      scheme: "HTTP"
                    },
                    periodSeconds: 10,
                    successThreshold: 1,
                    failureThreshold: 3,
                    timeoutSeconds: 1
                  }
                }
              ]
            }
          }
        }
      },
      kubeConfig
    )
  );

  resources.push(
    new Service(
      {
        apiVersion: "v1",
        kind: "Service",
        metadata: {
          name,
          namespace,
          labels
        },
        spec: {
          ports: [
            {
              name: "http",
              port: 80,
              // eslint-disable-next-line @typescript-eslint/no-explicit-any
              targetPort: "http" as any
            }
          ],
          selector: labels
        }
      },
      kubeConfig
    )
  );

  resources.push(
    new V1ServicemonitorResource(
      {
        apiVersion: "monitoring.coreos.com/v1",
        kind: "ServiceMonitor",
        metadata: {
          name,
          namespace,
          labels
        },
        spec: {
          jobLabel: "job", // point to 'job' label in the pod
          endpoints: [
            {
              // Spam it at 5s to get faster responses in tests
              interval: "5s",
              port: "http",
              path: "/metrics"
            }
          ],
          selector: {
            matchLabels
          }
        }
      },
      kubeConfig
    )
  );

  return resources;
}

async function storeE2EAlertsConfig(authToken: string, tenant: string) {
  // Configure tenant alertmanager to send alerts to e2ealerting service
  // Using cortex API proxied via config-api: https://cortexmetrics.io/docs/api/#set-alertmanager-configuration
  const alertmanagerConfigUrl = `https://config.${tenant}.${OPSTRACE_INSTANCE_DNS_NAME}/api/v1/alerts`;
  const alertmanagerPostResponse = await got.post(alertmanagerConfigUrl, {
    body: `alertmanager_config: |
  receivers:
    - name: e2e-alerting
      webhook_configs:
        - url: http://e2ealerting.${tenant}-tenant.svc.cluster.local/api/v1/receiver
  route:
      group_interval: 1s
      group_wait: 1s
      receiver: e2e-alerting
      repeat_interval: 1s
`,
    throwHttpErrors: false,
    timeout: httpTimeoutSettings,
    headers: enrichHeadersWithAuthToken(authToken, {}),
    https: { rejectUnauthorized: CORTEX_API_TLS_VERIFY }
  });
  logHTTPResponse(alertmanagerPostResponse);
  assert(
    alertmanagerPostResponse.statusCode == 201 ||
      alertmanagerPostResponse.statusCode == 202
  );

  // Configure tenant ruler to fire alert against metric scraped from e2ealerting pod
  // Using cortex API proxied via config-api: https://cortexmetrics.io/docs/api/#set-rule-group
  const ruleGroupConfigUrl = `https://config.${tenant}.${OPSTRACE_INSTANCE_DNS_NAME}/api/v1/rules/testremote`;
  const ruleGroupPostResponse = await got.post(ruleGroupConfigUrl, {
    body: `name: e2ealerting
rules:
  - alert: E2EAlertingAlwaysFiring
    annotations:
        time: '{{ $value }}'
    expr: e2ealerting_now_in_seconds > 0
    for: 5s
`,
    throwHttpErrors: false,
    timeout: httpTimeoutSettings,
    headers: enrichHeadersWithAuthToken(authToken, {}),
    https: { rejectUnauthorized: CORTEX_API_TLS_VERIFY }
  });
  logHTTPResponse(ruleGroupPostResponse);
  assert(
    ruleGroupPostResponse.statusCode == 201 ||
      ruleGroupPostResponse.statusCode == 202
  );
}

// Returns true if any deletes were performed.
async function deleteE2EAlertsConfig(
  authToken: string,
  tenant: string
): Promise<boolean> {
  log.info(`Deleting any alerts configs that exist for tenant=${tenant}`);
  let deletesOccurred = false;

  // Delete any alertmanager config created for tenant earlier
  // Using cortex API proxied via config-api: https://cortexmetrics.io/docs/api/#delete-alertmanager-configuration
  const alertmanagerConfigUrl = `https://config.${tenant}.${OPSTRACE_INSTANCE_DNS_NAME}/api/v1/alerts`;
  const alertmanagerGetResponse = await got.get(alertmanagerConfigUrl, {
    throwHttpErrors: false,
    timeout: httpTimeoutSettings,
    headers: enrichHeadersWithAuthToken(authToken, {}),
    https: { rejectUnauthorized: CORTEX_API_TLS_VERIFY }
  });
  logHTTPResponse(alertmanagerGetResponse);

  // Only delete if the config currently exists (non-404).
  // This reduces the possibility of a rapid delete+set causing a race condition
  if (alertmanagerGetResponse.statusCode !== 404) {
    const alertmanagerDeleteResponse = await got.delete(alertmanagerConfigUrl, {
      throwHttpErrors: false,
      timeout: httpTimeoutSettings,
      headers: enrichHeadersWithAuthToken(authToken, {}),
      https: { rejectUnauthorized: CORTEX_API_TLS_VERIFY }
    });
    logHTTPResponse(alertmanagerDeleteResponse);
    assert(
      alertmanagerDeleteResponse.statusCode == 202 ||
        alertmanagerDeleteResponse.statusCode == 200
    );
    deletesOccurred = true;
  }

  // Delete any 'testremote' rule namespace created for tenant earlier
  // Using cortex API proxied via config-api: https://cortexmetrics.io/docs/api/#delete-namespace
  const ruleGroupConfigUrl = `https://config.${tenant}.${OPSTRACE_INSTANCE_DNS_NAME}/api/v1/rules/testremote`;
  const ruleGroupGetResponse = await got.get(ruleGroupConfigUrl, {
    throwHttpErrors: false,
    timeout: httpTimeoutSettings,
    headers: enrichHeadersWithAuthToken(authToken, {}),
    https: { rejectUnauthorized: CORTEX_API_TLS_VERIFY }
  });
  logHTTPResponse(ruleGroupGetResponse);

  // Only delete if the config currently exists (non-404).
  // This reduces the possibility of a rapid delete+set causing a race condition
  if (ruleGroupGetResponse.statusCode !== 404) {
    const ruleGroupDeleteResponse = await got.delete(ruleGroupConfigUrl, {
      throwHttpErrors: false,
      timeout: httpTimeoutSettings,
      headers: enrichHeadersWithAuthToken(authToken, {}),
      https: { rejectUnauthorized: CORTEX_API_TLS_VERIFY }
    });
    logHTTPResponse(ruleGroupDeleteResponse);
    assert(
      ruleGroupDeleteResponse.statusCode == 202 ||
        ruleGroupDeleteResponse.statusCode == 404
    );
    deletesOccurred = true;
  }

  return deletesOccurred;
}

async function getE2EAlertCountMetric(
  cortexBaseUrl: string,
  authToken: string,
  uniqueScrapeJobName: string
): Promise<string> {
  // Instant query - get current value
  // https://prometheus.io/docs/prometheus/latest/querying/api/#instant-queries
  const queryParams = {
    query: `e2ealerting_webhook_receiver_evaluations_total{job="${uniqueScrapeJobName}"}`
  };
  const resultArray = await waitForCortexMetricResult(
    cortexBaseUrl,
    authToken,
    queryParams,
    "query"
  );

  // Sanity check: label should match
  assert.strictEqual(
    resultArray[0]["metric"]["job"],
    uniqueScrapeJobName,
    "Expected to get matching job label for e2ealerting webhook metric: ${resultArray}"
  );

  const value = resultArray[0]["value"][1];

  // debug-log for 0 to reduce verbosity
  if (value == 0) {
    log.debug(`Got alert count value: ${value}`);
  } else {
    log.info(`Got alert count value: ${value}`);
  }

  return value;
}

async function setupE2EAlertsForTenant(
  kubeConfig: KubeConfig,
  authToken: string,
  tenant: string,
  uniqueScrapeJobName: string
): Promise<Array<K8sResource>> {
  log.info(`Setting up E2E alerts webhook for tenant=${tenant}`);
  await storeE2EAlertsConfig(authToken, tenant);

  log.info(`Deploying E2E alerting resources into ${tenant}-tenant namespace`);
  const resources = getE2EAlertingResources(
    kubeConfig,
    tenant,
    uniqueScrapeJobName
  );
  await deployAll(resources);
  return resources;
}

async function waitForE2EAlertFiring(
  cortexBaseUrl: string,
  authToken: string,
  tenant: string,
  uniqueScrapeJobName: string
) {
  // Wait for the E2E pod to appear in prometheus scrape targets
  // This separate check shouldn't contribute to the time spent on the test, and can help with tracing a test failure.
  log.info(
    `Waiting for E2E scrape target to appear in tenant prometheus for tenant=${tenant} job=${uniqueScrapeJobName}`
  );
  await waitForPrometheusTarget(tenant, authToken, uniqueScrapeJobName);

  // With the alert outputs configured earlier, wait for the e2ealerting webhook to be queried
  // and the count of evaluations to be incremented at least once.
  log.info(
    `Waiting for cortex E2E alerting metric for tenant=${tenant} job=${uniqueScrapeJobName} with nonzero value`
  );
  const deadline = mtimeDeadlineInSeconds(300);
  while (true) {
    if (mtime() > deadline) {
      throw new Error(
        "Failed to get non-zero alerts metric value after 300s. Are alerts successfully reaching the e2ealerting pod?"
      );
    }

    const value = await getE2EAlertCountMetric(
      cortexBaseUrl,
      authToken,
      uniqueScrapeJobName
    );
    if (value !== "0") {
      log.info(`Got alerts metric value for tenant ${tenant}: ${value}`);
      break;
    }

    await sleep(15.0);
  }
}

suite("End-to-end alert tests", function () {
  suiteSetup(async function () {
    log.info("suite setup");
    await globalTestSuiteSetupOnce();
  });

  suiteTeardown(async function () {
    log.info("suite teardown");
  });

  test("config auth sanity checks", async function () {
    // Test against the alertmanager config endpoint
    const url = `https://config.default.${OPSTRACE_INSTANCE_DNS_NAME}/api/v1/alerts`;

    // Missing auth token
    try {
      await got.get(url, {
        throwHttpErrors: true,
        timeout: httpTimeoutSettings,
        headers: {},
        https: { rejectUnauthorized: CORTEX_API_TLS_VERIFY }
      });
      fail("Expected missing token to throw exception");
    } catch (e: any) {
      if (e instanceof HTTPError) {
        log.info(`Got expected error: ${e}`);
      } else {
        throw e;
      }
    }

    // Empty auth token
    try {
      await got.get(url, {
        throwHttpErrors: true,
        timeout: httpTimeoutSettings,
        headers: enrichHeadersWithAuthToken("", {}),
        https: { rejectUnauthorized: CORTEX_API_TLS_VERIFY }
      });
      fail("Expected empty token to throw exception");
    } catch (e: any) {
      if (e instanceof HTTPError) {
        log.info(`Got expected error: ${e}`);
      } else {
        throw e;
      }
    }

    // Bad auth token
    try {
      await got.get(url, {
        throwHttpErrors: true,
        timeout: httpTimeoutSettings,
        headers: enrichHeadersWithAuthToken(
          "bad" + TENANT_DEFAULT_API_TOKEN,
          {}
        ),
        https: { rejectUnauthorized: CORTEX_API_TLS_VERIFY }
      });
      fail("Expected bad token to throw exception");
    } catch (e: any) {
      if (e instanceof HTTPError) {
        log.info(`Got expected error: ${e}`);
      } else {
        throw e;
      }
    }

    // Token for wrong tenant
    try {
      await got.get(url, {
        throwHttpErrors: true,
        timeout: httpTimeoutSettings,
        headers: enrichHeadersWithAuthToken(TENANT_SYSTEM_API_TOKEN, {}),
        https: { rejectUnauthorized: CORTEX_API_TLS_VERIFY }
      });
      fail("Expected system token for default tenant to throw exception");
    } catch (e: any) {
      if (e instanceof HTTPError) {
        log.info(`Got expected error: ${e}`);
      } else {
        throw e;
      }
    }
    try {
      const systemUrl = `https://config.system.${OPSTRACE_INSTANCE_DNS_NAME}/api/v1/alerts`;
      await got.get(systemUrl, {
        throwHttpErrors: true,
        timeout: httpTimeoutSettings,
        headers: enrichHeadersWithAuthToken(TENANT_DEFAULT_API_TOKEN, {}),
        https: { rejectUnauthorized: CORTEX_API_TLS_VERIFY }
      });
      fail("Expected default token for system tenant to throw exception");
    } catch (e: any) {
      if (e instanceof HTTPError) {
        log.info(`Got expected error: ${e}`);
      } else {
        throw e;
      }
    }

    // Valid request
    await got.get(url, {
      throwHttpErrors: true,
      timeout: httpTimeoutSettings,
      headers: enrichHeadersWithAuthToken(TENANT_DEFAULT_API_TOKEN, {}),
      https: { rejectUnauthorized: CORTEX_API_TLS_VERIFY }
    });
  });

  // Verify that users cannot override the internal multitenant header in their requests.
  // For example if default tenant sends a request with a forged X-Scope-OrgID=system,
  // the forged header should be ignored and the request should be applied to the authed default tenant.
  test("config auth header override", async function () {
    // String to search for in the config to detect if it's our config or not.
    const testAuthConfigSearch = `test-auth-${rndstring().slice(0, 5)}`;
    // Test config needs to exactly match what we also get back with a GET
    const configBody = `template_files: {}
alertmanager_config: |
  receivers:
    - name: test-auth
      webhook_configs:
        - url: http://test-auth.svc.cluster.local/${testAuthConfigSearch}
  route:
      group_interval: 1s
      group_wait: 1s
      receiver: test-auth
      repeat_interval: 1s
`;

    const defaultConfigUrl = `https://config.default.${OPSTRACE_INSTANCE_DNS_NAME}/api/v1/alerts`;
    const systemConfigUrl = `https://config.system.${OPSTRACE_INSTANCE_DNS_NAME}/api/v1/alerts`;

    // Clear any existing configs
    await deleteE2EAlertsConfig(TENANT_DEFAULT_API_TOKEN, "default");
    await deleteE2EAlertsConfig(TENANT_SYSTEM_API_TOKEN, "system");

    // Assign config to default tenant, but with header pointing to system tenant (should be ignored)
    await got.post(defaultConfigUrl, {
      body: configBody,
      throwHttpErrors: true,
      timeout: httpTimeoutSettings,
      headers: enrichHeadersWithAuthToken(TENANT_DEFAULT_API_TOKEN, {
        "X-Scope-OrgID": "system"
      }),
      https: { rejectUnauthorized: CORTEX_API_TLS_VERIFY }
    });

    const defaultResponse = await got.get(defaultConfigUrl, {
      throwHttpErrors: true,
      timeout: httpTimeoutSettings,
      headers: enrichHeadersWithAuthToken(TENANT_DEFAULT_API_TOKEN, {}),
      https: { rejectUnauthorized: CORTEX_API_TLS_VERIFY }
    });
    logHTTPResponse(defaultResponse);

    const systemResponse = await got.get(systemConfigUrl, {
      throwHttpErrors: false,
      timeout: httpTimeoutSettings,
      headers: enrichHeadersWithAuthToken(TENANT_SYSTEM_API_TOKEN, {}),
      https: { rejectUnauthorized: CORTEX_API_TLS_VERIFY }
    });
    logHTTPResponse(systemResponse);

    // test-auth config should have been applied to default tenant, despite forged header for system tenant
    assert(defaultResponse.statusCode === 200);
    assert(
      defaultResponse.body.includes(testAuthConfigSearch),
      `expected "${testAuthConfigSearch}" in body: got=${defaultResponse.body}`
    );

    // test-auth config should NOT have been applied to system tenant
    // Cortex seems to have replication delays with fetching/deleting the alertmanager config.
    // As a result of this, we will sometimes get back the prior config that should have been deleted.
    // For this auth test, we just want to ensure that the system tenant never gets the test-auth config,
    // so either a 404 or a previous config is considered passing.
    // See also previous cases of this: https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/1130
    assert(
      systemResponse.statusCode === 404 ||
        !systemResponse.body.includes(testAuthConfigSearch),
      `expected 404 or "${testAuthConfigSearch}" not in body: got=${systemResponse.body}`
    );
  });

  test("End-to-end alerts for default and system tenants", async function () {
    // Give a random token to include for the 'job' label in metrics.
    // This is just in case e.g. tests are re-run against the same cluster.
    // Include '-job' at the end just to avoid punctuation at the end of the label which K8s disallows
    const defaultUniqueScrapeJobName = `testalerts-default-${rndstring().slice(
      0,
      5
    )}-job`;
    const systemUniqueScrapeJobName = `testalerts-system-${rndstring().slice(
      0,
      5
    )}-job`;

    // The test environment should already have kubectl working, so we can use that.
    const kubeConfig = new KubeConfig();
    kubeConfig.loadFromDefault();
    // loadFromDefault will fall back to e.g. localhost if it cant find something.
    // So let's explicitly try to communicate with the cluster.
    const kubeContext = kubeConfig.getCurrentContext();
    if (kubeContext === null) {
      throw new Error(
        "Unable to communicate with kubernetes cluster. Is kubectl set up?"
      );
    }

    // Before deploying anything, delete any existing alertmanager/rulegroup configuration.
    // This avoids an old alert config writing to the newly deployed webhook, making its hit count 1 when we expect 0
    // This should only be a problem when running the same test repeatedly against a cluster.
    log.info("Deleting any preexisting E2E alerts webhooks");
    const deletedDefault = await deleteE2EAlertsConfig(
      TENANT_DEFAULT_API_TOKEN,
      "default"
    );
    const deletedSystem = await deleteE2EAlertsConfig(
      TENANT_SYSTEM_API_TOKEN,
      "system"
    );
    // If any deletes were requested, do a quick sleep() to reduce the likelihood of a
    // race condition between configs being deleted and then assigned.
    // See also: https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/1130
    if (deletedDefault || deletedSystem) {
      log.info("Waiting 15s after configs were deleted");
      await sleep(15.0);
    } else {
      log.info("Continuing immediately after no configs needed to be deleted");
    }

    // To save time, we set up the default and system tenant E2E environments in parallel
    const defaultTestResources = await setupE2EAlertsForTenant(
      kubeConfig,
      TENANT_DEFAULT_API_TOKEN,
      "default",
      defaultUniqueScrapeJobName
    );
    const systemTestResources = await setupE2EAlertsForTenant(
      kubeConfig,
      TENANT_SYSTEM_API_TOKEN,
      "system",
      systemUniqueScrapeJobName
    );
    const testResources = defaultTestResources.concat(systemTestResources);

    // Wait for deployments to be Running/Ready.
    // This may take around 5 minutes if an image pull is flaking/timing out.
    await waitForAllReady(kubeConfig, testResources);

    // With everything deployed, wait for each of the tenants' alerts to start firing
    await waitForE2EAlertFiring(
      TENANT_DEFAULT_CORTEX_API_BASE_URL,
      TENANT_DEFAULT_API_TOKEN,
      "default",
      defaultUniqueScrapeJobName
    );
    await waitForE2EAlertFiring(
      TENANT_SYSTEM_CORTEX_API_BASE_URL,
      TENANT_SYSTEM_API_TOKEN,
      "system",
      systemUniqueScrapeJobName
    );

    // Clean up both tenants
    log.info("Deleting E2E alerts webhooks");
    await deleteE2EAlertsConfig(TENANT_DEFAULT_API_TOKEN, "default");
    await deleteE2EAlertsConfig(TENANT_SYSTEM_API_TOKEN, "system");
    // Don't worry about sleep()ing here since we aren't going to set a new config right away.

    log.info("Deleting E2E alerting resources");
    await deleteAll(testResources);
  });
});
