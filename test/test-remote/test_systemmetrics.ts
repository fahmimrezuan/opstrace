import { strict as assert } from "assert";
import { ZonedDateTime } from "@js-joda/core";
import {
  log,
  globalTestSuiteSetupOnce,
  TENANT_SYSTEM_CORTEX_API_BASE_URL,
  TENANT_SYSTEM_API_TOKEN
} from "./testutils";

import { waitForCortexMetricResult } from "./testutils/metrics";

suite("system metrics test suite", function () {
  suiteSetup(async function () {
    log.info("suite setup");
    await globalTestSuiteSetupOnce();
  });

  suiteTeardown(async function () {
    log.info("suite teardown");
  });

  test("check if container metrics exist", async function () {
    const ts = ZonedDateTime.now();
    const searchStart = ts.minusHours(1);
    const searchEnd = ts.plusHours(1);
    // See opstrace-prelaunch/issues/1866 for a little
    // bit of context -- this is not just an arbitrary query :-).
    const queryParams = {
      query:
        'process_cpu_seconds_total{container="ingester", namespace="cortex"}',
      start: searchStart.toEpochSecond().toString(),
      end: searchEnd.toEpochSecond().toString(),
      step: "1"
    };

    const resultArray = await waitForCortexMetricResult(
      TENANT_SYSTEM_CORTEX_API_BASE_URL,
      TENANT_SYSTEM_API_TOKEN,
      queryParams,
      "query_range"
    );

    // pragmatic criterion for starters: expect a number of values. with
    // the 1-second step size there should be hundreds of values/samples
    // by now.
    assert.strictEqual(resultArray[0]["values"].length > 5, true);
  });
});
