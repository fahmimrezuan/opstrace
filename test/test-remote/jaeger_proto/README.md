# jaeger_proto

This directory contains Jaeger gRPC API definitions used by tests to query Jaeger.

These were downloaded from the [Jaeger API repo](https://github.com/jaegertracing/jaeger-idl/) as follows:

```bash
curl -OL https://github.com/jaegertracing/jaeger-idl/raw/main/proto/api_v2/model.proto \
&& curl -OL https://github.com/jaegertracing/jaeger-idl/raw/main/proto/api_v2/query.proto
```
