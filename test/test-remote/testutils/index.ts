import events from "events";
import fs from "fs";
import os from "os";
import path from "path";
import crypto from "crypto";

import Docker from "dockerode";
import getPort from "get-port";
import mustache from "mustache";
import tmp from "tmp";
import winston from "winston";

import got, { Response as GotResponse } from "got";
import { ZonedDateTime, DateTimeFormatter } from "@js-joda/core";

import { PortForward } from "./portforward";

export const CORTEX_API_TLS_VERIFY = false;

const logFormat = winston.format.printf(({ level, message, timestamp }) => {
  return `${timestamp} ${level}: ${message}`;
});

export const log = winston.createLogger({
  levels: winston.config.syslog.levels,
  level: "info",
  format: winston.format.combine(
    winston.format.splat(),
    winston.format.timestamp(),
    winston.format.colorize(),
    logFormat
  ),
  transports: [new winston.transports.Console()]
});

const TMPDIRPATH = `${os.tmpdir()}${path.sep}opstrace-test-remote`;

export type Dict<T> = Record<string, T>;

/**
 * In mochajs there is no "global hook" that would allow for running setup code
 * once as part of a mocha test suite run. As of
 * https://mochajs.org/#run-cycle-overview we can see that each "spec file" is
 * actually a suite, and while each suite can have its setup code there is no
 * more global hook for the "global test suite" (comprised of multiple per-file
 * test suites, let's call this test session as in pytest). There is the
 * concept of "root hooks", but I found this topic to be too controversial to
 * rely on it, see "ROOT HOOKS ARE NOT GLOBAL"
 * https://mochajs.org/#root-hooks-are-not-global  and also see
 * https://github.com/mochajs/mocha/issues/4308
 *
 * Workaround: do all test session setup logic in here, and then call this from
 * every setupSuite(), apply logic to make sure that the code only runs once.
 *
 * Note that previously the workaround was to simply put relevant code into
 * the body of this module, i.e. this relied on execute-on-first-import.
 * We then started using this module in other contexts (looker) and the side
 * effect of import became intolerable.
 */

// `OPSTRACE_INSTANCE_DNS_NAME` points to a specific Opstrace instance,
// opens the UI if accessed in the browser via https://....
export let OPSTRACE_INSTANCE_DNS_NAME: string;
export let CLUSTER_BASE_URL: string;
export let CLUSTER_NAME: string;
export let TEST_REMOTE_ARTIFACT_DIRECTORY: string;

export let TENANT_DEFAULT_CORTEX_API_BASE_URL: string;
export let TENANT_DEFAULT_DD_API_BASE_URL: string;
export let TENANT_DEFAULT_API_TOKEN: string;

export let TENANT_SYSTEM_CORTEX_API_BASE_URL: string;
export let TENANT_SYSTEM_API_TOKEN: string;

export const CI_LOGIN_EMAIL = "ci-test@opstrace.com";
export const CI_LOGIN_PASSWORD = "This-is-not-a-secret!";

let globalTestSuiteSetupPerformed = false;
export async function globalTestSuiteSetupOnce() {
  log.info("globalTestSuiteSetupOnce()");

  if (globalTestSuiteSetupPerformed) {
    return;
  }
  globalTestSuiteSetupPerformed = true;

  const clusterName: string = process.env.OPSTRACE_CLUSTER_NAME || "";
  if (!clusterName) {
    log.error("env variable OPSTRACE_CLUSTER_NAME must be set");
    process.exit(1);
  }

  const provider: string = process.env.OPSTRACE_CLOUD_PROVIDER || "";
  if (!provider) {
    log.error(
      "env variable OPSTRACE_CLOUD_PROVIDER must be set to `aws` or `gcp`"
    );
    process.exit(1);
  }

  // Create a definite temporary directory for this test runner, within the
  // operating system's TMPDIR (shared across test runner invocation,
  // "insecure" properties are fine). Subsequently put unique and "secure"
  // per-run temporary files in there. The function `createTempfile()` below is
  // the way to do that.

  try {
    fs.mkdirSync(TMPDIRPATH, { mode: 0o777 });
    log.info("Created %s", TMPDIRPATH);
  } catch (err: any) {
    if (err.code !== "EEXIST") throw err;
  }

  // When the test runner is meant to create output artifacts (files) then it
  // needs to know which directory it's supposed to write them in. Default to
  // the current working directory but allow this to be set via env var -- used
  // for containerized invocation of the test runner during `make test-remote`.
  TEST_REMOTE_ARTIFACT_DIRECTORY =
    process.env.TEST_REMOTE_ARTIFACT_DIRECTORY || ".";

  // throws an error if TEST_REMOTE_ARTIFACT_DIRECTORY does not exist.
  if (!fs.lstatSync(TEST_REMOTE_ARTIFACT_DIRECTORY).isDirectory()) {
    log.error(
      "TEST_REMOTE_ARTIFACT_DIRECTORY does not seem to be a directory: %s",
      TEST_REMOTE_ARTIFACT_DIRECTORY
    );
    process.exit(1);
  }

  log.info(
    "Using TEST_REMOTE_ARTIFACT_DIRECTORY: %s",
    TEST_REMOTE_ARTIFACT_DIRECTORY
  );

  CLUSTER_NAME = clusterName;

  // default, if   OPSTRACE_INSTANCE_DNS_NAME is not set via env
  OPSTRACE_INSTANCE_DNS_NAME = `${clusterName}.opstrace.io`;

  if (process.env.OPSTRACE_INSTANCE_DNS_NAME) {
    log.info(
      "env variable OPSTRACE_INSTANCE_DNS_NAME is set: %s",
      process.env.OPSTRACE_INSTANCE_DNS_NAME
    );
    OPSTRACE_INSTANCE_DNS_NAME = process.env.OPSTRACE_INSTANCE_DNS_NAME;
  }

  // no trailing slash
  CLUSTER_BASE_URL = `https://${OPSTRACE_INSTANCE_DNS_NAME}`;

  TENANT_DEFAULT_DD_API_BASE_URL = `https://dd.default.${OPSTRACE_INSTANCE_DNS_NAME}`;
  TENANT_DEFAULT_CORTEX_API_BASE_URL = `https://cortex.default.${OPSTRACE_INSTANCE_DNS_NAME}`;
  TENANT_SYSTEM_CORTEX_API_BASE_URL = `https://cortex.system.${OPSTRACE_INSTANCE_DNS_NAME}`;

  log.info("CLUSTER_BASE_URL: %s", CLUSTER_BASE_URL);

  log.info(
    "TENANT_DEFAULT_DD_API_BASE_URL: %s",
    TENANT_DEFAULT_DD_API_BASE_URL
  );

  log.info(
    "TENANT_DEFAULT_CORTEX_API_BASE_URL: %s",
    TENANT_DEFAULT_CORTEX_API_BASE_URL
  );

  log.info(
    "TENANT_SYSTEM_CORTEX_API_BASE_URL: %s",
    TENANT_SYSTEM_CORTEX_API_BASE_URL
  );

  // Read auth tokens from provided files (optional), or get new tokens via port forward
  const defaultTokenPath = process.env.TENANT_DEFAULT_API_TOKEN_FILEPATH;
  if (defaultTokenPath) {
    log.info("Using default tenant token from file: %s", defaultTokenPath);
    TENANT_DEFAULT_API_TOKEN = readAuthTokenFile(defaultTokenPath);
  } else {
    TENANT_DEFAULT_API_TOKEN = await generatePortForwardAuthToken("default");
  }
  const systemTokenPath = process.env.TENANT_SYSTEM_API_TOKEN_FILEPATH;
  if (systemTokenPath) {
    log.info("Using system tenant token from file: %s", systemTokenPath);
    TENANT_SYSTEM_API_TOKEN = readAuthTokenFile(systemTokenPath);
  } else {
    TENANT_SYSTEM_API_TOKEN = await generatePortForwardAuthToken("system");
  }
}

/**
 * Create a new temporary file, with a base name containing randomness. The
 * absolute path has the structure $TMPDIR_ON_HOST/TEST_RUNNER_DIR/<basename>.
 *
 * The file is created with the lib https://raszi.github.io/node-tmp/
 *
 * Return the absolute path to the file.
 */
export function createTempfile(prefix: string, suffix: string): string {
  // Expected to return absolute path.
  return tmp.fileSync({
    dir: TMPDIRPATH,
    prefix: prefix,
    postfix: suffix,
    keep: true
  }).name;
}

// @ts-ignore: implicit any type
export function testName(thisFromMocha) {
  // https://stackoverflow.com/a/26534968/145400
  // `thisFromMocha` is the context object passed to a Mocha test. Extract the
  // test's full name (containing the test suite name) and generate an ID from
  // it which is human-readable but does not contain whitespace, only contains
  // alphanumeric and lowercase characters.
  return thisFromMocha.test.fullTitle().replace(/\W+/g, "-").toLowerCase();
}

export function rndstring(length = 5) {
  /*
  Calling

    rndstrings.push(rndstring(10));

  10^6 times on my machine takes ~4 seconds.
  */
  return crypto
    .randomBytes(length + 1)
    .toString("base64")
    .replace(/\//g, "_")
    .replace(/\+/g, "_")
    .replace(/=/g, "")
    .slice(0, length);
}

export function rndstringFast(length = 5) {
  return crypto
    .randomBytes(length + 1)
    .toString("base64")
    .slice(0, length);
}

export function rndstringFastBoringFill(rndlen: number, boringlen: number) {
  return (
    crypto
      .randomBytes(rndlen + 1)
      .toString("base64")
      .slice(0, rndlen) + "a".repeat(boringlen)
  );
}

/**
 * Translate a joda datetime object into a an integer string, indicating the
   number of nanoseconds passed since epoch. Example:

  timestamp('2001-01-05T10:00:01.123456789Z') --> '978688801123456789'

  The outcome is used common way to represent a specific point in time in the
  Prometheus ecosystem, e.g. used in the HTTP API.

  The `ZonedDateTime` object `ts` might not have a sub-second resolution, in
  which case `ts.nano()` seems to still return `0`. Zero-pad the nano-second
  part, i.e. if `ts.nano()` returns 1, append a '000000001'.
 */
export function timestampToNanoSinceEpoch(ts: ZonedDateTime): string {
  return `${ts.toEpochSecond()}${ts.nano().toString().padStart(9, "0")}`;
}

export function timestampToRFC3339Nano(ts: ZonedDateTime): string {
  /*
  Return a timestamp string using the text format that Go's standard library
  calls RFC3339Nano, an ISO 8601 timestamp string with nanosecond resolution.

  Ref: https://js-joda.github.io/js-joda/manual/formatting.html

  DateTimeFormatter is seemingly not documented, but
  https://github.com/js-joda/js-joda/issues/181 shows how to make complex
  patterns, in particular how to escape arbitrary text within the pattern
  string.

  */
  if (ts.zone().toString() !== "Z") throw Error("code assumes Zulu time");
  return ts.format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.n'Z'"));
}

export function sleep(seconds: number) {
  // https://stackoverflow.com/a/39914235/145400
  return new Promise(resolve => setTimeout(resolve, seconds * 1000.0));
}

// https://nodejs.org/api/process.html#process_process_hrtime_time
// https://stackoverflow.com/a/58945714/145400
export function mtime(): bigint {
  // The clock source is an in-process monotonic time source with high temporal
  // resolution. The absolute value is meaningless. The difference between
  // consecutively obtained values (type BigInt) is the wall time passed in
  // nanoseconds.
  return process.hrtime.bigint();
}

/*
Return time difference of now compared to `ref` in seconds. Ret

`ref` must be a value previously obtained from `mtime()`. Number()
converts a BigInt to a regular Number type, allowing for translating from
nanoseconds to seconds with a simple division, retaining sub-second
resolution. This assumes that the measured time duration does not grow
beyond 104 days.
*/
export function mtimeDiffSeconds(ref: bigint): number {
  return Number(process.hrtime.bigint() - ref) / 10 ** 9;
}

export function mtimeDeadlineInSeconds(seconds: number): bigint {
  return process.hrtime.bigint() + BigInt(seconds * 10 ** 9);
}

export function mtimeDeadlineTimeLeftSeconds(deadline: bigint): number {
  // given a deadline as returned by `mtimeDeadlineInSeconds` calculate
  // the time left in seconds from _now_ until that deadline is hit.
  return Number(deadline - mtime()) / 10 ** 9;
}

export async function readFirstNBytes(
  path: fs.PathLike,
  n: number
): Promise<Buffer> {
  const chunks = [];
  for await (const chunk of fs.createReadStream(path, { start: 0, end: n })) {
    chunks.push(chunk);
  }
  return Buffer.concat(chunks);
  //log.debug("read head of file: %s", fileHead);
}

// Generates a new auth token by querying the tenant Grafana via port forward
export async function generatePortForwardAuthToken(
  tenant: string
): Promise<string> {
  // We could technically use 'kubectl exec -- curl http://localhost' against the Grafana pod,
  // but the existing port-forward support is already stable and works well for HTTP requests.
  const portForwardGrafana = new PortForward(
    `authtoken-${tenant}`, // name (arbitrary/logging)
    "deployments/grafana", // k8sobj
    3000, // port
    `${tenant}-tenant` // namespace
  );
  const localPortGrafana = await portForwardGrafana.setup();

  const headers = {
    "x-auth-request-email": "test-remote@testing.opstrace"
  };

  try {
    const authToken = await addAuthToken(
      `http://localhost:${localPortGrafana}`,
      headers
    );
    return authToken;
  } finally {
    await portForwardGrafana.terminate();
  }
}

export async function addAuthToken(
  grafanaUrl: string,
  headers: Record<string, string>
): Promise<string> {
  headers["Content-Type"] = "application/json";
  const response = await got.post(`${grafanaUrl}/api/auth/keys`, {
    // - Key name needs to not collide with existing API keys.
    // - Set expiration to four hours. Should be plenty of time for tests to complete...
    body: `{"name": "test-remote-${rndstring()}", "role": "Editor", "secondsToLive": 14400}`,
    throwHttpErrors: false,
    timeout: httpTimeoutSettings,
    headers,
    https: { rejectUnauthorized: false } // skip tls cert verification, if any
  });

  // Don't log the response unless there's a failure - success response has credentials!
  if (response.statusCode != 200) {
    logHTTPResponse(response);
    throw new Error("failed to fetch auth token");
  }

  try {
    // Response should be something like:
    // {"id":6,"name":"test-remote-abc123","key":"[credential]"}
    return JSON.parse(response.body)["key"];
  } catch (e: any) {
    logHTTPResponse(response);
    throw e;
  }
}

// Reads and returns the auth token from the provided file
function readAuthTokenFile(authTokenFilepath: string): string {
  log.debug("read token from %s", authTokenFilepath);
  const tenantAuthToken = fs
    .readFileSync(authTokenFilepath, {
      encoding: "utf8"
    })
    .trim();
  if (!tenantAuthToken) {
    throw new Error(
      "auth token file defined, but file is empty: ${tokenFilepath}"
    );
  }
  return tenantAuthToken;
}

// Manually applies the specified token file to the headers.
export function enrichHeadersWithAuthToken(
  authToken: string,
  headers: Record<string, string>
): Record<string, string> {
  headers["Authorization"] = `Bearer ${authToken}`;
  return headers;
}

// get container host depending on docker env variables
function containerHost(): string {
  const hostEnv = process.env.DOCKER_HOST;
  if (hostEnv) {
    const hostUrl = new URL(hostEnv);
    return hostUrl.hostname;
  }

  return "127.0.0.1";
}

export async function sendMetricsWithPromContainer(
  remoteWriteUrl: string,
  indexFieldValue: string,
  apiToken: string
) {
  // Allow for more than one of these containers / operations to run
  // concurrently by choosing a dynamic port for that prometheus instance to
  // listen on. This `getPort()` technique is still subject to race conditions
  // because between calling getPort() and then starting prometheus a little
  // bit of time passes (~1 second), and within that time another racer might
  // steal this port from us. That's however already quite unlikely and much
  // better than using the same port for all instances.

  const promListenPort = await getPort();

  const prometheusConfPath = `${__dirname}/../containers/prometheus`;
  const renderedConfigText = mustache.render(
    fs.readFileSync(`${prometheusConfPath}/prom.conf.template`, {
      encoding: "utf-8"
    }),
    {
      remote_write_url: remoteWriteUrl,
      index_field_value: indexFieldValue,
      prom_listen_port: promListenPort,
      bearerToken: apiToken
    }
  );

  const promConfigFilePath = `${prometheusConfPath}/prom.conf`;
  fs.writeFileSync(promConfigFilePath, renderedConfigText, {
    encoding: "utf-8"
  });

  // Make Unix user in Prom container otherwise be able to read the file.
  fs.chmodSync(promConfigFilePath, 0o774);

  const outfilePath = createTempfile("prom-container-", ".outerr");
  const outstream = fs.createWriteStream(outfilePath);
  await events.once(outstream, "open");

  log.info(
    "start containerized Prometheus with config:\n%s",
    renderedConfigText
  );
  const docker = new Docker();

  const mounts: Docker.MountConfig = [
    {
      Type: "bind",
      Source: promConfigFilePath,
      Target: "/etc/prometheus/prometheus.yml",
      ReadOnly: true
    }
  ];

  const cont = await docker.createContainer({
    Image: "prom/prometheus:v2.33.5",
    AttachStdin: false,
    AttachStdout: false,
    AttachStderr: false,
    Tty: false,
    Cmd: ["--config.file=/etc/prometheus/prometheus.yml", "--log.level=debug"],
    HostConfig: {
      Mounts: mounts,
      PortBindings: {
        "9090/tcp": [{ HostPort: `${promListenPort}` }]
      }
    }
  });

  log.info("attach file-backed stream");
  const stream = await cont.attach({
    stream: true,
    stdout: true,
    stderr: true
  });
  stream.pipe(outstream);

  log.info("container start()");
  await cont.start();

  const logNeedle = "Server is ready to receive web requests";
  const maxWaitSeconds = 15;
  const deadline = mtimeDeadlineInSeconds(maxWaitSeconds);
  log.info(
    "Waiting for needle to appear in container log, deadline in %s s. Needle: %s",
    maxWaitSeconds,
    logNeedle
  );

  while (true) {
    if (mtime() > deadline) {
      log.info("deadline hit");
      await terminateContainer(cont, outfilePath);
      throw new Error("Prometheus container setup failed: deadline hit");
    }
    const fileHeadBytes = await readFirstNBytes(outfilePath, 10 ** 4);
    if (fileHeadBytes.includes(Buffer.from(logNeedle, "utf-8"))) {
      log.info("log needle found in container log, proceed");
      break;
    }
    await sleep(0.1);
  }

  const maxWaitSeconds2 = 30;
  const deadline2 = mtimeDeadlineInSeconds(maxWaitSeconds2);
  log.info(
    "wait for containerized Prom to scrape & remote_write metrics, deadline in %s s",
    maxWaitSeconds2
  );
  const t0 = mtime();
  const rgx =
    /\sprometheus_remote_storage_samples_total{.*} (?<count>[0-9]+)\s/;
  const metricsUrl = `http://${containerHost()}:${promListenPort}/metrics`;
  log.info("Using prometheus container metrics url: %s", metricsUrl);

  while (true) {
    await sleep(0.1);

    if (mtime() > deadline2) {
      log.info("deadline hit");
      await terminateContainer(cont, outfilePath);
      throw new Error(
        `deadline hit while waiting for expected data on ${metricsUrl}`
      );
    }

    let response: GotResponse<string>;
    try {
      response = await got(metricsUrl, {
        throwHttpErrors: false,
        timeout: httpTimeoutSettings
      });
    } catch (e) {
      log.error(e);
      continue;
    }
    if (response.statusCode == 200 && response.body) {
      // log.info(response.body);
      // With got 9.6 the `response.body` object is of type `string`
      const match = response.body.match(rgx);
      if (!match) continue;
      const groups = match.groups;
      if (!groups) continue;
      const count = groups.count;
      log.debug("count: %s", count);
      if (Number(count) > 0) {
        log.info("prometheus_remote_storage_samples_total > 0: %s", count);
        log.info("this took %s s", mtimeDiffSeconds(t0).toFixed(2));
        break;
      }
    } else {
      log.error("Got unexpected HTTP response from Prom /metrics");
      logHTTPResponse(response);
    }
  }

  await terminateContainer(cont, outfilePath);
}

export function logHTTPResponse(
  resp: GotResponse<string> | GotResponse<Buffer>
) {
  // `slice()` works regardless of Buffer or string.
  let bodyPrefix = resp.body.slice(0, 500);
  // If buffer: best-effort decode the buffer into text (this method does _not_
  // not blow up upon unexpected byte sequences).
  if (Buffer.isBuffer(bodyPrefix)) bodyPrefix = bodyPrefix.toString("utf-8");

  // about timings also see
  // https://gehrcke.de/2020/02/nodejs-http-clientrequest-finished-event-has-the-request-body-been-flushed-out/
  // Timings are exposed in milliseconds elapsed since the UNIX epoch, that is
  // where the /1000.0 comes from. Individual properties in the `.phases`
  // object may be `undefined` as far as the compiler is concerned and
  // potentially during runtime (I don't quite see how that can happen, but
  // anyway). In those cases use "nullish coalescing" to emit a negative
  // number.
  const ts = resp.timings;
  log.info(`HTTP resp to ${resp.request.options.method}(${resp.requestUrl}):
  status: ${resp.statusCode}
  body[:500]: ${bodyPrefix}
  headers: ${JSON.stringify(resp.headers)}
  totalTime: ${(ts.phases.total ?? -1000) / 1000.0} s
  dnsDone->TCPconnectDone: ${(ts.phases.tcp ?? -1000) / 1000.0} s
  connectDone->reqSent ${(ts.phases.request ?? -1000) / 1000.0} s
  reqSent->firstResponseByte: ${(ts.phases.firstByte ?? -1000) / 1000.0} s
  `);
}

export function logHTTPResponseLight(resp: GotResponse) {
  const t1 = (resp.timings.phases.total ?? -1000) / 1000.0;
  const t2 = (resp.timings.phases.firstByte ?? -1000) / 1000.0;
  log.info(
    `resp to ${resp.request.options.method} -> ${
      resp.statusCode
    }. total time: ${t1.toFixed(2)} s. resp time: ${t2.toFixed(2)} `
  );
}

// Generic HTTP timeout settings object for HTTP requests made with `got`. Note
// that every time that this test suite fires off an HTTP request we should
// timeout-control the individual request phases (by default `got` waits
// indefinitely, in every phase of the request). There is no one-size fits all
// solution. This here is optimized for HTTP requests that _should_ be snappy,
// i.e. where the request is _small_, and where the response generation is
// expected to take O(1 s) or less. If a test triggers an HTTP
// request where response generation is known to take a 'long' time then this
// should be overridden test-locally.
export const httpTimeoutSettings = {
  // If a TCP connect() takes longer then ~5 seconds then most certainly there
  // is a networking issue, fail fast in that case (whether or not to retry
  // needs to be decided on a test-by-test basis, do not use the got-internal
  // retrying here)
  connect: 6000,
  // After TCP-connect, this controls the time it takes for the TLS handshake
  secureConnect: 5000,
  // after (secure)connect, require request to be written to connection within
  // ~10 seconds.
  send: 6000,
  // After having written the request, expect response _headers_ to arrive
  // within that time (not the complete response).
  response: 10000,
  // global timeout, supposedly until final response byte arrived.
  request: 15000
};

/**
 * Return object (deserialized JSON response body) or throw an error
 */
export async function queryJSONAPI(
  url: string,
  authToken: string,
  queryParams: URLSearchParams
) {
  const headers = enrichHeadersWithAuthToken(authToken, {});

  const options = {
    throwHttpErrors: false,
    retry: 0,
    searchParams: queryParams,
    timeout: httpTimeoutSettings,
    headers: headers,
    https: { rejectUnauthorized: CORTEX_API_TLS_VERIFY } // disable TLS server cert verification for now
  };

  const response = await got(url, options);
  if (response.statusCode != 200) {
    logHTTPResponse(response);
  }

  try {
    return JSON.parse(response.body);
  } catch (e: any) {
    log.error("Failed to parse response as JSON:");
    logHTTPResponse(response);
    throw e;
  }
}

// Generic function for polling a JSON endpoint and returning a result after it passes a check.
export async function waitForQueryResult<T>(
  // Callback for executing the query itself
  queryFunc: { (): Promise<T> },
  // Callback for checking the result for an expected value, returning null if the query should retry
  testFunc: { (queryResponse: T): any | null },
  maxWaitSeconds = 120,
  logQueryResponse = false
) {
  const deadline = mtimeDeadlineInSeconds(maxWaitSeconds);
  log.info(
    "Waiting for query to return a match, deadline in %ss",
    maxWaitSeconds
  );

  while (true) {
    if (mtime() > deadline) {
      log.error("Reached %s s deadline, giving up", maxWaitSeconds);
      break;
    }

    let data: any;
    try {
      data = await queryFunc();
    } catch (e: any) {
      // handle any error that happened during http request processing
      if (e instanceof got.RequestError) {
        log.info(
          `waitForQueryResult() loop: http request failed: ${e.message} -- ignore, proceed with next iteration`
        );
        continue;
      } else {
        // Throw any other error, mainly programming error.
        throw e;
      }
    }

    if (logQueryResponse) {
      log.info("Query response data:\n%s", JSON.stringify(data, null, 2));
    }

    const testResult = testFunc(data);
    if (testResult != null) {
      return testResult;
    }

    // trade-off: snappiness and log volume. info-log every failed attempt,
    // this has turned out real important for being able to understand and
    // debug a test run's output.
    const delayseconds = 8.0;
    log.info(
      "query response data not yet as expected, retry query in %s s",
      delayseconds
    );
    await sleep(delayseconds);
  }
  throw new Error(`Expectation not fulfilled within ${maxWaitSeconds} s`);
}

async function sigkillContInNSeconds(
  cont: Docker.Container,
  n: number
): Promise<void> {
  await sleep(n);
  try {
    log.info("send SIGKILL");
    cont.kill({ signal: "SIGKILL" });
  } catch (err: any) {
    log.warning("error sending SIGKILL: %s", err.message);
  }
}

export async function terminateContainer(
  cont: Docker.Container,
  outfilePath: string
): Promise<void> {
  log.info("terminate container, send SIGTERM");

  try {
    await cont.kill({ signal: "SIGTERM" });
  } catch (err: any) {
    log.warning("could not kill container: %s", err.message);
  }

  // Spawn function that sends SIGKILL in 15 seconds, but don't wait for this
  // yet. This is because ususally a _clean_ shutdown procedure is initiated
  // by SIGTERM, but that may actually not lead to timely termination.
  const killjob = sigkillContInNSeconds(cont, 10);

  log.info("wait for container to stop");
  try {
    await cont.wait();
  } catch (err: any) {
    log.warning("error waiting for container: %s", err.message);
  }

  log.info("wait for killjob");
  await killjob;

  log.info("force-remove container");
  try {
    await cont.remove({ force: true });
  } catch (err: any) {
    log.warning("could not remove container: %s", err.message);
  }
  log.info(
    "log output emitted by container (stdout/err from %s):\n%s",
    outfilePath,
    fs.readFileSync(outfilePath, {
      encoding: "utf-8"
    })
  );
}
