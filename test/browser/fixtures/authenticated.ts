import { Cookie, TestType } from "@playwright/test";
import fs from "fs";

import { performLogin, log } from "../utils";

type AuthenticationFixture = {
  authCookies: Cookie[];
};

export const addAuthFixture = (test: TestType) =>
  test.extend<Record<string, never>, AuthenticationFixture>({
    authCookies: [
      async ({ browser, system, cluster, user }, use) => {
        if (system.workerAuth) {
          const REUSE_STATE =
            process.env.OPSTRACE_PLAYWRIGHT_REUSE_STATE === "true";
          const STATE_FILENAME = `contextState-${cluster.name}.json`;

          const statePresent = REUSE_STATE && validStatePresent(STATE_FILENAME);

          const context = await browser.newContext({
            ignoreHTTPSErrors: true,
            storageState: statePresent ? STATE_FILENAME : undefined
          });
          const page = await context.newPage();

          if (!statePresent) {
            await performLogin({ page, cluster, user });
            if (REUSE_STATE)
              await context.storageState({ path: STATE_FILENAME });
          }

          const cookies = await page.context().cookies();
          await page.close();

          await use(cookies);
        } else await use(null);
      },
      { scope: "worker", auto: true }
    ]
  });

const ONE_HOUR = 60 * 60 * 1000;
const ONE_DAY = ONE_HOUR * 24;

const validStatePresent = (filename: string) => {
  if (fs.existsSync(filename)) {
    // is the file less than a day old?
    return (
      new Date(fs.statSync(filename).ctime.getTime() + ONE_DAY) > new Date()
    );
  }

  return false;
};
