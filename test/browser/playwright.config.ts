import { PlaywrightTestConfig } from "@playwright/test";

const config: PlaywrightTestConfig = {
  reporter: "dot",
  // Time in milliseconds given to each test, 120 seconds in our case, this is NOT the timeout option that context and page functions accept
  timeout: 120_000,
  use: {
    headless: true,
    viewport: { width: 1280, height: 720 },
    ignoreHTTPSErrors: true,
    screenshot: "only-on-failure",
    trace: "retain-on-failure",
    video: "retain-on-failure"
  },
  projects: [
    {
      name: "Chromium",
      use: {
        browserName: "chromium",
        launchOptions: {
          args: ["--disable-dev-shm-usage"] // https://playwright.dev/python/docs/ci/#docker
        }
      }
    }
    // {
    //   name: "Firefox",
    //   use: { browserName: "firefox" }
    // },
    // {
    //   name: "WebKit",
    //   use: { browserName: "webkit" }
    // }
    // {
    //   name: "Pixel-4",
    //   use: {
    //     browserName: "chromium",
    //     ...devices["Pixel 4"]
    //   }
    // },
    // {
    //   name: "iPhone-11",
    //   use: {
    //     browserName: "webkit",
    //     ...devices["iPhone 11"]
    //   }
    // }
  ]
};

export default config;
