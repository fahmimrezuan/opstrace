import { expect } from "@playwright/test";

import useFixtures, { test } from "../fixtures";
import { restoreLogin } from "../utils/authentication";

const testWithAuth = useFixtures("auth");
const testNoAuth = test;

testWithAuth.describe("debugging", () => {
  testWithAuth.beforeEach(restoreLogin);

  testWithAuth("this should FAIL all the time", async () => {
    testWithAuth.skip(true, "for debugging only");
    expect(false).toBeTruthy();
  });
});

testNoAuth.describe("debugging - no auth", () => {
  testNoAuth(
    "can I manually login this way?",
    async ({ page, cluster, user }) => {
      testNoAuth.skip(true, "for debugging only");
      await page.goto(cluster.baseUrl);

      // <button class="MuiButtonBase-root Mui... MuiButton-sizeLarge" tabindex="0" type="button">
      // <span class="MuiButton-label">Log in</span>
      await page.waitForSelector("css=button");

      await page.click("text=Log in");

      // Wait for CI-specific username/pw login form to appear
      await page.waitForSelector("text=Don't remember your password?");

      await page.fill("css=input[type=email]", user.email);
      await page.fill("css=input[type=password]", user.password);

      await page.click("css=button[type=submit]");

      // The first view after successful login is expected to be the details page
      // for the `system` tenant, showing a link to Grafana.
      await page.waitForSelector("[data-test=getting-started]");

      expect(await page.isVisible("[data-test=getting-started]")).toBeTruthy();
    }
  );
});
