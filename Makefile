include Makefile.shared

# Load all go projects to be used for target generation
GO_PROJECTS ?= $(shell sed -n -e 's/^.*GOMOD_PATH: //p' .goprojects.yml)

# The version string representing the current checkout / working directory.
# This for example defines the controller docker image tag. The default `dev`
# suffix represents a local dev environment. Override CHECKOUT_VERSION_STRING
# with a different suffix (e.g. `ci`) in the CI environment so that the
# version string attached to build artifacts reveals the environment that the
# build artifact was created in.
export CHECKOUT_VERSION_STRING ?= $(shell git rev-parse --short=9 HEAD)-dev

# CI image used as a base image for most of the CI jobs.
# This can be used locally to run Makefile targets as if running in CI.
CI_IMAGE ?= $(IMAGE_REGISTRY)/ci:$(CHECKOUT_VERSION_STRING)

# Allow this to be set via environment, default for local dev setup.
export OPSTRACE_KUBECFG_FILEPATH_ONHOST ?= ${HOME}/.kube/config

# For the local dev setup set the build dir to be the absolute path to cwd. For
# example, that is required to make `make test-remote` work when started
# locally. Note(JP): I think this might be in conflict with some other
# thinking, question that, test things.
export OPSTRACE_BUILD_DIR ?= $(shell pwd)

# buildinfo path used in tests
export OPSTRACE_BUILDINFO_PATH = $(shell pwd)/buildinfo.json

# Name of the cloud platform. Supported values are gcp and aws.
export OPSTRACE_CLOUD_PROVIDER ?= gcp

export OPSTRACE_GCP_PROJECT_ID ?= vast-pad-240918

# Defaults for GCP cloud platform (does not work for AWS)
ifeq (gcp,$(OPSTRACE_CLOUD_PROVIDER))
	OPSTRACE_REGION ?= us-west2
	OPSTRACE_ZONE ?= a
endif

# Defaults for AWS cloud platform
ifeq (aws,$(OPSTRACE_CLOUD_PROVIDER))
	OPSTRACE_REGION ?= us-west-2
endif

KERNEL_NAME := $(shell uname -s | tr A-Z a-z)


$(info --------------------------------------------------------------)
$(info OPSTRACE_CLUSTER_NAME is $(OPSTRACE_CLUSTER_NAME))
$(info OPSTRACE_BUILD_DIR is $(OPSTRACE_BUILD_DIR))
$(info OPSTRACE_ARTIFACT_DIR is $(OPSTRACE_ARTIFACT_DIR))
$(info OPSTRACE_CLOUD_PROVIDER is $(OPSTRACE_CLOUD_PROVIDER))
$(info OPSTRACE_KUBECFG_FILEPATH_ONHOST is $(OPSTRACE_KUBECFG_FILEPATH_ONHOST))
$(info CHECKOUT_VERSION_STRING is $(CHECKOUT_VERSION_STRING))
$(info KERNEL_NAME is $(KERNEL_NAME))
$(info --------------------------------------------------------------)

# Public interface. Below are the important targets, used on a daily basis by
# people. These targets need to stabilize (first the concepts, then the names).

lint_targets = $(addprefix lint-code-, $(GO_PROJECTS))

.PHONY: lint-code
lint-code: $(lint_targets)
lint-code-%:
	cd $* && golangci-lint run --allow-parallel-runners --timeout 5m

.PHONY: lint-docs
lint-docs:
	yarn run markdownlint README.md 'docs/**/*.md'


build_targets = $(addprefix build-, $(GO_PROJECTS))
.PHONY: build
build: $(build_targets)

build-%:
	cd $* && make build

tests_targets = $(addprefix unit-tests-, $(GO_PROJECTS))
.PHONY: unit-tests
unit-tests: $(tests_targets)

unit-tests-%:
	cd $* && make unit-tests


.PHONY: tsc
tsc: set-build-info-constants
	@# tsc-compile the opstrace cli and controller cli
	# --ignore-optional to skip playwright
	yarn --frozen-lockfile --ignore-optional
	yarn build:controller
	yarn build:cli


# `make dependencies` has been part of a local dev workflow for many months,
# support this for legacy reasons for now. Will have to get used to new,
# cleaner concepts in the future, though
.PHONY: dependencies
dependencies: tsc cli-pkg


.PHONY: cli
cli: cli-tsc cli-pkg


# Files specific to Opstrace, Inc. development.
.PHONY: fetch-secrets
fetch-secrets:
	@echo "--- fetching secrets"
	@# Fetch secrets, expected to be done before every cloud deployment.
	aws s3 cp "s3://buildkite-managedsecretsbucket-100ljuov8ugv2/" secrets/ --recursive --exclude "*" \
	--include "aws-credentials.json" \
	--include "aws-dev-svc-acc-env.sh" \
	--include "docker-credentials.json" \
	--include "opstrace_dockerhub_creds.sh" \
	--include "ci.id_rsa" \
	--include "ci.id_rsa.pub" \
	--include "opstrace-collection-cluster-authtoken-secrets.yaml" \
	--include "dns-service-login-for-ci.json" \
	--include "dns-service-magic-id-token-for-ci" \
	--include "gcp-svc-acc-dev-dns-service.json" \
	--include "gcp-svc-acc-ci-shard-aaa.json" \
	--include "gcp-svc-acc-ci-shard-bbb.json" \
	--include "gcp-svc-acc-ci-shard-ccc.json" \
	--include "gcp-svc-acc-ci-shard-ddd.json" \
	--include "gcp-svc-acc-ci-shard-eee.json" \
	--include "gcp-svc-acc-ci-shard-fff.json" \
	--include "gcp-svc-acc-ci-shard-ggg.json" \
	--include "gcp-svc-acc-ci-shard-hhh.json" \
	--include "gcp-svc-acc-ci-shard-iii.json" \
	--include "gcp-svc-acc-cost-test-1.json"
	chmod 600 secrets/ci.id_rsa


.PHONY: kconfig
kconfig: checkenv-clustername kconfig-$(OPSTRACE_CLOUD_PROVIDER)

.PHONY: kconfig-gcp
kconfig-gcp:
	gcloud container clusters get-credentials \
		$(OPSTRACE_CLUSTER_NAME) \
		--zone $(OPSTRACE_REGION)-$(OPSTRACE_ZONE) \
		--project $(OPSTRACE_GCP_PROJECT_ID)

.PHONY: kconfig-aws
kconfig-aws:
	source ./secrets/aws-dev-svc-acc-env.sh && \
	aws eks update-kubeconfig --name $(OPSTRACE_CLUSTER_NAME) --region us-west-2
	@echo 'WARNING'
	@echo 'To fix "You must be logged in to the server (Unauthorized)" errors'
	@echo 'when using kubectl you need to run "source ./secrets/aws-dev-svc-acc-env.sh"'
	@echo 'in your shell.'


# Maybe rename to run-controller?
# Maybe create an executable script for `node ./packages/controller/build/cmd.js`
# and no makefile target at all? (so that one would type
#
# $./controller --external testcluster
#
.PHONY: controller-local
controller-local:
	node ./packages/controller/build/cmd.js --external $(OPSTRACE_CLUSTER_NAME)

define get_controller_image_name
	$(IMAGE_REGISTRY)/controller:$(CHECKOUT_VERSION_STRING)
endef

.PHONY: print-controller-image-name
print-controller-image-name:
	@echo $(call get_controller_image_name)

# build and push image for the controller only
.PHONY: build-and-push-controller-image
build-and-push-controller-image: set-build-info-constants
	$(eval IMAGE := $(call get_controller_image_name))
	docker build . -f containers/controller/Dockerfile -t $(IMAGE)  --platform linux/amd64
	echo "Size of docker image:"
	docker images --format "{{.Size}}"$(IMAGE)
	docker push $(IMAGE)

image_targets = $(addprefix images-publish-, $(GO_PROJECTS))
.PHONY: images-publish
images-publish: $(image_targets)

images-publish-%:
	cd $* && make docker

# build and push api images in go/ directory.
# for now, we only care about tracing
.PHONY: api-images
api-images: update-docker-images.json
	cd go && make build-image-tracing && make publish-tracing

# build and push image for the scheduler only
.PHONY: scheduler-image
scheduler-image: update-docker-images.json
	cd scheduler && make docker

# build and push image for the tenant-operator only
.PHONY: tenant-operator-image
tenant-operator-image: update-docker-images.json
	cd tenant-operator && make docker

# build and push image for the gatekeeper only
.PHONY: gatekeeper-image
gatekeeper-image: update-docker-images.json
	cd gatekeeper && make all

# install CRDs and deploy scheduler
.PHONY: deploy
deploy:
	cd scheduler && make install && make deploy

# create a local kind cluster
.PHONY: kind
kind:
	cd scheduler && make kind

# destroy a local kind cluster
.PHONY: destroy
destroy:
	cd scheduler && make kind-delete

# update controller docker-images at `packages/controller-config/src/docker-images.json`
.PHONY: update-docker-images.json
update-docker-images.json:
	./ci/update-docker-images.json.sh

# update docker-images.json and push referenced images
.PHONY: update-controller-config-and-push-images
update-controller-config-and-push-images: update-docker-images.json
	cd go && make all
	cd packages/app && make all

.PHONY: clean
clean:
	@# Wipe state (might not be complete)
	yarn clean
	rm -rf ./secrets
	rm -rf node_modules
	rm -rf build


# Logic and targets below may change frequently. The targets below either carry
# implementation details used in public targets above or do not yet have
# an important role in human daily operations. They might be exercised by CI!

.PHONY: run-app-unit-tests
run-app-unit-tests:
	@echo "--- run app unit tests"
	CI=true yarn workspace @opstrace/app test

.PHONY: cli-crashtest
cli-crashtest:
	# Confirm that single-binary opstrace CLI shows stack trace with TS-based
	# line numbers. To that end, disable pipefail for the moment because the
	# first command in the pipeline is expected to exit non-zero; we're
	# interested in confirming whether grep as the last command in the pipeline
	# exits non-zero (did not find match -> error), or with exit code 0 (did
	# find match -> test passed).
	@echo "--- make cli-crashtest"
	./build/bin/opstrace crashtest || exit 0 # so that the output is visible in build log
	set +o pipefail && ./build/bin/opstrace crashtest 2>&1 | grep 'cli/src/index.ts:'


lint-codebase.js:
	# yarn --frozen-lockfile
	yarn run lint


.PHONY: cli-tsc
cli-tsc: set-build-info-constants
	@# tsc-compile the opstrace cli (not the controller cli)
	yarn --frozen-lockfile --ignore-optional
	yarn build:cli


.PHONY: cli-pkg
cli-pkg:
	@# pkg-build CLI for current platform (rely on the fact that this is linux
	@# in CI :).
	mkdir -p build/bin
	@export TPLATFORM="linux" && \
	if [ "${KERNEL_NAME}" = "darwin" ]; then \
        export TPLATFORM="macos";\
	fi; \
	set -o xtrace && \
	yarn run pkg packages/cli/package.json --public \
		--targets node16-$${TPLATFORM}-x64 \
		--output build/bin/opstrace \
		--options stack-trace-limit=100 && \
        set +o xtrace && \
        echo "Successfully built CLI for $${TPLATFORM}: ./build/bin/opstrace"

.PHONY: cli-pkg-macos
cli-pkg-macos:
	@# pkg-build CLI for macos (use this in CI -- linux -- to create macos
	@# builds.
	mkdir -p build/bin/macos && \
	set -o xtrace && \
        yarn run pkg packages/cli/package.json --public \
		--targets node16-macos-x64 \
		--output build/bin/macos/opstrace \
		--options stack-trace-limit=100 && \
        set +o xtrace && \
        echo "Successfully built CLI for macos: ./build/bin/macos/opstrace"


.PHONY: generate-aws-api-call-list
generate-aws-api-call-list:
	cat opstrace_cli_create_* | \
		grep -o '\[AWS .*({' | \
		grep -vE 'headBucket|describe|list|getInstanceProfile' | \
		sed 's/ [0-9]\{3\}.*retries//g' | \
		sort | uniq


PHONY: publish-artifacts
publish-artifacts: fetch-secrets
	@# If in doubt: never trigger this manually (this is used by CI)
	source secrets/aws-dev-svc-acc-env.sh && bash ci/publish-artifacts.sh


.PHONY: cli-test-s3-latest
cli-test-s3-latest:
	curl https://opstrace-ci-main-artifacts.s3-us-west-2.amazonaws.com/cli/main/latest/opstrace-cli-linux-amd64-latest.tar.bz2 \
		--fail --silent --show-error \
		--output opstrace-cli-linux-amd64-latest.tar.bz2
	tar xjf opstrace-cli-linux-amd64-latest.tar.bz2
	stat opstrace
	file opstrace
	./opstrace --help


.PHONY: set-build-info-constants
set-build-info-constants:
	# Write buildinfo.json to the root of the repository. CLI and controller
	# etc require to discover this file upon runtime. During local dev,
	# set OPSTRACE_BUILDINFO_PATH.
	# Note that the version information also affects default config values
	# such as `controller_config`.
	#
	# Use `sed` for these replacements. Expect the exit code to be 0 for both
	# cases: replacement made, or replacement not made. Expect non-zero exit
	# code only for e.g. file not existing.
	#
	# Notes:
	# - `buildinfo.json` is not tracked in the repository.
	# - `git branch --show-current` requires at least git 2.22 -- expected to
	#   exit non-zero in CI (there we use CI_COMMIT_REF_NAME, however).
	# - use in-place editing with `sed` to make this portable across Linux and
	#   macOS: https://stackoverflow.com/a/16746032/145400
	cat ci/buildinfo.json.template > buildinfo.json
	_GITBRANCH="$$(git branch --show-current)" || true; BRANCH_NAME=$${CI_COMMIT_REF_NAME:-$$_GITBRANCH} && \
	sed -i.bak "s|<BUILD_INFO_BRANCH_NAME>|$${BRANCH_NAME}|g" \
		buildinfo.json
	sed -i.bak "s|<BUILD_INFO_VERSION_STRING>|${CHECKOUT_VERSION_STRING}|g" \
		buildinfo.json
	sed -i.bak "s|<BUILD_INFO_COMMIT>|$$(git rev-parse --short HEAD)|g" \
		buildinfo.json
	sed -i.bak "s|<BUILD_INFO_TIME_RFC3339>|$$(date --rfc-3339=seconds --utc)|g" \
		buildinfo.json
	sed -i.bak "s|<BUILD_INFO_HOSTNAME>|$$(hostname)|g" \
		buildinfo.json
	rm buildinfo.json.bak
	echo "(re)generated buildinfo.json"
	cat buildinfo.json

# credits: https://stackoverflow.com/a/4731504/145400
checkenv-clustername:
ifndef OPSTRACE_CLUSTER_NAME
	$(error The environment variable OPSTRACE_CLUSTER_NAME is not set)
endif

.PHONY: set-dockerhub-credentials
set-dockerhub-credentials:
	mkdir -p $(HOME)/.docker
	cat ./secrets/docker-credentials.json > $(HOME)/.docker/config.json


.PHONY: rebuild-ci-container-image
rebuild-ci-container-image: BUILD_ARGS=
rebuild-ci-container-image:
	docker pull "${CI_IMAGE}" || echo "${CI_IMAGE} image is not available. Will not use remote cache."
	docker build --cache-from "${CI_IMAGE}" ${BUILD_ARGS} --build-arg CIUID=$(shell id -u) --build-arg CIGID=$(shell id -g) \
		-t "${CI_IMAGE}"  \
		. -f containers/ci/opstrace-ci.Dockerfile


.PHONY: push-ci-container-image
push-ci-container-image:
	docker push "${CI_IMAGE}"


# pull images that are created in the remote tests
.PHONY: pull-testrunner-images
pull-testrunner-images:
	docker pull prom/prometheus:v2.33.5
	docker pull gcr.io/datadoghq/agent:7


#
# Show cluster info for the opstrace cluster we are targetting.
# To configure the relevant credentials first run kconfig-gcp or kconfig-aws.
#
.PHONY: kubectl-cluster-info
kubectl-cluster-info:
	kubectl cluster-info

# run non-ui remote tests
.PHONY: test-remote
test-remote: kubectl-cluster-info pull-testrunner-images
	cd test/test-remote && yarn install --frozen-lockfile && yarn tsc && \
		yarn run mocha --grep test_ui_api --invert


#
# remote-ui-api tests need playwright chromium browser.
# Install here with hermetic install rather than global.
# These binaries will get cached in CI.
#
.PHONY: test-remote-ui-api
test-remote-ui-api:
	mkdir -p ${OPSTRACE_BUILD_DIR}/test-remote-artifacts
	cd test/test-remote && yarn install --frozen-lockfile && yarn tsc && \
		export PLAYWRIGHT_BROWSERS_PATH=0; npx playwright install chromium && \
		export TEST_REMOTE_ARTIFACT_DIRECTORY=${OPSTRACE_BUILD_DIR}/test-remote-artifacts; yarn run mocha --grep test_ui_api


#
# This runs our set of browser based tests using the Playwright Test Runner.
# Install all playwright browsers here rather than globally.
#
.PHONY: test-browser
test-browser:
	cd test/browser && yarn install --frozen-lockfile && \
		export PLAYWRIGHT_BROWSERS_PATH=0; npx playwright install && \
		export OPSTRACE_PLAYWRIGHT_REUSE_STATE=true; ./run_on_ci.sh


.PHONY: invoke-looker
invoke-looker: LOOKER_ARGS=
invoke-looker:
	cd test/test-remote/looker && yarn install --frozen-lockfile && yarn tsc && \
		yarn run looker ${LOOKER_ARGS}


.PHONY: website-build
website-build:
	cd website && yarn install && yarn build


.PHONY: deploy-testremote-teardown
deploy-testremote-teardown:
	bash "ci/deploy-testremote-teardown.sh"


# used by pipeline as `make ci-test-upgrade-run-sh`, i.e. in the CI container
.PHONY: test-upgrade-run-sh
test-upgrade-run-sh:
	bash "ci/test-upgrade/run.sh"


# Target that runs a script in the ci/testupgrade/ directory. Check
# .buildkite/test-upgrade-pipeline.yml to see how it can be used.
#
.PHONY: testupgrade-%
testupgrade-%:
	./ci/test-upgrade/$*.sh

#
# Target that runs a script in the ci/cd/ directory. Check
# .buildkite/cd-pipeline.yml to see how it can be used.
#
.PHONY: cd-%
cd-%:
	./ci/cd/$*.sh


node_modules:
	yarn --frozen-lockfile

.PHONY: cli-tests-no-cluster
cli-tests-no-cluster:
	@echo "--- run CLI tests that do not need a cluster (cli-tests-pre-cluster.sh)"
	CHECKOUT_VERSION_STRING=${CHECKOUT_VERSION_STRING} source ci/test-cli/cli-tests-pre-cluster.sh

.PHONY: go-unit-tests
go-unit-tests:
	@echo "--- run go unit tests"
	cd go && make unit-tests

.PHONY: ts-unit-tests
ts-unit-tests:
	@echo "--- run lib unit tests"
	cd lib/kubernetes && CI=true yarn test
	@echo "--- run cli unit tests"
	cd packages/cli && CI=true yarn test
	@echo "--- run controller unit tests"
	cd packages/controller && CI=true yarn test
