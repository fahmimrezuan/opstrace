package v1alpha1

const (
	// StatusInProgress when we're still waiting for cluster creation.
	StatusInProgress ClickHouseClusterStatus = "InProgress"

	// StatusComplete when the cluster is entirely available.
	StatusCompleted ClickHouseClusterStatus = "Completed"

	// StatusTerminating when the resource is being deleted.
	// Note(joe): not currently used, but likely will be if we need graceful shutdown.
	StatusTerminating ClickHouseClusterStatus = "Terminating"
)
