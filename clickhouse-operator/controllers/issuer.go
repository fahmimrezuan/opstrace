package controllers

import (
	certmanagerv1 "github.com/cert-manager/cert-manager/pkg/apis/certmanager/v1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	rootIssuerName = "selfsigned-issuer"
)

func issuer(ch *clickHouseCluster) *certmanagerv1.ClusterIssuer {
	return &certmanagerv1.ClusterIssuer{
		ObjectMeta: v1.ObjectMeta{
			Name:      rootIssuerName,
			Namespace: ch.Namespace,
		},
		Spec: certmanagerv1.IssuerSpec{
			IssuerConfig: certmanagerv1.IssuerConfig{
				SelfSigned: &certmanagerv1.SelfSignedIssuer{},
			},
		},
	}
}

func issuerMutator(ch *clickHouseCluster, current *certmanagerv1.ClusterIssuer) {
	issuer := issuer(ch)
	current.Name = rootIssuerName
	current.Namespace = ch.Namespace
	current.Spec.SelfSigned = issuer.Spec.SelfSigned
}
