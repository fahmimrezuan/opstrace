/*
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
// +kubebuilder:docs-gen:collapse=Apache License

package controllers

import (
	"fmt"
	"io/ioutil"
	"path/filepath"
	"strings"
	"time"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/kubernetes/scheme"
	"sigs.k8s.io/controller-runtime/pkg/client"
	logf "sigs.k8s.io/controller-runtime/pkg/log"

	clickhousev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/clickhouse-operator/api/v1alpha1"
	//+kubebuilder:scaffold:imports
)

// +kubebuilder:docs-gen:collapse=Imports

var _ = Describe("ClickHouse controller", func() {

	// Define utility constants for object names and testing timeouts/durations and intervals.
	const (
		clickhouseNamespace = "default"

		timeout  = time.Second * 10
		interval = time.Millisecond * 250
	)

	// definition loaded from disk
	var clickhouseDefinition *clickhousev1alpha1.ClickHouse
	// created CRD in the testenv
	var clickhouseCreated *clickhousev1alpha1.ClickHouse
	// namespaced name for the loaded CR
	var clickhouseName types.NamespacedName

	AfterEach(func() {
		By("Delete created ClickHouse cluster")
		Expect(k8sClient.Delete(ctx, clickhouseCreated)).To(Succeed())

		By("Ensure all managed resources are deleted")
		// EnvTest doesn't handle owner refs and delete these
		managed := []client.Object{
			&appsv1.StatefulSet{},
			&corev1.Service{},
			&corev1.Secret{},
			&corev1.ConfigMap{},
		}
		for _, m := range managed {
			Expect(k8sClient.DeleteAllOf(ctx, m, client.InNamespace(clickhouseNamespace))).To(Succeed())
		}
	})

	beforeEachInstall := func(example string) {
		BeforeEach(func() {
			By("Reading example yaml to create ClickHouse install")
			filename, _ := filepath.Abs(example)
			exampleYamlBytes, err := ioutil.ReadFile(filename)
			Expect(err).NotTo(HaveOccurred())

			decoder := scheme.Codecs.UniversalDeserializer()

			// Break up the yaml into separate strings as we'll have multiple docs per file
			exampleYamlRecs := strings.Split(string(exampleYamlBytes), "---")

			for _, f := range exampleYamlRecs {
				runtimeObj, groupKind, err := decoder.Decode([]byte(f), nil, nil)
				Expect(err).NotTo(HaveOccurred())

				if groupKind.GroupVersion() == clickhousev1alpha1.GroupVersion {
					clickhouseDefinition = runtimeObj.(*clickhousev1alpha1.ClickHouse)
				}

				Expect(k8sClient.Create(ctx, runtimeObj.(client.Object))).To(Succeed())
			}

			Expect(clickhouseDefinition).ToNot(BeNil())

			clickhouseName = types.NamespacedName{
				Name:      clickhouseDefinition.Name,
				Namespace: clickhouseNamespace,
			}

			// Check that the CR appears
			clickhouseCreated = &clickhousev1alpha1.ClickHouse{}
			Eventually(func() bool {
				return getResource(clickhouseName, clickhouseCreated)
			}, timeout, interval).Should(BeTrue())

			By("Waiting for resources to appear")
			// Wait for configmap to appear
			configMap := &corev1.ConfigMap{}
			Eventually(func() bool {
				return getResource(clickhouseName, configMap)
			}, timeout, interval).Should(BeTrue())

			// Wait for cluster service to appear
			clusterService := &corev1.Service{}
			Eventually(func() bool {
				return getResource(clickhouseName, clusterService)
			}, timeout, interval).Should(BeTrue())

			// Wait for per-node services and statefulsets to appear
			for idx := int32(0); idx < *clickhouseCreated.Spec.Replicas; idx++ {
				nodeNameKey := types.NamespacedName{
					Name:      fmt.Sprintf("%s-0-%d", clickhouseCreated.Name, idx),
					Namespace: clickhouseNamespace,
				}

				nodeService := &corev1.Service{}
				Eventually(func() bool {
					return getResource(nodeNameKey, nodeService)
				}, timeout, interval).Should(BeTrue())

				nodeStatefulSet := &appsv1.StatefulSet{}
				Eventually(func() bool {
					return getResource(nodeNameKey, nodeStatefulSet)
				}, timeout, interval).Should(BeTrue())
			}
		})
	}

	expectStatusInProgress := func() {
		By("Check for status InProgress in ClickHouse status")
		updatedClickhouse := &clickhousev1alpha1.ClickHouse{}
		Eventually(func() bool {
			return getResource(clickhouseName, updatedClickhouse) &&
				updatedClickhouse.Status.Status == clickhousev1alpha1.StatusInProgress
		}, timeout, interval).Should(BeTrue())
	}

	Context("When adding ClickHouse CR", func() {
		beforeEachInstall("../config/samples/example.yaml")

		It("Should create other expected objects and have status InProgress", func() {
			expectStatusInProgress()
		})

		It("Should not update status repicas and shards until StatefulSets are ready", func() {
			expectStatusInProgress()

			// in EnvTest there are no controllers, so we will manipulate the state
			cr := getClickHouse(clickhouseName)
			Expect(cr.Status.ReadyReplicas).To(BeZero())
			Expect(cr.Status.ReadyShards).To(BeZero())

			getOwnedStatefulSets := func() []*appsv1.StatefulSet {
				ss := &appsv1.StatefulSetList{}
				Expect(k8sClient.List(ctx, ss, client.InNamespace(clickhouseNamespace))).To(Succeed())
				owned := []*appsv1.StatefulSet{}
				for _, s := range ss.Items {
					for _, o := range s.GetOwnerReferences() {
						if o.UID == cr.UID {
							owned = append(owned, s.DeepCopy())
						}
					}
				}
				return owned
			}

			By("Update owned StatefulSets to have ready replicas")
			ss := getOwnedStatefulSets()
			for _, s := range ss {
				u := s
				u.Status.Replicas = *u.Spec.Replicas
				u.Status.ReadyReplicas = *u.Spec.Replicas
				Expect(k8sClient.Status().Patch(ctx, u, client.Merge)).To(Succeed())
			}

			Eventually(func(g Gomega) {
				cr := getClickHouse(clickhouseName)
				g.Expect(cr.Status.ReadyReplicas).To(BeNumerically("==", *clickhouseCreated.Spec.Replicas))
				g.Expect(cr.Status.ReadyShards).To(BeNumerically("==", 1))
			}, timeout, interval).Should(Succeed())
		})
	})

	Context("When CHProxy is enabled", func() {
		beforeEachInstall("../config/samples/example-with-proxy.yaml")

		It("Should create proxy objects", func() {

			// Wait for proxy Deployment to appear
			proxyDeployment := &appsv1.Deployment{}
			Expect(getResource(types.NamespacedName{
				Name:      fmt.Sprintf("%s-proxy", clickhouseCreated.Name),
				Namespace: clickhouseNamespace,
			}, proxyDeployment)).To(BeTrue())

			// Wait for proxy Deployment to appear
			proxyService := &corev1.Service{}
			Expect(getResource(types.NamespacedName{
				Name:      fmt.Sprintf("%s-proxy", clickhouseCreated.Name),
				Namespace: clickhouseNamespace,
			}, proxyService)).To(BeTrue())

			expectStatusInProgress()
		})
	})
})

func getResource(name types.NamespacedName, obj client.Object) bool {
	err := k8sClient.Get(ctx, name, obj)
	if err != nil {
		logf.Log.Info("didn't find resource", "namespace", name.Namespace, "name", name.Name, "err", err)
		return false
	}
	logf.Log.Info("found resource", "namespace", name.Namespace, "name", name.Name)
	return true
}

// get ClickHouse if you know it must exist.
func getClickHouse(name types.NamespacedName) *clickhousev1alpha1.ClickHouse {
	out := &clickhousev1alpha1.ClickHouse{}
	if got := getResource(name, out); !got {
		panic(fmt.Sprintf("Could not get ClickHouse: %s", name))
	}
	return out
}
