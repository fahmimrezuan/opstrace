package controllers

import (
	certmanagerv1 "github.com/cert-manager/cert-manager/pkg/apis/certmanager/v1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	caIssuerName = "ca-issuer"
)

func caIssuer(ch *clickHouseCluster) *certmanagerv1.Issuer {
	return &certmanagerv1.Issuer{
		ObjectMeta: v1.ObjectMeta{
			Name:      caIssuerName,
			Namespace: ch.Namespace,
		},
		Spec: certmanagerv1.IssuerSpec{
			IssuerConfig: certmanagerv1.IssuerConfig{
				CA: &certmanagerv1.CAIssuer{
					SecretName: caSecretName,
					//CRLDistributionPoints: nil,
					//OCSPServers:           nil,
				},
			},
		},
	}
}

func caIssuerMutator(ch *clickHouseCluster, current *certmanagerv1.Issuer) {
	issuer := caIssuer(ch)
	current.Name = caIssuerName
	current.Namespace = ch.Namespace
	current.Spec.IssuerConfig.CA = issuer.Spec.IssuerConfig.CA
}
