package e2e

import (
	"github.com/anthhub/forwarder"
	"k8s.io/client-go/tools/portforward"

	. "github.com/onsi/gomega"
)

// proxy will try to configure a port-forward proxy for the given opts.
// Uses gomega checks and will fail any calling test if the proxy cannot be made.
// Returns forwarded ports and closer function.
func proxyService(service *forwarder.Option, g Gomega) ([]portforward.ForwardedPort, func()) {
	var forward *forwarder.Result

	g.Eventually(func(g Gomega) {
		res, err := forwarder.WithRestConfig(ctx, []*forwarder.Option{service}, restConfig)
		g.Expect(err).NotTo(HaveOccurred())

		forward = res
	}).Should(Succeed())

	g.Expect(forward).NotTo(BeNil())

	var ports [][]portforward.ForwardedPort

	done := make(chan struct{})
	go func() {
		var err error
		ports, err = forward.Ready()
		Expect(err).NotTo(HaveOccurred())
		close(done)
	}()

	g.Eventually(done).Should(BeClosed(), "waiting for port-forward to be ready")
	g.Expect(ports).To(HaveLen(1))

	return ports[0], forward.Close
}
