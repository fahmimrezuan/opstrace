package e2e

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"time"

	clickhouseclient "github.com/ClickHouse/clickhouse-go/v2"
	"github.com/anthhub/forwarder"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	clickhousev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/clickhouse-operator/api/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
)

var _ = Describe("MergeTree replication", func() {
	var adminSecret *corev1.Secret
	var clickhouseCluster *clickhousev1alpha1.ClickHouse
	var internalSSL bool

	const adminUser = "admin"
	const adminPass = "passw0rd"
	// Note: This would be better if we can reference this out of the controller
	const clickhouseCertName = "clickhouse-cert"

	initSecret := func() {
		adminSecret = &corev1.Secret{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "admin-secret",
				Namespace: "default",
			},
			StringData: map[string]string{
				"password": adminPass,
			},
		}
	}
	initClickhouseCluster := func(ssl bool) {
		replicas := int32(3)
		clickhouseCluster = &clickhousev1alpha1.ClickHouse{
			ObjectMeta: metav1.ObjectMeta{
				// generates a name for our e2e test
				GenerateName: "cluster-e2e-test-",
				Namespace:    "default",
			},
			Spec: clickhousev1alpha1.ClickHouseSpec{
				// TODO(joe): run these tests with different image versions
				// need to define a support matrix of images for this operator
				Image:       clickHouseServer,
				Replicas:    &replicas,
				StorageSize: resource.MustParse("512M"),
				InternalSSL: &ssl,
				AdminUsers: []clickhousev1alpha1.ClickHouseAdmin{
					{
						Name: adminUser,
						SecretKeyRef: corev1.SecretKeySelector{
							LocalObjectReference: corev1.LocalObjectReference{
								Name: adminSecret.Name,
							},
							Key: "password",
						},
					},
				},
			},
		}
	}

	BeforeEach(func() {
		By("create the cluster")
		initSecret()
		Expect(k8sClient.Create(ctx, adminSecret)).To(Succeed())
		initClickhouseCluster(internalSSL)
		Expect(k8sClient.Create(ctx, clickhouseCluster)).To(Succeed())

		By("clickhouse is ready")
		Eventually(func(g Gomega) {
			ch := &clickhousev1alpha1.ClickHouse{}
			g.Expect(k8sClient.Get(ctx, types.NamespacedName{
				Namespace: "default",
				Name:      clickhouseCluster.Name,
			}, ch)).To(Succeed())

			g.Expect(ch.Status.Status).To(Equal(clickhousev1alpha1.StatusCompleted))
		}).Should(Succeed())
	})

	AfterEach(func() {
		By("delete the cluster")
		Expect(k8sClient.Delete(ctx, clickhouseCluster)).To(Succeed())
		Expect(k8sClient.Delete(ctx, adminSecret)).To(Succeed())
	})

	openDB := func(port uint16, database string, g Gomega, tlsConf *tls.Config) clickhouseclient.Conn {
		c, err := clickhouseclient.Open(&clickhouseclient.Options{
			Addr: []string{fmt.Sprintf("0.0.0.0:%d", port)},
			Auth: clickhouseclient.Auth{
				Username: adminUser,
				Password: adminPass,
				Database: database,
			},
			TLS:   tlsConf,
			Debug: true,
		})
		g.Expect(err).NotTo(HaveOccurred())
		return c
	}

	type row struct {
		Dt   time.Time `ch:"dt"`
		Text string    `ch:"text"`
	}

	insertData := func(db clickhouseclient.Conn) []row {
		By("inserting rows into distributed table")

		batch, err := db.PrepareBatch(ctx, "INSERT into events.things")
		Expect(err).NotTo(HaveOccurred())

		es := make([]row, 1000)
		t0 := time.Now().UTC()
		txt := []string{"foo", "bar", "baz"}
		for i := 0; i < 1_000; i++ {
			es[i] = row{
				Dt:   t0.Add(time.Second * time.Duration(i)),
				Text: txt[i%3],
			}
		}

		for _, e := range es {
			e := e
			Expect(batch.AppendStruct(&e)).To(Succeed())
		}

		Expect(batch.Send()).To(Succeed())

		return es
	}

	assertReplicaBehaviour := func(g Gomega, es []row, remotePort int, tlsConf *tls.Config) {
		for i := 0; i < int(*clickhouseCluster.Spec.Replicas); i++ {
			ports, close := proxyService(&forwarder.Option{
				RemotePort:  remotePort,
				ServiceName: fmt.Sprintf("%s-0-%d", clickhouseCluster.Name, i),
			}, g)

			db2 := openDB(ports[0].Local, "", g, tlsConf)

			g.Expect(db2.Ping(ctx)).To(Succeed())

			var res []row
			g.Expect(db2.Select(ctx, &res, "SELECT * FROM events.things_local")).Should(Succeed())

			g.Expect(res).To(ConsistOf(es))
			close()
			db2.Close()
		}
	}

	assertReplicatedTableCreation := func(db clickhouseclient.Conn) {
		// Note(joe): this demonstrates various sane defaults:
		// - Atomic database engine.
		// - Default Replicated* args for zookeeper path and replica name

		Expect(db.Exec(ctx, "CREATE DATABASE events ON CLUSTER '{cluster}'")).To(Succeed())
		Expect(db.Exec(ctx,
			`CREATE TABLE events.things_local ON CLUSTER '{cluster}' (
				dt DateTime64(9, 'UTC'),
				text String
			) ENGINE = ReplicatedMergeTree
			ORDER BY (dt)`)).To(Succeed())
		Expect(db.Exec(ctx,
			`CREATE TABLE events.things ON CLUSTER '{cluster}' AS events.things_local
			ENGINE = Distributed('{cluster}', events, things_local)`)).To(Succeed())

	}

	Context("ReplicatedMergeTree and Distributed tables", func() {
		internalSSL = false
		var db clickhouseclient.Conn
		var closeDB func() error
		var closeProxy func()

		BeforeEach(func() {
			By("creating replicated and distributed example schemas")

			ports, close := proxyService(&forwarder.Option{
				RemotePort:  9000,
				ServiceName: clickhouseCluster.Name,
			}, Default)
			closeProxy = close

			db = openDB(ports[0].Local, "", Default, nil)
			closeDB = db.Close
			Expect(db.Ping(ctx)).To(Succeed())

			assertReplicatedTableCreation(db)
		})

		AfterEach(func() {
			By("closing db and proxy")
			Expect(closeDB()).To(Succeed())
			closeProxy()
		})

		It("allows writing and reading from distributed tables", func() {
			es := insertData(db)
			var res []row

			By("verifying inserts with each replica local table")
			assertReplicaBehaviour(Default, es, 9000, nil)

			By("verifying data is still available from distributed table")
			Expect(db.Select(ctx, &res, "SELECT * FROM events.things")).Should(Succeed())
			Expect(res).To(ConsistOf(es))
		})

		// TODO(joe): replica scaling doesn't work at the moment.
		PIt("replicates data when the cluster is scaled up", func() {
			es := insertData(db)

			By("scaling cluster and reading from new replica")
			ch := &clickhousev1alpha1.ClickHouse{}
			Expect(k8sClient.Get(ctx, types.NamespacedName{
				Name:      clickhouseCluster.Name,
				Namespace: clickhouseCluster.Namespace,
			}, ch)).To(Succeed())
			*ch.Spec.Replicas++
			Expect(k8sClient.Update(ctx, ch)).To(Succeed())

			Eventually(func(g Gomega) {
				ports, close := proxyService(&forwarder.Option{
					RemotePort:  9000,
					ServiceName: fmt.Sprintf("%s-0-%d", clickhouseCluster.Name, *ch.Spec.Replicas),
				}, g)
				defer close()

				db2 := openDB(ports[0].Local, "events", g, nil)
				defer db2.Close()

				g.Expect(db2.Ping(ctx)).To(Succeed())

				var res []row
				g.Expect(db2.Select(ctx, &res, "SELECT * FROM things_local")).Should(Succeed())

				g.Expect(res).To(ConsistOf(es))
			}).Should(Succeed())
		})
	})

	Context("ReplicatedMergeTree and Distributed tables with SSL enabled", func() {
		internalSSL = true
		var (
			sslDB      clickhouseclient.Conn
			tlsCert    []byte
			tlsKey     []byte
			caCert     []byte
			dhParam    []byte
			closeDB    func() error
			closeProxy func()
			tlsConf    *tls.Config
		)

		BeforeEach(func() {
			By("creating replicated and distributed example schemas with ssl")

			ports, close := proxyService(&forwarder.Option{
				RemotePort:  9440,
				ServiceName: clickhouseCluster.Name,
			}, Default)
			closeProxy = close

			secretList := &corev1.SecretList{}
			Expect(k8sClient.List(ctx, secretList)).To(Succeed())
			for _, secret := range secretList.Items {
				if secret.Name == clickhouseCertName {
					for k, v := range secret.Data {
						if k == "tls.crt" {
							tlsCert = v
						} else if k == "tls.key" {
							tlsKey = v
						} else if k == "ca.crt" {
							caCert = v
						}
					}
				} else if secret.Name == fmt.Sprintf("%s-dhparam", clickhouseCluster.Name) {
					v, ok := secret.Data["dhparam.pem"]
					if ok {
						dhParam = v
					}
				}
			}
			Expect(tlsCert).ShouldNot(HaveLen(0))
			Expect(tlsKey).ShouldNot(HaveLen(0))
			Expect(caCert).ShouldNot(HaveLen(0))
			Expect(dhParam).ShouldNot(HaveLen(0))

			caCertPool := x509.NewCertPool()
			Expect(caCertPool.AppendCertsFromPEM(caCert)).To(BeTrue())
			tlsConf = &tls.Config{
				InsecureSkipVerify: false,
				RootCAs:            caCertPool,
				MinVersion:         tls.VersionTLS12,
			}
			sslDB = openDB(ports[0].Local, "", Default, tlsConf)
			closeDB = sslDB.Close
			Expect(sslDB.Ping(ctx)).To(Succeed())

			assertReplicatedTableCreation(sslDB)
		})

		AfterEach(func() {
			By("closing db and proxy")
			Expect(closeDB()).To(Succeed())
			closeProxy()
		})

		It("allows writing and reading from distributed tables with ssl", func() {
			es := insertData(sslDB)
			var res []row

			By("verifying inserts with each replica local table")
			assertReplicaBehaviour(Default, es, 9440, tlsConf)

			By("verifying data is still available from distributed table")
			Expect(sslDB.Select(ctx, &res, "SELECT * FROM events.things")).Should(Succeed())
			Expect(res).To(ConsistOf(es))
		})
	})
})
