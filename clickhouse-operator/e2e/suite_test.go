package e2e

import (
	"context"
	"testing"
	"time"

	"github.com/cert-manager/cert-manager/pkg/apis/certmanager"
	certmanagerv1 "github.com/cert-manager/cert-manager/pkg/apis/certmanager/v1"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	apiextensions "k8s.io/apiextensions-apiserver/pkg/apis/apiextensions/v1"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/client/config"
	"sigs.k8s.io/controller-runtime/pkg/envtest/printer"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"

	clickhousev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/clickhouse-operator/api/v1alpha1"

	// support GCP auth for e2e CI environment.
	_ "k8s.io/client-go/plugin/pkg/client/auth/gcp"
	//+kubebuilder:scaffold:imports
)

// These tests use Ginkgo (BDD-style Go testing framework). Refer to
// http://onsi.github.io/ginkgo/ to learn more about Ginkgo.

var (
	k8sClient  client.Client // You'll be using this client in your tests.
	restConfig *rest.Config  // Config of configured k8s REST interface
	ctx        context.Context
	cancel     context.CancelFunc
)

func TestE2E(t *testing.T) {
	RegisterFailHandler(Fail)

	// default Eventually settings for real clusters
	// image download might take a while on fresh installs
	SetDefaultEventuallyTimeout(time.Minute * 5)
	SetDefaultEventuallyPollingInterval(time.Second)

	RunSpecsWithDefaultAndCustomReporters(t,
		"E2E Suite",
		[]Reporter{printer.NewlineReporter{}})
}

var _ = BeforeSuite(func() {
	logf.SetLogger(zap.New(zap.WriteTo(GinkgoWriter), zap.UseDevMode(true)))

	ctx, cancel = context.WithCancel(context.TODO())

	Expect(scheme.AddToScheme(scheme.Scheme)).To(Succeed())
	Expect(apiextensions.AddToScheme(scheme.Scheme)).To(Succeed())
	Expect(clickhousev1alpha1.AddToScheme(scheme.Scheme)).To(Succeed())
	Expect(certmanagerv1.AddToScheme(scheme.Scheme)).To(Succeed())

	cfg, err := config.GetConfig()
	Expect(err).NotTo(HaveOccurred())
	restConfig = cfg

	k8sClient, err = client.New(cfg, client.Options{Scheme: scheme.Scheme})
	Expect(err).NotTo(HaveOccurred())
	Expect(k8sClient).NotTo(BeNil())

	By("check clickhouse CRD exists in cluster")
	crd := &apiextensions.CustomResourceDefinition{}
	Expect(k8sClient.Get(context.TODO(), client.ObjectKey{
		Name: "clickhouses." + clickhousev1alpha1.GroupVersion.Group,
	}, crd)).To(Succeed())

	By("check certmanager CRD exists in cluster")
	Expect(k8sClient.Get(context.TODO(), client.ObjectKey{
		Name: "certificates." + certmanager.GroupName,
	}, crd)).To(Succeed())

}, 60)

var _ = AfterSuite(func() {
	By("no teardown required")
	cancel()
})
