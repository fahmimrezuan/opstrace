#!/usr/bin/env bash
set -o errexit
set -o errtrace
set -o nounset
set -o pipefail

# Use kubectl exec to generate a tenant auth token from Grafana directly
# Use bash '$RANDOM' util var to ensure that token names do not collide
TOKEN_RESPONSE_JSON=$(
    kubectl exec -n default-tenant deployment/grafana -c grafana -- \
            curl \
            -H 'x-auth-request-email: ci-invoke-looker@testing.opstrace' \
            -H 'Content-Type: application/json' \
            -d "{\"name\":\"invoke-looker-${RANDOM}\", \"role\":\"Editor\", \"secondsToLive\": 14400}" \
            http://localhost:3000/api/auth/keys
)
TENANT_DEFAULT_API_TOKEN=$(echo $TOKEN_RESPONSE_JSON | jq -r .key)
if [ "$TENANT_DEFAULT_API_TOKEN" = "null" ]; then
    echo "Failed to get Grafana API token: $TOKEN_RESPONSE_JSON"
    exit 1
fi

TENANT_DEFAULT_CORTEX_API_BASE_URL="https://cortex.default.${OPSTRACE_INSTANCE_DNS_NAME}"

# fail this script upon first looker failure -- i.e. it's OK for now to not run
# the other looker-based tests (as a sane test runner should do).

# Test metrics mode of looker
# do not show output in main build log (it's a lot!)
# instead make sure that the *.log files are collected as build
# artifacts.
LPREFIX="metrics" && TSTRING="$(date +%Y%m%d-%H%M%S)" && LNAME="looker-${LPREFIX}-${TSTRING}"
echo -e "\n\n Invoke looker test: ${LNAME}\n"
ARGS="\"${TENANT_DEFAULT_CORTEX_API_BASE_URL}\" \
--bearer-token \"${TENANT_DEFAULT_API_TOKEN}\" \
--metrics-mode \
--n-series 3 \
--n-samples-per-series-fragment 25000 \
--change-series-every-n-cycles 1 \
--cycle-stop-write-after-n-fragments 2 \
--n-cycles 2"
make LOOKER_ARGS="${ARGS}" invoke-looker > ${LNAME}.log 2>&1
echo -e "\n\n looker stdout/err tail:\n" && cat ${LNAME}.log | tail -n 15

# Metrics mode: use --n-fragments-per-push-message to create an HTTP request
# payload that contains samples from _many_ streams (individual time series);
# allowing for cranking up the number of concurrent streams (individual time
# series) to synthetically generate data from. Here, the parameters are chosen
# so that the expected HTTP request payload size (snappy-compressed protobuf)
# is about 0.95 MiB, just a little bit below the limit of 1.00 MiB implemented
# on the receiving end. The readout is skipped because otherwise it would
# take way too long (when done stream-by-stream and therefore it
# taking while (O(1) HTTP request per time series, i.e. O(10^5) HTTP requests).
LPREFIX="metrics" && TSTRING="$(date +%Y%m%d-%H%M%S)" && LNAME="looker-${LPREFIX}-${TSTRING}"
echo -e "\n\n Invoke looker test: ${LNAME}\n"
ARGS="\"${TENANT_DEFAULT_CORTEX_API_BASE_URL}\" \
--bearer-token \"${TENANT_DEFAULT_API_TOKEN}\" \
--metrics-mode \
--n-cycles 1 \
--n-series 100000 \
--n-samples-per-series-fragment 5 \
--n-fragments-per-push-message 15000 \
--cycle-stop-write-after-n-fragments 2 \
--metrics-time-increment-ms 2000 \
--max-concurrent-writes 6 \
--skip-read"
make LOOKER_ARGS="${ARGS}" invoke-looker > ${LNAME}.log 2>&1
echo -e "\n\n looker stdout/err tail:\n" && cat ${LNAME}.log | tail -n 15

# Test --read-n-series-only
LPREFIX="metrics" && TSTRING="$(date +%Y%m%d-%H%M%S)" && LNAME="looker-${LPREFIX}-${TSTRING}"
echo -e "\n\n Invoke looker test: ${LNAME}\n"
ARGS="\"${TENANT_DEFAULT_CORTEX_API_BASE_URL}\" \
--bearer-token \"${TENANT_DEFAULT_API_TOKEN}\" \
--n-cycles 1 \
--metrics-mode \
--n-series 100000 \
--n-samples-per-series-fragment 5 \
--n-fragments-per-push-message 15000 \
--cycle-stop-write-after-n-fragments 10 \
--metrics-time-increment-ms 2000 \
--max-concurrent-writes 6 \
--read-n-series-only 1"
make LOOKER_ARGS="${ARGS}" invoke-looker > ${LNAME}.log 2>&1
echo -e "\n\n looker stdout/err tail:\n" && cat ${LNAME}.log | tail -n 15


# Test throttling in metrics mode. Send from just one series/stream, and make
# each HTTP request cover a 100 seconds wide time window (diff between
# timestamp of first and last sample). Starting ~30 mins in the past, within
# just a small amount of POST HTTP requests we're approaching 'now' (walltime)
# with the generated samples. This is supposed to happen before the write-stop
# criterion (which is: stop after 20 seconds of writing). Enable debug log to
# see these log messages: "DummyTimeseries(..): current lag compared to wall
# time is 9.9 minutes. Sample generation is too fast. Delay generating and
# pushing the next fragment. This may take up to 10 minutes."
LPREFIX="metrics" && TSTRING="$(date +%Y%m%d-%H%M%S)" && LNAME="looker-${LPREFIX}-${TSTRING}"
echo -e "\n\n Invoke looker test: ${LNAME}\n"
ARGS="\"${TENANT_DEFAULT_CORTEX_API_BASE_URL}\" \
--bearer-token \"${TENANT_DEFAULT_API_TOKEN}\" \
--metrics-mode \
--n-cycles 1 \
--n-series 1 \
--n-samples-per-series-fragment 10 \
--metrics-time-increment-ms 10000 \
--cycle-stop-write-after-n-seconds 20 \
--log-level=debug"
make LOOKER_ARGS="${ARGS}" invoke-looker > ${LNAME}.log 2>&1
echo -e "\n\n looker stdout/err tail:\n" && cat ${LNAME}.log | tail -n 15
