include ../Makefile.shared

DOCKER_IMAGE_NAME ?= gatekeeper
# DOCKER_IMAGE_TAG is a shasum of all the files (except hidden) in the go
# directory. We use it to have a deterministic method to calculate a docker
# image tag.
DOCKER_IMAGE_TAG ?= $(shell find ../go . -type f -not -name ".*" -not -name "docker-images.json" -print0 | sort -z -d -f | xargs -0 cat | shasum | cut -d' ' -f1)

DOCKERFILE = ./Dockerfile

all: build-image publish-image

export GO111MODULE=on
export GOPRIVATE=github.com/opstrace

.PHONY: clean
clean:
	rm -f server

define get_docker_image_name
	$(IMAGE_REGISTRY)/$(DOCKER_IMAGE_NAME):$(DOCKER_IMAGE_TAG)
endef

.PHONY: print-docker-image-name-tag
print-docker-image-name-tag:
	@echo $(call get_docker_image_name)


.PHONY: build-image
build-image: DOCKERFILE = Dockerfile.gatekeeper
build-image:
# use multi-stage dockerfile with release target
	docker build -f $(DOCKERFILE) -t $(call get_docker_image_name) --platform linux/amd64 --target release ..

define publish_docker_image
	docker push $(call get_docker_image_name)
endef

.PHONY: publish-image
publish-image:
	$(call publish_docker_image)

# Note(joe): docker target required for CI consistency
.PHONY: docker
docker: all

.PHONY: build
build:
	go build -o server .


.PHONY: unit-tests
unit-tests:
	# These tests need access to a Docker daemon: they use the library
	# `testcontainers-go`) to manage Docker container(s). If this target
	# is executed in a Docker container, then that Docker container needs to
	# have /var/run/docker.sock mounted into the container (from the host),
	# and should be started in host networking mode (at least that's what's
	# known to work).
	#docker info
	go test -v ./...

.PHONY: start-remote-dev
gatekeeper-start-remote-dev:
	telepresence connect
	@echo "Removing annotation on gatekeeper deployment to prevent controller opposing the injected telepresence sidecar container"
	kubectl annotate deployment gatekeeper opstrace-
	telepresence intercept gatekeeper --port 3001:http --env-file .env.server.remote --workload gatekeeper
	@echo ""
	@echo "Congrats, ready to start developing locally! Remember to run make stop-remote-dev when you're done, to close telepresence down."

.PHONY: stop-remote-dev
gatekeeper-stop-remote-dev:
	telepresence leave gatekeeper || echo Telepresence intercept not running
	telepresence quit
	@echo ""
	@echo "Adding annotation back on gatekeeper deployment for controller to resume its management"
	kubectl annotate deployment gatekeeper opstrace=owned
