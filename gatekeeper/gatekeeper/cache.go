package gatekeeper

import (
	"context"
	"time"

	"github.com/gin-gonic/gin"
	cache "github.com/go-redis/cache/v8"
	redis "github.com/go-redis/redis/v8"
	log "github.com/sirupsen/logrus"
)

const (
	cacheClientKey = "gatekeeper/redisCache"
)

type CacheOptions struct {
	RedisAddr          string
	RedisPassword      string
	ConnectionPoolSize int
}

type RedisCache interface {
	Set(item *cache.Item) error
	Get(ctx context.Context, key string, value interface{}) error
}

// Middleware to set the database clients on the context for downstream handlers.
func Cache(o *CacheOptions) gin.HandlerFunc {
	rdb := redis.NewFailoverClient(&redis.FailoverOptions{
		MasterName:    "mymaster",
		SentinelAddrs: []string{o.RedisAddr},
		Password:      o.RedisPassword,
		DB:            0, // use default DB
		PoolSize:      o.ConnectionPoolSize,
	})

	// Cache heavily used items with an LFU locally so subsequent resource access
	// within this instance of gatekeeper does not require any network requests.
	// Caching up to 10000 items for 10 seconds seems reasonable to start with.
	//
	// The JSON response returned by /v4/groups/:id for example
	// is ~1.16KB, so 10000 of those is around 11MB.
	//
	// After that, the cache will lookup the key in redis (great for multiple instances
	// of gatekeeper to access a common cache layer).
	// If that returns redis.NIL then finally we'll hit the gitlab API.
	//
	// TODO(mat): expose prom metrics for cache
	c := cache.New(&cache.Options{
		Redis:      rdb,
		LocalCache: cache.NewTinyLFU(10000, 10*time.Second),
	})

	return func(ctx *gin.Context) {
		ctx.Set(cacheClientKey, c)
	}
}

// Helper to get the Redis client from the context.
func GetCache(ctx *gin.Context) RedisCache {
	client, exists := ctx.Get(cacheClientKey)
	if !exists {
		log.Fatal("must use Cache() middleware")
	}
	return client.(RedisCache)
}
