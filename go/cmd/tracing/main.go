package main

import (
	"fmt"
	"log"

	// There's still a jaegerexporter in the stock/non-contrib collector
	// at "go.opentelemetry.io/collector/exporter/jaegerexporter",
	// however it hasn't had changes as recently.
	"github.com/open-telemetry/opentelemetry-collector-contrib/exporter/jaegerexporter"
	"github.com/open-telemetry/opentelemetry-collector-contrib/extension/pprofextension"
	"github.com/open-telemetry/opentelemetry-collector-contrib/processor/tailsamplingprocessor"
	"go.opentelemetry.io/collector/component"
	"go.opentelemetry.io/collector/exporter/loggingexporter"
	"go.opentelemetry.io/collector/extension/ballastextension"
	"go.opentelemetry.io/collector/extension/zpagesextension"
	"go.opentelemetry.io/collector/processor/batchprocessor"
	"go.opentelemetry.io/collector/processor/memorylimiterprocessor"
	"go.opentelemetry.io/collector/receiver/otlpreceiver"
	"go.opentelemetry.io/collector/service"
)

// Components returns the enabled components in our collector.
// This omits components that we don't use, and adds the opstrace authentication validator.
// Lots of other formats are available, for example Prom remote_write input/output are supported.
// See also the opentelemetry-collector and opentelemetry-collector-contrib repos.
func Components() (component.Factories, error) {
	extensions, err := component.MakeExtensionFactoryMap(
		ballastextension.NewFactory(), // for performance optimization via memory pool
		pprofextension.NewFactory(),   // for in-process debugging if needed
		zpagesextension.NewFactory(),  // for in-process debugging if needed
	)
	if err != nil {
		return component.Factories{}, err
	}

	receivers, err := component.MakeReceiverFactoryMap(
		otlpreceiver.NewFactory(),
	)
	if err != nil {
		return component.Factories{}, err
	}

	// see also recommendations here:
	// https://github.com/open-telemetry/opentelemetry-collector/tree/main/processor#recommended-processors
	processors, err := component.MakeProcessorFactoryMap(
		memorylimiterprocessor.NewFactory(),
		tailsamplingprocessor.NewFactory(),
		batchprocessor.NewFactory(),
	)
	if err != nil {
		return component.Factories{}, err
	}

	exporters, err := component.MakeExporterFactoryMap(
		jaegerexporter.NewFactory(),
		loggingexporter.NewFactory(), // optional, for debugging
	)
	if err != nil {
		return component.Factories{}, err
	}

	return component.Factories{
		Extensions: extensions,
		Receivers:  receivers,
		Processors: processors,
		Exporters:  exporters,
	}, nil
}

func main() {
	factories, err := Components()
	if err != nil {
		log.Fatalf("failed to build components: %v", err)
	}

	info := component.BuildInfo{
		Command:     "otelcol-opstrace",
		Description: "OpenTelemetry Collector + Opstrace auth",
		Version:     "",
	}

	if err = runInteractive(service.CollectorSettings{BuildInfo: info, Factories: factories}); err != nil {
		log.Fatal(err)
	}
}

func runInteractive(params service.CollectorSettings) error {
	cmd := service.NewCommand(params)
	if err := cmd.Execute(); err != nil {
		return fmt.Errorf("collector server run finished with error: %w", err)
	}

	return nil
}
