package common

import (
	"net/http"
	"net/url"
	"time"

	"github.com/opstrace/opstrace/go/pkg/argusapi"
	"github.com/opstrace/opstrace/go/pkg/constants"
	"github.com/xanzy/go-gitlab"
)

const (
	NoFolderTitle = "no-title"
)

// Grafana roles.
const (
	GrafanaViewer string = "Viewer"
	GrafanaEditor string = "Editor"
	GrafanaAdmin  string = "Admin"
)

// Map a GitLab group membership access_level to a Grafana role.
func GrafanaRoleFromGroupAccessLevel(groupAccessLevel gitlab.AccessLevelValue) string {
	if groupAccessLevel >= gitlab.MaintainerPermissions {
		return GrafanaAdmin
	}
	if groupAccessLevel >= gitlab.DeveloperPermissions {
		return GrafanaEditor
	}
	return GrafanaViewer
}

type ArgusClient struct {
	Client  *argusapi.Client
	groupID int64
}

func NewArgusClientFromURL(argusURL string, groupID int64, transport *http.Transport) (*ArgusClient, error) {
	parsedURL, err := url.Parse(argusURL)
	if err != nil {
		return nil, err
	}
	username := parsedURL.User.Username()
	password, _ := parsedURL.User.Password()

	return NewArgusClient(argusURL, username, password, transport, groupID)
}

func NewArgusClient(baseURL, user, password string, transport *http.Transport, groupID int64) (*ArgusClient, error) {
	client := &http.Client{
		Transport: transport,
		Timeout:   time.Second * time.Duration(constants.ArgusDefaultClientTimeoutSeconds),
	}

	userInfo := url.UserPassword(user, password)

	ac, err := argusapi.New(baseURL, argusapi.Config{
		OrgID:     groupID,
		Client:    client,
		BasicAuth: userInfo,
		// Setting this to a higher number would provide more resilience against
		// transient network errors but would make the entire flow longer
		// during the likes of CreateOrUpdate type calls because if the create
		// doesn't succeed, it'll be retried with delays before the update is attempted
		NumRetries: 1,
	})

	return &ArgusClient{
		Client:  ac,
		groupID: groupID,
	}, err
}

func (r *ArgusClient) GetDashboard(uid string) (*argusapi.Dashboard, error) {
	return r.Client.DashboardByUID(uid)
}

func (r *ArgusClient) CreateOrUpdateFolder(name string) (argusapi.Folder, error) {
	allfolders, err := r.Client.Folders()
	if err != nil {
		return argusapi.Folder{}, err
	}

	for _, folder := range allfolders {
		if folder.Title == name {
			return folder, nil
		}
	}
	var title = name
	if title == "" {
		title = NoFolderTitle
	}

	return r.Client.NewFolder(title)
}

// CreateOrUpdateDashboard saves the dashboard in argus.
func (r *ArgusClient) CreateOrUpdateDashboard(
	dashboard map[string]interface{},
	folderID int64,
) (*argusapi.DashboardSaveResponse, error) {
	return r.Client.NewDashboard(argusapi.Dashboard{
		Model: dashboard,

		Folder: folderID,
		// We always want to set `overwrite` so we update existing if it exists
		Overwrite: true,
	})
}

// Delete a dashboard by a UID.
func (r *ArgusClient) DeleteDashboardByUID(uid string) error {
	return r.Client.DeleteDashboardByUID(uid)
}

// Delete Folder by UID.
func (r *ArgusClient) DeleteFolderByUID(uid string) error {
	return r.Client.DeleteFolderByUID(uid)
}

// Delete Folder by ID.
func (r *ArgusClient) DeleteFolderByID(id *int64) error {
	folder, err := r.Client.Folder(*id)
	if err != nil {
		return err
	}
	return r.DeleteDashboardByUID(folder.UID)
}

// Returns true if folder is empty.
func (r *ArgusClient) SafeToDelete(folderID *int64) (bool, error) {
	ids := []int64{*folderID}
	response, err := r.Client.DashboardsByFolderIDs(ids)

	if err != nil {
		return false, err
	}
	if len(response) > 0 {
		return false, nil
	} else {
		return true, nil
	}
}

// GroupExists checks if Group exists.
func (r *ArgusClient) GroupExists(groupID int64) error {
	_, err := r.Client.Org(groupID)
	return err
}

func (r *ArgusClient) DeleteGroup(groupID int64) error {
	return r.Client.DeleteOrg(groupID)
}

func (r *ArgusClient) CreateOrUpdateGroup(group *argusapi.Org) error {
	err := r.Client.UpdateOrg(group.ID, group.Name)
	if err != nil {
		// try to create
		_, err := r.Client.NewOrg(*group)
		return err
	}
	return nil
}

// Create or update user and return ID.
func (r *ArgusClient) CreateOrUpdateUser(user *gitlab.User) (int64, error) {
	var userID int64
	var justCreated = false

	// Attempt to retrieve user first so we can get their ID.
	// Argus has an autoincrementing ID field
	u, err := r.Client.UserByEmail(user.Email)
	if err == nil {
		userID = u.ID
	} else {
		// User may not exist, attempt to create
		userID, err = r.Client.CreateUser(argusapi.User{
			Email: user.Email,
			Name:  user.Name,
			Login: user.Username,
			// No need to provide admin priviledges for anyone
			// since users only interact with orgs/groups.
			IsAdmin: false,
			// Password is not used because the auth proxy controls access.
			// This value is required when creating the user via api
			Password:   RandStringRunes(8),
			IsExternal: true,
			AvatarURL:  user.AvatarURL,
		})
		if err != nil {
			return 0, err
		}
		justCreated = true
	}
	if !justCreated {
		// update user
		err = r.Client.UserUpdate(argusapi.User{
			ID:    userID,
			Email: user.Email,
			Name:  user.Name,
			Login: user.Username,
			// No need to provide admin priviledges for anyone
			// since users only interact with orgs/groups.
			// Admins also need a password.
			IsAdmin:    false,
			IsExternal: true,
			AvatarURL:  user.AvatarURL,
		})
		if err != nil {
			return 0, err
		}
	}

	return userID, nil
}

// Updates or creates the user membership for group.
func (r *ArgusClient) CreateOrUpdateGroupUser(groupID int64, user *gitlab.User, role string) error {
	// Attempt to update the group membership role
	err := r.Client.UpdateOrgUser(groupID, int64(user.ID), role)
	if err != nil {
		// Attempt to add the group membership role if update failed
		return r.Client.AddOrgUser(groupID, user.Email, role)
	}

	return nil
}
