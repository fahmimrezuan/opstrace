# Argus HTTP API Client for Go

This library provides a low-level client to access Grafana [HTTP API](https://gitlab.com/gitlab-org/opstrace/opstrace-ui).
