package errortracking

import (
	"fmt"
	"io"
	"net/url"
	"strconv"

	"github.com/go-openapi/runtime/middleware"
	log "github.com/sirupsen/logrus"

	"github.com/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/errors"
	"github.com/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/events"
	"github.com/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/projects"
)

type Controller struct {
	db      Database
	baseURL *url.URL
}

func NewController(baseURL *url.URL, db Database) *Controller {
	return &Controller{
		baseURL: baseURL,
		db:      db,
	}
}

// PostEnvelopeHandler handles the POST request sent to the
// /projects/{projectID}/envelope route.
func (c *Controller) PostEnvelopeHandler(params events.PostProjectsAPIProjectIDEnvelopeParams) middleware.Responder {
	// Parse the http request body into a string and unmarshall it manually.
	defer params.HTTPRequest.Body.Close()
	payload, err := io.ReadAll(params.HTTPRequest.Body)
	if err != nil {
		log.WithError(err).Debug("failed to read request body")
		return events.NewPostProjectsAPIProjectIDEnvelopeBadRequest()
	}

	e, err := NewEnvelopeFrom(payload)
	if err != nil {
		log.WithError(err).Debug("failed to parse request")
		return events.NewPostProjectsAPIProjectIDEnvelopeBadRequest()
	}

	// Sentry sends 2 requests on each exception: transaction and event.
	// Everything else is not a desired behavior.
	if e.Type.Type != "transaction" && e.Type.Type != "event" {
		log.WithField("type", e.Type.Type).Debug("envelope type not supported")
		return events.NewPostProjectsAPIProjectIDEnvelopeBadRequest()
	}

	err = c.db.InsertErrorTrackingErrorEvent(NewErrorTrackingErrorEvent(params.ProjectID, e.Event, payload))
	if err != nil {
		log.WithError(err).Debug("failed to create event")
		return events.NewPostProjectsAPIProjectIDEnvelopeInternalServerError()
	}

	return events.NewPostProjectsAPIProjectIDEnvelopeOK()
}

func (c *Controller) PostStoreHandler(params events.PostProjectsAPIProjectIDStoreParams) middleware.Responder {
	defer params.HTTPRequest.Body.Close()
	payload, err := io.ReadAll(params.HTTPRequest.Body)
	if err != nil {
		log.WithError(err).Debug("failed to read request body")
		return events.NewPostProjectsAPIProjectIDEnvelopeBadRequest()
	}

	e, err := NewEventFrom(payload)
	if err != nil {
		log.WithError(err).Debug("failed to parse request")
		return events.NewPostProjectsAPIProjectIDStoreBadRequest()
	}

	err = c.db.InsertErrorTrackingErrorEvent(NewErrorTrackingErrorEvent(params.ProjectID, e, payload))
	if err != nil {
		log.WithError(err).Debug("failed to create event")
		return events.NewPostProjectsAPIProjectIDEnvelopeInternalServerError()
	}

	return events.NewPostProjectsAPIProjectIDStoreOK()
}

func (c *Controller) ListErrors(params errors.ListErrorsParams) middleware.Responder {
	logger := log.
		WithField("projectId", params.ProjectID).
		WithField("sort", *params.Sort).
		WithField("status", *params.Status).
		WithField("limit", *params.Limit)

	if params.Query != nil {
		logger = logger.WithField("query", *params.Query)
	}

	if params.Cursor != nil {
		logger = logger.WithField("cursor", *params.Cursor)
	}
	logger.Debug("got list errors request")

	res, err := c.db.ListErrors(params)
	if err != nil {
		log.WithError(err).Error("listing errors")
		return errors.NewListErrorsInternalServerError()
	}

	resp := errors.NewListErrorsOK()
	resp.Payload = res
	resp.Link = buildListErrorsLink(c.baseURL, params)

	return resp
}

func buildListErrorsLink(baseURL *url.URL, params errors.ListErrorsParams) string {
	page, _ := decodePage(params.Cursor)
	prev := page - 1
	prevLink := ""
	next := page + 1

	buildListErrorsLinkPage := func(page int) string {
		u, _ := url.Parse(baseURL.String() + params.HTTPRequest.URL.String())
		q := u.Query()

		if params.Limit != nil {
			q.Set("limit", strconv.Itoa(int(*params.Limit)))
		}
		if params.Query != nil {
			q.Set("query", *params.Query)
		}
		if params.Sort != nil {
			q.Set("sort", *params.Sort)
		}
		if params.Status != nil {
			q.Set("status", *params.Status)
		}
		q.Set("cursor", encodePage(page))
		u.RawQuery = q.Encode()

		return u.String()
	}

	if prev >= 1 {
		prevLink = buildListErrorsLinkPage(prev)
	}
	nextLink := buildListErrorsLinkPage(next)

	link := ""
	if prevLink != "" {
		link = fmt.Sprintf(`<%s>; rel="prev", `, prevLink)
	}
	link += fmt.Sprintf(`<%s>; rel="next"`, nextLink)

	return link
}

func (c *Controller) GetError(params errors.GetErrorParams) middleware.Responder {
	logger := log.
		WithField("projectId", params.ProjectID).
		WithField("fingerprint", params.Fingerprint)

	logger.Debug("got get error request")

	res, err := c.db.GetError(params)
	if err != nil {
		logger.WithError(err).Error("get error")
		return errors.NewGetErrorInternalServerError()
	}

	if res == nil {
		return errors.NewGetErrorNotFound()
	}

	return errors.NewGetErrorOK().WithPayload(res)
}

func (c *Controller) UpdateError(params errors.UpdateErrorParams) middleware.Responder {
	logger := log.
		WithField("projectId", params.ProjectID).
		WithField("fingerprint", params.Fingerprint).
		WithField("userID", params.Body.UpdatedByID).
		WithField("status", params.Body.Status)

	logger.Debug("got update error request")

	res, err := c.db.UpdateError(params)
	if err != nil {
		logger.WithError(err).Error("update error")
		return errors.NewUpdateErrorInternalServerError()
	}

	if res == nil {
		return errors.NewUpdateErrorNotFound()
	}

	return errors.NewUpdateErrorOK().WithPayload(res)
}

func (c *Controller) ListEvents(params errors.ListEventsParams) middleware.Responder {
	logger := log.
		WithField("projectId", params.ProjectID).
		WithField("sort", *params.Sort).
		WithField("limit", *params.Limit)

	if params.Cursor != nil {
		logger = logger.WithField("cursor", *params.Cursor)
	}
	logger.Debug("got list errors request")

	res, err := c.db.ListEvents(params)
	if err != nil {
		log.WithError(err).Error("listing errors")
		return errors.NewListErrorsInternalServerError()
	}

	resp := errors.NewListEventsOK()
	resp.Payload = res
	resp.Link = buildListEventsLink(c.baseURL, params)

	return resp
}

func buildListEventsLink(baseURL *url.URL, params errors.ListEventsParams) string {
	page, _ := decodePage(params.Cursor)
	prev := page - 1
	prevLink := ""
	next := page + 1

	buildListEventsLinkPage := func(page int) string {
		u, _ := url.Parse(baseURL.String() + params.HTTPRequest.URL.String())
		q := u.Query()

		if params.Limit != nil {
			q.Set("limit", strconv.Itoa(int(*params.Limit)))
		}
		if params.Sort != nil {
			q.Set("sort", *params.Sort)
		}
		q.Set("cursor", encodePage(page))
		u.RawQuery = q.Encode()

		return u.String()
	}

	if prev >= 1 {
		prevLink = buildListEventsLinkPage(prev)
	}
	nextLink := buildListEventsLinkPage(next)

	link := ""
	if prevLink != "" {
		link = fmt.Sprintf(`<%s>; rel="prev", `, prevLink)
	}
	link += fmt.Sprintf(`<%s>; rel="next"`, nextLink)

	return link
}

func (c *Controller) DeleteProject(params projects.DeleteProjectParams) middleware.Responder {
	logger := log.WithField("projectId", params.ID)
	logger.Debug("got delete project request")
	// TODO: handle project not found but since this endpoint is to be used for
	// testing just go ahead and proceed with the deletion.
	err := c.db.DeleteProject(params)
	if err != nil {
		return projects.NewDeleteProjectInternalServerError()
	}

	return projects.NewDeleteProjectCreated()
}
