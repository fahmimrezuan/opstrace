package errortracking

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"strconv"
	"time"
)

type EnvelopeMetadata struct {
	EventID string    `json:"event_id"`
	SentAt  time.Time `json:"sent_at"`
}

type EnvelopeType struct {
	Type   string `json:"type"`
	Length int    `json:"length"`
}

type Frame struct {
	Function    string                 `json:"function,omitempty"`
	Symbol      string                 `json:"symbol,omitempty"`
	Module      string                 `json:"module,omitempty"`
	Package     string                 `json:"package,omitempty"`
	Filename    string                 `json:"filename,omitempty"`
	AbsPath     string                 `json:"abs_path,omitempty"`
	Lineno      int                    `json:"lineno,omitempty"`
	Colno       int                    `json:"colno,omitempty"`
	PreContext  []string               `json:"pre_context,omitempty"`
	ContextLine string                 `json:"context_line,omitempty"`
	PostContext []string               `json:"post_context,omitempty"`
	InApp       bool                   `json:"in_app,omitempty"`
	Vars        map[string]interface{} `json:"vars,omitempty"`
}

type Stacktrace struct {
	Frames        []Frame `json:"frames,omitempty"`
	FramesOmitted []uint  `json:"frames_omitted,omitempty"`
}

type Exception struct {
	Type       string      `json:"type,omitempty"`
	Value      string      `json:"value,omitempty"`
	Module     string      `json:"module,omitempty"`
	ThreadID   interface{} `json:"thread_id,omitempty"`
	Stacktrace *Stacktrace `json:"stacktrace,omitempty"`
}

type Span struct {
	TraceID      string                 `json:"trace_id"`
	SpanID       string                 `json:"span_id"`
	ParentSpanID string                 `json:"parent_span_id"`
	Op           string                 `json:"op,omitempty"`
	Description  string                 `json:"description,omitempty"`
	Status       string                 `json:"status,omitempty"`
	Tags         map[string]string      `json:"tags,omitempty"`
	StartTime    time.Time              `json:"start_timestamp"`
	EndTime      time.Time              `json:"timestamp"`
	Data         map[string]interface{} `json:"data,omitempty"`
}

type Event struct {
	Dist        string    `json:"dist,omitempty"`
	Environment string    `json:"environment,omitempty"`
	EventID     string    `json:"event_id,omitempty"`
	Level       string    `json:"level,omitempty"`
	Message     string    `json:"message,omitempty"`
	Platform    string    `json:"platform,omitempty"`
	Timestamp   time.Time `json:"timestamp"`
	Transaction string    `json:"transaction,omitempty"`
	// This is field is decoded in a special way
	Exception []*Exception `json:"-"`
	// The fields below are only relevant for transactions.
	Type      string    `json:"type,omitempty"`
	StartTime time.Time `json:"start_timestamp"`
	Spans     []*Span   `json:"spans,omitempty"`
	// This will hold a pointer to  the first exception that has a stacktrace
	// since the first exception may not provide adequate context (e.g. in the
	// Go SDK).
	exception *Exception `json:"-"`
}

// helper struct to unmarshall a json error event.
type unmarshaller struct {
	objmap map[string]*json.RawMessage
	err    error
}

func (u *unmarshaller) do(key string, value interface{}) {
	if u.err != nil {
		return
	}
	if _, found := u.objmap[key]; !found {
		return
	}
	err := json.Unmarshal(*u.objmap[key], value)
	if err != nil {
		u.err = fmt.Errorf("decoding %s: %w", key, err)
	}
}

// The sentry ruby and javascript sdk wrap the exception in a values field.
type ExceptionValues struct {
	Values []*Exception `json:"values"`
}

// NodeTimestamp is a helper struct to parse a timestamp sent by the sentry
// javascript sdk. The sentry javascript sdk sends timestamps as unix timestamp
// in milliseconds.
type NodeTimestamp struct {
	time.Time
}

// Unmarshall a timestamp in the format 1655840960.614 to a time.Time.
func (t *NodeTimestamp) UnmarshalJSON(data []byte) error {
	f, err := strconv.ParseFloat(string(data), 64)
	if err != nil {
		return err
	}

	i := int64(f * 1000)

	t.Time = time.Unix(0, i*int64(time.Millisecond))
	return nil
}

func (e *Event) UnmarshalJSON(data []byte) error {
	var objmap map[string]*json.RawMessage

	err := json.Unmarshal(data, &objmap)
	if err != nil {
		return err
	}

	u := &unmarshaller{objmap: objmap}
	u.do("dist", e.Dist)
	u.do("environment", &e.Environment)
	u.do("event_id", &e.EventID)
	u.do("level", &e.Level)
	u.do("message", &e.Message)
	u.do("platform", &e.Platform)
	u.do("transaction", &e.Transaction)

	switch e.Platform {
	case "node":
		var t NodeTimestamp
		u.do("timestamp", &t)
		e.Timestamp = t.Time
	default:
		u.do("timestamp", &e.Timestamp)
	}

	switch e.Platform {
	case "go":
		u.do("exception", &e.Exception)
	case "ruby", "node":
		var ex ExceptionValues
		u.do("exception", &ex)
		if u.err == nil {
			e.Exception = ex.Values
		}
	default:
		u.err = fmt.Errorf("unsupported platform: %s", e.Platform)
	}

	if u.err != nil {
		return u.err
	}

	// Find pointer to the first exception that has a stacktrace since the first
	// exception may not provide adequate context (e.g. in the Go SDK).
	// Original ruby code:
	//nolint:lll
	// - https://gitlab.com/gitlab-org/gitlab/-/blob/cb84795fae22fa8d87bddda8810e636cd729030d/app/services/error_tracking/collect_error_service.rb#L41-48
	// - https://gitlab.com/gitlab-org/gitlab/-/blob/cb84795fae22fa8d87bddda8810e636cd729030d/app/services/error_tracking/collect_error_service.rb#L56-67
	for i, ex := range e.Exception {
		if ex.Stacktrace == nil {
			continue
		}
		e.exception = e.Exception[i]
		break
	}

	if e.exception == nil {
		u.err = fmt.Errorf("invalid error event")
	}

	if u.err != nil {
		return u.err
	}

	return e.Validate()
}

// Validate Event has all fields set.
// Original ruby code:
// - https://gitlab.com/gitlab-org/gitlab/-/blob/master/app/models/error_tracking/error.rb#L20-25
func (e Event) Validate() error {
	if e.Name() == "" {
		return fmt.Errorf("invalid error event: name is not set")
	}

	if e.Description() == "" {
		return fmt.Errorf("invalid error event: description is not set")
	}

	if e.Actor() == "" {
		return fmt.Errorf("invalid error event: actor is not set")
	}

	if e.Platform == "" {
		return fmt.Errorf("invalid error event: platform is not set")
	}

	return nil
}

// Actor returns the transaction name. If the error does not have one set it
// returns the first item in stacktrace that has a function and module name.
// Original ruby code:
//nolint:lll
// - https://gitlab.com/gitlab-org/gitlab/-/blob/90b6bea21c74ccc98205aff69e0e0f2acfc7993f/app/services/error_tracking/collect_error_service.rb#L11
// - https://gitlab.com/gitlab-org/gitlab/-/blob/daa5796df89d775c635dc900a532efb657877b75/app/services/error_tracking/collect_error_service.rb#L66
func (e Event) Actor() string {
	if e.Transaction != "" {
		return e.Transaction
	}

	// This means there's no actor name and we should fail.
	l := len(e.exception.Stacktrace.Frames)
	if l == 0 {
		return ""
	}

	// Some SDKs do not have a transaction attribute. So we build it by
	// combining function name and module name from the last item in stacktrace.
	//nolint:lll
	// https://gitlab.com/gitlab-org/gitlab/-/blob/8565b9d4770baafa560915b6b06da6026ecab41c/app/services/error_tracking/collect_error_service.rb#L62

	f := e.exception.Stacktrace.Frames[l-1]
	return fmt.Sprintf("%s(%s)", f.Function, f.Module)
}

// Name returns the exception type.
// Original ruby code:
//nolint:lll
// https://gitlab.com/gitlab-org/gitlab/-/blob/cb84795fae22fa8d87bddda8810e636cd729030d/app/services/error_tracking/collect_error_service.rb#L9
func (e Event) Name() string {
	return e.exception.Type
}

// Description returns the exception value.
// Original ruby code:
//nolint:lll
// - https://gitlab.com/gitlab-org/gitlab/-/blob/90b6bea21c74ccc98205aff69e0e0f2acfc7993f/app/services/error_tracking/collect_error_service.rb#L10
func (e Event) Description() string {
	return e.exception.Value
}

type Envelope struct {
	Metadata *EnvelopeMetadata
	Type     *EnvelopeType
	Event    *Event
}

type envelopeScanner struct {
	err error
	s   *bufio.Scanner
}

func (e *envelopeScanner) do(v interface{}) {
	if e.err != nil {
		return
	}

	ok := e.s.Scan()
	if !ok {
		e.err = e.s.Err()
		return
	}

	e.err = json.Unmarshal(e.s.Bytes(), v)
}

// NewEnvelopeFrom expects a payload request containing 3 objects: sentry
// metadata, type data and event data. It parses the payload and returns an
// Envelope object or an error.
//
// For reference, this is how the upstream sentry-go client sdk encodes the
// payload:
// https://github.com/getsentry/sentry-go/blob/409df0940aada10321428286de0c0b59fde0a796/transport.go#L96
func NewEnvelopeFrom(payload []byte) (*Envelope, error) {
	e := &Envelope{
		Metadata: &EnvelopeMetadata{},
		Type:     &EnvelopeType{},
	}

	r := bytes.NewReader(payload)
	s := &envelopeScanner{
		s: bufio.NewScanner(r),
	}

	e.Metadata = &EnvelopeMetadata{}
	e.Type = &EnvelopeType{}
	e.Event = &Event{}

	s.do(e.Metadata)
	s.do(e.Type)
	s.do(e.Event)

	return e, s.err
}

// NewEventFrom parses a payload request to an Event object.
func NewEventFrom(payload []byte) (*Event, error) {
	e := &Event{}

	err := e.UnmarshalJSON(payload)
	return e, err
}
