include ../Makefile.shared

# DOCKER_IMAGE_TAG is a shasum of all the files (except hidden) in the go
# directory. We use it to have a deterministic method to calculate a docker
# image tag.
DOCKER_IMAGE_TAG ?= $(shell find . -type f -not -name ".*" -not -name "docker-images.json" -print0 | sort -z -d -f | xargs -0 cat | shasum | cut -d' ' -f1)

DOCKERFILE = ./Dockerfile

all: docker-build docker-push

export GO111MODULE=on
export GOPRIVATE=github.com/opstrace

.PHONY: clean
clean: clean-tracing clean-errortracking

.PHONY: clean-tracing
clean-tracing:
	rm -f tracing-api

.PHONY: clean-errortracking
clean-errortracking:
	rm -f errortracking-api

.PHONY: docker-build
docker-build: docker-build-tracing docker-build-errortracking

# Note(joe): docker target required for CI consistency
.PHONY: docker
docker: docker-build docker-push

define get_docker_image_name
	$(IMAGE_REGISTRY)/$(DOCKER_IMAGE_NAME):$(DOCKER_IMAGE_TAG)
endef

.PHONY: print-docker-image-name-tag
print-docker-image-name-tag:
	@echo $(call get_docker_image_name)

# Function to build a docker image. Parameters are passed via
# Makefile variables.
define build_docker_image
	docker build -f $(DOCKERFILE) . -t $(call get_docker_image_name) --platform linux/amd64
endef

.PHONY: docker-build-tracing
docker-build-tracing: DOCKERFILE = Dockerfile.tracing
docker-build-tracing: DOCKER_IMAGE_NAME = tracing-api
docker-build-tracing:
	$(call build_docker_image)

.PHONY: docker-build-errortracking
docker-build-errortracking: DOCKERFILE = Dockerfile.errortracking-api
docker-build-errortracking: DOCKER_IMAGE_NAME = errortracking-api
docker-build-errortracking:
	$(call build_docker_image)

.PHONY: docker-push
docker-push: docker-push-tracing docker-push-errortracking

define docker-push_docker_image
	docker push $(call get_docker_image_name)
endef

.PHONY: docker-push-tracing
docker-push-tracing: DOCKER_IMAGE_NAME = tracing-api
docker-push-tracing:
	$(call docker-push_docker_image)

.PHONY: docker-push-errortracking
docker-push-errortracking: DOCKER_IMAGE_NAME = errortracking-api
docker-push-errortracking:
	$(call docker-push_docker_image)

.PHONY: build
build: build-tracing build-errortracking

.PHONY: build-tracing
build-tracing: tracing-api

tracing-api:
	go build -o tracing-api ./cmd/tracing/

.PHONY: build-errortracking
build-errortracking: errortracking-api

errortracking-api:
	go build -o errortracking-api ./cmd/errortracking/

.PHONY: unit-tests
unit-tests:
	# These tests need access to a Docker daemon: they use the library
	# `testcontainers-go`) to manage Docker container(s). If this target
	# is executed in a Docker container, then that Docker container needs to
	# have /var/run/docker.sock mounted into the container (from the host),
	# and should be started in host networking mode (at least that's what's
	# known to work).
	#docker info
	go test -v ./...

