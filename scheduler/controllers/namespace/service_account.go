package namespace

import (
	"github.com/opstrace/opstrace/scheduler/api/v1alpha1"
	"github.com/opstrace/opstrace/scheduler/controllers/config"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

func getServiceAccountName() string {
	return GetTenantOperatorName()
}

func getServiceAccountLabels() map[string]string {
	return GetTenantOperatorSelector()
}

func getServiceAccountAnnotations(cr *v1alpha1.GitLabNamespace, existing map[string]string) map[string]string {
	return existing
}

func getServiceAccountImagePullSecrets() []v1.LocalObjectReference {
	return config.Get().ClusterSpec().Spec.ImagePullSecrets
}

func ServiceAccount(cr *v1alpha1.GitLabNamespace) *v1.ServiceAccount {
	return &v1.ServiceAccount{
		ObjectMeta: metav1.ObjectMeta{
			Name:        getServiceAccountName(),
			Namespace:   cr.Namespace(),
			Labels:      getServiceAccountLabels(),
			Annotations: getServiceAccountAnnotations(cr, nil),
		},
		ImagePullSecrets: getServiceAccountImagePullSecrets(),
	}
}

func ServiceAccountSelector(cr *v1alpha1.GitLabNamespace) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace(),
		Name:      getServiceAccountName(),
	}
}

func ServiceAccountMutator(cr *v1alpha1.GitLabNamespace, current *v1.ServiceAccount) error {
	current.Labels = getServiceAccountLabels()
	current.Annotations = getServiceAccountAnnotations(cr, current.Annotations)
	current.ImagePullSecrets = getServiceAccountImagePullSecrets()

	return nil
}
