package config

import (
	"net/url"
	"sync"

	"github.com/opstrace/opstrace/scheduler/api/v1alpha1"
)

type ClickHouseEndpoints struct {
	Http   url.URL
	Native url.URL
}

type ControllerConfig struct {
	mu          *sync.Mutex
	Loaded      bool
	spec        v1alpha1.Cluster
	chEndpoints *ClickHouseEndpoints
	pgEndpoint  *url.URL
}

var instance *ControllerConfig
var once sync.Once

// Note(joe): what is this for? Will copying the struct value not suffice?
func cloneURL(u url.URL) url.URL {
	str := u.String()
	//nolint:errcheck
	clone, _ := url.Parse(str)

	return *clone
}

func Get() *ControllerConfig {
	once.Do(func() {
		instance = &ControllerConfig{
			Loaded: false,
			mu:     &sync.Mutex{},
		}
	})
	return instance
}

// SetClusterSpec stores the controller spec for all controllers to consume
func (c *ControllerConfig) SetClusterSpec(spec v1alpha1.Cluster) {
	c.mu.Lock()
	defer c.mu.Unlock()

	c.spec = spec
	c.Loaded = true
}

// ClusterSpec gets the controller spec
func (c *ControllerConfig) ClusterSpec() v1alpha1.Cluster {
	c.mu.Lock()
	defer c.mu.Unlock()

	return *c.spec.DeepCopy()
}

// SetClickHouseEndpoints stores the authenticated clickhouse endpoints for all controllers to consume
func (c *ControllerConfig) SetClickHouseEndpoints(chEndpoints *ClickHouseEndpoints) {
	c.mu.Lock()
	defer c.mu.Unlock()

	c.chEndpoints = chEndpoints
}

// ClickHouseEndpoints gets the controller chEndpoints
func (c *ControllerConfig) ClickHouseEndpoints() *ClickHouseEndpoints {
	c.mu.Lock()
	defer c.mu.Unlock()

	if c.chEndpoints == nil {
		return nil
	}

	return &ClickHouseEndpoints{
		Http:   cloneURL(c.chEndpoints.Http),
		Native: cloneURL(c.chEndpoints.Native),
	}
}

// SetPostgresEndpoint stores the authenticated postgres endpoint for all controllers to consume
func (c *ControllerConfig) SetPostgresEndpoint(endpoint *url.URL) {
	c.mu.Lock()
	defer c.mu.Unlock()

	c.pgEndpoint = endpoint
}

// PostgresEndpoint gets the controller postgres endpoint
func (c *ControllerConfig) PostgresEndpoint() *url.URL {
	c.mu.Lock()
	defer c.mu.Unlock()

	if c.pgEndpoint == nil {
		return nil
	}
	clone := cloneURL(*c.pgEndpoint)

	return &clone
}
