package externalDNS

import (
	"github.com/opstrace/opstrace/go/pkg/constants"
	"github.com/opstrace/opstrace/scheduler/api/v1alpha1"
	"github.com/opstrace/opstrace/scheduler/controllers/cluster/externalDNS/gcp"
	appsv1 "k8s.io/api/apps/v1"
	v1 "k8s.io/api/core/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

var panicMessage = "no supported externaldns provider configured"

func ExternalDNSSelector(cr *v1alpha1.Cluster) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace(),
		Name:      constants.ExternalDNSName,
	}
}

func ExternalDNSServiceAccount(cr *v1alpha1.Cluster) *v1.ServiceAccount {
	if cr.Spec.DNS.ExternalDNSProvider.GCP != nil {
		return gcp.ExternalDNSServiceAccount(cr)
	}
	panic(panicMessage)
}

func ExternalDNSServiceAccountMutator(cr *v1alpha1.Cluster, current *v1.ServiceAccount) {
	if cr.Spec.DNS.ExternalDNSProvider.GCP != nil {
		gcp.ExternalDNSServiceAccountMutator(cr, current)
		return
	}
	panic(panicMessage)
}

func GetRBACObjects(cr *v1alpha1.Cluster) ([]client.Object, error) {
	if cr.Spec.DNS.ExternalDNSProvider.GCP != nil {
		return gcp.GetRBACObjects(cr)
	}
	panic(panicMessage)
}

func ExternalDNSDeployment(cr *v1alpha1.Cluster) *appsv1.Deployment {
	if cr.Spec.DNS.ExternalDNSProvider.GCP != nil {
		return gcp.ExternalDNSDeployment(cr)
	}
	panic(panicMessage)
}

func ExternalDNSDeploymentMutator(cr *v1alpha1.Cluster, current *appsv1.Deployment) error {
	if cr.Spec.DNS.ExternalDNSProvider.GCP != nil {
		return gcp.ExternalDNSDeploymentMutator(cr, current)
	}
	panic(panicMessage)
}
