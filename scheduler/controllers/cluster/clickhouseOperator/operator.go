package clickhouseOperator

import (
	"fmt"

	v1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"

	utils "github.com/opstrace/opstrace/go/pkg/common"
	"github.com/opstrace/opstrace/go/pkg/constants"
	"github.com/opstrace/opstrace/scheduler/api/v1alpha1"
)

const (
	MemoryRequest = "256Mi"
	CpuRequest    = "100m"
	MemoryLimit   = "1024Mi"
	CpuLimit      = "500m"
	// entrypoint script to insert password env var into users xml
	usersInitEntrypoint = "sed \"s/%%OPERATOR_PASSWORD%%/$(echo -n $OPERATOR_PASSWORD | sha256sum | tr -d ' -')/g\" /tmp/clickhouse-users.xml.tmpl > /etc/clickhouse-operator/users.d/clickhouse-users.xml"
)

var Replicas int32 = 1

func GetClickHouseOperatorDeploymentName() string {
	return constants.ClickHouseOperatorName
}

func GetClickHouseOperatorDeploymentSelector() map[string]string {
	return map[string]string{
		"app": constants.ClickHouseOperatorName,
	}
}

func getResources() corev1.ResourceRequirements {
	return corev1.ResourceRequirements{
		Requests: corev1.ResourceList{
			corev1.ResourceMemory: resource.MustParse(MemoryRequest),
			corev1.ResourceCPU:    resource.MustParse(CpuRequest),
		},
		Limits: corev1.ResourceList{
			corev1.ResourceMemory: resource.MustParse(MemoryLimit),
			corev1.ResourceCPU:    resource.MustParse(CpuLimit),
		},
	}
}

func getDeploymentStrategy() v1.DeploymentStrategy {
	return v1.DeploymentStrategy{
		Type: v1.RecreateDeploymentStrategyType,
	}
}

func getDeploymentLabels() map[string]string {
	return map[string]string{
		"app": constants.ClickHouseOperatorName,
	}
}

func getDeploymentAnnotations(cr *v1alpha1.Cluster, existing map[string]string) map[string]string {
	return existing
}

func getPodAnnotations(cr *v1alpha1.Cluster, existing map[string]string) map[string]string {
	hash, err := GetClickHouseOperatorConfigHash(cr)

	if err != nil {
		return existing
	}
	return utils.MergeMap(existing, map[string]string{
		constants.LastConfigAnnotation: hash,
	})
}

func getPodLabels() map[string]string {
	return map[string]string{
		"app": constants.ClickHouseOperatorName,
	}
}

func getVolumes() []corev1.Volume {
	return []corev1.Volume{{
		Name: "operator-config",
		VolumeSource: corev1.VolumeSource{
			ConfigMap: &corev1.ConfigMapVolumeSource{
				LocalObjectReference: corev1.LocalObjectReference{
					Name: constants.ClickHouseOperatorName,
				},
				Items: []corev1.KeyToPath{
					{
						Key:  "operator-config.yaml",
						Path: "config.yaml",
					},
				},
			},
		},
	}, {
		Name: "clickhouse-config",
		VolumeSource: corev1.VolumeSource{
			ConfigMap: &corev1.ConfigMapVolumeSource{
				LocalObjectReference: corev1.LocalObjectReference{
					Name: constants.ClickHouseOperatorName,
				},
				Items: []corev1.KeyToPath{
					{
						Key:  "clickhouse-config.xml",
						Path: "clickhouse-config.xml",
					},
				},
			},
		},
	}, {
		Name: "clickhouse-user-config",
		VolumeSource: corev1.VolumeSource{
			ConfigMap: &corev1.ConfigMapVolumeSource{
				LocalObjectReference: corev1.LocalObjectReference{
					Name: constants.ClickHouseOperatorName,
				},
				Items: []corev1.KeyToPath{
					{
						Key:  "clickhouse-users.xml.tmpl",
						Path: "clickhouse-users.xml.tmpl",
					},
				},
			},
		},
	}, {
		Name: "clickhouse-user-dir",
		VolumeSource: corev1.VolumeSource{
			EmptyDir: &corev1.EmptyDirVolumeSource{},
		},
	}}
}

func getOperatorContainerEnv() []corev1.EnvVar {
	return []corev1.EnvVar{{
		Name: "OPERATOR_POD_NODE_NAME",
		ValueFrom: &corev1.EnvVarSource{
			FieldRef: &corev1.ObjectFieldSelector{FieldPath: "spec.nodeName"},
		},
	}, {
		Name: "OPERATOR_POD_NAME",
		ValueFrom: &corev1.EnvVarSource{
			FieldRef: &corev1.ObjectFieldSelector{FieldPath: "metadata.name"},
		},
	}, {
		Name: "OPERATOR_POD_NAMESPACE",
		ValueFrom: &corev1.EnvVarSource{
			FieldRef: &corev1.ObjectFieldSelector{FieldPath: "metadata.namespace"},
		},
	}, {
		Name: "OPERATOR_POD_IP",
		ValueFrom: &corev1.EnvVarSource{
			FieldRef: &corev1.ObjectFieldSelector{FieldPath: "status.podIP"},
		},
	}, {
		Name: "OPERATOR_POD_SERVICE_ACCOUNT",
		ValueFrom: &corev1.EnvVarSource{
			FieldRef: &corev1.ObjectFieldSelector{FieldPath: "spec.serviceAccountName"},
		},
	}, {
		Name: "OPERATOR_CONTAINER_CPU_REQUEST",
		ValueFrom: &corev1.EnvVarSource{
			ResourceFieldRef: &corev1.ResourceFieldSelector{
				ContainerName: "clickhouse-operator",
				Resource:      "requests.cpu",
			},
		},
	}, {
		Name: "OPERATOR_CONTAINER_CPU_LIMIT",
		ValueFrom: &corev1.EnvVarSource{
			ResourceFieldRef: &corev1.ResourceFieldSelector{
				ContainerName: "clickhouse-operator",
				Resource:      "limits.cpu",
			},
		},
	}, {
		Name: "OPERATOR_CONTAINER_MEM_REQUEST",
		ValueFrom: &corev1.EnvVarSource{
			ResourceFieldRef: &corev1.ResourceFieldSelector{
				ContainerName: "clickhouse-operator",
				Resource:      "requests.memory",
			},
		},
	}, {
		Name: "OPERATOR_CONTAINER_MEM_LIMIT",
		ValueFrom: &corev1.EnvVarSource{
			ResourceFieldRef: &corev1.ResourceFieldSelector{
				ContainerName: "clickhouse-operator",
				Resource:      "limits.memory",
			},
		},
	}}
}

func getVolumeMounts() []corev1.VolumeMount {
	return []corev1.VolumeMount{{
		Name:      "operator-config",
		MountPath: "/etc/clickhouse-operator",
	}, {
		Name:      "clickhouse-config",
		MountPath: "/etc/clickhouse-operator/config.d",
	}, {
		Name:      "clickhouse-user-config",
		MountPath: "/tmp/",
	}, {
		Name:      "clickhouse-user-dir",
		MountPath: "/etc/clickhouse-operator/users.d",
	}}
}

func getContainers() []corev1.Container {
	env := []corev1.EnvVar{{
		Name: "OPERATOR_PASSWORD",
		ValueFrom: &corev1.EnvVarSource{
			SecretKeyRef: &corev1.SecretKeySelector{
				LocalObjectReference: corev1.LocalObjectReference{
					Name: constants.ClickHouseOperatorCredentialsSecretName,
				},
				Key: "password",
			},
		},
	}}

	return []corev1.Container{{
		Name:  "clickhouse-operator",
		Image: constants.OpstraceImages().ClickHouseOperatorImage,
		Command: []string{
			"/bin/sh",
			"-c",
			fmt.Sprintf("%s && /clickhouse-operator -logtostderr=true -v=1", usersInitEntrypoint),
		},
		Env:             append(env, getOperatorContainerEnv()...),
		Resources:       getResources(),
		VolumeMounts:    getVolumeMounts(),
		ImagePullPolicy: corev1.PullIfNotPresent,
	}, {
		Name:  "exporter",
		Image: constants.OpstraceImages().ClickHouseOperatorExporterImage,
		Command: []string{
			"/bin/sh",
			"-c",
			fmt.Sprintf("%s && /metrics-exporter -logtostderr=true -v=1", usersInitEntrypoint),
		},
		Ports: []corev1.ContainerPort{
			{
				Name:          "metrics",
				ContainerPort: 8888,
			},
		},
		Resources:       getResources(),
		VolumeMounts:    getVolumeMounts(),
		ImagePullPolicy: corev1.PullIfNotPresent,
	}}
}

func getDeploymentSpec(cr *v1alpha1.Cluster, current v1.DeploymentSpec) v1.DeploymentSpec {
	return v1.DeploymentSpec{
		Replicas: &Replicas,
		Selector: &metav1.LabelSelector{
			MatchLabels: GetClickHouseOperatorDeploymentSelector(),
		},
		Template: corev1.PodTemplateSpec{
			ObjectMeta: metav1.ObjectMeta{
				Name:        GetClickHouseOperatorDeploymentName(),
				Labels:      getPodLabels(),
				Annotations: getPodAnnotations(cr, current.Template.Annotations),
			},
			Spec: corev1.PodSpec{
				Volumes:            getVolumes(),
				Containers:         getContainers(),
				ServiceAccountName: GetClickHouseOperatorDeploymentName(),
			},
		},
		Strategy: getDeploymentStrategy(),
	}
}

func Deployment(cr *v1alpha1.Cluster) *v1.Deployment {
	return &v1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:      GetClickHouseOperatorDeploymentName(),
			Namespace: cr.Namespace(),
		},
	}
}

func DeploymentSelector(cr *v1alpha1.Cluster) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace(),
		Name:      GetClickHouseOperatorDeploymentName(),
	}
}

func DeploymentMutator(cr *v1alpha1.Cluster, current *v1.Deployment) error {
	currentSpec := &current.Spec
	spec := getDeploymentSpec(cr, current.Spec)
	// Apply default overrides
	if err := utils.PatchObject(
		currentSpec,
		&spec,
	); err != nil {
		return err
	}
	// Apply CR overrides
	if err := utils.PatchObject(
		currentSpec,
		cr.Spec.Overrides.ClickHouseOperator.Components.Deployment.Spec,
	); err != nil {
		return err
	}
	current.Spec = *currentSpec
	current.Annotations = utils.MergeMap(
		getDeploymentAnnotations(cr, current.Annotations),
		cr.Spec.Overrides.ClickHouseOperator.Components.Deployment.Annotations,
	)
	current.Labels = utils.MergeMap(
		getDeploymentLabels(),
		cr.Spec.Overrides.ClickHouseOperator.Components.Deployment.Labels,
	)

	return nil
}
