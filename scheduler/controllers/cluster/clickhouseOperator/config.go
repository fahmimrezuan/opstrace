package clickhouseOperator

import (
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"

	"github.com/iancoleman/orderedmap"
	"github.com/opstrace/opstrace/go/pkg/constants"
	"github.com/opstrace/opstrace/scheduler/api/v1alpha1"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

var clickhouseUserConfigTemplate string = `<yandex>

<users>
  <clickhouse_operator>
	<networks>
	  <ip>127.0.0.1</ip>
	  <ip>0.0.0.0/0</ip>
	  <ip>::/0</ip>
	</networks>
	<!-- replaced with actual secret SHA on pod startup -->
	<password_sha256_hex>%%OPERATOR_PASSWORD%%</password_sha256_hex>
	<profile>clickhouse_operator</profile>
	<quota>default</quota>
  </clickhouse_operator>
</users>

<profiles>
  <clickhouse_operator>
	<log_queries>0</log_queries>
	<skip_unavailable_shards>1</skip_unavailable_shards>
	<http_connection_timeout>10</http_connection_timeout>
  </clickhouse_operator>

  <default>
	<log_queries>1</log_queries>
	<connect_timeout_with_failover_ms>1000</connect_timeout_with_failover_ms>
	<distributed_aggregation_memory_efficient>1</distributed_aggregation_memory_efficient>
	<parallel_view_processing>1</parallel_view_processing>
	<default_database_engine>Ordinary</default_database_engine>
  </default>
</profiles>

</yandex>`

var clickhouseConfig string = `<yandex>

<!-- Listen wildcard address to allow accepting connections from other containers and host network. -->
<listen_host>::</listen_host>
<listen_host>0.0.0.0</listen_host>

<listen_try>1</listen_try>

<logger>
  <!-- Possible levels: https://github.com/pocoproject/poco/blob/develop/Foundation/include/Poco/Logger.h#L105 -->
  <level>debug</level>
  <log>/var/log/clickhouse-server/clickhouse-server.log</log>
  <errorlog>/var/log/clickhouse-server/clickhouse-server.err.log</errorlog>
  <size>1000M</size>
  <count>10</count>
  <!-- Default behavior is autodetection (log to console if not daemon mode and is tty) -->
  <console>1</console>
</logger>

<prometheus>
  <endpoint>/metrics</endpoint>
  <port>8001</port>
  <metrics>true</metrics>
  <events>true</events>
  <asynchronous_metrics>true</asynchronous_metrics>
</prometheus>

<query_log replace="1">
  <database>system</database>
  <table>query_log</table>
  <engine>Engine = MergeTree PARTITION BY event_date ORDER BY event_time TTL event_date + interval 30 day</engine>
  <flush_interval_milliseconds>7500</flush_interval_milliseconds>
</query_log>

<query_thread_log remove="1"/>

<part_log replace="1">
  <database>system</database>
  <table>part_log</table>
  <engine>Engine = MergeTree PARTITION BY event_date ORDER BY event_time TTL event_date + interval 30 day</engine>
  <flush_interval_milliseconds>7500</flush_interval_milliseconds>
</part_log>

</yandex>`

func getUserConfig() []byte {
	return []byte(clickhouseUserConfigTemplate)
}

func getOperatorConfig(namespace string) ([]byte, error) {
	// Use ordered map to conserve key order for hashing
	c := orderedmap.New()
	// watched namespaces
	c.Set("watchNamespaces", []string{namespace})

	// config paths
	c.Set("chCommonConfigsPath", "config.d")
	//chHostConfigsPath: "conf.d",
	c.Set("chUsersConfigsPath", "users.d")
	//chiTemplatesPath: "templates.d",

	// cluster objects
	c.Set("statefulSetUpdateTimeout", 300)
	c.Set("statefulSetUpdatePollPeriod", 5)
	c.Set("onStatefulSetCreateFailureAction", "ignore")
	c.Set("onStatefulSetUpdateFailureAction", "rollback")

	// user defaults for profiles and quotas
	c.Set("chConfigUserDefaultProfile", "default")
	c.Set("chConfigUserDefaultQuota", "default")
	// allow default access via localhost for debugging purposes
	c.Set("chConfigUserDefaultNetworksIP", []string{"::1", "127.0.0.1"})
	// assume access via replica services at {chi}-0-0 (numbers are replica/shard ids)
	// or via {chi} load balanced service name
	c.Set("chConfigNetworksHostRegexpTemplate", "({chi}-[^.]+\\d+-\\d+|{chi})\\.{namespace}\\.svc\\.cluster\\.local$")

	// operator access namespace and password secret
	c.Set("chCredentialsSecretNamespace", namespace)
	c.Set("chCredentialsSecretName", constants.ClickHouseOperatorCredentialsSecretName)
	c.Set("chPort", 8123)

	// logging
	c.Set("logtostderr", "true")
	c.Set("alsologtostderr", "false")
	c.Set("v", "1")
	c.Set("stderrthreshold", "")
	c.Set("vmodule", "")
	c.Set("log_backtrace_at", "")

	// annotations propagation
	c.Set("excludeFromPropagationAnnotations", []string{})

	// runtime
	c.Set("reconcileThreadsNumber", 10)
	c.Set("reconcileWaitExclude", true)
	c.Set("reconcileWaitInclude", false)

	// labels management
	c.Set("appendScopeLabels", "no")

	// Grace period for Pod termination
	c.Set("terminationGracePeriod", 30)

	return json.Marshal(c)
}

// Default configuration for the ClickHouse operator
func NewClickHouseConfigDefaults(cr *v1alpha1.Cluster) (*orderedmap.OrderedMap, error) {
	c := orderedmap.New()
	operatorConfig, err := getOperatorConfig(cr.Namespace())
	if err != nil {
		return c, err
	}
	userConfig := getUserConfig()
	// Use ordered map to conserve key order for hashing
	c.Set("operator-config.yaml", operatorConfig)
	c.Set("clickhouse-config.xml", []byte(clickhouseConfig))
	c.Set("clickhouse-users.xml.tmpl", userConfig)

	return c, nil
}

func getDefaultsWithOverrides(cr *v1alpha1.Cluster) (*orderedmap.OrderedMap, error) {
	defaults, err := NewClickHouseConfigDefaults(cr)
	// We're not actually overriding anything here yet
	return defaults, err
}

func GetClickHouseOperatorConfigHash(cr *v1alpha1.Cluster) (string, error) {
	c, err := getDefaultsWithOverrides(cr)
	if err != nil {
		return "", err
	}
	h := sha256.New()
	h.Reset()
	size := [sha256.Size]byte{}

	for _, k := range c.Keys() {
		val, _ := c.Get(k)
		h.Write(val.([]byte))
	}
	return hex.EncodeToString(h.Sum(size[:0])), nil
}

func Config(cr *v1alpha1.Cluster) (*v1.ConfigMap, error) {
	configMap := &v1.ConfigMap{}
	configMap.ObjectMeta = metav1.ObjectMeta{
		Name:      GetClickHouseOperatorDeploymentName(),
		Namespace: cr.Namespace(),
	}
	hash, err := GetClickHouseOperatorConfigHash(cr)
	if err != nil {
		return configMap, err
	}
	// Store the hash of the current configuration for later
	// comparisons
	configMap.Annotations = map[string]string{
		constants.LastConfigAnnotation: hash,
	}

	data, err := getData(cr)
	if err != nil {
		return configMap, err
	}

	configMap.Data = data

	return configMap, err
}

func getData(cr *v1alpha1.Cluster) (map[string]string, error) {
	c, err := getDefaultsWithOverrides(cr)
	if err != nil {
		return nil, err
	}
	stringMap := map[string]string{}
	for _, key := range c.Keys() {
		val, _ := c.Get(key)
		stringMap[key] = string(val.([]byte))
	}

	return stringMap, nil
}

func ConfigMutator(cr *v1alpha1.Cluster, current *v1.ConfigMap) error {
	hash, err := GetClickHouseOperatorConfigHash(cr)
	if err != nil {
		return err
	}

	current.Annotations = map[string]string{
		constants.LastConfigAnnotation: hash,
	}
	data, err := getData(cr)
	if err != nil {
		return err
	}
	if data != nil {
		current.Data = data
	}

	return nil
}

func ConfigSelector(cr *v1alpha1.Cluster) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace(),
		Name:      GetClickHouseOperatorDeploymentName(),
	}
}
