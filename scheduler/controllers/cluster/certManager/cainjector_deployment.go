package certManager

import (
	v1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"

	utils "github.com/opstrace/opstrace/go/pkg/common"
	"github.com/opstrace/opstrace/go/pkg/constants"
	"github.com/opstrace/opstrace/scheduler/api/v1alpha1"
)

const (
	CaInjectorMemoryRequest = "128Mi"
	CaInjectorCpuRequest    = "100m"
	CaInjectorMemoryLimit   = "512Mi"
	CaInjectorCpuLimit      = "500m"
)

var CaInjectorReplicas int32 = 1

func GetCainjectorDeploymentName() string {
	return constants.CainjectorName
}

func GetCainjectorDeploymentSelector() map[string]string {
	return map[string]string{
		"app": constants.CainjectorName,
	}
}

func getCainjectorResources() corev1.ResourceRequirements {
	return corev1.ResourceRequirements{
		Requests: corev1.ResourceList{
			corev1.ResourceMemory: resource.MustParse(CaInjectorMemoryRequest),
			corev1.ResourceCPU:    resource.MustParse(CaInjectorCpuRequest),
		},
		Limits: corev1.ResourceList{
			corev1.ResourceMemory: resource.MustParse(CaInjectorMemoryLimit),
			corev1.ResourceCPU:    resource.MustParse(CaInjectorCpuLimit),
		},
	}
}

func getCainjectorDeploymentStrategy() v1.DeploymentStrategy {
	return v1.DeploymentStrategy{
		Type: v1.RecreateDeploymentStrategyType,
	}
}

func getCainjectorDeploymentLabels() map[string]string {
	return map[string]string{
		"app": constants.CainjectorName,
	}
}

func getCainjectorDeploymentAnnotations(cr *v1alpha1.Cluster, existing map[string]string) map[string]string {
	return existing
}

func getCainjectorPodAnnotations(cr *v1alpha1.Cluster, existing map[string]string) map[string]string {
	return existing
}

func getCainjectorPodLabels() map[string]string {
	return map[string]string{
		"app": constants.CainjectorName,
	}
}

func getCainjectorContainerEnv() []corev1.EnvVar {
	return []corev1.EnvVar{
		{
			Name: "POD_NAMESPACE",
			ValueFrom: &corev1.EnvVarSource{
				FieldRef: &corev1.ObjectFieldSelector{FieldPath: "metadata.namespace"},
			},
		},
	}
}

func getCainjectorContainers() []corev1.Container {
	return []corev1.Container{{
		Name:  constants.CainjectorName,
		Image: constants.OpstraceImages().CainjectorImage,
		Args: []string{
			"--v=2",
			"--leader-elect=false",
		},
		Env:             getCainjectorContainerEnv(),
		Resources:       getCainjectorResources(),
		ImagePullPolicy: corev1.PullIfNotPresent,
	}}
}

func getCainjectorDeploymentSpec(cr *v1alpha1.Cluster, current v1.DeploymentSpec) v1.DeploymentSpec {
	return v1.DeploymentSpec{
		Replicas: &CaInjectorReplicas,
		Selector: &metav1.LabelSelector{
			MatchLabels: GetCainjectorDeploymentSelector(),
		},
		Template: corev1.PodTemplateSpec{
			ObjectMeta: metav1.ObjectMeta{
				Name:        GetCainjectorDeploymentName(),
				Labels:      getCainjectorPodLabels(),
				Annotations: getCainjectorPodAnnotations(cr, current.Template.Annotations),
			},
			Spec: corev1.PodSpec{
				Containers:         getCainjectorContainers(),
				ServiceAccountName: GetCainjectorDeploymentName(),
			},
		},
		Strategy: getCainjectorDeploymentStrategy(),
	}
}

func CainjectorDeployment(cr *v1alpha1.Cluster) *v1.Deployment {
	return &v1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:      GetCainjectorDeploymentName(),
			Namespace: cr.Namespace(),
		},
	}
}

func CainjectorDeploymentSelector(cr *v1alpha1.Cluster) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace(),
		Name:      GetCainjectorDeploymentName(),
	}
}

func CainjectorDeploymentMutator(cr *v1alpha1.Cluster, current *v1.Deployment) error {
	currentSpec := &current.Spec
	spec := getCainjectorDeploymentSpec(cr, current.Spec)
	// Apply default overrides
	if err := utils.PatchObject(
		currentSpec,
		&spec,
	); err != nil {
		return err
	}
	// Apply CR overrides
	if err := utils.PatchObject(
		currentSpec,
		cr.Spec.Overrides.Cainjector.Components.Deployment.Spec,
	); err != nil {
		return err
	}
	current.Spec = *currentSpec
	current.Annotations = utils.MergeMap(
		getCainjectorDeploymentAnnotations(cr, current.Annotations),
		cr.Spec.Overrides.Cainjector.Components.Deployment.Annotations,
	)
	current.Labels = utils.MergeMap(
		getCainjectorDeploymentLabels(),
		cr.Spec.Overrides.Cainjector.Components.Deployment.Labels,
	)

	return nil
}
