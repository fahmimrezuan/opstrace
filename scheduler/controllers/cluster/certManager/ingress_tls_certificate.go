package certManager

import (
	certmanager "github.com/cert-manager/cert-manager/pkg/apis/certmanager/v1"
	cmmeta "github.com/cert-manager/cert-manager/pkg/apis/meta/v1"
	"github.com/opstrace/opstrace/go/pkg/constants"
	"github.com/opstrace/opstrace/scheduler/api/v1alpha1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

func Certificate(cr *v1alpha1.Cluster) *certmanager.Certificate {
	return &certmanager.Certificate{
		ObjectMeta: v1.ObjectMeta{
			Name:      constants.HTTPSCertSecretName,
			Namespace: cr.Namespace(),
		},
		Spec: certmanager.CertificateSpec{
			DNSNames: []string{
				cr.Spec.GetHost(),
				// TODO
				"errortracking." + cr.Spec.GetHost(),
			},
			IssuerRef: cmmeta.ObjectReference{
				Kind: "ClusterIssuer",
				Name: cr.Spec.DNS.CertificateIssuer,
			},
			SecretName: constants.HTTPSCertSecretName,
		},
	}
}

func CertificateMutator(cr *v1alpha1.Cluster, current *certmanager.Certificate) {
	cert := Certificate(cr)
	current.Spec.DNSNames = cert.Spec.DNSNames
	current.Spec.IssuerRef = cert.Spec.IssuerRef
	current.Spec.SecretName = cert.Spec.SecretName
}

func CertificateSelector(cr *v1alpha1.Cluster) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace(),
		Name:      constants.HTTPSCertSecretName,
	}
}
