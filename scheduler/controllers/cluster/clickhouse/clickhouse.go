package clickhouse

import (
	"bytes"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"html/template"
	"net/url"

	// Import the fork because original operator uses a symbolic link for it's deepcopy generated code and golang doesn't follow it.
	// Interpreter will complain that clickhouse doesn't implement deepcopy when using it with controller-runtime
	// https://github.com/Altinity/clickhouse-operator/blob/7ec06b79eb70430d546f9a085c92a5339c8f3e4a/pkg/apis/clickhouse.altinity.com/v1/zz_generated.deepcopy.go

	clickhouse "github.com/matapple/clickhouse-operator/pkg/apis/clickhouse.altinity.com/v1"
	"github.com/opstrace/opstrace/go/pkg/common"
	"github.com/opstrace/opstrace/go/pkg/constants"
	"github.com/opstrace/opstrace/scheduler/api/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

const (
	MemoryRequest  = "256Mi"
	CpuRequest     = "100m"
	MemoryLimit    = "8024Mi"
	CpuLimit       = "2"
	DataVolumeSize = "25Gi"
	LogVolumeSize  = "1Gi"
)

var Replicas int32 = 3
var StorageClass = constants.StorageClassName

// port for http access
var httpPort = corev1.ContainerPort{
	Name:          "http",
	ContainerPort: 8123,
}

// port for native client access
var clientPort = corev1.ContainerPort{
	Name:          "client",
	ContainerPort: 9000,
}

// cluster interserver communication port
var interserverPort = corev1.ContainerPort{
	Name:          "interserver",
	ContainerPort: 9009,
}

// keeper zookeeper compatible API port
var keeperPort = corev1.ContainerPort{
	Name:          "keeper",
	ContainerPort: 2181,
}

// raft protocol port
var raftPort = corev1.ContainerPort{
	Name:          "raft",
	ContainerPort: 9444,
}

// metrics port, enabled by default in operator-generated config
var metricsPort = corev1.ContainerPort{
	Name:          "metrics",
	ContainerPort: 8001,
}

func servicePorts(ps []corev1.ContainerPort) []corev1.ServicePort {
	sp := make([]corev1.ServicePort, len(ps))
	for i, port := range ps {
		sp[i] = corev1.ServicePort{
			Name: port.Name,
			Port: port.ContainerPort,
		}
	}
	return sp
}

type RaftServer struct {
	Hostname string
	Port     int32
}

// each raft server needs list of all other servers in the set
// server ids are integer identifiers
// assumes replica service generated name with {chi}-{host} macros
func getRaftServers(cr *v1alpha1.Cluster) []RaftServer {
	raftServers := make([]RaftServer, Replicas)

	for i := range raftServers {
		raftServers[i] = RaftServer{
			Port: raftPort.ContainerPort,
			Hostname: fmt.Sprintf("%s-0-%d.%s.svc.cluster.local",
				constants.ClickHouseClusterServiceName,
				i,
				cr.Namespace(),
			),
		}
	}
	return raftServers
}

// clickhouse keeper config
// clickhouse operator doesn't support creation of these configs at the moment
// config makes use of CLICKHOUSE_REPLICA env var to set the server id for the current server
// reference: https://clickhouse.com/docs/en/operations/clickhouse-keeper/#configuration
const keeperConfigTemplate = `<yandex>
  <keeper_server>
    <tcp_port>{{.keeperPort}}</tcp_port>
    <server_id from_env="CLICKHOUSE_REPLICA"/>
    <log_storage_path>/var/log/clickhouse-server/coordination/log</log_storage_path>
    <snapshot_storage_path>/var/lib/clickhouse/coordination/snapshots</snapshot_storage_path>
    <raft_configuration>
		{{range $i, $server := .raftServers}}
		<server>
			<id>{{$i}}</id>
			<hostname>{{$server.Hostname}}</hostname>
			<port>{{$server.Port}}</port>
		</server>
		{{end}}
    </raft_configuration>
  </keeper_server>
</yandex>`

func getKeeperConfig(cr *v1alpha1.Cluster) (string, error) {
	raftServers := getRaftServers(cr)

	t := template.New("clickhouse-keeper-config")

	t, err := t.Parse(keeperConfigTemplate)
	if err != nil {
		return "", err
	}
	var b bytes.Buffer
	err = t.Execute(&b, map[string]interface{}{
		"keeperPort":  keeperPort.ContainerPort,
		"raftServers": raftServers,
	})
	if err != nil {
		return "", err
	}
	return b.String(), nil
}

func GetClickHouseName(cr *v1alpha1.Cluster) string {
	return constants.ClickHouseClusterServiceName
}

func getResources() corev1.ResourceRequirements {
	return corev1.ResourceRequirements{
		Requests: corev1.ResourceList{
			corev1.ResourceMemory: resource.MustParse(MemoryRequest),
			corev1.ResourceCPU:    resource.MustParse(CpuRequest),
		},
		Limits: corev1.ResourceList{
			corev1.ResourceMemory: resource.MustParse(MemoryLimit),
			corev1.ResourceCPU:    resource.MustParse(CpuLimit),
		},
	}
}

func getReadinessProbe() *corev1.Probe {
	return &corev1.Probe{
		ProbeHandler: corev1.ProbeHandler{
			TCPSocket: &corev1.TCPSocketAction{
				Port: intstr.FromString(raftPort.Name),
			},
		},
		InitialDelaySeconds: 10,
		PeriodSeconds:       3,
	}
}

func getLivenessProbe() *corev1.Probe {
	return &corev1.Probe{
		ProbeHandler: corev1.ProbeHandler{
			HTTPGet: &corev1.HTTPGetAction{
				Path:   "/ping",
				Port:   intstr.FromString(httpPort.Name),
				Scheme: "HTTP",
			},
		},
		InitialDelaySeconds: 60,
		PeriodSeconds:       3,
		FailureThreshold:    10,
	}
}

func getContainers() []corev1.Container {
	env := []corev1.EnvVar{{
		// derive replica number from field, used in keeper config
		Name: "CLICKHOUSE_REPLICA",
		ValueFrom: &corev1.EnvVarSource{
			FieldRef: &corev1.ObjectFieldSelector{
				FieldPath: "metadata.labels['clickhouse.altinity.com/replica']",
			},
		},
	}, {
		// init timeout is used in clickhouse image entrypoint script
		// increase to 60s to give it chance to initialize keeper cluster
		Name:  "CLICKHOUSE_INIT_TIMEOUT",
		Value: "60",
	}}

	var user int64 = 101

	return []corev1.Container{{
		Name:  "clickhouse",
		Image: constants.OpstraceImages().ClickHouseImage,
		Ports: []corev1.ContainerPort{
			httpPort,
			clientPort,
			interserverPort,
			keeperPort,
			raftPort,
			metricsPort,
		},
		Env:             env,
		Resources:       getResources(),
		ReadinessProbe:  getReadinessProbe(),
		LivenessProbe:   getLivenessProbe(),
		ImagePullPolicy: corev1.PullIfNotPresent,
		SecurityContext: &corev1.SecurityContext{
			// Trying to tidy up startup warnings about missing capabilities,
			// but doesn't seem to work according to container logs...
			Capabilities: &corev1.Capabilities{
				Drop: []corev1.Capability{"ALL"},
				Add:  []corev1.Capability{"IPC_LOCK", "SYS_NICE"},
			},
			// Required for startup when we specify securityContext here
			RunAsUser:  &user, // clickhouse uid
			RunAsGroup: &user, // clickhouse gid
		},
	}}
}

func getClickhouseSpec(
	cr *v1alpha1.Cluster, user *url.Userinfo) (clickhouse.ChiSpec, error) {
	username := user.Username()
	password, _ := user.Password()
	settings := clickhouse.NewSettings()
	settings.Set(
		"default_replica_path",
		clickhouse.NewSettingScalar("/clickhouse/{installation}/{cluster}/tables/{shard}/{database}/{table}"),
	)
	settings.Set(
		"default_replica_name",
		clickhouse.NewSettingScalar("{replica}"),
	)

	users := clickhouse.NewSettings()
	users.Set(
		fmt.Sprintf("%s/password", username),
		clickhouse.NewSettingScalar(password),
	)
	users.Set(
		fmt.Sprintf("%s/networks/ip", username),
		clickhouse.NewSettingVector([]string{"::/0"}),
	)
	users.Set(
		fmt.Sprintf("%s/access_management", username),
		clickhouse.NewSettingScalar("1"),
	)

	keeperConfig, err := getKeeperConfig(cr)
	if err != nil {
		return clickhouse.ChiSpec{}, err
	}

	files := clickhouse.NewSettings()
	files.Set(
		"keeper.xml",
		clickhouse.NewSettingScalar(keeperConfig),
	)

	dataVolume, err := resource.ParseQuantity(DataVolumeSize)
	if err != nil {
		return clickhouse.ChiSpec{}, err
	}
	logVolume, err := resource.ParseQuantity(LogVolumeSize)
	if err != nil {
		return clickhouse.ChiSpec{}, err
	}
	r := getRaftServers(cr)
	keeperNodes := make([]clickhouse.ChiZookeeperNode, len(r))
	// Hostname/Host is same for Raft server and KeeperNodes config.
	// Both use the headless service
	for i, s := range r {
		keeperNodes[i] = clickhouse.ChiZookeeperNode{
			Port: keeperPort.ContainerPort,
			Host: s.Hostname,
		}
	}

	return buildCHISpec(
		getClickHouseConfiguration(settings, users, files, keeperNodes), dataVolume, logVolume), nil
}

func buildCHISpec(configuration *clickhouse.Configuration,
	dataVolume, logVolume resource.Quantity) clickhouse.ChiSpec {
	return clickhouse.ChiSpec{
		Configuration: configuration,
		Defaults: &clickhouse.ChiDefaults{
			Templates: &clickhouse.ChiTemplateNames{
				// Replace default PUBLIC LoadBalancer service with an internal ClusterIP service.
				// We don't want ClickHouse accessible to the world!!
				ServiceTemplate: "service-template",
				// service used for accessing members of cluster
				ReplicaServiceTemplate: "replica-template",
				// Configure volumes for storage (/var/lib/clickhouse) and logs (/var/log/clickhouse-server)
				DataVolumeClaimTemplate: "data-volume",
				LogVolumeClaimTemplate:  "logs-volume",
				// Configure image version, and add capabilities to tidy up startup warnings
				PodTemplate: "pod-template",
			},
		},
		Templates: getClickHouseTemplates(dataVolume, logVolume),
	}
}

func getClickHouseConfiguration(settings, users, files *clickhouse.Settings,
	keeperNodes []clickhouse.ChiZookeeperNode) *clickhouse.Configuration {
	return &clickhouse.Configuration{
		// assume one cluster per installation for now until new operator
		Clusters: []*clickhouse.ChiCluster{{
			Name: constants.ClickHouseClusterServiceName,
			Layout: &clickhouse.ChiClusterLayout{
				// assume single shard until we are dealing with significant data storage per node
				ShardsCount:   1,
				ReplicasCount: int(Replicas),
			},
			Templates: &clickhouse.ChiTemplateNames{
				// Replace default PUBLIC LoadBalancer service with an internal ClusterIP service.
				// We don't want ClickHouse accessible to the world!!
				ServiceTemplate: "service-template",
				// service used for accessing members of cluster
				ReplicaServiceTemplate: "replica-template",
				// Configure volumes for storage (/var/lib/clickhouse) and logs (/var/log/clickhouse-server)
				DataVolumeClaimTemplate: "data-volume",
				LogVolumeClaimTemplate:  "logs-volume",
				// Configure image version, and add capabilities to tidy up startup warnings
				PodTemplate: "pod-template",
			},
		}},
		// add ClickHouse settings here, slash-delimited
		// reference: https://clickhouse.com/docs/en/operations/server-configuration-parameters/settings/
		Settings: settings,
		Users:    users,
		Files:    files,
		Zookeeper: &clickhouse.ChiZookeeperConfig{
			Nodes: keeperNodes,
		},
	}
}

func getClickHouseTemplates(dataVolume, logVolume resource.Quantity) *clickhouse.ChiTemplates {
	var fsuser int64 = 101
	return &clickhouse.ChiTemplates{
		// reference: https://github.com/Altinity/clickhouse-operator/blob/master/docs/custom_resource_explained.md#spectemplatespodtemplates
		PodTemplates: []clickhouse.ChiPodTemplate{
			{
				Name: "pod-template",
				// generateName is used by StatefulSet and PodSpec naming
				GenerateName: "{chi}-{host}",
				Spec: corev1.PodSpec{
					SecurityContext: &corev1.PodSecurityContext{
						// Required for startup when we specify securityContext in container
						FSGroup: &fsuser, // clickhouse gid
					},
					Containers: getContainers(),
				},
			},
		},
		ServiceTemplates: []clickhouse.ChiServiceTemplate{
			{
				GenerateName: "{chi}",
				Name:         "service-template",
				Spec: corev1.ServiceSpec{
					// Must be provided manually or else deployment fails
					// Cluster level service just needs http and client ports
					Ports: servicePorts([]corev1.ContainerPort{httpPort, clientPort}),
					// Default is a PUBLIC LoadBalancer, we don't want that!
					Type: "ClusterIP",
				},
			},
			// replica specific service used for interserver/keeper replication/sharding
			{
				GenerateName: "{chi}-{host}",
				Name:         "replica-template",
				Spec: corev1.ServiceSpec{
					// Must be provided manually or else deployment fails
					// Cluster level service just needs http and client ports
					Ports: servicePorts([]corev1.ContainerPort{
						httpPort,
						clientPort,
						interserverPort,
						keeperPort,
						raftPort,
						metricsPort,
					}),
					// headless StatefulSet service
					ClusterIP: "None",
				},
			},
		},
		VolumeClaimTemplates: []clickhouse.ChiVolumeClaimTemplate{
			{
				Name: "data-volume",
				Spec: corev1.PersistentVolumeClaimSpec{
					AccessModes: []corev1.PersistentVolumeAccessMode{"ReadWriteOnce"},
					Resources: corev1.ResourceRequirements{
						Requests: corev1.ResourceList{
							corev1.ResourceStorage: dataVolume, // TODO(nickbp): Configurable data volume size
						},
					},
					StorageClassName: &StorageClass,
				},
			},
			{
				Name: "logs-volume",
				Spec: corev1.PersistentVolumeClaimSpec{
					AccessModes: []corev1.PersistentVolumeAccessMode{"ReadWriteOnce"},
					Resources: corev1.ResourceRequirements{
						Requests: corev1.ResourceList{
							corev1.ResourceStorage: logVolume, // TODO(nickbp): Configurable data volume size
						},
					},
					StorageClassName: &StorageClass,
				},
			},
		},
	}
}

// Returns the sha256 checksum for the ClickHouse definition
func GetClickHouseHash(j *clickhouse.ClickHouseInstallation) (string, error) {
	if j.Annotations == nil {
		j.Annotations = map[string]string{}
	}
	c := j.Annotations[constants.LastConfigAnnotation]
	// remove hash annotation so it's not recursively including the hash in the hash
	j.Annotations[constants.LastConfigAnnotation] = ""
	data, err := json.Marshal(j)
	if err != nil {
		return "", err
	}
	sha := sha256.Sum256(data)

	j.Annotations[constants.LastConfigAnnotation] = c
	return hex.EncodeToString(sha[:28]), nil
}

// Performs a deepequal by comparing the checksum instead of using the
// reflect package in controllerutil.createOrUpdate. clickhouse.ClickHouseInstallation has
// unexported fields that cause a panic
func DeepEqual(a, b *clickhouse.ClickHouseInstallation) (bool, error) {
	// remove hash annotation so it's not recursively including the hash in the hash
	aHash, err := GetClickHouseHash(a)
	if err != nil {
		return false, err
	}
	bHash, err := GetClickHouseHash(b)
	if err != nil {
		return false, err
	}
	return aHash == bHash, nil
}

func ClickHouse(cr *v1alpha1.Cluster, user *url.Userinfo) *clickhouse.ClickHouseInstallation {
	spec, err := getClickhouseSpec(cr, user)
	if err != nil {
		return nil
	}
	return &clickhouse.ClickHouseInstallation{
		ObjectMeta: metav1.ObjectMeta{
			Name:      GetClickHouseName(cr),
			Namespace: cr.Namespace(),
		},
		Spec: spec,
	}
}

func ClickHouseSelector(cr *v1alpha1.Cluster) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace(),
		Name:      GetClickHouseName(cr),
	}
}

func ClickHouseMutator(cr *v1alpha1.Cluster, current *clickhouse.ClickHouseInstallation, user *url.Userinfo) error {
	currentSpec := &current.Spec
	spec, err := getClickhouseSpec(cr, user)
	if err != nil {
		return err
	}
	// Apply default overrides
	if err := common.PatchObject(
		currentSpec,
		&spec,
	); err != nil {
		return err
	}
	overrides := cr.Spec.Overrides.ClickHouse.Components.Deployment.Spec
	if overrides != nil {
		// Turn currentSpec into bytes and merge with overrides (already in bytes)
		c, err := json.Marshal(currentSpec)
		if err != nil {
			return err
		}
		// Apply CR overrides
		patched, err := common.PatchBytes(
			c,
			*overrides,
		)
		if err != nil {
			return err
		}
		// Unmarshall into original spec
		if err := json.Unmarshal(patched, currentSpec); err != nil {
			return err
		}
	}

	current.Spec = *currentSpec
	current.Annotations = common.MergeMap(
		map[string]string{},
		current.Annotations,
	)
	current.Annotations = common.MergeMap(
		current.Annotations,
		cr.Spec.Overrides.ClickHouse.Components.Deployment.Annotations,
	)
	hash, err := GetClickHouseHash(current)
	if err != nil {
		return err
	}
	current.Annotations[constants.LastConfigAnnotation] = hash
	return nil
}
