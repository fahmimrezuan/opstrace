package kubestatemetrics

import (
	"github.com/opstrace/opstrace/go/pkg/constants"
	"github.com/opstrace/opstrace/scheduler/api/v1alpha1"
	monitors "github.com/opstrace/opstrace/scheduler/controllers/cluster/monitoring"
	v1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func Deployment(cr *v1alpha1.Cluster) *v1.Deployment {
	return &v1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:      getDeploymentName(),
			Namespace: cr.Namespace(),
			Labels:    getDeploymentLabels(),
		},
		Spec: getDeploymentSpec(),
	}
}

func DeploymentMutator(cr *v1alpha1.Cluster, current *v1.Deployment) error {
	return nil
}

func getDeploymentName() string {
	return "kube-state-metrics"
}

func getDeploymentLabels() map[string]string {
	return map[string]string{
		"app": monitors.KubeStateMetrics,
	}
}

var (
	replicas     int32 = 1
	runAsNonRoot bool  = true
	runAsUser    int64 = 65534
)

func getDeploymentSpec() v1.DeploymentSpec {
	return v1.DeploymentSpec{
		Replicas: &replicas,
		Selector: &metav1.LabelSelector{
			MatchLabels: getDeploymentLabels(),
		},
		Template: corev1.PodTemplateSpec{
			ObjectMeta: metav1.ObjectMeta{
				Labels: getDeploymentLabels(),
			},
			Spec: corev1.PodSpec{
				Containers: getDeploymentContainers(),
				NodeSelector: map[string]string{
					"kubernetes.io/os": "linux",
				},
				SecurityContext: &corev1.PodSecurityContext{
					RunAsNonRoot: &runAsNonRoot,
					RunAsUser:    &runAsUser,
				},
				ServiceAccountName: monitors.KubeStateMetrics,
			},
		},
	}
}

//nolint:funlen
func getDeploymentContainers() []corev1.Container {
	return []corev1.Container{
		{
			Name:  "kube-rbac-proxy-main",
			Image: constants.OpstraceImages().KubeRBACProxyImage,
			Ports: []corev1.ContainerPort{
				{
					Name:          "https-main",
					ContainerPort: 8443,
				},
			},
			Resources: corev1.ResourceRequirements{
				Requests: corev1.ResourceList{
					corev1.ResourceCPU:    resource.MustParse("50m"),
					corev1.ResourceMemory: resource.MustParse("20Mi"),
				},
				Limits: corev1.ResourceList{
					corev1.ResourceCPU:    resource.MustParse("1000m"),
					corev1.ResourceMemory: resource.MustParse("40Mi"),
				},
			},
			Args: []string{
				"--logtostderr",
				"--secure-listen-address=:8443",
				"--tls-cipher-suites=TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,TLS_RSA_WITH_AES_128_CBC_SHA256,TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256,TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256",
				"--upstream=http://127.0.0.1:8081/",
			},
		},
		{
			Name:  "kube-rbac-proxy-self",
			Image: constants.OpstraceImages().KubeRBACProxyImage,
			Ports: []corev1.ContainerPort{
				{
					Name:          "https-self",
					ContainerPort: 9443,
				},
			},
			Resources: corev1.ResourceRequirements{
				Requests: corev1.ResourceList{
					corev1.ResourceCPU:    resource.MustParse("50m"),
					corev1.ResourceMemory: resource.MustParse("20Mi"),
				},
				Limits: corev1.ResourceList{
					corev1.ResourceCPU:    resource.MustParse("500m"),
					corev1.ResourceMemory: resource.MustParse("40Mi"),
				},
			},
			Args: []string{
				"--logtostderr",
				"--secure-listen-address=:9443",
				"--tls-cipher-suites=TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,TLS_RSA_WITH_AES_128_CBC_SHA256,TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256,TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256",
				"--upstream=http://127.0.0.1:8082/",
			},
		},
		{
			Name:  "kube-state-metrics",
			Image: constants.OpstraceImages().KubeStateMetricsImage,
			Resources: corev1.ResourceRequirements{
				Requests: corev1.ResourceList{
					corev1.ResourceCPU:    resource.MustParse("50m"),
					corev1.ResourceMemory: resource.MustParse("100Mi"),
				},
				Limits: corev1.ResourceList{
					corev1.ResourceCPU:    resource.MustParse("200m"),
					corev1.ResourceMemory: resource.MustParse("150Mi"),
				},
			},
			Args: []string{
				"--host=127.0.0.1",
				"--port=8081",
				"--telemetry-host=127.0.0.1",
				"--telemetry-port=8082",
			},
		},
		{
			Name:  "addon-resizer",
			Image: constants.OpstraceImages().AddonResizerImage,
			Resources: corev1.ResourceRequirements{
				Requests: corev1.ResourceList{
					corev1.ResourceCPU:    resource.MustParse("50m"),
					corev1.ResourceMemory: resource.MustParse("30Mi"),
				},
				Limits: corev1.ResourceList{
					corev1.ResourceCPU:    resource.MustParse("250m"),
					corev1.ResourceMemory: resource.MustParse("30Mi"),
				},
			},
			Env: []corev1.EnvVar{
				{
					Name: "MY_POD_NAME",
					ValueFrom: &corev1.EnvVarSource{
						FieldRef: &corev1.ObjectFieldSelector{
							APIVersion: "v1",
							FieldPath:  "metadata.name",
						},
					},
				},
				{
					Name: "MY_POD_NAMESPACE",
					ValueFrom: &corev1.EnvVarSource{
						FieldRef: &corev1.ObjectFieldSelector{
							APIVersion: "v1",
							FieldPath:  "metadata.namespace",
						},
					},
				},
			},
			Command: []string{
				"/pod_nanny",
				"--container=kube-state-metrics",
				"--cpu=200m",
				"--extra-cpu=2m",
				"--memory=150Mi",
				"--extra-memory=30Mi",
				"--threshold=5",
				"--deployment=kube-state-metrics",
			},
		},
	}
}
