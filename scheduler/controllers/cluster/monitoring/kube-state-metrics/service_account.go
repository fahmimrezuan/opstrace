package kubestatemetrics

import (
	"github.com/opstrace/opstrace/scheduler/api/v1alpha1"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

func ServiceAccount(cr *v1alpha1.Cluster) *v1.ServiceAccount {
	return &v1.ServiceAccount{
		ObjectMeta: metav1.ObjectMeta{
			Name:        getServiceAccountName(),
			Namespace:   cr.Namespace(),
			Labels:      getServiceAccountLabels(),
			Annotations: getServiceAccountAnnotations(),
		},
	}
}

func ServiceAccountSelector(cr *v1alpha1.Cluster) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace(),
		Name:      getServiceAccountName(),
	}
}

//nolint:unparam
func ServiceAccountMutator(cr *v1alpha1.Cluster, current *v1.ServiceAccount) {
	current.Labels = getServiceAccountLabels()
	current.Annotations = getServiceAccountAnnotations()
}

func getServiceAccountName() string {
	return "kube-state-metrics"
}

func getServiceAccountLabels() map[string]string {
	return map[string]string{}
}

func getServiceAccountAnnotations() map[string]string {
	return map[string]string{}
}
