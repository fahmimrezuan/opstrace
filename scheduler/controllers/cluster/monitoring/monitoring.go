package monitoring

const (
	APIServer             string = "apiserver"
	Kubernetes            string = "kubernetes"
	CoreDNS               string = "coredns"
	KubeControllerManager string = "kube-controller-manager"
	KubeStateMetrics      string = "kube-state-metrics"
	Kubelet               string = "kubelet"
	KubeScheduler         string = "kube-scheduler"
	NodeExporter          string = "node-exporter"
	Node                  string = "node"
	Prometheus            string = "prometheus"
)
