package nginxIngress

import (
	"fmt"

	utils "github.com/opstrace/opstrace/go/pkg/common"
	"github.com/opstrace/opstrace/scheduler/api/v1alpha1"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

func getMetricsServiceName() string {
	return fmt.Sprintf("%s-metrics", GetNginxIngressDeploymentName())
}

func getMetricsServiceLabelsSelector() map[string]string {
	return map[string]string{"service": "nginx-ingress-metrics"}
}

func getMetricsServiceLabels() map[string]string {
	labels := GetNginxIngressDeploymentSelector()
	// Add additional label to differentiate this service for the ServiceMonitor
	labels["service"] = "nginx-ingress-metrics"

	return labels
}

func getMetricsServiceAnnotations(cr *v1alpha1.Cluster, existing map[string]string) map[string]string {
	return existing
}

func getMetricsServicePorts() []v1.ServicePort {
	return []v1.ServicePort{
		{
			Name: "metrics",
			Port: 10254,
		},
	}
}

func getMetricsServiceSpec() v1.ServiceSpec {
	return v1.ServiceSpec{
		Ports:    getMetricsServicePorts(),
		Selector: GetNginxIngressDeploymentSelector(),
		Type:     v1.ServiceTypeClusterIP,
	}
}

func MetricsService(cr *v1alpha1.Cluster) *v1.Service {
	return &v1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name:        getMetricsServiceName(),
			Namespace:   cr.Namespace(),
			Labels:      getMetricsServiceLabels(),
			Annotations: getMetricsServiceAnnotations(cr, nil),
		},
		Spec: getMetricsServiceSpec(),
	}
}

func MetricsServiceMutator(cr *v1alpha1.Cluster, current *v1.Service) error {
	current.Name = getMetricsServiceName()
	currentSpec := current.Spec.DeepCopy()
	spec := getMetricsServiceSpec()
	// Apply default overrides
	if err := utils.PatchObject(
		currentSpec,
		&spec,
	); err != nil {
		return err
	}
	// We don't bother providing any CR overrides on this since it's
	// a simple internal component
	current.Spec = *currentSpec
	current.Annotations = getMetricsServiceAnnotations(cr, current.Annotations)
	current.Labels = getMetricsServiceLabels()

	return nil
}

func MetricsServiceSelector(cr *v1alpha1.Cluster) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace(),
		Name:      getMetricsServiceName(),
	}
}
