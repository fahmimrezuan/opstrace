package nginxIngress

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"

	"github.com/iancoleman/orderedmap"
	"github.com/opstrace/opstrace/go/pkg/constants"
	"github.com/opstrace/opstrace/scheduler/api/v1alpha1"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

func getConfig() *orderedmap.OrderedMap {
	// use orderedmap to keep the key ordering consistent for consistent hashing
	c := orderedmap.New()

	c.Set("keep-alive", "3700") // This is 100 seconds longer than the Loadbalancer

	return c
}

func GetNginxControllerConfigHash() string {
	c := getConfig()

	h := sha256.New()
	h.Reset()
	size := [sha256.Size]byte{}

	for _, k := range c.Keys() {
		val, _ := c.Get(k)
		h.Write([]byte(fmt.Sprintf("%v", val)))
	}
	return hex.EncodeToString(h.Sum(size[:0]))
}

func getData() map[string]string {
	c := getConfig()

	stringMap := map[string]string{}
	for _, key := range c.Keys() {
		val, _ := c.Get(key)
		stringMap[key] = fmt.Sprintf("%v", val)
	}

	return stringMap
}

func Config(cr *v1alpha1.Cluster) *v1.ConfigMap {
	configMap := &v1.ConfigMap{}
	configMap.ObjectMeta = metav1.ObjectMeta{
		Name:      GetNginxIngressDeploymentName(),
		Namespace: cr.Namespace(),
	}
	hash := GetNginxControllerConfigHash()

	// Store the hash of the current configuration for later
	// comparisons
	configMap.Annotations = map[string]string{
		constants.LastConfigAnnotation: hash,
	}
	configMap.Data = getData()

	return configMap
}

func ConfigMutator(current *v1.ConfigMap) {
	hash := GetNginxControllerConfigHash()
	current.Annotations = map[string]string{
		constants.LastConfigAnnotation: hash,
	}
	current.Data = getData()
}

func ConfigSelector(cr *v1alpha1.Cluster) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace(),
		Name:      GetNginxIngressDeploymentName(),
	}
}
