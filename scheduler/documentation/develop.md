# Develop

## Read the Makefile

We strongly recommend that you read through the Makefile,
we are heavily relying on make commands to help you getting started quicker.
## Local deployment using go

```shell
# Create a Kind cluster (uses kind.yaml for configuration)
make kind
# Install CRDs into Kind cluster
make install
# Create the Opstrace cluster CR for the operator to reconcile. This will also
# deploy an HA Postgres deployment in the postgres namespace for local development
make deploy-without-manager
# Build the operator
make build
# Run the operator locally, configured to watch resources in the default namespace
# Note that resources have already been added by make deploy-without-manager
# so the operator should have work to do on startup
make run
```

You can of course run the deployment using a debugger or similar tools.

## Local deployment using docker

If you want a solution closer to reality you can instead build your own containers.
We will go through two sections.

- Using kind load docker-image.
- Using your own quay repo to upload a manually built image.

### Kind load docker-image

This solution assumes that you are using kind in your development environment.

```shell
# Create a Kind cluster with ingress controller installed
make kind
make install
make docker-build
```

We will pre-load the container image to kind. To make sure that we only use our locally built container, edit the kustomize file to never pull the image from a external source.

Remember to not commit these changes.

```shell
cat <<EOF >> config/manager/kustomization.yaml

patchesJson6902:
  - target:
      version: v1
      kind: Deployment
      name: scheduler-controller-manager
    patch: |-
      - op: add
        path: /spec/template/spec/containers/0/imagePullPolicy
        value: Never
EOF
```

Load the image in to kind and start the deployment.

```shell
make kind-load
make deploy
```

### Remote repo

If you want to build and upload your container image to your own remote repo you can follow these instructions.

```shell
# Create a Kind cluster with ingress controller installed
make kind
make install
# Login to remote repo
export DOCKER_USER=username1
# This is one way of many on how to login using docker, perform the one that works for you.
docker login -u $DOCKER_USER

# If you don't want to add the IMG= all the time you can also edit the IMG variable in the Makefile
make docker-build IMG=$DOCKER_USER/scheduler:$DOCKER_IMAGE_TAG
make docker-push IMG=$DOCKER_USER/scheduler:$DOCKER_IMAGE_TAG
make deploy IMG=$DOCKER_USER/scheduler:$DOCKER_IMAGE_TAG
```
