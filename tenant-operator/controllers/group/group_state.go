package group

import (
	"context"

	"github.com/opstrace/opstrace/tenant-operator/api/v1alpha1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

type GroupState struct {
	Tracing TracingState
}

func NewGroupState() *GroupState {
	return &GroupState{}
}

func (i *GroupState) Read(ctx context.Context, cr *v1alpha1.Group, client client.Client) error {
	err := i.readTracingState(ctx, cr, client)

	return err
}

func (i *GroupState) readTracingState(ctx context.Context, cr *v1alpha1.Group, client client.Client) error {
	tracingState := NewTracingState()
	if err := tracingState.Read(ctx, cr, client); err != nil {
		return err
	}

	i.Tracing = *tracingState
	return nil
}
