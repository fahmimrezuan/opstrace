package argus

import (
	v1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
	"sigs.k8s.io/controller-runtime/pkg/client"

	utils "github.com/opstrace/opstrace/go/pkg/common"
	"github.com/opstrace/opstrace/go/pkg/constants"
	"github.com/opstrace/opstrace/tenant-operator/api/v1alpha1"
)

const (
	InitMemoryRequest = "128Mi"
	InitCpuRequest    = "250m"
	InitMemoryLimit   = "512Mi"
	InitCpuLimit      = "1000m"
	MemoryRequest     = "256Mi"
	CpuRequest        = "100m"
	MemoryLimit       = "1024Mi"
	CpuLimit          = "500m"
)

var Replicas int32 = 2

func getInitResources() corev1.ResourceRequirements {
	return corev1.ResourceRequirements{
		Requests: corev1.ResourceList{
			corev1.ResourceMemory: resource.MustParse(InitMemoryRequest),
			corev1.ResourceCPU:    resource.MustParse(InitCpuRequest),
		},
		Limits: corev1.ResourceList{
			corev1.ResourceMemory: resource.MustParse(InitMemoryLimit),
			corev1.ResourceCPU:    resource.MustParse(InitCpuLimit),
		},
	}
}

func getResources() corev1.ResourceRequirements {
	return corev1.ResourceRequirements{
		Requests: corev1.ResourceList{
			corev1.ResourceMemory: resource.MustParse(MemoryRequest),
			corev1.ResourceCPU:    resource.MustParse(CpuRequest),
		},
		Limits: corev1.ResourceList{
			corev1.ResourceMemory: resource.MustParse(MemoryLimit),
			corev1.ResourceCPU:    resource.MustParse(CpuLimit),
		},
	}
}

func getDeploymentStrategy() v1.DeploymentStrategy {
	// Keep maxUnaval at 0 to prevent early kill off of pod during rolling
	// update when Replicas=1
	var maxUnaval intstr.IntOrString = intstr.FromInt(0)
	var maxSurge intstr.IntOrString = intstr.FromInt(25)
	return v1.DeploymentStrategy{
		Type: "RollingUpdate",
		RollingUpdate: &v1.RollingUpdateDeployment{
			MaxUnavailable: &maxUnaval,
			MaxSurge:       &maxSurge,
		},
	}
}

func getPodLabels() map[string]string {
	return map[string]string{
		"app": constants.ArgusPodLabel,
	}
}

func getVolumes() []corev1.Volume {
	var volumes []corev1.Volume

	volumes = append(volumes, corev1.Volume{
		Name: constants.ArgusProvisionPluginVolumeName,
		VolumeSource: corev1.VolumeSource{
			EmptyDir: &corev1.EmptyDirVolumeSource{},
		},
	})

	volumes = append(volumes, corev1.Volume{
		Name: constants.ArgusProvisionDashboardVolumeName,
		VolumeSource: corev1.VolumeSource{
			EmptyDir: &corev1.EmptyDirVolumeSource{},
		},
	})

	volumes = append(volumes, corev1.Volume{
		Name: constants.ArgusProvisionNotifierVolumeName,
		VolumeSource: corev1.VolumeSource{
			EmptyDir: &corev1.EmptyDirVolumeSource{},
		},
	})

	// Volume to mount the config file from a config map
	volumes = append(volumes, corev1.Volume{
		Name: constants.ArgusConfigName,
		VolumeSource: corev1.VolumeSource{
			ConfigMap: &corev1.ConfigMapVolumeSource{
				LocalObjectReference: corev1.LocalObjectReference{
					Name: constants.ArgusConfigName,
				},
			},
		},
	})

	// Volume to store the logs
	volumes = append(volumes, corev1.Volume{
		Name: constants.ArgusLogsVolumeName,
		VolumeSource: corev1.VolumeSource{
			EmptyDir: &corev1.EmptyDirVolumeSource{},
		},
	})

	volumes = append(volumes, corev1.Volume{
		Name: constants.ArgusDataVolumeName,
		VolumeSource: corev1.VolumeSource{
			EmptyDir: &corev1.EmptyDirVolumeSource{},
		},
	})

	volumes = append(volumes, corev1.Volume{
		Name: constants.ArgusPluginsVolumeName,
		VolumeSource: corev1.VolumeSource{
			EmptyDir: &corev1.EmptyDirVolumeSource{},
		},
	})

	// Volume to store the datasources
	volumes = append(volumes, corev1.Volume{
		Name: constants.DatasourcesConfigMapName,
		VolumeSource: corev1.VolumeSource{
			ConfigMap: &corev1.ConfigMapVolumeSource{
				LocalObjectReference: corev1.LocalObjectReference{
					Name: constants.DatasourcesConfigMapName,
				},
			},
		},
	})

	return volumes
}

func getVolumeMounts() []corev1.VolumeMount {
	var mounts []corev1.VolumeMount

	mounts = append(mounts, corev1.VolumeMount{
		Name:      constants.ArgusConfigName,
		MountPath: "/etc/argus/",
	})

	mounts = append(mounts, corev1.VolumeMount{
		Name:      constants.ArgusDataVolumeName,
		MountPath: constants.ArgusDataPath,
	})

	mounts = append(mounts, corev1.VolumeMount{
		Name:      constants.ArgusPluginsVolumeName,
		MountPath: constants.ArgusPluginsPath,
	})

	mounts = append(mounts, corev1.VolumeMount{
		Name:      constants.ArgusProvisionPluginVolumeName,
		MountPath: constants.ArgusProvisioningPluginsPath,
	})

	mounts = append(mounts, corev1.VolumeMount{
		Name:      constants.ArgusProvisionDashboardVolumeName,
		MountPath: constants.ArgusProvisioningDashboardsPath,
	})

	mounts = append(mounts, corev1.VolumeMount{
		Name:      constants.ArgusProvisionNotifierVolumeName,
		MountPath: constants.ArgusProvisioningNotifiersPath,
	})

	mounts = append(mounts, corev1.VolumeMount{
		Name:      constants.ArgusLogsVolumeName,
		MountPath: constants.ArgusLogsPath,
	})

	mounts = append(mounts, corev1.VolumeMount{
		Name:      constants.DatasourcesConfigMapName,
		MountPath: "/etc/argus/provisioning/datasources",
	})

	return mounts
}

func getContainers(cr *v1alpha1.Tenant, configHash, dsHash string) []corev1.Container {
	return []corev1.Container{{
		Name:  "argus",
		Image: constants.OpstraceImages().ArgusImage,
		Args:  []string{"-config=/etc/argus/grafana.ini"},
		Ports: []corev1.ContainerPort{
			{
				Name:          "argus-http",
				ContainerPort: int32(GetArgusPort(cr)),
				Protocol:      "TCP",
			},
		},
		Env:          getEnv(configHash, dsHash),
		Resources:    getResources(),
		VolumeMounts: getVolumeMounts(),
		LivenessProbe: &corev1.Probe{
			ProbeHandler: corev1.ProbeHandler{
				HTTPGet: &corev1.HTTPGetAction{
					Path:   constants.ArgusHealthEndpoint,
					Port:   intstr.FromInt(GetArgusPort(cr)),
					Scheme: corev1.URISchemeHTTP,
				},
			},
			InitialDelaySeconds: 60,
			TimeoutSeconds:      30,
			PeriodSeconds:       10,
			SuccessThreshold:    1,
			FailureThreshold:    10,
		},
		ReadinessProbe: &corev1.Probe{
			ProbeHandler: corev1.ProbeHandler{
				HTTPGet: &corev1.HTTPGetAction{
					Path:   constants.ArgusHealthEndpoint,
					Port:   intstr.FromInt(GetArgusPort(cr)),
					Scheme: corev1.URISchemeHTTP,
				},
			},
			InitialDelaySeconds: 5,
			TimeoutSeconds:      3,
			PeriodSeconds:       10,
			SuccessThreshold:    1,
			FailureThreshold:    1,
		},
		TerminationMessagePath:   "/dev/termination-log",
		TerminationMessagePolicy: "File",
		ImagePullPolicy:          "IfNotPresent",
	}}
}

func getEnv(configHash, dsHash string) []corev1.EnvVar {
	return []corev1.EnvVar{
		{
			Name:  constants.LastConfigEnvVar,
			Value: configHash,
		},
		{
			Name:  constants.LastDatasourcesConfigEnvVar,
			Value: dsHash,
		},
		{
			Name: constants.ArgusAdminUserEnvVar,
			ValueFrom: &corev1.EnvVarSource{
				SecretKeyRef: &corev1.SecretKeySelector{
					LocalObjectReference: corev1.LocalObjectReference{
						Name: constants.ArgusAdminSecretName,
					},
					Key: constants.ArgusAdminUserEnvVar,
				},
			},
		},
		{
			Name: constants.ArgusAdminPasswordEnvVar,
			ValueFrom: &corev1.EnvVarSource{
				SecretKeyRef: &corev1.SecretKeySelector{
					LocalObjectReference: corev1.LocalObjectReference{
						Name: constants.ArgusAdminSecretName,
					},
					Key: constants.ArgusAdminPasswordEnvVar,
				},
			},
		},
		{
			Name: constants.ArgusPostgresURLKey,
			ValueFrom: &corev1.EnvVarSource{
				SecretKeyRef: &corev1.SecretKeySelector{
					LocalObjectReference: corev1.LocalObjectReference{
						Name: constants.ArgusPostgresSecretName,
					},
					Key: constants.ArgusPostgresURLKey,
				},
			},
		},
	}
}

func getInitContainers(plugins string) []corev1.Container {
	return []corev1.Container{
		{
			Name:  constants.ArgusInitContainerName,
			Image: constants.OpstraceImages().ArgusPluginsInitContainerImage,
			Env: []corev1.EnvVar{
				{
					Name:  "GRAFANA_PLUGINS",
					Value: plugins,
				},
			},
			Resources: getInitResources(),
			VolumeMounts: []corev1.VolumeMount{
				{
					Name:      constants.ArgusPluginsVolumeName,
					ReadOnly:  false,
					MountPath: "/opt/plugins",
				},
			},
			TerminationMessagePath:   "/dev/termination-log",
			TerminationMessagePolicy: "File",
			ImagePullPolicy:          "IfNotPresent",
		},
	}
}

func getDeploymentLabels() map[string]string {
	return map[string]string{
		"app": constants.ArgusPodLabel,
	}
}

func getDeploymentAnnotations(cr *v1alpha1.Tenant, existing map[string]string) map[string]string {
	return existing
}

func getDeploymentSpec(cr *v1alpha1.Tenant, configHash, plugins, dsHash string, current v1.DeploymentSpec) v1.DeploymentSpec {
	return v1.DeploymentSpec{
		Replicas: &Replicas,
		Selector: &metav1.LabelSelector{
			MatchLabels: getPodLabels(),
		},
		Template: corev1.PodTemplateSpec{
			ObjectMeta: metav1.ObjectMeta{
				Name:        constants.ArgusDeploymentName,
				Labels:      getPodLabels(),
				Annotations: getDeploymentAnnotations(cr, current.Template.Annotations),
			},
			Spec: corev1.PodSpec{
				Volumes:            getVolumes(),
				InitContainers:     getInitContainers(plugins),
				Containers:         getContainers(cr, configHash, dsHash),
				ServiceAccountName: constants.ArgusServiceAccountName,
			},
		},
		Strategy: getDeploymentStrategy(),
	}
}

func Deployment(cr *v1alpha1.Tenant) *v1.Deployment {
	return &v1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:      constants.ArgusDeploymentName,
			Namespace: cr.Namespace,
		},
	}
}

func DeploymentSelector(cr *v1alpha1.Tenant) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace,
		Name:      constants.ArgusDeploymentName,
	}
}

func DeploymentMutator(cr *v1alpha1.Tenant, current *v1.Deployment, configHash, plugins, dshash string) error {
	currentSpec := &current.Spec
	spec := getDeploymentSpec(
		cr,
		configHash,
		plugins,
		dshash,
		current.Spec)
	// Apply default overrides
	if err := utils.PatchObject(
		currentSpec,
		&spec,
	); err != nil {
		return err
	}
	// Apply CR overrides
	if err := utils.PatchObject(
		currentSpec,
		cr.Spec.Overrides.Argus.Components.Deployment.Spec,
	); err != nil {
		return err
	}
	current.Spec = *currentSpec
	current.Annotations = utils.MergeMap(
		getDeploymentAnnotations(cr, current.Annotations),
		cr.Spec.Overrides.Argus.Components.Deployment.Annotations,
	)
	current.Labels = utils.MergeMap(
		getDeploymentLabels(),
		cr.Spec.Overrides.Argus.Components.Deployment.Labels,
	)

	return nil
}
