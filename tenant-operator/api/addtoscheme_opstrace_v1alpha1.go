package api

import (
	jaeger "github.com/jaegertracing/jaeger-operator/apis/v1"
	"github.com/opstrace/opstrace/tenant-operator/api/v1alpha1"
	monitoring "github.com/prometheus-operator/prometheus-operator/pkg/apis/monitoring/v1"
)

func init() {
	// Register the types with the Scheme so the components can map objects to GroupVersionKinds and back
	AddToSchemes = append(AddToSchemes,
		v1alpha1.SchemeBuilder.AddToScheme,
		monitoring.AddToScheme,
		jaeger.AddToScheme,
	)
}
