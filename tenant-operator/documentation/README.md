# Tenant Operator Documentation

* [Dashboards](./dashboards.md)
* [Data Sources](./datasources.md)
* [Develop](./develop.md)
* [Plugins](./plugins.md)
* [Jsonnet support](./jsonnet.md)
* [API docs](./api.md)

## Examples

The following example CRs are provided:

### Tenants

* [Tenant.yaml](../config/examples/Tenant.yaml): Installs all Tenant resources using the default configuration and an Ingress.
* [ha/Grafana.yaml](../config/examples/ha): Installs Grafana in high availability mode with Postgres as a database.
* [env/Grafana.yaml](../config/examples/env/Grafana.yaml): Shows how to provide env vars including admin credentials from a secret.
* [datasource-env-vars/Grafana.yaml](../config/examples/datasource-env-vars/Grafana.yaml): Shows how to provide env vars and use them in a data source to provide credentials.

### Dashboards

* [SimpleDashboard.yaml](../config/examples/dashboards/SimpleDashboard.yaml): Minimal empty dashboard.
* [DashboardWithPlugins.yaml](../config/examples/dashboards/DashboardWithPlugins.yaml): Minimal empty dashboard with plugin dependencies.
* [DashboardFromURL.yaml](../config/examples/dashboards/DashboardFromURL.yaml): A dashboard that downloads its contents from a URL and falls back to embedded json if the URL cannot be resolved.
* [KeycloakDashboard.yaml](../config/examples/dashboards/KeycloakDashboard.yaml): A dashboard that shows keycloak metrics and demonstrates how to use datasource inputs.

### Data sources

* [Prometheus.yaml](../config/examples/datasources/Prometheus.yaml): Prometheus data source, expects a service named `prometheus-service` listening on port 9090 in the same namespace.
* [SimpleJson.yaml](../config/examples/datasources/SimpleJson.yaml): Simple JSON data source, requires the [grafana-simple-json-datasource](https://grafana.com/grafana/plugins/grafana-simple-json-datasource) plugin to be installed.
