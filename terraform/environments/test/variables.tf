variable "project_id" {
  description = "project id"
}

variable "region" {
  description = "region"
}

variable "zone" {
  description = "Google Cloud zone where the instance will be created."
  type        = string
}

variable "location" {
  default     = ""
  description = "the location (region or zone) in which the cluster master will be created, as well as the default node location. If you specify a zone (such as us-central1-a), the cluster will be a zonal cluster with a single cluster master. If you specify a region (such as us-west1), the cluster will be a regional cluster with multiple masters spread across zones in the region, and with default node locations in those zones as well"
}

variable "gke_username" {
  default     = ""
  description = "gke username (optional)"
}

variable "gke_password" {
  default     = ""
  description = "gke password (optional)"
}

variable "instance_name" {
  default     = "example"
  description = "the Opstrace instance name"
}

variable "num_nodes" {
  default     = 3
  description = "number of gke nodes"
}

variable "kubeconfig_path" {
  default     = "./kubeconfig"
  description = "relative path to kubeconfig from this module's directory"
}

variable "domain" {
  default     = ""
  description = "URL where the Opstrace instance will be available"
}

variable "acme_email" {
  default     = ""
  description = "email address to be associated with the ACME account. This field is optional, but it is strongly recommended to be set. It will be used to contact you in case of issues with your account or certificates, including expiry notification emails. This field may be updated after the account is initially registered."
}

variable "acme_server" {
  default     = ""
  description = "the URL used to access the ACME server's 'directory' endpoint. This field is optional. For example, for Let's Encrypt's staging endpoint, you would use: 'https://acme-staging-v02.api.letsencrypt.org/directory'. Only ACME v2 endpoints (i.e. RFC 8555) are supported."
}

variable "cert_issuer" {
  default     = "letsencrypt-staging"
  description = "Defines the issuer to use for all TLS-terminating certificates, such as for the externally available data API endpoints."
}

variable "gitlab_domain" {
  description = "Domain for hosting gitlab"
  type        = string
}

variable "cloud_dns_zone" {
  description = "The name of the Cloud DNS managed zone to add the NS A record pointing at the instance"
  type        = string
}

variable "registry_username" {
  description = "Username for the GitLab Registry"
  type        = string
}

variable "registry_auth_token" {
  description = "AuthToken for the GitLab Registry"
  type        = string
}

variable "gitlab_image" {
  description = "GitLab image to deploy"
  type        = string
}