provider "kubernetes" {
  host  = "https://${data.google_container_cluster.primary.endpoint}"
  token = data.google_client_config.primary.access_token
  cluster_ca_certificate = base64decode(
    data.google_container_cluster.primary.master_auth[0].cluster_ca_certificate,
  )
}

# TODO: create a secret for the docker hub credentials.
# When we do this, we'll also have to plumb all imagePullSecrets in the
# CRs (Cluster, Tenant) through to the container definitions.
# Some are plumbed through, but not all.
# https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/1717

resource "kubernetes_secret" "postgres-secret" {
  metadata {
    name      = "postgres-secret"
    namespace = "kube-system"
  }

  data = {
    endpoint = "postgres://${google_sql_user.opstrace.name}:${google_sql_user.opstrace.password}@${google_sql_database_instance.postgres.private_ip_address}:5432/"
  }

}

provider "kustomization" {
  kubeconfig_raw = local_file.kubeconfig.content
}

data "kustomization_build" "scheduler-crds" {
  path = "${path.module}/../../../../scheduler/config/crd"
}

resource "kustomization_resource" "scheduler-crds" {
  for_each = data.kustomization_build.scheduler-crds.ids
  manifest = data.kustomization_build.scheduler-crds.manifests[each.value]
}

data "kustomization_build" "tenant-operator-crds" {
  path = "${path.module}/../../../../tenant-operator/config/crd"
}

resource "kustomization_resource" "tenant-operator-crds" {
  for_each = data.kustomization_build.tenant-operator-crds.ids
  manifest = data.kustomization_build.tenant-operator-crds.manifests[each.value]
}

data "kustomization_build" "scheduler" {
  path = "${path.module}/../../../../scheduler/config/deploy"
}

resource "kustomization_resource" "scheduler" {
  for_each = data.kustomization_build.scheduler.ids
  manifest = data.kustomization_build.scheduler.manifests[each.value]

  depends_on = [
    kustomization_resource.scheduler-crds,
    kustomization_resource.tenant-operator-crds
  ]
}
