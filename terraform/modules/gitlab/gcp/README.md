# GitLab Omnibus on GCP

This terraform module creates an instance in GCP, adds a DNS A record to a GCP Cloud DNS to point at the newly created instance, and creates firewall rules to send HTTP and HTTPS traffic to the instance.

It then proceeds to to run GitLab Omnibus via docker on the newly created instance and once ready,
will create an Admin Token and Oauth Application for Opstrace.

The Admin Token, Root User Password and Oauth Application are available as outputs for configuring a connected Opstrace instance or for running e2e tests (admin token can be used to create users/groups etc).

## Outputs

The following terraform outputs are available through `terraform output`:

* `oauth_client_id`: OAuth Application client ID
* `oauth_client_secret`: OAuth Application client Secret
* `gitlab_url`: The URL of the GitLab instance
* `admin_token`: An Admin Auth Token for the GitLab instance. Can be used to create/manage users/groups for testing
* `root_password`: The Root User Password for the GitLab instance to login as the administrator
