variable "instance_name" {
  description = "A unique name that will be appended to all managed resources."
  type        = string
}

variable "project" {
  description = "The ID of the project in which to provision resources."
  type        = string
}

variable "region" {
  description = "Google Cloud region where the instance will be created."
  type        = string
}

variable "zone" {
  description = "Google Cloud zone where the instance will be created."
  type        = string
}

variable "domain" {
  description = "Domain for hosting gitlab"
  type        = string
}

variable "cloud_dns_zone" {
  description = "The name of the Cloud DNS managed zone to add the NS A record pointing at the instance"
  type        = string
}

variable "opstrace_domain" {
  description = "Domain for hosting opstrace"
  type        = string
}

variable "registry_username" {
  description = "Username for the GitLab Registry"
  type        = string
}

variable "registry_auth_token" {
  description = "AuthToken for the GitLab Registry"
  type        = string
}

variable "gitlab_image" {
  description = "GitLab image to deploy"
  type        = string
}