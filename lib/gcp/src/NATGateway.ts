import { delay, call, CallEffect } from "redux-saga/effects";

//@ts-ignore : don't know the reason but have to add one now for ESLint :-).
import Compute from "@google-cloud/compute";
import { SECOND } from "@opstrace/utils";

class Router extends Compute {
  constructor(options = {}) {
    super(options);
  }
  createGateway(
    name: string,
    region: string,
    network: string,
    project: string,
    callback: (err: Error, data: unknown) => Record<string, unknown> | void
  ) {
    //@ts-ignore : don't know the reason but have to add one now for ESLint :-).
    this.request(
      {
        method: "POST",
        uri: `/regions/${region}/routers`,
        json: {
          name,
          network: `projects/${project}/global/networks/${network}`,
          nats: [
            {
              name,
              natIpAllocateOption: "AUTO_ONLY",
              sourceSubnetworkIpRangesToNat: "ALL_SUBNETWORKS_ALL_IP_RANGES"
            }
          ]
        }
      },
      callback
    );
  }
  destroyGateway(
    name: string,
    region: string,
    callback: (err: Error, data: unknown) => Record<string, unknown> | void
  ) {
    //@ts-ignore: don't know the reason but have to add one now for ESLint :-).
    this.request(
      {
        method: "DELETE",
        uri: `/regions/${region}/routers/${name}`
      },
      callback
    );
  }
  getGateway(
    name: string,
    region: string,
    callback: (err: Error, data: unknown) => void
  ) {
    //@ts-ignore: don't know the reason but have to add one now for ESLint :-).
    this.request(
      {
        method: "GET",
        uri: `/regions/${region}/routers/${name}`
      },
      callback
    );
  }
}

const doesGatewayExist = async (
  client: Router,
  { name, region }: { name: string; region: string }
): Promise<boolean> =>
  // Note(JP): we certainly want to clean that code up.
  // eslint-disable-next-line no-async-promise-executor
  new Promise(async res => {
    try {
      await new Promise((resolve, reject) => {
        client.getGateway(name, region, (err: Error) => {
          if (err) {
            reject(err);
          } else {
            resolve(true);
          }
        });
      });
      res(true);
    } catch (e: any) {
      res(false);
    }
  });

const createGateway = (
  client: Router,
  {
    name,
    region,
    network,
    project
  }: {
    name: string;
    region: string;
    network: string;
    project: string;
  }
) =>
  new Promise((resolve, reject) => {
    client.createGateway(name, region, network, project, (err: Error) => {
      if (err) {
        reject(err);
      } else {
        resolve(true);
      }
    });
  });

const destroyGateway = (
  client: Router,
  {
    name,
    region
  }: {
    name: string;
    region: string;
  }
) =>
  new Promise((resolve, reject) => {
    client.destroyGateway(name, region, (err: Error) => {
      if (err) {
        reject(err);
      } else {
        resolve(true);
      }
    });
  });

export interface NatRequest {
  name: string;
  region: string;
  gcpProjectID: string;
}

export function* ensureGatewayExists({
  name,
  region,
  gcpProjectID
}: NatRequest): Generator<CallEffect, boolean, boolean> {
  const client = new Router();

  while (true) {
    const existingGateway: boolean = yield call(doesGatewayExist, client, {
      name,
      region
    });

    if (!existingGateway) {
      try {
        yield call(createGateway, client, {
          name,
          network: name,
          project: gcpProjectID,
          region
        });
      } catch (e: any) {
        if (!e.code || (e.code && e.code !== 409)) {
          throw e;
        }
      }
    }
    if (existingGateway) {
      return existingGateway;
    }

    yield delay(10 * SECOND);
  }
}

export function* ensureGatewayDoesNotExist(
  opstraceClusterName: string,
  gcpRegion: string
): Generator<CallEffect, void, boolean> {
  const client = new Router();

  while (true) {
    const existingGateway: boolean = yield call(doesGatewayExist, client, {
      name: opstraceClusterName,
      region: gcpRegion
    });

    if (!existingGateway) {
      return;
    }
    try {
      yield call(destroyGateway, client, {
        name: opstraceClusterName,
        region: gcpRegion
      });
    } catch (e: any) {
      if (!e.code || (e.code && e.code !== 404)) {
        throw e;
      }
    }

    yield delay(30 * SECOND);
  }
}
