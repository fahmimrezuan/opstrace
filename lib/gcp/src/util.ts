import { readJSONFromFile } from "@opstrace/utils";

import { protos as gkeProtos } from "@google-cloud/container";

import { GCPAuthOptions, serviceAccountSchema } from "./types";

export function generateKubeconfigStringForGkeCluster(
  projectid: string,
  cluster: gkeProtos.google.container.v1.ICluster
): string {
  const context = `${projectid}_${cluster.name}`;
  return `apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: ${cluster.masterAuth?.clusterCaCertificate}
    server: https://${cluster.endpoint}
  name: ${context}
contexts:
- context:
    cluster: ${context}
    user: ${context}
  name: ${context}
current-context: ${context}
kind: Config
preferences: {}
users:
- name: ${context}
  user:
    auth-provider:
      config:
        cmd-args: config config-helper --format=json
        cmd-path: gcloud
        expiry-key: '{.credential.token_expiry}'
        token-key: '{.credential.access_token}'
      name: gcp
`;
}

/**
 * Returns GCPAuthOptions or throws if not valid
 * @param gcpCredFilePath string
 */
export const getValidatedGCPAuthOptionsFromFile = (
  gcpCredFilePath: string
): GCPAuthOptions => {
  const gcpCredsRaw = readJSONFromFile(gcpCredFilePath);
  const gcpServiceAccount = serviceAccountSchema.validateSync(gcpCredsRaw);
  const gcpAuthOptions: GCPAuthOptions = {
    projectId: gcpServiceAccount.project_id,
    credentials: gcpServiceAccount
  };
  return gcpAuthOptions;
};

let certManagerSA: string | undefined;

export function setCertManagerServiceAccount(sa: string): void {
  certManagerSA = sa;
}

export function getCertManagerServiceAccount(): string {
  if (certManagerSA === undefined) {
    throw new Error("call setCertManagerServiceAccount() first");
  }
  return certManagerSA;
}

let externalDNSSA: string | undefined;

export function setExternalDNSServiceAccount(sa: string): void {
  externalDNSSA = sa;
}

export function getExternalDNSServiceAccount(): string {
  if (externalDNSSA === undefined) {
    throw new Error("call setExternalDNSServiceAccount() first");
  }
  return externalDNSSA;
}

let cortexSA: string | undefined;

export function setCortexServiceAccount(sa: string): void {
  cortexSA = sa;
}

export function getCortexServiceAccount(): string {
  if (cortexSA === undefined) {
    throw new Error("call setCortexServiceAccount() first");
  }
  return cortexSA;
}
