import * as errors from "./errors";

/**
 * Shortcut for exiting the runtime with a non-zero exit code, and logging the
 * reason beforehand.
 */
export function die(errorMessage: string): never {
  throw new errors.ExitError(1, errorMessage);
}
