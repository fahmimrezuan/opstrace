import { CreateZoneResponse, DNS } from "@google-cloud/dns";
import { DNSZone, DNSRecord, PartialRequired } from "./types";
import { constructNSRecordOptions } from "./util";
import {
  CreateChangeResponse,
  DeleteZoneResponse
} from "@google-cloud/dns/build/src/zone";

const getName = (dnsName: string): string =>
  dnsName.replace(/\.$/, "").replace(/\./g, "-");

export const getZone = async ({
  dnsName,
  dns
}: {
  dnsName: string;
  dns: DNS;
}): Promise<{
  zone: DNSZone | undefined;
  records: DNSRecord[] | undefined;
}> => {
  return new Promise((resolve, reject) =>
    dns
      .getZones()
      .then(async results => {
        const zones = results[0];
        const zone = zones.find(z => z.metadata.dnsName === dnsName);
        if (zone) {
          await zone.getRecords().then(res => {
            const records: DNSRecord[] = res[0]
              .filter(r => r.type === "NS")
              .map(r => ({
                name: r.metadata.name,
                type: r.type,
                ttl: r.metadata.ttl,
                rrdatas: r.data as string[]
              }));
            const { name, dnsName } = zone.metadata;
            resolve({ zone: { name, dnsName }, records });
          });
        }
        resolve({ zone: undefined, records: undefined });
      })
      .catch(err => {
        reject(err);
      })
  );
};

export const createZone = async ({
  dnsName,
  dns
}: {
  dnsName: string;
  dns: DNS;
}): Promise<CreateZoneResponse> => {
  const name = getName(dnsName);
  return dns.createZone(name, {
    description: `Managed by Opstrace`,
    dnsName,
    name
  });
};

export const deleteZone = async ({
  dnsName,
  dns
}: {
  dnsName: string;
  dns: DNS;
}): Promise<DeleteZoneResponse> => {
  const name = getName(dnsName);
  return dns.zone(name).delete({
    force: true
  });
};

export const addNSRecord = async ({
  dns,
  zone,
  record
}: {
  dns: DNS;
  zone: PartialRequired<DNSZone, "name">;
  record: ReturnType<typeof constructNSRecordOptions>;
}): Promise<CreateChangeResponse> => {
  return dns
    .zone(zone.name)
    .addRecords([dns.zone(zone.name).record("ns", record)]);
};

export const removeNSRecord = async ({
  dns,
  zone,
  record
}: {
  dns: DNS;
  zone: PartialRequired<DNSZone, "name">;
  record: PartialRequired<DNSRecord, "name" | "rrdatas" | "ttl">;
}): Promise<CreateChangeResponse> => {
  return dns.zone(zone.name).deleteRecords([
    dns.zone(zone.name).record("ns", {
      name: record.name,
      data: record.rrdatas,
      ttl: record.ttl
    })
  ]);
};
