import { DaemonSetType } from "../kinds";

export function getDaemonSetRolloutMessage(ds: DaemonSetType): string {
  const daemonset = ds.spec;
  const spec = ds.spec.spec;
  const metadata = daemonset.metadata;
  const status = daemonset.status;

  const updatedNumberScheduled = status?.updatedNumberScheduled || 0;
  const desiredNumberScheduled = status?.desiredNumberScheduled || 0;
  const numberAvailable = status?.numberAvailable || 0;

  // Taken from https://github.com/kubernetes/kubectl/blob/0a26b53c373b22de64bf667dad7a2440359334d3/pkg/polymorphichelpers/rollout_status.go#L95
  if (spec?.updateStrategy && spec?.updateStrategy.type !== "RollingUpdate") {
    return "";
  }
  if ((metadata?.generation || 0) <= (status?.observedGeneration || 0)) {
    if (updatedNumberScheduled < desiredNumberScheduled) {
      return `Waiting for DaemonSet ${ds.namespace}/${ds.name} rollout to finish: ${updatedNumberScheduled} out of ${desiredNumberScheduled} new pods have been updated`;
    }
    if (numberAvailable < desiredNumberScheduled) {
      return `Waiting for DaemonSet ${ds.namespace}/${ds.name} rollout to finish: ${numberAvailable} of ${desiredNumberScheduled} updated pods are available`;
    }
    return "";
  }
  return `Waiting for DaemonSet spec update to be observed for ${ds.namespace}/${ds.name}`;
}
