import { DeploymentType } from "../kinds";

export function getDeploymentRolloutMessage(d: DeploymentType): string {
  const deployment = d.spec;
  const spec = d.spec.spec;
  const metadata = deployment.metadata;
  const status = deployment.status;

  const replicas = spec?.replicas || 0;
  const updatedReplicas = status?.updatedReplicas || 0;
  const availableReplicas = status?.availableReplicas || 0;

  // Taken from https://github.com/kubernetes/kubectl/blob/0a26b53c373b22de64bf667dad7a2440359334d3/pkg/polymorphichelpers/rollout_status.go#L59

  if ((metadata?.generation || 0) <= (status?.observedGeneration || 0)) {
    // TODO: add case to handle progressDeadlineSeconds condition.
    if (updatedReplicas < replicas) {
      return `Waiting for Deployment ${d.namespace}/${d.name} rollout to finish: ${updatedReplicas} out of ${replicas} new replicas have been updated`;
    }
    if (replicas > updatedReplicas) {
      return `Waiting for Deployment ${d.namespace}/${
        d.name
      } rollout to finish: ${
        replicas - updatedReplicas
      } old replicas are pending termination`;
    }
    if (availableReplicas < updatedReplicas) {
      return `Waiting for Deployment ${d.namespace}/${d.name} rollout to finish: ${availableReplicas} of ${updatedReplicas} updated replicas are available`;
    }
    return "";
  }
  return `Waiting for Deployment spec update to be observed for ${d.namespace}/${d.name}`;
}
