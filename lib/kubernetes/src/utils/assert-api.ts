import { KubeConfig, CoreV1Api } from "@kubernetes/client-node";

/**
 * Use global gcloud authentication state and try to get namespace listing
 * for configured k8s cluster. This might throw various kinds of errors,
 * including authentication errors.
 */
export async function k8sListNamespacesOrError(
  kubeConfig: KubeConfig
): Promise<boolean> {
  await kubeConfig.makeApiClient(CoreV1Api).listNamespace();
  return true;
}
