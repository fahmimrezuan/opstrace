import { KubeConfig } from "@kubernetes/client-node";

export type KubeConfiguration = KubeConfig;

export const getKubeConfig = ({
  loadFromCluster,
  kubeconfig
}: {
  loadFromCluster?: boolean;
  kubeconfig: string;
}): KubeConfig => {
  const kubeConfig = new KubeConfig();
  if (loadFromCluster) {
    kubeConfig.loadFromCluster();
  } else {
    kubeConfig.loadFromString(kubeconfig);
  }
  return kubeConfig;
};
