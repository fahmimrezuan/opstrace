# Prometheus CRDs

These are CRDs for the Prometheus Operator that will be installed into the cluster by the controller.

To update these CRDs, perform the following:

Download/unpack the latest [Prometheus-operator release source](https://github.com/prometheus-operator/prometheus-operator/releases/), for example:

```bash
VERSION=0.xx.yy
curl -O https://github.com/prometheus-operator/prometheus-operator/archive/refs/tags/v${VERSION}.tar.gz
tar xzvf prometheus-operator-${VERSION}.tar.gz
cd prometheus-operator-${VERSION}/
```

Copy pre-generated JSON CRDs from Prometheus repo to Opstrace repo, while also stripping out any `creationTimestamp: null` which causes typing issues in the typescript. These can be regenerated using `make bundle.yaml` in the Prometheus repo root, but that shouldn't be necessary.

```bash
cd jsonnet/prometheus-operator/
ls *-crd.json | xargs -I{} sh -c 'cat {} | grep -v "\"creationTimestamp\": null" > ../../../opstrace/lib/kubernetes/src/crds/kube-prometheus/{}'
```

In Opstrace repo, regenerate typescript based on the JSON files:

```bash
cd opstrace/lib/kubernetes/
yarn generate-apis # updates lib/kubernetes/src/custom-resources
```
