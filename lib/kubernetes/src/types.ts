// Denotes support for these two providers
export type Provider = "aws" | "gcp";
