export class KubernetesApiNotAvailableError extends Error {
  constructor(...args: (string | undefined)[]) {
    super(...args);
    Error.captureStackTrace(this, KubernetesApiNotAvailableError);
  }
}
