import { isDeepStrictEqual } from "util";
import { V1ServiceSpec, V1ServicePort } from "@kubernetes/client-node";
import { isResourceListEqual } from "./utils";

export const isServiceSpecEqual = (
  desired?: V1ServiceSpec,
  existing?: V1ServiceSpec
): boolean => {
  if (!desired || !existing) {
    return !desired && !existing;
  }
  if (!isDeepStrictEqual(desired.selector, existing.selector)) {
    return false;
  }
  if (!areServicePortsEqual(desired, existing)) {
    return false;
  }

  if (
    Array.isArray(desired.loadBalancerSourceRanges) &&
    desired.loadBalancerSourceRanges.length &&
    !Array.isArray(existing.loadBalancerSourceRanges)
  ) {
    return false;
  }

  if (
    Array.isArray(desired.loadBalancerSourceRanges) &&
    !(
      desired.loadBalancerSourceRanges.length ===
        (existing.loadBalancerSourceRanges?.length || 0) &&
      !desired.loadBalancerSourceRanges.find(
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        (p, i) => p !== existing.loadBalancerSourceRanges![i]
      )
    )
  ) {
    return false;
  }

  return true;
};

const areServicePortsEqual = (
  desired: V1ServiceSpec,
  existing: V1ServiceSpec
): boolean => {
  if (typeof desired.ports !== typeof existing.ports) {
    return false;
  }

  if (
    !isResourceListEqual(
      desired.ports,
      existing.ports,
      (desiredPort, existingPort) =>
        isServicePortEqual(desiredPort, existingPort)
    )
  ) {
    return false;
  }

  return true;
};

const isServicePortEqual = (
  desired: V1ServicePort,
  existing: V1ServicePort
): boolean => {
  if (
    (desired.name && desired.name !== existing.name) ||
    (desired.nodePort && desired.nodePort !== existing.nodePort) ||
    (desired.port && desired.port !== existing.port) ||
    (desired.targetPort && desired.targetPort !== existing.targetPort)
  ) {
    return false;
  }
  if (
    desired.protocol !== undefined &&
    desired.protocol !== existing.protocol
  ) {
    return false;
  }

  return true;
};
