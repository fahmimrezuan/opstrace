/**
 * A list of resources which is `undefined` is the equivalent of an empty list.
 * To keep the code simple we therefor use `[]` as default parameters.
 */
export const isResourceListEqual = <Resource>(
  desiredList: Array<Resource> = [],
  existingList: Array<Resource> = [],
  isResourceEqual: (desired: Resource, existing: Resource) => boolean
): boolean => {
  if (desiredList.length !== existingList.length) {
    return false;
  }

  return desiredList.every((desired, i) =>
    isResourceEqual(desired, existingList[i])
  );
};
