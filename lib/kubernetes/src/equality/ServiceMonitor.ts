import { isDeepStrictEqual } from "util";
import { V1Servicemonitor } from "..";
import { isResourceListEqual } from "./utils";

export const isServiceMonitorEqual = (
  desired: V1Servicemonitor,
  existing: V1Servicemonitor
): boolean => {
  if (!isDeepStrictEqual(desired.spec.selector, existing.spec.selector)) {
    return false;
  }
  if (desired.spec.jobLabel !== existing.spec.jobLabel) {
    return false;
  }
  if (!areEndpointsEqual(desired, existing)) {
    return false;
  }

  return true;
};

type Endpoint = {
  interval: string;
  port: string;
  path: string;
};

const areEndpointsEqual = (
  desired: V1Servicemonitor,
  existing: V1Servicemonitor
): boolean => {
  if (typeof desired.spec.endpoints !== typeof existing.spec.endpoints) {
    return false;
  }

  if (
    !isResourceListEqual(
      desired.spec.endpoints,
      existing.spec.endpoints,
      (desiredEndpoint, existingEndpoint) =>
        isEndpointEqual(
          desiredEndpoint as Endpoint,
          existingEndpoint as Endpoint
        )
    )
  ) {
    return false;
  }

  return true;
};

const isEndpointEqual = (desired: Endpoint, existing: Endpoint): boolean => {
  return (
    desired.interval === existing.interval &&
    desired.port === existing.port &&
    desired.path === existing.path
  );
};
